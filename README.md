# Le Jydreux pas pied

Companion App:
- React + Typescript

Back
- Java 14
- SpringBoot 

## Companion App
Requirements:
- Node 12

### Setup
Export the Auth DOMAIN and CLIENT ID
```
export REACT_APP_AUTH0_DOMAIN="domain"
export REACT_APP_AUTH0_CLIENT_ID="client_id"
```

Connect to Docker registry
```
docker login registry.gitlab.com -u jozolr -p <token>
```

Install the dependencies:
```
cd companion
npm ci
```

### Test
```
npm test
```

### Start back
```
docker-compose up
```

### Run
```
npm run watch
```

### Deploy
The application is deployed automatically with ansible when pushing on master

## Backend
Requirements:
- Java 17
- [docker](https://hub.docker.com/editions/community/docker-ce-desktop-mac/)
- docker-compose

### Setup
Export the Auth DOMAIN and CLIENT ID
```
export AUTH_ISSUER="issuer"
```

### Build
```
./gradlew clean build
```

### Run
```
./gradlew start
```

### Access to Swagger
http://localhost:8080/swagger-ui.html#/

### Deploy
The application is deployed automatically with ansible when pushing on master
