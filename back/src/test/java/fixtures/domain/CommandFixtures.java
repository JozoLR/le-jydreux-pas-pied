package fixtures.domain;

import jydreux.domain.game.command.gear.DeleteGearCommand;
import jydreux.domain.game.command.weapon.DeleteWeaponCommand;

import static fixtures.domain.PlayerFixture.*;

public class CommandFixtures {
  public static DeleteGearCommand uneCommandeDeleteGear() {
    return new DeleteGearCommand(
      aFreshPlayer().getGameId(),
      aFreshPlayer().getId(),
      unParcheminAvecMalus().getId()
    );
  }

  public static DeleteWeaponCommand uneCommandeDeleteWeapon() {
    return new DeleteWeaponCommand(
      aFreshPlayer().getGameId(),
      aFreshPlayer().getId(),
      unArc().getId()
    );
  }
}
