package fixtures.domain;

import jydreux.domain.cross.StuffType;
import jydreux.domain.event.Event;
import jydreux.domain.event.EventType;
import jydreux.domain.game.command.gear.DeleteGearCommand;
import jydreux.domain.notification.Notification;
import jydreux.domain.notification.query.model.RollInfosQueryModel;
import jydreux.domain.notification.query.model.RollQueryModel;
import jydreux.domain.notification.query.model.StuffQueryModel;
import jydreux.domain.game.notification.PlayerUpdate;
import jydreux.domain.roll.model.Roll;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import static fixtures.domain.CommandFixtures.uneCommandeDeleteGear;
import static fixtures.domain.PlayerFixture.*;
import static fixtures.domain.RollDiceFixture.*;

public class EventFixtures {
  public static final ZonedDateTime uneDate = LocalDateTime.of(2020, 3, 31, 13, 46, 5).atZone(ZoneId.of("UTC"));

  public static String unId() {
    return "77920965-cb4a-49f4-863f-7fb1827fcde2";
  }

  public static String unAutreId() {
    return "77920965-cb4a-49f4-863f-7fb1827fcde3";
  }

  public static Event<PlayerUpdate> unEvent() {
    var player = aFreshPlayer();
    var oldGear = unParchemin();
    var newGear = unParcheminAvecMalus();
    var playerUpdate = new PlayerUpdate(oldGear.getId(), oldGear.getLabel(), oldGear.getMalus(), newGear.getMalus());
    return new Event<>(
      unId(),
      uneDate,
      player.getId(),
      player.getGameId(),
      EventType.GEAR_UPDATED,
      playerUpdate);
  }

  public static Notification<PlayerUpdate> uneNotification() {
    var player = aFreshPlayer();
    var event = unEvent();
    return new Notification<>(
      event.getId(),
      event.getDate(),
      event.getPlayerId(),
      player.getName(),
      event.getGameId(),
      EventType.GEAR_UPDATED,
      event.getContent());
  }

  public static Event<PlayerUpdate> unEventSurUnAutreGame() {
    var player = aPlayerInAnotherGame();
    var oldGear = unParchemin();
    var newGear = unParcheminAvecMalus();
    var playerUpdate = new PlayerUpdate(oldGear.getId(), oldGear.getLabel(), oldGear.getMalus(), newGear.getMalus());
    return new Event<>(
      unAutreId(),
      uneDate,
      player.getId(),
      player.getGameId(),
      EventType.GEAR_UPDATED,
      playerUpdate);
  }

  public static Event<Roll> uneCompleteRollEvent() {
    var player = aFreshPlayer();
    var roll = aRoll(5);

    return new Event<>(
      unAutreId(),
      uneDate,
      player.getId(),
      player.getGameId(),
      EventType.PLAYER_ROLLED,
      roll);
  }

  public static Event<Roll> uneSkillRollEvent() {
    var player = aFreshPlayer();
    var roll = aSkillRoll(5);

    return new Event<>(
      unAutreId(),
      uneDate,
      player.getId(),
      player.getGameId(),
      EventType.PLAYER_ROLLED,
      roll);
  }

  public static Event<DeleteGearCommand> unEventNonNotifiable() {
    var player = aFreshPlayer();
    var command = uneCommandeDeleteGear();

    return new Event<>(
      unAutreId(),
      uneDate,
      player.getId(),
      player.getGameId(),
      EventType.DELETE_GEAR,
      command);
  }

  public static Notification<RollQueryModel> uneCompleteRollNotificationQueryModel() {
    var player = aFreshPlayer();
    var roll = aRoll(5);
    var rollInfo = weaponRollInfo();
    var stuffQueryModel = new StuffQueryModel(unArc().getId(), unArc().getLabel());
    var rollInfoQueryModel = new RollInfosQueryModel(
      rollInfo.getSkillId(),
      "Tir",
      rollInfo.getStuffType(),
      List.of(stuffQueryModel)
    );
    var rollQueryModel = new RollQueryModel(
      roll.getAttributRolls(),
      roll.getSkillRolls(),
      roll.getStuffRolls(),
      roll.getMalusRolls(),
      roll.getRollType(),
      rollInfoQueryModel
    );

    return new Notification<>(
      unAutreId(),
      uneDate,
      player.getId(),
      player.getName(),
      player.getGameId(),
      EventType.PLAYER_ROLLED,
      rollQueryModel);
  }

  public static Notification<RollQueryModel> uneSkillRollNotificationQueryModel() {
    var player = aFreshPlayer();
    var roll = aRoll(5);
    var rollInfoQueryModel = new RollInfosQueryModel(
      "tir",
      "Tir",
      StuffType.NONE,
      Collections.emptyList()
    );
    var rollQueryModel = new RollQueryModel(
      roll.getAttributRolls(),
      roll.getSkillRolls(),
      roll.getStuffRolls(),
      roll.getMalusRolls(),
      roll.getRollType(),
      rollInfoQueryModel
    );

    return new Notification<>(
      unAutreId(),
      uneDate,
      player.getId(),
      player.getName(),
      player.getGameId(),
      EventType.PLAYER_ROLLED,
      rollQueryModel);
  }

  public static Notification<RollQueryModel> uneBasicRollNotificationQueryModel() {
    var player = aFreshPlayer();
    var roll = aBasicRoll();
    var rollInfo = basicRollInfo();
    var rollInfoQueryModel = new RollInfosQueryModel(
      null,
      null,
      rollInfo.getStuffType(),
      Collections.emptyList()
    );
    var rollQueryModel = new RollQueryModel(
      roll.getAttributRolls(),
      roll.getSkillRolls(),
      roll.getStuffRolls(),
      roll.getMalusRolls(),
      roll.getRollType(),
      rollInfoQueryModel
    );

    return new Notification<>(
      rollId,
      uneDate,
      player.getId(),
      player.getName(),
      player.getGameId(),
      EventType.PLAYER_ROLLED,
      rollQueryModel);
  }
}
