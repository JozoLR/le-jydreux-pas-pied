package fixtures.domain;

import java.util.List;

import jydreux.domain.playerCreation.Attribut;
import jydreux.domain.playerCreation.PlayerToCreate;
import static fixtures.domain.GameFixtures.*;

public class PlayerToCreateFixture {
  public static String aPlayerName = "John";
  public static String anotherPlayerName = "Bob";
  public static String gameClassIdChasseur = "chasseur";
  public static String gameClassIdRafistoleur = "rafistoleur";
  public static String talentIdSkinner = "skinner";
  public static String playerGameId = gameId;
  public static String anotherGameId = "c5c4ad9f-02c1-4daf-97ae-75c90007cf4a";

  private static final Attribut vigueur = new Attribut("vigueur", "Vigueur", 5);
  private static final Attribut agilite = new Attribut("agilite", "Agilité", 4);

  public static PlayerToCreate aPlayerToCreateChasseur = new PlayerToCreate("77920965-cb4a-49f4-863f-7fb1827fcde2", aPlayerName,
      List.of(vigueur, agilite), gameClassIdChasseur, talentIdSkinner, playerGameId);

  public static PlayerToCreate aPlayerToCreateRafistoleur = new PlayerToCreate("7f3c4efa-9069-4c39-86c4-09ab48d015b0",
      anotherPlayerName, List.of(vigueur, agilite), gameClassIdRafistoleur, talentIdSkinner, playerGameId);

  public static PlayerToCreate aPlayerToCreateInAnotherGame = new PlayerToCreate("7f3c4efa-9069-4c39-86c4-09ab48d015b0",
    anotherPlayerName, List.of(vigueur, agilite), gameClassIdRafistoleur, talentIdSkinner, anotherGameId);
}
