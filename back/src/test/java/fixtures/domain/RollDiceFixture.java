package fixtures.domain;

import jydreux.domain.cross.StuffType;
import jydreux.domain.event.Event;
import jydreux.domain.event.EventType;
import jydreux.domain.roll.model.Roll;
import jydreux.domain.roll.model.RollInfos;
import jydreux.domain.roll.model.RollType;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

import static fixtures.domain.PlayerFixture.*;
import static java.util.Collections.emptyList;

public class RollDiceFixture {
  public static final String rollId = "a7594571-44de-4028-8ea9-0d67ae3f28eb";
  public static final String anotherRollId = "a7594571-44de-4028-8ea9-0d67ae3f28ec";
  public static final String aThirdRollId = "a7594571-44de-4028-8ea9-0d67ae3f28ee";
  public static final String aRerollId = "a7594571-44de-4028-8ea9-0d67ae3f28ed";
  public static final ZonedDateTime aRollDate = LocalDateTime.of(2020, 3, 31, 13, 46, 5).atZone(ZoneId.of("UTC"));
  public static final ZonedDateTime aRerollDate = LocalDateTime.of(2020, 3, 31, 13, 46, 15).atZone(ZoneId.of("UTC"));
  public static final ZonedDateTime aMostRecentRollDate = LocalDateTime.of(2020, 3, 31, 13, 46, 55).atZone(ZoneId.of("UTC"));

  public static RollInfos basicRollInfo() {
    return new RollInfos();
  }

  public static RollInfos weaponRollInfo() {
    return new RollInfos("tir", StuffType.WEAPON, List.of(unArc().getId()));
  }

  public static RollInfos skillRollInfo() {
    return new RollInfos("tir", StuffType.NONE, emptyList());
  }

  public static Roll aRoll(int score) {
    return aRollOf(emptyList(), List.of(score), emptyList(), emptyList());
  }

  public static Roll aSkillRoll(int score) {
    return aSkillRollOf(emptyList(), List.of(score), emptyList(), emptyList());
  }

  public static Roll aBasicRoll() {
    return new Roll(emptyList(), List.of(5), emptyList(), emptyList(), RollType.ROLL, basicRollInfo());
  }

  public static Roll aRollOf(List<Integer> attribut, List<Integer> skill, List<Integer> stuff, List<Integer> malus) {
    return new Roll(attribut, skill, stuff, malus, RollType.ROLL, weaponRollInfo());
  }

  public static Roll aSkillRollOf(List<Integer> attribut, List<Integer> skill, List<Integer> stuff, List<Integer> malus) {
    return new Roll(attribut, skill, stuff, malus, RollType.ROLL, skillRollInfo());
  }

  public static Roll aMoreRecentRoll() {
    return new Roll(emptyList(), List.of(3), emptyList(), emptyList(), RollType.ROLL, weaponRollInfo());
  }

  public static Event<Roll> aNotificationMoreRecentRoll() {
    return new Event<>(aThirdRollId, aMostRecentRollDate, aPlayerId(), aGameId(), EventType.PLAYER_ROLLED, aMoreRecentRoll());
  }

  public static Event<Roll> aBasicRollEvent() {
    return new Event<>(rollId, aRollDate, aPlayerId(), aGameId(), EventType.PLAYER_ROLLED, aBasicRoll());
  }

  public static Roll aRollOfPlayerX() {
    return new Roll(emptyList(), List.of(3), List.of(4), emptyList(), RollType.ROLL, weaponRollInfo());
  }

  public static Event<Roll> aNotificationRollOfPlayerX(String playerId) {
    return new Event<>(anotherRollId, aRollDate, playerId, aGameId(), EventType.PLAYER_ROLLED, aRollOfPlayerX());
  }

  public static Roll aReroll() {
    return new Roll(emptyList(), List.of(4), emptyList(), emptyList(), RollType.REROLL, weaponRollInfo());
  }

  public static Event<Roll> aNotificationReroll() {
    return new Event<>(aRerollId, aRerollDate, aPlayerId(), aGameId(), EventType.PLAYER_ROLLED, aReroll());
  }

  public static Roll aRollsOnMultipleCategory() {
    return aRollOf(List.of(2), List.of(3), List.of(4), emptyList());
  }

  public static Event<Roll> aNotificationRollsOnMultipleCategory() {
    return new Event<>(rollId, aRollDate, aPlayerId(), aGameId(), EventType.PLAYER_ROLLED, aRollsOnMultipleCategory());
  }
}
