package fixtures.domain;

import jydreux.domain.game.Game;

import java.util.Collections;
import java.util.List;

import static java.util.Collections.emptyList;

public class GameFixtures {
  public static final String gameId = "d1f86083-9e1d-40aa-8028-c3ba92649837";
  public static final String gameName = "La partie du turfu";
  private static final String gameMaster = "toto@gmail.com";
  private static final List<String> players = List.of("jozo@gmail.com", "pozo@gmail.com");

  public static Game unGame() {
    return new Game(gameId, gameName, gameMaster, players, emptyList());
  }
  public static Game unAutreGame() {
    return new Game("d7b8f1dc-75e0-4869-ac08-599899ab5985", "autre_name", gameMaster, players, emptyList());
  }
  public static Game unGameAvecDesInconnus() {
    return new Game("8ba26f53-689f-4705-8766-f9c5e9b701a2",
      "troisieme_name",
      "bob", List.of("john", "jack"), emptyList());
  }
}
