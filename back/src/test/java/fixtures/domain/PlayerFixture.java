package fixtures.domain;

import jydreux.domain.cross.SkillType;
import jydreux.domain.game.model.*;

import java.util.ArrayList;
import java.util.List;

import static fixtures.domain.GameFixtures.gameId;

public class PlayerFixture {

  public static final String DES_NOTES = "des notes";

  public static String aPlayerId() {
    return "77920965-cb4a-49f4-863f-7fb1827fcde2";
  }

  public static String anotherPlayerId() {
    return "77920965-cb4a-49f4-863f-7fb1827fcd44";
  }

  public static String aGameId() {
    return gameId;
  }

  public static String anotherGameId() {
    return gameId;
  }

  public static String aPlayerName() {
    return "John";
  }

  public static String aGameClassIdChasseur() {
    return "chasseur";
  }

  private static Attribut vigueur4() {
    return new Attribut("vigueur", "Vigueur", 5, 4);
  }

  public static Attribut vigueur5() {
    return new Attribut("vigueur", "Vigueur", 5, 5);
  }

  private static Attribut agilite4() {
    return new Attribut("agilite", "Agilité", 4, 4);
  }

  public static Attribut deLaVigueurAvecMalus() {
    return new Attribut("vigueur", "Vigueur", 5, 4);
  }

  public static Skill force3() {
    return new Skill("force", "Force", 3, SkillType.BASE);
  }

  public static Skill forceAZero() {
    return new Skill("force", "Force", 0, SkillType.BASE);
  }

  public static Skill endurance2() {
    return new Skill("endurance", "Endurance", 2, SkillType.BASE);
  }

  private static Skill enduranceAZero() {
    return new Skill("endurance", "Endurance", 0, SkillType.BASE);
  }

  private static Skill armureAZero() {
    return new Skill("armor", "Armure", 0, SkillType.ARMOR);
  }

  public static Skill chasse0() {
    return new Skill("chasse", "Chasse", 0, SkillType.CLASS);
  }

  public static Skill chasse() {
    return new Skill("chasse", "Chasse", 4, SkillType.CLASS);
  }

  public static String unIdDeMassue() {
    return "77920965-cb4a-49f4-863f-7fb1827faaaa";
  }

  public static String unIdDarc() {
    return "0fa73377-c947-48b7-94de-78cf7e8b703d";
  }

  public static Weapon uneMassue() {
    return new Weapon(unIdDeMassue(), "massue", "force", 2, 2, "cac", 0, null);
  }

  public static Weapon uneMassueAvecMalus() {
    return new Weapon(unIdDeMassue(), "massue", "force", 2, 2, "cac", 1, null);
  }

  public static Weapon unArc() {
    return new Weapon(unIdDarc(), "Arc", "tir", 3, 2, "loin", 0, "arrow");
  }

  public static String unIdDeParchemin() {
    return "77920965-cb4a-49f4-863f-7fb1827fbbbb";
  }

  public static String unIdDeBouclier() {
    return "77920965-cb4a-49f4-863f-7fb1827fbcbb";
  }

  public static Gear unBouclierAvecMalus() {
    var skills = new ArrayList<String>();
    skills.add("armor");
    return new Gear(unIdDeBouclier(), "bouclier", skills, 2, 1);
  }

  public static Gear unParchemin() {
    var skills = new ArrayList<String>();
    skills.add("endurance");
    return new Gear(unIdDeParchemin(), "parchemin", skills, 2);
  }

  public static Gear unParcheminAvecMalus() {
    var skills = new ArrayList<String>();
    skills.add("endurance");
    return new Gear(unIdDeParchemin(), "parchemin", skills, 2, 1);
  }

  public static Resource noWater() {
    return new Resource("water", "Eau", 2);
  }

  public static Resource someWater() {
    return new Resource("water", "Eau", 3);
  }

  public static Resource noFood() {
    return new Resource("food", "Nourriture", 0);
  }

  public static Resource someFood() {
    return new Resource("food", "Nourriture", 1);
  }

  public static Resource someArrows() {
    return new Resource("arrow", "Flèches", 3);
  }

  private final static String unTalentSkinnerId = "skinner";
  public final static String unTalentArcherieId = "acherie";
  public final static String unTalentTrappeurId = "trappeur";

  public static Player aFreshPlayer() {
    return new Player("77920965-cb4a-49f4-863f-7fb1827fcde2", aPlayerName(), List.of(agilite4(), vigueur5()),
      List.of(chasse0(), enduranceAZero(), forceAZero()), List.of(noWater()), aGameClassIdChasseur(), unTalentSkinnerId, aGameId());
  }

  public static Player aRandomPlayer(String id) {
    return new Player(id, aPlayerName(), List.of(agilite4(), vigueur5()),
      List.of(chasse0(), enduranceAZero(), forceAZero()), List.of(noWater()), aGameClassIdChasseur(), unTalentSkinnerId, aGameId());
  }

  public static Player aPlayerInAnotherGame() {
    return new Player("77920965-cb4a-49f4-863f-7fb1827fcded", aPlayerName(), List.of(agilite4(), vigueur5()),
      List.of(chasse0(), enduranceAZero(), forceAZero()), List.of(noWater()), aGameClassIdChasseur(), unTalentSkinnerId, "77920965-cb4a-49f4-863f-7fb1827fcdea");
  }

  public static Player aNakedPlayer() {
    return new Player("77920965-cb4a-49f4-863f-7fb1827fcde2", aPlayerName(),
      List.of(agilite4(), vigueur5()), List.of(chasse(), endurance2(), force3(), armureAZero()), new ArrayList<>(), new ArrayList<>(),
      new ArrayList<>(), 0, 0, 0, aGameClassIdChasseur(), List.of(unTalentSkinnerId), aGameId(), DES_NOTES);
  }

  public static Player aCompletePlayerWithBasicGear() {
    return new Player("77920965-cb4a-49f4-863f-7fb1827fcde2", aPlayerName(),
      List.of(agilite4(), vigueur4()), List.of(chasse(), enduranceAZero(), forceAZero()), List.of(uneMassueAvecMalus(), unArc()), List.of(unParcheminAvecMalus()),
      List.of(someArrows(), noFood(), noWater()), 1, 2, 3, aGameClassIdChasseur(), List.of(unTalentSkinnerId), aGameId(), DES_NOTES);
  }

  public static Player aCompletePlayer() {
    return new Player("77920965-cb4a-49f4-863f-7fb1827fcde2", aPlayerName(),
      List.of(agilite4(), vigueur5()), List.of(chasse(), endurance2(), force3()), List.of(uneMassue(), unArc()), List.of(unParchemin()),
      List.of(someArrows(), someFood(), someWater()), 2, 3, 4, aGameClassIdChasseur(), List.of(unTalentSkinnerId), aGameId(), DES_NOTES);
  }
}
