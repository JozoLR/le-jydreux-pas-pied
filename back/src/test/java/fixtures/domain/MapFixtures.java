package fixtures.domain;

import jydreux.domain.map.Map;

import static fixtures.domain.PlayerFixture.aGameId;

public class MapFixtures {
  public static Map uneMap() {
    return Map.generate(aGameId());
  }
}
