package fixtures.query;

import jydreux.query.model.*;

import java.util.List;

import static fixtures.domain.GameFixtures.gameId;
import static fixtures.domain.PlayerFixture.DES_NOTES;
import static fixtures.domain.RollDiceFixture.aRollsOnMultipleCategory;
import static jydreux.infra.RulesRepositoryInMemory.skinnerDescription;

public class PlayerFixtures {
  private static final String aPlayerName = "John";

  public static String aGameId() {
    return gameId;
  }

  private static final AttributQueryModel vigueur = new AttributQueryModel("vigueur", "Vigueur", 5, 5);
  private static final AttributQueryModel agilite = new AttributQueryModel("agilite", "Agilité", 4, 4);

  private static final SkillQueryModel force = new SkillQueryModel("force", "Force", 3, "BASE");
  private static final SkillQueryModel endurance = new SkillQueryModel("endurance", "Endurance", 2, "BASE");

  private static SkillQueryModel chasse() {
    return new SkillQueryModel("chasse", "Chasse", 4, "CLASS");
  }

  public static String unIdDeMassue = "77920965-cb4a-49f4-863f-7fb1827faaaa";
  public static WeaponQueryModel uneMassueQM = new WeaponQueryModel(unIdDeMassue, "massue", "force", null, 2, 2, "cac", 0);

  public static String unIdDarc() {
    return "0fa73377-c947-48b7-94de-78cf7e8b703d";
  }

  public static WeaponQueryModel unArcQM = new WeaponQueryModel(unIdDarc(), "Arc", "tir", "arrow", 3, 2, "loin", 0);

  public static String unIdDeParchemin = "77920965-cb4a-49f4-863f-7fb1827fbbbb";
  public static GearQueryModel unParcheminQM = new GearQueryModel(unIdDeParchemin, "parchemin", List.of("endurance"), 2, 0);
  public static ResourceQueryModel someWater = new ResourceQueryModel("water", "Eau", 3);
  public static ResourceQueryModel someFood = new ResourceQueryModel("food", "Nourriture", 1);
  public static ResourceQueryModel someArrows = new ResourceQueryModel("arrow", "Flèches", 3);

  public static GameClassQueryModel uneClasseChasseur() {
    return new GameClassQueryModel("chasseur", "Chasseur", "chasse");
  }

  public static TalentQueryModel talentQueryModel = new TalentQueryModel("skinner", "Skinner", "chasseur", skinnerDescription);

  public static PlayerQueryModel aPlayerQM = new PlayerQueryModel("77920965-cb4a-49f4-863f-7fb1827fcde2", aPlayerName,
    List.of(agilite, vigueur), List.of(chasse(), endurance, force), List.of(uneMassueQM, unArcQM), List.of(unParcheminQM),
    List.of(someArrows, someFood, someWater), uneClasseChasseur(), List.of(talentQueryModel), 2, 3, 4, aRollsOnMultipleCategory(), aGameId(), DES_NOTES);

}
