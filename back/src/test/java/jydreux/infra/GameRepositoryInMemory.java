package jydreux.infra;

import jydreux.domain.game.GameRepository;
import jydreux.domain.game.Game;
import jydreux.domain.game.model.Gear;
import jydreux.domain.game.model.Weapon;
import jydreux.domain.user.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GameRepositoryInMemory implements GameRepository {
  private final List<Game> games;

  public GameRepositoryInMemory() {
    this.games = new ArrayList<>();
  }

  @Override
  public Game get(String id) {
    return this.games.stream().filter(game -> game.getId().equals(id)).findFirst().get();
  }

  @Override
  public void save(Game game) {
    this.games.add(game);
  }

  @Override
  public List<Game> findByUser(User user) {
    return games.stream().filter(game ->
      game.getGameMaster().equals(user.getEmail()) ||
      game.getUsers().contains(user.getEmail()) )
      .collect(Collectors.toList());
  }

  @Override
  public Game findByPlayer(String playerId) {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public Gear getGear(String gearId) {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public Weapon getWeapon(String weaponId) {
    throw new RuntimeException("Not implemented");
  }
}
