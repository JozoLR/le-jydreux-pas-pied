package jydreux.infra;

import jydreux.domain.map.Map;
import jydreux.domain.map.MapRepository;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class MapRepositoryInMemory implements MapRepository {
  private List<Map> maps;

  public MapRepositoryInMemory() {
    this.maps = new ArrayList<>();
  }

  @Override
  public void save(Map map) {
    this.maps.add(map);
  }

  @Override
  public Map get(String gameId) {
    return this.maps.stream().filter(map -> map.getGameId().equals(gameId)).findAny().get();
  }

  @Override
  public void update(Map mapToUpdate) {
    var newMaps = this.maps.stream().filter(map -> !map.getGameId().equals(mapToUpdate.getGameId())).collect(toList());
    newMaps.add(mapToUpdate);
    this.maps = newMaps;
  }
}
