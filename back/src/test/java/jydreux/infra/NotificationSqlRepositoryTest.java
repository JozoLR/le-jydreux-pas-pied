package jydreux.infra;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.TestDb;

import java.sql.SQLException;

import static fixtures.domain.EventFixtures.unEvent;
import static fixtures.domain.PlayerFixture.aFreshPlayer;
import static org.assertj.core.api.Assertions.assertThat;

class NotificationSqlRepositoryTest {
  private static TestDb testDb;
  private static EventSqlRepository eventSqlRepository ;
  private static PlayerSqlRepository playerSqlRepository;

  @BeforeAll
  static void setUp() throws SQLException {
    testDb = TestDb.init();
    eventSqlRepository = new EventSqlRepository(testDb);
    playerSqlRepository = new PlayerSqlRepository(testDb);
  }

  @BeforeEach
  void beforeEach() throws SQLException {
    testDb.reset();
  }

  @Test
  void save() {
    // Given
    playerSqlRepository.save(aFreshPlayer());

    // When
    eventSqlRepository.save(unEvent());

    // Then
    var notification = eventSqlRepository.get(unEvent().getId());
    assertThat(notification).isEqualToComparingFieldByFieldRecursively(unEvent());
  }
}
