package jydreux.infra;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.TestDb;

import java.sql.SQLException;

import static fixtures.domain.PlayerFixture.*;
import static org.assertj.core.api.Assertions.assertThat;

class PlayerSqlRepositoryTest {
  private static PlayerSqlRepository playerSqlRepository;
  private static TestDb testDb;

  @BeforeAll
  static void setUp() throws SQLException {
    testDb = TestDb.init();
    playerSqlRepository = new PlayerSqlRepository(testDb);
  }

  @BeforeEach
  void beforeEach() throws SQLException {
    testDb.reset();
  }

  @Test
  void save() {
    //When
    playerSqlRepository.save(aFreshPlayer());

    // Then
    var player = playerSqlRepository.get(aFreshPlayer().getId());
    assertThat(player.getId()).isEqualTo(aFreshPlayer().getId());
  }

  @Test
  void update() {
    // Given
    playerSqlRepository.save(aCompletePlayerWithBasicGear());

    //When
    playerSqlRepository.update(aCompletePlayer());

    // Then
    var player = playerSqlRepository.get(aCompletePlayerWithBasicGear().getId());
    assertThat(player).isEqualToComparingFieldByFieldRecursively(aCompletePlayer());
  }

  @Test
  void getByGame() {
    // Given
    playerSqlRepository.save(aCompletePlayerWithBasicGear());
    playerSqlRepository.save(aPlayerInAnotherGame());
    playerSqlRepository.save(aRandomPlayer("77920965-cb4a-49f4-863f-7fb1527fcde2"));

    //When
    var players = playerSqlRepository.getByGame(aCompletePlayerWithBasicGear().getGameId());

    // Then
    assertThat(players).hasSize(2);
    assertThat(players.get(0)).isEqualToComparingFieldByFieldRecursively(aCompletePlayerWithBasicGear());
    assertThat(players.get(1)).isEqualToComparingFieldByFieldRecursively(aRandomPlayer("77920965-cb4a-49f4-863f-7fb1527fcde2"));
  }


  @Test
  void getPlayerName() {
    // Given
    playerSqlRepository.save(aFreshPlayer());

    //When
    var playerName = playerSqlRepository.getPlayerName(aFreshPlayer().getId());

    // Then
    assertThat(playerName).isEqualTo(aFreshPlayer().getName());
  }

  @Test
  void addGear() {
    // Given
    playerSqlRepository.save(aFreshPlayer());

    // When
    playerSqlRepository.addGear(aFreshPlayer().getId(), unParchemin());

    // Then
    var player = playerSqlRepository.get(aFreshPlayer().getId());
    assertThat(player.getGears().get(0)).isEqualToComparingFieldByField(unParchemin());
  }

  @Test
  void addWeapon() {
    // Given
    playerSqlRepository.save(aFreshPlayer());

    // When
    playerSqlRepository.addWeapon(aFreshPlayer().getId(), unArc());

    // Then
    var player = playerSqlRepository.get(aFreshPlayer().getId());
    assertThat(player.getWeapons().get(0)).isEqualToComparingFieldByField(unArc());
  }

  @Test
  void deleteWeapon() {
    // Given
    playerSqlRepository.save(aFreshPlayer());
    playerSqlRepository.addWeapon(aFreshPlayer().getId(), unArc());

    // When
    playerSqlRepository.deleteWeapon(unArc().getId());

    // Then
    var player = playerSqlRepository.get(aFreshPlayer().getId());
    assertThat(player.getWeapons()).hasSize(0);
  }

  @Test
  void giveWeapon() {
    // Given
    playerSqlRepository.save(aFreshPlayer());
    playerSqlRepository.save(aRandomPlayer(anotherPlayerId()));
    playerSqlRepository.addWeapon(aFreshPlayer().getId(), uneMassue());

    // When
    playerSqlRepository.giveWeapon(uneMassue().getId(), anotherPlayerId());

    // Then
    var player = playerSqlRepository.get(anotherPlayerId());
    assertThat(player.getWeapons()).hasSize(1);
    assertThat(player.getWeapons().get(0)).isEqualToComparingFieldByFieldRecursively(uneMassue());
  }

  @Test
  void deleteGear() {
    // Given
    playerSqlRepository.save(aFreshPlayer());
    playerSqlRepository.addGear(aFreshPlayer().getId(), unParchemin());

    // When
    playerSqlRepository.deleteGear(unParchemin().getId());

    // Then
    var player = playerSqlRepository.get(aFreshPlayer().getId());
    assertThat(player.getGears()).hasSize(0);
  }

  @Test
  void giveGear() {
    // Given
    playerSqlRepository.save(aFreshPlayer());
    playerSqlRepository.save(aRandomPlayer(anotherPlayerId()));
    playerSqlRepository.addGear(aFreshPlayer().getId(), unParchemin());

    // When
    playerSqlRepository.giveGear(unParchemin().getId(), anotherPlayerId());

    // Then
    var player = playerSqlRepository.get(anotherPlayerId());
    assertThat(player.getGears()).hasSize(1);
    assertThat(player.getGears().get(0)).isEqualToComparingFieldByFieldRecursively(unParchemin());
  }
}
