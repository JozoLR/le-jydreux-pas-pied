package jydreux.infra;

import jydreux.domain.game.PlayerRepository;
import jydreux.domain.game.model.Gear;
import jydreux.domain.game.model.Player;
import jydreux.domain.game.model.Weapon;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

public class PlayerInMemoryRepository implements PlayerRepository {
    private final List<Player> players;

    public PlayerInMemoryRepository() {
        this.players = new ArrayList<>();
    }

    @Override
    public void save(Player player) {
        this.players.add(player);
    }

    @Override
    public void update(Player playerToUpdate) {
        this.players.removeIf(player -> player.getId().equals(playerToUpdate.getId()));
        this.players.add(playerToUpdate);
    }

    @Override
    public Player get(String id) {
        return this.players.stream()
        .filter(player -> player.getId().equals(id))
        .collect(toList())
        .get(0);
    }

    @Override
    public String getPlayerName(String playerId) {
        return this.players.stream()
        .filter(player -> player.getId().equals(playerId))
        .map(Player::getName)
        .collect(toList())
        .get(0);
    }

  @Override
  public void addGear(String playerId, Gear gear) {
    this.players.stream()
      .filter(player -> player.getId().equals(playerId)).findFirst()
      .ifPresent(player -> player.getGears().add(gear));
  }

  @Override
  public void addWeapon(String playerId, Weapon weapon) {
    this.players.stream()
      .filter(player -> player.getId().equals(playerId)).findFirst()
      .ifPresent(player -> player.getWeapons().add(weapon));
  }

  @Override
  public void deleteWeapon(String weaponId) {
    for (Player player : this.players) {
      if(player.getWeapon(weaponId).isPresent()) {
        player.deleteWeapon(weaponId);
      }
    }
  }

  @Override
  public void giveWeapon(String weaponId, String playerId) {
      Optional<Weapon> weapon = null;
    for (Player player : this.players) {
      if (player.getWeapon(weaponId).isPresent()) {
        weapon = player.getWeapon(weaponId);
        player.deleteWeapon(weaponId);
      }
    }
    for (Player player : this.players) {
      if (player.getId().equals(playerId)) {
        if (weapon != null && weapon.isPresent()) {
          player.getWeapons().add(weapon.get());
        }
      }
    }
  }

  @Override
  public void deleteGear(String gearId) {
    for (Player player : this.players) {
      if(player.getGear(gearId).isPresent()) {
        player.deleteGear(gearId);
      }
    }
  }

  @Override
  public void giveGear(String gearId, String playerId) {
    Optional<Gear> gear = null;
    for (Player player : this.players) {
      if (player.getGear(gearId).isPresent()) {
        gear = player.getGear(gearId);
        player.deleteGear(gearId);
      }
    }
    for (Player player : this.players) {
      if (player.getId().equals(playerId)) {
        if (gear != null && gear.isPresent()) {
          player.getGears().add(gear.get());
        }
      }
    }
  }

  @Override
    public List<Player> getByGame(String gameId) {
        return this.players.stream()
          .filter(player -> player.getGameId().equals(gameId))
          .collect(toList());
    }
}
