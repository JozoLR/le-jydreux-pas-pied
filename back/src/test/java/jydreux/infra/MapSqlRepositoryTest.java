package jydreux.infra;

import jydreux.domain.map.Map;
import jydreux.domain.map.model.Marker;
import jydreux.domain.map.model.MarkerType;
import jydreux.domain.map.model.Position;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.TestDb;

import java.sql.SQLException;

import static fixtures.domain.MapFixtures.uneMap;
import static fixtures.domain.PlayerFixture.aGameId;
import static org.assertj.core.api.Assertions.assertThat;

class MapSqlRepositoryTest {
  private static TestDb testDb;
  private MapSqlRepository mapSqlRepository;

  @BeforeEach
  void setUp() throws SQLException {
    testDb = TestDb.init();
    mapSqlRepository = new MapSqlRepository(testDb);
  }

  @BeforeEach
  void setup() throws SQLException {
    testDb.reset();
  }

  @Test
  void get() {
    // Given
    var generatedMap = Map.generate(aGameId());
    mapSqlRepository.save(generatedMap);

    // When
    var map = mapSqlRepository.get(aGameId());

    // Then
    assertThat(map).isEqualToComparingFieldByFieldRecursively(generatedMap);
  }

  @Test
  void update() {
    // Given
    var map = uneMap();
    mapSqlRepository.save(map);

    var mapWithMarker = uneMap();
    var position = new Position(1.0, 5.0);
    var marker = new Marker("Ark", 5, position, MarkerType.ARK);
    mapWithMarker.addMarker(marker);

    // When
    mapSqlRepository.update(mapWithMarker);

    // Then
    var updatedMap = mapSqlRepository.get(mapWithMarker.getGameId());
    assertThat(updatedMap).isEqualToComparingFieldByFieldRecursively(mapWithMarker);
  }
}
