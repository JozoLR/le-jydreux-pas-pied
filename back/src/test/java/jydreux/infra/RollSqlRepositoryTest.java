package jydreux.infra;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.TestDb;

import java.sql.SQLException;

import static fixtures.domain.PlayerFixture.aFreshPlayer;
import static fixtures.domain.PlayerFixture.aRandomPlayer;
import static fixtures.domain.RollDiceFixture.*;
import static org.assertj.core.api.Assertions.assertThat;

class RollSqlRepositoryTest {
  private static EventSqlRepository notificationSqlRepository;
  private static RollSqlRepository rollSqlRepository;
  private static PlayerSqlRepository playerSqlRepository;
  private static TestDb testDb;

  @BeforeAll
  static void setUp() throws SQLException {
    testDb = TestDb.init();
    notificationSqlRepository = new EventSqlRepository(testDb);
    rollSqlRepository = new RollSqlRepository(testDb);
    playerSqlRepository = new PlayerSqlRepository(testDb);
  }

  @BeforeEach
  void beforeEach() throws SQLException {
    testDb.reset();
  }

  @Test
  void getMoreRecentByPlayer() {
    // Given
    playerSqlRepository.save(aFreshPlayer());
    var anotherPlayerId = "3438b5c6-74f7-4594-9314-8566b451af86";
    playerSqlRepository.save(aRandomPlayer(anotherPlayerId));
    notificationSqlRepository.save(aNotificationRollsOnMultipleCategory());
    notificationSqlRepository.save(aNotificationRollOfPlayerX(anotherPlayerId));
    notificationSqlRepository.save(aNotificationMoreRecentRoll());

    //When
    var roll = rollSqlRepository.getMoreRecentByPlayer(aFreshPlayer().getId());

    // Then
    assertThat(roll).isEqualToComparingFieldByFieldRecursively(aMoreRecentRoll());
  }
}
