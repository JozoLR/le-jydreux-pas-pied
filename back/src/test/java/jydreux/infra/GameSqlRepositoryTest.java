package jydreux.infra;

import jydreux.domain.user.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import utils.TestDb;

import java.sql.SQLException;

import static fixtures.domain.GameFixtures.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

class GameSqlRepositoryTest {
  private static GameSqlRepository gameSqlRepository;
  private static TestDb testDb;

  @BeforeAll
  static void setUp() throws SQLException {
    testDb = TestDb.init();
    gameSqlRepository = new GameSqlRepository(testDb, mock(PlayerSqlRepository.class));
  }

  @AfterEach
  void tearDown() throws SQLException {
    testDb.reset();
  }

  @Test
  void get() {
    // Given
    gameSqlRepository.save(unGame());

    // When
    var game = gameSqlRepository.get(unGame().getId());

    // Then
    assertThat(game).isEqualToComparingFieldByFieldRecursively(unGame());
  }

  @Test
  void findByUser() {
    // Given
    gameSqlRepository.save(unGame());
    gameSqlRepository.save(unGameAvecDesInconnus());
    gameSqlRepository.save(unAutreGame());

    var user = new User("jozo@gmail.com");

    // When
    var games = gameSqlRepository.findByUser(user);

    // Then
    assertThat(games).hasSize(2);
    assertThat(games.get(0)).isEqualToComparingFieldByFieldRecursively(unGame());
    assertThat(games.get(1)).isEqualToComparingFieldByFieldRecursively(unAutreGame());
  }
}
