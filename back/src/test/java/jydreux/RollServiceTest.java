package jydreux;

import jydreux.domain.roll.RollService;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class RollServiceTest {

  @Test
  void roll_should_return_a_value_between_1_and_6() {
    // Given
    var rollService = new RollService();

    for (var i = 0; i < 100; i++) {
      // When
      var roll = rollService.roll();

      // Then
      assertThat(roll).isLessThanOrEqualTo(6);
      assertThat(roll).isGreaterThanOrEqualTo(1);
    }
  }

  @Test
  void roll_should_return_a_value_well_spreaded() {
    // Given
    var rollService = new RollService();

    var rolls = new ArrayList<Integer>();
    for (var i = 0; i < 1000000; i++) {
      // When
      var roll = rollService.roll();
      rolls.add(roll);
    }

    // Then
    for (var i = 1; i < 7; i++) {
      var number = getNumberOf(i, rolls);
      assertThat(number).isLessThanOrEqualTo(170000);
      assertThat(number).isGreaterThanOrEqualTo(160000);
    }
  }

  private Integer getNumberOf(Integer number, List<Integer> rolls) {
    int count = 0;
    for (Integer roll : rolls) {
      if (number.equals(roll)) {
        count = count + 1;
      }
    }
    return count;
  }
}
