package jydreux.domain.roll;

import jydreux.domain.roll.command.RerollDiceCommand;
import jydreux.domain.roll.RollService;
import jydreux.domain.event.Event;
import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.roll.RollRepository;
import jydreux.infra.PlayerInMemoryRepository;
import jydreux.service.DateService;
import jydreux.service.IdService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.List;

import static fixtures.domain.PlayerFixture.aFreshPlayer;
import static fixtures.domain.RollDiceFixture.*;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class RerollDiceCommandTest {
  private RerollDiceCommand rerollDice;
  private EventFactory eventFactory;
  private EventBus eventBus;
  private RollRepository rollRepository;

  @BeforeEach
  void setUp() {
    eventBus = mock(EventBus.class);
    rollRepository = mock(RollRepository.class);
    var dateService = mock(DateService.class);
    var idService = mock(IdService.class);
    eventFactory = new EventFactory(idService, dateService);
    var playerRepository = new PlayerInMemoryRepository();
    var rollService = mock(RollService.class);
    rerollDice = new RerollDiceCommand(rollRepository, eventFactory, eventBus, playerRepository, rollService);

    // Given
    given(idService.generate()).willReturn(aRerollId);
    given(dateService.now()).willReturn(aRerollDate);
    playerRepository.save(aFreshPlayer());
    given(rollService.roll()).willReturn(4);
  }

  @Test
  void handle_should_roll_the_same_roll_if_no_special_values() {
    // Given
    var player = aFreshPlayer();
    given(rollRepository.getMoreRecentByPlayer(player.getId())).willReturn(aRoll(5));

    // When
    var reroll = rerollDice.handle(player.getId());

    // Then¬
    assertThat(reroll).isEqualToComparingFieldByFieldRecursively(aReroll());
  }

  @Test
  void handle_should_roll_all_the_categorie_of_last_dice_roll() {
    // Given
    var player = aFreshPlayer();
    given(rollRepository.getMoreRecentByPlayer(player.getId())).willReturn(aRollsOnMultipleCategory());

    // When
    var reroll = rerollDice.handle(player.getId());

    // Then
    assertThat(reroll.getAttributRolls()).hasSize(1);
    assertThat(reroll.getSkillRolls()).hasSize(1);
    assertThat(reroll.getStuffRolls()).hasSize(1);
    assertThat(reroll.getMalusRolls()).hasSize(0);
    assertThat(reroll.getAttributRolls()).hasSize(1);
  }

  @Test
  void handle_should_not_reroll_the_skill_6s() {
    // Given
    var player = aFreshPlayer();
    given(rollRepository.getMoreRecentByPlayer(player.getId())).willReturn(aRoll(6));

    // When
    var reroll = rerollDice.handle(player.getId());

    // Then
    assertThat(reroll.getSkillRolls()).hasSize(1);
    assertThat(reroll.getSkillRolls().get(0)).isEqualTo(6);
  }

  @Test
  void handle_should_not_reroll_the_stuff_1s_and_6s() {
    // Given
    var player = aFreshPlayer();
    given(rollRepository.getMoreRecentByPlayer(player.getId())).willReturn(aRollOf(emptyList(), emptyList(), List.of(6, 1), emptyList()));

    // When
    var reroll = rerollDice.handle(aFreshPlayer().getId());

    // Then
    assertThat(reroll.getStuffRolls()).hasSize(2);
    assertThat(reroll.getStuffRolls().get(0)).isEqualTo(6);
    assertThat(reroll.getStuffRolls().get(1)).isEqualTo(1);
  }

  @Test
  void handle_should_not_reroll_the_attribut_1s_and_6s() {
    // Given
    var player = aFreshPlayer();
    given(rollRepository.getMoreRecentByPlayer(player.getId())).willReturn(aRollOf(List.of(6, 1), emptyList(), emptyList(), emptyList()));

    // When
    var reroll = rerollDice.handle(aFreshPlayer().getId());

    // Then
    assertThat(reroll.getAttributRolls()).hasSize(2);
    assertThat(reroll.getAttributRolls().get(0)).isEqualTo(6);
    assertThat(reroll.getAttributRolls().get(1)).isEqualTo(1);
  }

  @Test
  void handle_should_not_reroll_the_malus_1s() {
    // Given
    var player = aFreshPlayer();
    given(rollRepository.getMoreRecentByPlayer(player.getId())).willReturn(aRollOf(emptyList(), emptyList(), emptyList(), List.of(1)));

    // When
    var reroll = rerollDice.handle(aFreshPlayer().getId());

    // Then
    assertThat(reroll.getMalusRolls()).hasSize(1);
    assertThat(reroll.getMalusRolls().get(0)).isEqualTo(1);
  }

  @Test
  void handle_should_emit_a_roll_notification() {
    // Given
    var player = aFreshPlayer();
    given(rollRepository.getMoreRecentByPlayer(player.getId())).willReturn(aRoll(5));

    // When
    rerollDice.handle(aFreshPlayer().getId());

    // Then
    var argument = ArgumentCaptor.forClass(Event.class);
    verify(eventBus).dispatch(argument.capture());
    assertThat(argument.getValue())
      .isEqualToComparingFieldByFieldRecursively(aNotificationReroll());
  }
}
