package jydreux.domain.roll;

import jydreux.domain.roll.model.Dice;
import jydreux.domain.roll.model.RollType;
import jydreux.service.DateService;
import jydreux.service.IdService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static fixtures.domain.PlayerFixture.aPlayerId;
import static fixtures.domain.RollDiceFixture.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;

class DiceTest {
  DateService dateService;
  RollService rollService;
  IdService idService;

  @BeforeEach
  void init() {
    dateService = mock(DateService.class);
    rollService = mock(RollService.class);
    idService = mock(IdService.class);
    given(dateService.now()).willReturn(aRollDate);
    given(idService.generate()).willReturn(rollId);
  }

  @AfterEach
  void tearDown() {
    reset(rollService);
  }

  @Test
  void handle_should_roll_one_dice_and_return_a_roll() {
    // Given
    given(rollService.roll()).willReturn(5);
    var aRollDiceCommand = new Dice(0, 1, 0, 0, aPlayerId(), RollType.ROLL, weaponRollInfo(), rollService);

    // When
    var roll = aRollDiceCommand.roll();

    // Then
    assertThat(roll).isEqualToComparingFieldByFieldRecursively(aRoll(5));
  }

  @Test
  void handle_should_roll_one_dice_by_category_and_return_a_roll() {
    // Given
    given(rollService.roll()).willReturn(2, 3, 4);
    var aRollDiceCommandOnMultipleCategory = new Dice(1, 1, 1, 0, aPlayerId(), RollType.ROLL, weaponRollInfo(), rollService);

    // When
    var roll = aRollDiceCommandOnMultipleCategory.roll();

    // Then
    assertThat(roll).isEqualToComparingFieldByFieldRecursively(aRollsOnMultipleCategory());
  }

  @Test
  void handle_should_remove_a_skill_dice_for_one_malus_dice() {
    // Given
    given(rollService.roll()).willReturn(2);
    var aRollDiceCommandWithSimpleMalus = new Dice(0, 1, 0, 1, aPlayerId(), RollType.ROLL, weaponRollInfo(), rollService);

    // When
    var roll = aRollDiceCommandWithSimpleMalus.roll();

    // Then
    assertThat(roll.getSkillRolls()).hasSize(0);
  }

  @Test
  void handle_should_add_a_malus_dice_for_each_skil_dice_missing() {
    // Given
    given(rollService.roll()).willReturn(2);
    var aRollDiceCommandWithMoreMalus = new Dice(0, 1, 0, 2, aPlayerId(), RollType.ROLL, weaponRollInfo(), rollService);

    // When
    var roll = aRollDiceCommandWithMoreMalus.roll();

    // Then
    assertThat(roll.getMalusRolls()).hasSize(1);
    assertThat(roll.getSkillRolls()).hasSize(0);
  }

  @Test
  void handle_should_remove_a_skill_dice_for_each_malus_dice() {
    // Given
    given(rollService.roll()).willReturn(2);
    var aRollDiceCommandWithMoreSkill = new Dice(0, 2, 0, 1, aPlayerId(), RollType.ROLL, weaponRollInfo(), rollService);

    // When
    var roll = aRollDiceCommandWithMoreSkill.roll();

    // Then
    assertThat(roll.getSkillRolls()).hasSize(1);
    assertThat(roll.getMalusRolls()).hasSize(0);
  }
}
