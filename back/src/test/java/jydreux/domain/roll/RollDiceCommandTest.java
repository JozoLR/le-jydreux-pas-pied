package jydreux.domain.roll;

import jydreux.domain.roll.command.RollDiceCommand;
import jydreux.domain.event.Event;
import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.roll.model.Dice;
import jydreux.infra.PlayerInMemoryRepository;
import jydreux.service.DateService;
import jydreux.service.IdService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static fixtures.domain.PlayerFixture.aFreshPlayer;
import static fixtures.domain.RollDiceFixture.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class RollDiceCommandTest {
  DateService dateService;
  RollService rollService;
  RollDiceCommand rollDice;
  EventBus eventBus;
  EventFactory eventFactory;
  IdService idService;
  Dice dice;
  PlayerInMemoryRepository playerRepository;

  @BeforeEach
  void init() {
    dateService = mock(DateService.class);
    rollService = mock(RollService.class);
    eventBus = mock(EventBus.class);
    idService = mock(IdService.class);
    eventFactory = new EventFactory(idService, dateService);
    dice = mock(Dice.class);
    playerRepository = new PlayerInMemoryRepository();
    rollDice = new RollDiceCommand(playerRepository, eventFactory, eventBus);

    playerRepository.save(aFreshPlayer());
    given(dateService.now()).willReturn(aRollDate);
    given(idService.generate()).willReturn(rollId);
  }

  @AfterEach
  void tearDown() {
    reset(rollService);
  }


  @Test
  void handle_should_publish_the_roll() {
    // Given
    given(dice.roll()).willReturn(aRollsOnMultipleCategory());
    given(dice.getPlayerId()).willReturn(aFreshPlayer().getId());

    // When
    rollDice.handle(dice);

    // Then
    var argument = ArgumentCaptor.forClass(Event.class);
    verify(eventBus).dispatch(argument.capture());
    assertThat(argument.getValue())
      .isEqualToComparingFieldByFieldRecursively(aNotificationRollsOnMultipleCategory());
  }

}
