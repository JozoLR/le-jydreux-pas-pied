package jydreux.domain.map.handler;

import jydreux.domain.event.Event;
import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.map.MapRepository;
import jydreux.domain.map.model.Marker;
import jydreux.domain.map.model.MarkerType;
import jydreux.domain.map.model.Position;
import jydreux.infra.MapRepositoryInMemory;
import jydreux.service.DateService;
import jydreux.service.IdService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.time.LocalDateTime;
import java.time.ZoneId;

import static fixtures.domain.MapFixtures.uneMap;
import static fixtures.domain.PlayerFixture.aGameId;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class UpdateMapHandlerTest {
  private UpdateMapHandler updateMapHandler;
  private MapRepository mapRepository;
  private EventFactory eventFactory;
  private EventBus eventBus;

  @BeforeEach
  void setUp() {
    mapRepository = new MapRepositoryInMemory();
    eventBus = mock(EventBus.class);
    var idService = mock(IdService.class);
    var dateService = mock(DateService.class);

    var aDate = LocalDateTime.of(2020, 3, 31, 13, 46, 5).atZone(ZoneId.of("UTC"));
    given(dateService.now()).willReturn(aDate);
    given(idService.generate()).willReturn("un_id");

    eventFactory = new EventFactory(idService, dateService);

    updateMapHandler = new UpdateMapHandler(mapRepository, eventBus, eventFactory);
  }

  @Test
  void handle() {
    // Given
    var map = uneMap();
    mapRepository.save(map);

    var mapWithMarker = uneMap();
    var position = new Position(1.0, 5.0);
    var marker = new Marker("Ark", 5, position, MarkerType.ARK);
    mapWithMarker.addMarker(marker);
    var event = eventFactory.build(map.getGameId(), EventType.UPDATE_MAP, mapWithMarker);

    // When
    updateMapHandler.handle(event);

    // Then
    var updatedMap = mapRepository.get(aGameId());
    assertThat(updatedMap).isEqualToComparingFieldByFieldRecursively(mapWithMarker);
  }

  @Test
  void handle_should_dispatch_a_notification() {
    // Given
    var map = uneMap();
    mapRepository.save(map);

    var mapWithMarker = uneMap();
    var position = new Position(1.0, 5.0);
    var marker = new Marker("Ark", 5, position, MarkerType.ARK);
    mapWithMarker.addMarker(marker);
    var event = eventFactory.build(map.getGameId(), EventType.UPDATE_MAP, mapWithMarker);

    // When
    updateMapHandler.handle(event);

    var argument = ArgumentCaptor.forClass(Event.class);
    verify(eventBus, times(1)).dispatch(argument.capture());
    assertThat(argument.getAllValues().get(0).getType()).isEqualTo(EventType.MAP_UPDATED);
  }
}
