package jydreux.domain.map.query;

import jydreux.infra.MapRepositoryInMemory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static fixtures.domain.MapFixtures.uneMap;
import static org.assertj.core.api.Assertions.assertThat;

class GetMapTest {

  @Test
  void handle() {
    // Given
    var mapRepository = new MapRepositoryInMemory();
    mapRepository.save(uneMap());

    // When
    var map = mapRepository.get(uneMap().getGameId());

    // Then
    assertThat(map).isEqualToComparingFieldByFieldRecursively(uneMap());
  }
}
