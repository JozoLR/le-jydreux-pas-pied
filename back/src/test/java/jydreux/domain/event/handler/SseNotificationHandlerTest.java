package jydreux.domain.event.handler;

import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventRepository;
import jydreux.domain.notification.query.NotificationQueryModelMapper;
import jydreux.infra.GameRepositoryInMemory;
import jydreux.infra.PlayerInMemoryRepository;
import jydreux.infra.RulesRepositoryInMemory;
import jydreux.service.DateService;
import jydreux.service.IdService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;

import static fixtures.domain.EventFixtures.*;
import static fixtures.domain.PlayerFixture.aCompletePlayer;
import static fixtures.domain.RollDiceFixture.aBasicRollEvent;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class SseNotificationHandlerTest {
  EventRepository notificationRepository;
  SseEmitter sseEmitter;
  SseNotificationHandler notificationHandler;
  EventFactory eventFactory;

  @BeforeEach
  void setUp() {
    // Given
    sseEmitter = mock(SseEmitter.class);
    var eventBus = mock(EventBus.class);
    var rulesRepository = new RulesRepositoryInMemory();
    var playerRepository = new PlayerInMemoryRepository();
    var gameRepository = new GameRepositoryInMemory();
    var notificationQueryModelMapper = new NotificationQueryModelMapper(rulesRepository, gameRepository);
    notificationRepository = mock(EventRepository.class);
    playerRepository.save(aCompletePlayer());

    notificationHandler = new SseNotificationHandler(sseEmitter, eventBus, playerRepository, notificationQueryModelMapper, unEvent().getGameId(), "client_id");

    eventFactory = new EventFactory(mock(IdService.class), mock(DateService.class));
  }

  @Test
  void handle_should_emit_the_notification_in_the_sse_if_same_game_id() throws IOException {
    // Given
    var event = unEvent();

    // When
    notificationHandler.handle(event);

    // Then
    var argument = ArgumentCaptor.forClass(SseEmitter.SseEventBuilder.class);
    verify(sseEmitter).send(argument.capture());

    var eventBuilder = SseEmitter.event().data(uneNotification())
      .name(event.getType().name());

    assertThat(argument.getValue()).isEqualToComparingFieldByFieldRecursively(eventBuilder);
  }

  @Test
  void handle_should_map_rolls_with_query_model_with_info() throws IOException {
    // Given
    var event = uneCompleteRollEvent();

    // When
    notificationHandler.handle(event);

    // Then
    var argument = ArgumentCaptor.forClass(SseEmitter.SseEventBuilder.class);
    verify(sseEmitter).send(argument.capture());

    var eventBuilder = SseEmitter.event().data(uneCompleteRollNotificationQueryModel())
      .name(event.getType().name());

    assertThat(argument.getValue()).isEqualToComparingFieldByFieldRecursively(eventBuilder);
  }

  @Test
  void handle_should_map_rolls_with_query_model_with_non_skilled_roll() throws IOException {
    // Given
    var event = aBasicRollEvent();

    // When
    notificationHandler.handle(event);

    // Then
    var argument = ArgumentCaptor.forClass(SseEmitter.SseEventBuilder.class);
    verify(sseEmitter).send(argument.capture());

    var eventBuilder = SseEmitter.event().data(uneBasicRollNotificationQueryModel())
      .name(event.getType().name());

    assertThat(argument.getValue()).isEqualToComparingFieldByFieldRecursively(eventBuilder);
  }
}
