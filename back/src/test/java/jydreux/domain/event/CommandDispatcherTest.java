package jydreux.domain.event;

import jydreux.domain.exception.ForbiddenException;
import jydreux.domain.game.command.gear.UpdateGearCommand;
import jydreux.domain.user.User;
import jydreux.domain.user.UserRepository;
import jydreux.infra.GameRepositoryInMemory;
import jydreux.service.DateService;
import jydreux.service.IdService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static fixtures.domain.GameFixtures.unGame;
import static fixtures.domain.PlayerFixture.aFreshPlayer;
import static fixtures.domain.PlayerFixture.unParcheminAvecMalus;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class CommandDispatcherTest {
  private CommandDispatcher commandDispatcher;
  private UserRepository userRepository;
  private EventBus eventBus;
  private EventFactory eventFactory;

  @BeforeEach
  void setUp() {
    userRepository = mock(UserRepository.class);
    var gameRepository = new GameRepositoryInMemory();
    gameRepository.save(unGame());
    eventBus = mock(EventBus.class);
    eventFactory = new EventFactory(mock(IdService.class), mock(DateService.class));
    commandDispatcher = new CommandDispatcher(userRepository, gameRepository, eventBus);
  }

  @AfterEach
  void tearDown() {
  }

  @Test
  void validateAndDispatch_update_gear_event() {
    // Given
    var gear = unParcheminAvecMalus();
    var player = aFreshPlayer();
    var token = "un_token_valide";
    var updateGearCommand = new UpdateGearCommand(
      gear.getId(),
      gear.getMalus(),
      player.getId(),
      player.getGameId()
    );

    var event = eventFactory.build(player.getGameId(), player.getId(), EventType.UPDATE_GEAR, updateGearCommand);

    var user = new User("pozo@gmail.com");
    given(userRepository.getByToken(token)).willReturn(user);

    // When
    commandDispatcher.validateAndDispatch(event, token);

    // Then
    var argument = ArgumentCaptor.forClass(Event.class);
    verify(eventBus).dispatch(argument.capture());
    assertThat(argument.getValue()).isEqualToComparingFieldByFieldRecursively(event);
  }

  @Test
  void validateAndDispatch_doesnt_emit_an_event_if_user_is_not_in_game() {
    // Given
    var gear = unParcheminAvecMalus();
    var player = aFreshPlayer();
    var token = "un_token_valide";
    var updateGearCommand = new UpdateGearCommand(
      gear.getId(),
      gear.getMalus(),
      player.getId(),
      player.getGameId()
    );

    var event = eventFactory.build(player.getGameId(), player.getId(), EventType.UPDATE_GEAR, updateGearCommand);

    var user = new User("john@gmail.com");
    given(userRepository.getByToken(token)).willReturn(user);

    // When
    assertThrows(ForbiddenException.class, () -> commandDispatcher.validateAndDispatch(event, token));
  }
}
