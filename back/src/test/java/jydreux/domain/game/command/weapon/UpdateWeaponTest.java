package jydreux.domain.game.command.weapon;

import jydreux.domain.event.Event;
import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.command.weapon.AddWeaponCommand;
import jydreux.domain.game.command.weapon.UpdateWeaponCommand;
import jydreux.domain.game.notification.PlayerUpdate;
import jydreux.infra.PlayerInMemoryRepository;
import jydreux.service.DateService;
import jydreux.service.IdService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static fixtures.domain.EventFixtures.unId;
import static fixtures.domain.EventFixtures.uneDate;
import static fixtures.domain.PlayerFixture.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class UpdateWeaponTest {
  private static PlayerInMemoryRepository playerRepository;
  private static EventFactory eventFactory;
  private static EventBus eventBus;
  private static UpdateWeaponCommand updateWeapon;

  @BeforeEach
  void beforeEach() {
    playerRepository = new PlayerInMemoryRepository();
    eventBus = mock(EventBus.class);
    var idService = mock(IdService.class);
    var dateService = mock(DateService.class);
    eventFactory = new EventFactory(idService, dateService);
    AddWeaponCommand addWeapon = new AddWeaponCommand(playerRepository, eventFactory, eventBus);
    updateWeapon = new UpdateWeaponCommand(playerRepository, eventFactory, eventBus);

    // Given
    playerRepository.save(aFreshPlayer());
    addWeapon.handle(uneMassue(), aFreshPlayer().getId());

    given(idService.generate()).willReturn(unId());
    given(dateService.now()).willReturn(uneDate);
  }

  @Test
  void handle_should_change_a_player_gear() {
    // When
    updateWeapon.handle(uneMassueAvecMalus(), aFreshPlayer().getId());

    // Then
    var player = playerRepository.get(aFreshPlayer().getId());
    assertThat(player.getWeapons().get(0)).isEqualToComparingFieldByFieldRecursively(uneMassueAvecMalus());
  }

  @Test
  void handle_should_dispatch_the_player() {
    //Given
    var player = aFreshPlayer();
    var playerUpdate = new PlayerUpdate(
      uneMassue().getId(),
      uneMassue().getLabel(),
      uneMassue().getMalus(),
      uneMassueAvecMalus().getMalus()
    );
    var event = new Event<>(
      unId(),
      uneDate,
      player.getId(),
      player.getGameId(),
      EventType.WEAPON_UPDATED,
      playerUpdate
    );

    // When
    updateWeapon.handle(uneMassueAvecMalus(), aFreshPlayer().getId());

    // Then
    var argument = ArgumentCaptor.forClass(Event.class);
    verify(eventBus, times(2)).dispatch(argument.capture());
    assertThat(argument.getValue()).isEqualToComparingFieldByFieldRecursively(event);
  }
}
