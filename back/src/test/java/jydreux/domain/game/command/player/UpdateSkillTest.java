package jydreux.domain.game.command.player;

import jydreux.domain.event.Event;
import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.command.player.UpdateSkillCommand;
import jydreux.domain.game.notification.PlayerUpdate;
import jydreux.infra.PlayerInMemoryRepository;
import jydreux.service.DateService;
import jydreux.service.IdService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.stream.Collectors;

import static fixtures.domain.EventFixtures.unId;
import static fixtures.domain.EventFixtures.uneDate;
import static fixtures.domain.PlayerFixture.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class UpdateSkillTest {
  PlayerInMemoryRepository playerRepository;
  EventBus eventBus;
  EventFactory eventFactory;
  UpdateSkillCommand updateSkill;

  @BeforeEach
  void setUp() {
    playerRepository = new PlayerInMemoryRepository();
    eventBus = mock(EventBus.class);
    var idService = mock(IdService.class);
    var dateService = mock(DateService.class);
    eventFactory = new EventFactory(idService, dateService);
    updateSkill = new UpdateSkillCommand(playerRepository, eventFactory, eventBus);

    // Given
    playerRepository.save(aFreshPlayer());
    given(idService.generate()).willReturn(unId());
    given(dateService.now()).willReturn(uneDate);
  }

  @Test
  void handle_should_change_a_player_skill() {
    // When
    updateSkill.handle(force3(), aFreshPlayer().getId());

    // Then
    var player = playerRepository.get(aFreshPlayer().getId());
    var force = player.getSkills().stream().filter(skill -> skill.getId().equals(force3().getId()))
      .collect(Collectors.toList()).get(0);
    assertThat(force).isEqualToComparingFieldByFieldRecursively(force3());
  }

  @Test
  void handle_should_dispatch_the_player() {
    //Given
    var player = aFreshPlayer();
    var playerUpdate = new PlayerUpdate(
      forceAZero().getId(),
      forceAZero().getLabel(),
      forceAZero().getActual(),
      force3().getActual()
    );
    var event = new Event<>(
      unId(),
      uneDate,
      player.getId(),
      player.getGameId(),
      EventType.SKILL_UPDATED,
      playerUpdate
    );

    // When
    updateSkill.handle(force3(), aFreshPlayer().getId());

    // Then
    var argument = ArgumentCaptor.forClass(Event.class);
    verify(eventBus).dispatch(argument.capture());
    assertThat(argument.getValue()).isEqualToComparingFieldByFieldRecursively(event);

  }
}
