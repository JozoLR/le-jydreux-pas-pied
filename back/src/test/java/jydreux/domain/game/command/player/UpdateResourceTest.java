package jydreux.domain.game.command.player;

import jydreux.domain.event.Event;
import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.command.player.UpdateResourceCommand;
import jydreux.domain.game.notification.PlayerUpdate;
import jydreux.infra.PlayerInMemoryRepository;
import jydreux.service.DateService;
import jydreux.service.IdService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static fixtures.domain.EventFixtures.unId;
import static fixtures.domain.EventFixtures.uneDate;
import static fixtures.domain.PlayerFixture.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class UpdateResourceTest {
  static private PlayerInMemoryRepository playerRepository;
  static private EventFactory eventFactory;
  static private EventBus eventBus;
  static private UpdateResourceCommand updateResource;

  @BeforeAll
  static void setUp() {
    playerRepository = new PlayerInMemoryRepository();
    eventBus = mock(EventBus.class);
    var idService = mock(IdService.class);
    var dateService = mock(DateService.class);
    eventFactory = new EventFactory(idService, dateService);
    updateResource = new UpdateResourceCommand(playerRepository, eventFactory, eventBus);

    // Given
    playerRepository.save(aFreshPlayer());
    given(idService.generate()).willReturn(unId());
    given(dateService.now()).willReturn(uneDate);
  }

  @Test
  void handle_should_change_a_player_gear() {
    // When
    updateResource.handle(someWater(), aFreshPlayer().getId());

    // Then
    var player = playerRepository.get(aFreshPlayer().getId());
    assertThat(player.getResources().get(0)).isEqualToComparingFieldByFieldRecursively(someWater());
  }

  @Test
  void handle_should_dispatch_a_notification() {
    //Given
    var player = aFreshPlayer();
    var playerUpdate = new PlayerUpdate(
      someWater().getId(),
      someWater().getLabel(),
      noWater().getQuantity(),
      someWater().getQuantity()
    );
    var notification = new Event<>(
      unId(),
      uneDate,
      player.getId(),
      player.getGameId(),
      EventType.RESOURCE_UPDATED,
      playerUpdate
    );

    // When
    updateResource.handle(someWater(), aFreshPlayer().getId());

    // Then
    var argument = ArgumentCaptor.forClass(Event.class);
    verify(eventBus).dispatch(argument.capture());
    assertThat(argument.getValue()).isEqualToComparingFieldByFieldRecursively(notification);
  }
}
