package jydreux.domain.game.command.gear;

import jydreux.domain.event.Event;
import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.command.gear.AddGearCommand;
import jydreux.domain.game.notification.StuffAdded;
import jydreux.infra.PlayerInMemoryRepository;
import jydreux.service.DateService;
import jydreux.service.IdService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static fixtures.domain.EventFixtures.unId;
import static fixtures.domain.EventFixtures.uneDate;
import static fixtures.domain.PlayerFixture.aFreshPlayer;
import static fixtures.domain.PlayerFixture.unParchemin;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class AddGearCommandTest {
  private static PlayerInMemoryRepository playerRepository;
  private static AddGearCommand addGear;
  private static EventBus eventBus;
  private static EventFactory eventFactory;

  @BeforeEach
  void beforeEach() {
    // Given
    playerRepository = new PlayerInMemoryRepository();
    eventBus = mock(EventBus.class);
    var idService = mock(IdService.class);
    var dateService = mock(DateService.class);
    eventFactory = new EventFactory(idService, dateService);
    playerRepository.save(aFreshPlayer());
    addGear = new AddGearCommand(playerRepository, eventFactory, eventBus);

    given(idService.generate()).willReturn(unId());
    given(dateService.now()).willReturn(uneDate);
  }

  @Test
  void handle_should_add_a_gear_to_a_player() {
    // When
    addGear.handle(unParchemin(), aFreshPlayer().getId());

    // Then
    var player = playerRepository.get(aFreshPlayer().getId());
    assertThat(player.getGears()).hasSize(1);
    assertThat(player.getGears().get(0)).isEqualToComparingFieldByFieldRecursively(unParchemin());
  }

  @Test
  void handle_should_dispatch_the_event() {
    // Given
    var player = aFreshPlayer();
    var stuffAdded = new StuffAdded(unParchemin().getId(), unParchemin().getLabel());
    var notification = new Event<>(
      unId(),
      uneDate,
      player.getId(),
      player.getGameId(),
      EventType.GEAR_ADDED,
      stuffAdded
    );

    // When
    addGear.handle(unParchemin(), aFreshPlayer().getId());

    // Then
    var argument = ArgumentCaptor.forClass(Event.class);
    verify(eventBus).dispatch(argument.capture());
    assertThat(argument.getValue()).isEqualToComparingFieldByFieldRecursively(notification);
  }
}
