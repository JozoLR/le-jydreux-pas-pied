package jydreux.domain.game.command.player;

import jydreux.domain.game.command.player.CreatePlayerCommand;
import jydreux.domain.cross.SkillType;
import jydreux.domain.game.PlayerRepository;
import jydreux.infra.RulesRepositoryInMemory;
import jydreux.infra.PlayerInMemoryRepository;
import jydreux.service.IdService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;

import static fixtures.domain.PlayerFixture.aFreshPlayer;
import static fixtures.domain.PlayerToCreateFixture.aPlayerToCreateChasseur;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

class CreatePlayerTest {
  private PlayerRepository playerRepository;
  private String id;

  @BeforeEach
  void setup() {
    // Given
    IdService idService = mock(IdService.class);
    playerRepository = new PlayerInMemoryRepository();
    RulesRepositoryInMemory gameRepository = new RulesRepositoryInMemory();
    var createPlayer = new CreatePlayerCommand(playerRepository, gameRepository);

    given(idService.generate()).willReturn(aFreshPlayer().getId());

    // When
    id = createPlayer.handle(aPlayerToCreateChasseur);
  }

  @Test
  void handle_should_create_a_player_and_add_missing_skills() {
    // Then
    var basicSkills = playerRepository.get(id).getSkills().stream()
      .filter(skill -> skill.getId().equals("force") || skill.getId().equals("endurance"))
      .collect(Collectors.toList());
    assertThat(basicSkills).hasSize(2);
  }

  @Test
  void handle_should_create_a_player_and_add_the_special_skill() {
    // Then
    var chasseurSkill = playerRepository.get(id).getSkills().stream()
      .filter(skill -> skill.getSkillType() == SkillType.CLASS).collect(Collectors.toList());
    assertThat(chasseurSkill).hasSize(1);
    assertThat(chasseurSkill.get(0).getId()).isEqualTo("chasse");
  }

  @Test
  void handle_should_create_a_player_and_add_the_armor_skill() {
    // Then
    var chasseurSkill = playerRepository.get(id).getSkills().stream()
      .filter(skill -> skill.getId().equals("armor")).collect(Collectors.toList());
    assertThat(chasseurSkill).hasSize(1);
  }
}
