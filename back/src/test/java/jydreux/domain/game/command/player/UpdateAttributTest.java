package jydreux.domain.game.command.player;

import jydreux.domain.event.Event;
import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.command.player.UpdateAttributCommand;
import jydreux.domain.game.notification.PlayerUpdate;
import jydreux.infra.PlayerInMemoryRepository;
import jydreux.service.DateService;
import jydreux.service.IdService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static fixtures.domain.EventFixtures.unId;
import static fixtures.domain.EventFixtures.uneDate;
import static fixtures.domain.PlayerFixture.*;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class UpdateAttributTest {
  PlayerInMemoryRepository playerRepository;
  EventBus eventBus;
  EventFactory eventFactory;
  UpdateAttributCommand updateAttribut;

  @BeforeEach
  void setUp() {
    playerRepository = new PlayerInMemoryRepository();
    eventBus = mock(EventBus.class);
    var idService = mock(IdService.class);
    var dateService = mock(DateService.class);
    eventFactory = new EventFactory(idService, dateService);
    updateAttribut = new UpdateAttributCommand(playerRepository, eventFactory, eventBus);

    // Given
    playerRepository.save(aFreshPlayer());
    given(idService.generate()).willReturn(unId());
    given(dateService.now()).willReturn(uneDate);
  }

  @Test
  void handle_should_change_a_player_attribut() {
    // When
    updateAttribut.handle(deLaVigueurAvecMalus(), aFreshPlayer().getId());

    // Then
    var player = playerRepository.get(aFreshPlayer().getId());
    var vigueur = player.getAttributs().stream()
      .filter(attribut -> attribut.getId().equals(deLaVigueurAvecMalus().getId())).collect(toList()).get(0);
    assertThat(vigueur).isEqualToComparingFieldByFieldRecursively(deLaVigueurAvecMalus());
  }

  @Test
  void handle_should_dispatch_a_notification() {
    //Given
    var player = aFreshPlayer();
    var playerUpdate = new PlayerUpdate(
      vigueur5().getId(),
      vigueur5().getLabel(),
      vigueur5().getActual(),
      deLaVigueurAvecMalus().getActual()
    );
    var notification = new Event<>(
      unId(),
      uneDate,
      player.getId(),
      player.getGameId(),
      EventType.ATTRIBUT_UPDATED,
      playerUpdate
    );

    // When
    updateAttribut.handle(deLaVigueurAvecMalus(), aFreshPlayer().getId());

    // Then
    var argument = ArgumentCaptor.forClass(Event.class);
    verify(eventBus).dispatch(argument.capture());
    assertThat(argument.getValue()).isEqualToComparingFieldByFieldRecursively(notification);
  }

}
