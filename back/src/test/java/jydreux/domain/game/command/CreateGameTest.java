package jydreux.domain.game.command;

import jydreux.domain.game.Game;
import jydreux.domain.game.GameRepository;
import jydreux.domain.game.GameToCreate;
import jydreux.domain.game.command.CreateGameCommand;
import jydreux.domain.map.Map;
import jydreux.domain.map.MapRepository;
import jydreux.domain.user.User;
import jydreux.domain.user.UserRepository;
import jydreux.infra.GameRepositoryInMemory;
import jydreux.infra.MapRepositoryInMemory;
import jydreux.service.IdService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static fixtures.domain.GameFixtures.gameName;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

class CreateGameTest {
  private UserRepository userRepository;
  private GameRepository gameRepository;
  private MapRepository mapRepository;

  private String id;
  private String token;
  private List<String> players;
  private Game game;

  @BeforeEach
  void setUp() {
    userRepository = mock(UserRepository.class);
    gameRepository = new GameRepositoryInMemory();
    mapRepository = new MapRepositoryInMemory();
    var idService = mock(IdService.class);
    var createGame = new CreateGameCommand(userRepository, gameRepository, mapRepository, idService);

    // Given
    id = "un_id";
    given(idService.generate()).willReturn(id);

    token = "un_token";
    var user = new User("toto@gmail.com");
    given(userRepository.getByToken(token)).willReturn(user);

    players = List.of("jozo@gmail.com", "pozo@gmail.com");

    var gameToCreate = new GameToCreate(token, gameName, players);

    // When
    game = createGame.handle(gameToCreate);
  }

  @Test
  void handle_should_get_the_user() {
    // Then
    then(userRepository).should().getByToken(token);
  }

  @Test
  void handle_should_save_a_new_game() {
    // Then
    var game = gameRepository.get(id);
    var expectedGame = new Game(id, gameName, "toto@gmail.com", players, null);
    assertThat(game).isEqualToComparingFieldByFieldRecursively(expectedGame);
  }

  @Test
  void handle_should_generate_a_map() {
    // Then
    var map = mapRepository.get(id);
    var expectedGame = Map.generate(id);
    assertThat(map).isEqualToComparingFieldByFieldRecursively(expectedGame);
  }

  @Test
  void handle_should_return_the_game() {
    // Then
    var expectedGame = new Game(id, gameName, "toto@gmail.com", players, null);
    assertThat(game).isEqualToComparingFieldByFieldRecursively(expectedGame);
  }
}
