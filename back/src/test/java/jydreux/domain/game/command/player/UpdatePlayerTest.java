package jydreux.domain.game.command.player;

import jydreux.api.resources.UpdatePlayerResource;
import jydreux.domain.event.Event;
import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.command.player.UpdatePlayerCommand;
import jydreux.domain.game.model.Player;
import jydreux.domain.game.notification.PlayerUpdate;
import jydreux.infra.PlayerInMemoryRepository;
import jydreux.service.DateService;
import jydreux.service.IdService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static fixtures.domain.EventFixtures.unId;
import static fixtures.domain.EventFixtures.uneDate;
import static fixtures.domain.PlayerFixture.aFreshPlayer;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class UpdatePlayerTest {
  private PlayerInMemoryRepository playerRepository;
  private EventBus eventBus;
  private EventFactory eventFactory;
  private UpdatePlayerCommand updatePlayer;

  @BeforeEach
  void setup() {
    playerRepository = new PlayerInMemoryRepository();
    eventBus = mock(EventBus.class);
    var idService = mock(IdService.class);
    var dateService = mock(DateService.class);
    eventFactory = new EventFactory(idService, dateService);
    updatePlayer = new UpdatePlayerCommand(playerRepository, eventFactory, eventBus);

    // Given
    playerRepository.save(aFreshPlayer());
    given(idService.generate()).willReturn(unId());
    given(dateService.now()).willReturn(uneDate);
  }

  @Test
  void handle_should_update_player_points() {
    // When
    var updatePlayerResource = new UpdatePlayerResource(aFreshPlayer().getId(), 2, 3, 4);
    updatePlayer.handle(updatePlayerResource);

    // Then
    var player = playerRepository.get(aFreshPlayer().getId());
    assertThat(player.getExperience()).isEqualTo(2);
    assertThat(player.getRot()).isEqualTo(3);
    assertThat(player.getMutation()).isEqualTo(4);
  }

  @Test
  void handle_should_dispatch_a_notification_for_each_change() {
    // Given
    var player = aFreshPlayer();
    var experienceNotification = buildNotification(player, "experience", "Experience", 2);
    var rotNotification = buildNotification(player, "rot", "Rot", 3);
    var mutationNotification = buildNotification(player, "mutation", "Mutation", 4);

    // When
    var updatePlayerResource = new UpdatePlayerResource(aFreshPlayer().getId(), 2, 3, 4);
    updatePlayer.handle(updatePlayerResource);

    // Then
    var argument = ArgumentCaptor.forClass(Event.class);
    verify(eventBus, times(3)).dispatch(argument.capture());
    assertThat(argument.getAllValues().get(0)).isEqualToComparingFieldByFieldRecursively(experienceNotification);
    assertThat(argument.getAllValues().get(1)).isEqualToComparingFieldByFieldRecursively(rotNotification);
    assertThat(argument.getAllValues().get(2)).isEqualToComparingFieldByFieldRecursively(mutationNotification);
  }

  private Event<PlayerUpdate> buildNotification(Player player, String id, String label, int currentValue) {
    var experienceUpdate = new PlayerUpdate(
      id,
      label,
      0,
      currentValue
    );

    return new Event<>(
      unId(),
      uneDate,
      player.getId(),
      player.getGameId(),
      EventType.PLAYER_UPDATED,
      experienceUpdate
    );
  }
}
