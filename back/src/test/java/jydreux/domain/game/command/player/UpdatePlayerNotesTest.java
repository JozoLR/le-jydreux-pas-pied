package jydreux.domain.game.command.player;

import jydreux.domain.game.command.player.UpdatePlayerNotesCommand;
import jydreux.infra.PlayerInMemoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static fixtures.domain.PlayerFixture.aFreshPlayer;
import static org.assertj.core.api.Assertions.assertThat;

class UpdatePlayerNotesTest {
  private PlayerInMemoryRepository playerRepository;

  @BeforeEach
  void setup() {
    playerRepository = new PlayerInMemoryRepository();
    UpdatePlayerNotesCommand updatePlayerNotes = new UpdatePlayerNotesCommand(playerRepository);

    // Given
    playerRepository.save(aFreshPlayer());

    // When
    updatePlayerNotes.handle(aFreshPlayer().getId(), "Des nouvelles notes");
  }

  @Test
  void handle() {
    // Then
    var player = playerRepository.get(aFreshPlayer().getId());
    assertThat(player.getNotes()).isEqualTo("Des nouvelles notes");
  }
}
