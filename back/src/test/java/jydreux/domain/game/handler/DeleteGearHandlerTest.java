package jydreux.domain.game.handler;

import jydreux.domain.event.Event;
import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.command.gear.DeleteGearCommand;
import jydreux.domain.game.notification.StuffDeleted;
import jydreux.infra.PlayerInMemoryRepository;
import jydreux.service.DateService;
import jydreux.service.IdService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.time.LocalDateTime;
import java.time.ZoneId;

import static fixtures.domain.CommandFixtures.uneCommandeDeleteGear;
import static fixtures.domain.PlayerFixture.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class DeleteGearHandlerTest {
  private static PlayerInMemoryRepository playerRepository;
  private static DeleteGearHandler deleteGearHandler;
  private static EventBus eventBus;
  private static EventFactory eventFactory;
  private Event<DeleteGearCommand> event;

  @BeforeEach
  void setUp() {
    playerRepository = new PlayerInMemoryRepository();
    eventBus = mock(EventBus.class);
    var idService = mock(IdService.class);
    var dateService = mock(DateService.class);
    eventFactory = new EventFactory(idService, dateService);
    deleteGearHandler = new DeleteGearHandler(playerRepository, eventBus, eventFactory);

    // Given
    var aDate = LocalDateTime.of(2020, 3, 31, 13, 46, 5).atZone(ZoneId.of("UTC"));
    given(dateService.now()).willReturn(aDate);
    given(idService.generate()).willReturn("un_id");
    playerRepository.save(aCompletePlayerWithBasicGear());

    var deleteGearCommand = uneCommandeDeleteGear();

    event = eventFactory.build(aFreshPlayer().getGameId(), aFreshPlayer().getId(), EventType.DELETE_GEAR, deleteGearCommand);
  }

  @Test
  void handle_should_delete_a_gear() {
    // When
    deleteGearHandler.handle(event);

    // Then
    var playerGears = playerRepository.get(aCompletePlayerWithBasicGear().getId()).getGears();
    assertThat(playerGears).hasSize(0);
  }

  @Test
  void handle_should_emit_an_event() {
    // When
    deleteGearHandler.handle(event);

    // Then
    playerRepository.get(aCompletePlayerWithBasicGear().getId());

    var argument = ArgumentCaptor.forClass(Event.class);
    verify(eventBus).dispatch(argument.capture());
    var stuffDeleted = new StuffDeleted(
      aCompletePlayerWithBasicGear().getId(),
      aCompletePlayerWithBasicGear().getName(),
      unParcheminAvecMalus().getId(),
      unParcheminAvecMalus().getLabel()
    );
    var eventEmitted = eventFactory.build(event.getGameId(), event.getPlayerId(), EventType.GEAR_DELETED, stuffDeleted);
    assertThat(argument.getValue()).isEqualToComparingFieldByFieldRecursively(eventEmitted);
  }
}
