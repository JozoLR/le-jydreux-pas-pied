package jydreux.domain.game.handler;

import jydreux.domain.game.command.gear.AddGearCommand;
import jydreux.domain.event.Event;
import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.command.gear.GiveGearCommand;
import jydreux.infra.PlayerInMemoryRepository;
import jydreux.service.DateService;
import jydreux.service.IdService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.time.LocalDateTime;
import java.time.ZoneId;

import static fixtures.domain.PlayerFixture.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class GiveGearHandlerTest {
  private static PlayerInMemoryRepository playerRepository;
  private static GiveGearHandler giveGearHandler;
  private static EventBus eventBus;
  private static EventFactory eventFactory;
  private Event<GiveGearCommand> event;

  @BeforeEach
  void setUp() {
    playerRepository = new PlayerInMemoryRepository();
    eventBus = mock(EventBus.class);
    var idService = mock(IdService.class);
    var dateService = mock(DateService.class);
    eventFactory = new EventFactory(idService, dateService);
    var addGear = new AddGearCommand(playerRepository, eventFactory, eventBus);
    giveGearHandler = new GiveGearHandler(playerRepository, eventFactory, eventBus);

    // Given
    var aDate = LocalDateTime.of(2020, 3, 31, 13, 46, 5).atZone(ZoneId.of("UTC"));
    given(dateService.now()).willReturn(aDate);
    given(idService.generate()).willReturn("un_id");
    var giver = aFreshPlayer();
    playerRepository.save(giver);
    playerRepository.save(aRandomPlayer(anotherPlayerId()));
    addGear.handle(unParchemin(), giver.getId());

    var giveGearCommand = new GiveGearCommand(
      unParchemin().getId(),
      aFreshPlayer().getId(),
      aRandomPlayer(anotherPlayerId()).getId(),
      aFreshPlayer().getGameId()
    );

    event = eventFactory.build(aFreshPlayer().getGameId(), aFreshPlayer().getId(), EventType.GIVE_GEAR, giveGearCommand);
  }

  @Test
  void handle_should_give_a_weapon_to_another_player() {
    // When
    giveGearHandler.handle(event);

    // Then
    var giverGears = playerRepository.get(aFreshPlayer().getId()).getGears();
    assertThat(giverGears).hasSize(0);

    var beneficiary = playerRepository.get(aRandomPlayer(anotherPlayerId()).getId());
    assertThat(beneficiary.getGears()).hasSize(1);
    assertThat(beneficiary.getGear(unParchemin().getId()).get()).isEqualToComparingFieldByFieldRecursively(unParchemin());
  }

  @Test
  void handle_should_emit_a_notification() {
    // When
    giveGearHandler.handle(event);

    // Then
    playerRepository.get(aFreshPlayer().getId());

    var argument = ArgumentCaptor.forClass(Event.class);
    verify(eventBus, times(2)).dispatch(argument.capture());
    assertThat(argument.getAllValues().get(1).getType()).isEqualTo(EventType.GEAR_GIVEN);
  }
}
