package jydreux.domain.game.handler;

import jydreux.domain.event.Event;
import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.game.command.player.UpgradePlayerCommand;
import jydreux.infra.PlayerInMemoryRepository;
import jydreux.service.DateService;
import jydreux.service.IdService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.List;

import static fixtures.domain.PlayerFixture.*;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class UpgradePlayerHandlerTest {
  private UpgradePlayerHandler upgradePlayerHandler;
  private PlayerRepository playerRepository;
  private EventBus eventBus;
  private EventFactory eventFactory;

  @BeforeEach
  void setUp() {
    playerRepository = new PlayerInMemoryRepository();
    var idService = mock(IdService.class);
    var dateService = mock(DateService.class);
    eventFactory = new EventFactory(idService, dateService);
    eventBus = mock(EventBus.class);
    upgradePlayerHandler = new UpgradePlayerHandler(playerRepository, eventFactory, eventBus);
  }

  @Test
  void handle_should_remove_5_xp_points_for_a_one_skill_upgrade() {
    // Given
    var player = aFreshPlayer();
    player.updateExperience(6);
    playerRepository.save(player);

    var command = new UpgradePlayerCommand(player.getId(), List.of("force"), emptyList());
    var event = eventFactory.build(player.getGameId(), player.getId(), EventType.UPGRADE_PLAYER, command);

    // When
    upgradePlayerHandler.handle(event);

    // Then
    var ugradedPlayer = playerRepository.get(player.getId());
    assertThat(ugradedPlayer.getExperience()).isEqualTo(1);
    assertThat(ugradedPlayer.getSkill("force").get().getActual()).isEqualTo(1);
  }

  @Test
  void handle_should_remove_5_xp_points_for_a_one_talent_upgrade() {
    // Given
    var player = aFreshPlayer();
    player.updateExperience(5);
    playerRepository.save(player);

    var command = new UpgradePlayerCommand(player.getId(), emptyList(), List.of(unTalentArcherieId));
    var event = eventFactory.build(player.getGameId(), player.getId(), EventType.UPGRADE_PLAYER, command);

    // When
    upgradePlayerHandler.handle(event);

    // Then
    var ugradedPlayer = playerRepository.get(player.getId());
    assertThat(ugradedPlayer.getExperience()).isEqualTo(0);
    assertThat(ugradedPlayer.getTalents()).contains(unTalentArcherieId);
  }

  @Test
  void handle_should_remove_10_xp_points_for_a_two_talent_upgrade() {
    // Given
    var player = aFreshPlayer();
    player.updateExperience(10);
    playerRepository.save(player);

    var command = new UpgradePlayerCommand(player.getId(), emptyList(), List.of(unTalentArcherieId, unTalentTrappeurId));
    var event = eventFactory.build(player.getGameId(), player.getId(), EventType.UPGRADE_PLAYER, command);

    // When
    upgradePlayerHandler.handle(event);

    // Then
    var ugradedPlayer = playerRepository.get(player.getId());
    assertThat(ugradedPlayer.getExperience()).isEqualTo(0);
    assertThat(ugradedPlayer.getTalents()).contains(unTalentArcherieId);
    assertThat(ugradedPlayer.getTalents()).contains(unTalentTrappeurId);
  }

  @Test
  void handle_should_not_upgrade_player_if_he_has_not_enough_experience() {
    // Given
    var player = aFreshPlayer();
    player.updateExperience(6);
    playerRepository.save(player);

    var command = new UpgradePlayerCommand(player.getId(), emptyList(), List.of(unTalentArcherieId, unTalentTrappeurId));
    var event = eventFactory.build(player.getGameId(), player.getId(), EventType.UPGRADE_PLAYER, command);

    // When
    upgradePlayerHandler.handle(event);

    // Then
    var notUpgradedPlayer = playerRepository.get(player.getId());
    assertThat(notUpgradedPlayer.getExperience()).isEqualTo(6);
    assertThat(notUpgradedPlayer.getTalents()).doesNotContain(unTalentArcherieId);
    assertThat(notUpgradedPlayer.getTalents()).doesNotContain(unTalentTrappeurId);
  }

  @Test
  void handle_should_send_and_upgrade_notification() {
    // Given
    var player = aFreshPlayer();
    player.updateExperience(6);
    playerRepository.save(player);

    var command = new UpgradePlayerCommand(player.getId(), List.of("force"), emptyList());
    var event = eventFactory.build(player.getGameId(), player.getId(), EventType.UPGRADE_PLAYER, command);

    // When
    upgradePlayerHandler.handle(event);

    // Then
    var argument = ArgumentCaptor.forClass(Event.class);
    verify(eventBus).dispatch(argument.capture());
    assertThat(argument.getValue().getType()).isEqualTo(EventType.PLAYER_UPGRADED);
  }
}
