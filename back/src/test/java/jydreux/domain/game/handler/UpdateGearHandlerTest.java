package jydreux.domain.game.handler;

import jydreux.domain.game.command.gear.AddGearCommand;
import jydreux.domain.event.Event;
import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.command.gear.UpdateGearCommand;
import jydreux.infra.PlayerInMemoryRepository;
import jydreux.service.DateService;
import jydreux.service.IdService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.time.LocalDateTime;
import java.time.ZoneId;

import static fixtures.domain.PlayerFixture.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class UpdateGearHandlerTest {
  private static PlayerInMemoryRepository playerRepository;
  private static UpdateGearHandler updateGearHandler;
  private static EventBus eventBus;
  private static EventFactory eventFactory;
  private Event<UpdateGearCommand> event;

  @BeforeEach
  void setUp() {
    playerRepository = new PlayerInMemoryRepository();
    eventBus = mock(EventBus.class);
    var idService = mock(IdService.class);
    var dateService = mock(DateService.class);
    eventFactory = new EventFactory(idService, dateService);
    var addGear = new AddGearCommand(playerRepository, eventFactory, eventBus);
    updateGearHandler = new UpdateGearHandler(playerRepository, eventFactory, eventBus);

    // Given
    var aDate = LocalDateTime.of(2020, 3, 31, 13, 46, 5).atZone(ZoneId.of("UTC"));
    given(dateService.now()).willReturn(aDate);
    given(idService.generate()).willReturn("un_id");
    playerRepository.save(aFreshPlayer());
    addGear.handle(unParchemin(), aFreshPlayer().getId());

    // Given
    var gear = unParcheminAvecMalus();
    var aFreshPlayer = aFreshPlayer();
    var updateGearCommand = new UpdateGearCommand(
      gear.getId(),
      gear.getMalus(),
      aFreshPlayer.getId(),
      aFreshPlayer.getGameId()
    );

    event = eventFactory.build(aFreshPlayer.getGameId(), aFreshPlayer.getId(), EventType.UPDATE_GEAR, updateGearCommand);
  }

  @Test
  void handle_should_change_a_player_gear() {
    // When
    updateGearHandler.handle(event);

    // Then
    var player = playerRepository.get(aFreshPlayer().getId());
    assertThat(player.getGears().get(0)).isEqualToComparingFieldByFieldRecursively(unParcheminAvecMalus());
  }

  @Test
  void handle_should_emit_a_notification() {
    // When
    updateGearHandler.handle(event);

    // Then
    playerRepository.get(aFreshPlayer().getId());

    var argument = ArgumentCaptor.forClass(Event.class);
    verify(eventBus, times(2)).dispatch(argument.capture());
    assertThat(argument.getAllValues().get(1).getType()).isEqualTo(EventType.GEAR_UPDATED);
  }
}
