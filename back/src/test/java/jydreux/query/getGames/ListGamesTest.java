package jydreux.query.getGames;

import jydreux.domain.game.GameRepository;
import jydreux.domain.game.Game;
import jydreux.domain.user.User;
import jydreux.domain.user.UserRepository;
import jydreux.infra.GameRepositoryInMemory;
import jydreux.query.ListGames;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static fixtures.domain.GameFixtures.unAutreGame;
import static fixtures.domain.GameFixtures.unGame;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

class ListGamesTest {
  private GameRepository gameRepository;
  private UserRepository userRepository;
  private ListGames listGames;

  @BeforeEach
  void setUp() {
    gameRepository = new GameRepositoryInMemory();
    userRepository = mock(UserRepository.class);

    listGames = new ListGames(gameRepository, userRepository);
    gameRepository.save(unGame());
    gameRepository.save(unAutreGame());

    var user = new User("pozo@gmail.com");
    given(userRepository.getByToken("un_token")).willReturn(user);
  }

  @Test
  void handle() {
    // When
    List<Game> games = listGames.handle("un_token");

    // Then
    assertThat(games).hasSize(2);
    assertThat(games.get(0)).isEqualToComparingFieldByFieldRecursively(unGame());
    assertThat(games.get(1)).isEqualToComparingFieldByFieldRecursively(unAutreGame());
  }
}
