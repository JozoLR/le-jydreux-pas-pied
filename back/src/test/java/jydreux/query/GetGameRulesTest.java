package jydreux.query;

import org.junit.jupiter.api.Test;

import jydreux.infra.RulesRepositoryInMemory;
import jydreux.query.model.GameRulesQueryModel;

import static org.assertj.core.api.Assertions.assertThat;

class GetGameRulesTest {

    @Test
    void handle_should_return_the_list_of_skills() {
        // Given
        var gameRepository = new RulesRepositoryInMemory();
        var getGameRules = new GetGameRules(gameRepository);

        // When
        var gameskills = getGameRules.handle();

        // Then
        var gameRulesQueryModel = new GameRulesQueryModel(gameRepository.getGameRules());
        assertThat(gameskills).isEqualToComparingFieldByFieldRecursively(gameRulesQueryModel);
    }
}
