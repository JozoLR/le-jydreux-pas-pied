package jydreux.query;

import jydreux.domain.event.EventRepository;
import jydreux.domain.roll.RollRepository;
import jydreux.infra.EventSqlRepository;
import jydreux.infra.PlayerSqlRepository;
import jydreux.infra.RollSqlRepository;
import jydreux.infra.RulesRepositoryInMemory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.TestDb;

import java.sql.SQLException;

import static fixtures.domain.PlayerFixture.aCompletePlayer;
import static fixtures.domain.RollDiceFixture.aNotificationRollsOnMultipleCategory;
import static fixtures.query.PlayerFixtures.aPlayerQM;
import static org.assertj.core.api.Assertions.assertThat;


class GetPlayerTest {
  private static TestDb testDb;
  private static PlayerSqlRepository playerSqlRepository;
  private static EventRepository notificationRepository;
  private static RollRepository rollRepository;
  private static GetPlayer getPlayer;

  @BeforeAll
  static void setUp() throws SQLException {
    testDb = TestDb.init();
    playerSqlRepository = new PlayerSqlRepository(testDb);
    notificationRepository = new EventSqlRepository(testDb);
    rollRepository = new RollSqlRepository(testDb);
    var gameRepository = new RulesRepositoryInMemory();
    getPlayer = new GetPlayer(playerSqlRepository, rollRepository, gameRepository);
  }

  @BeforeEach
  void beforeEach() throws SQLException {
    testDb.reset();
  }

  @Test
  void handle() {
    // Given
    playerSqlRepository.save(aCompletePlayer());
    notificationRepository.save(aNotificationRollsOnMultipleCategory());

    //When
    var player = getPlayer.handle(aCompletePlayer().getId());

    // Then
    assertThat(player).isEqualToComparingFieldByFieldRecursively(aPlayerQM);
  }
}
