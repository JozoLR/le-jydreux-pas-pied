package jydreux.query;

import jydreux.domain.game.command.player.CreatePlayerCommand;
import jydreux.domain.game.GameRepository;
import jydreux.infra.GameSqlRepository;
import jydreux.infra.RulesRepositoryInMemory;
import jydreux.infra.PlayerSqlRepository;
import jydreux.infra.RollSqlRepository;
import jydreux.query.model.GameQueryModel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.TestDb;

import java.sql.SQLException;

import static fixtures.domain.GameFixtures.unGame;
import static fixtures.domain.PlayerToCreateFixture.*;
import static org.assertj.core.api.Assertions.assertThat;


class GetGameTest {
  private static TestDb testDb;
  private static GetGame getGame;
  private static CreatePlayerCommand createPlayer;
  private GameQueryModel gameQueryModel;
  private static GameRepository gameRepository;

  @BeforeAll
  static void setUp() throws SQLException {
    testDb = TestDb.init();
    var playerSqlRepository = new PlayerSqlRepository(testDb);
    var rollRepository = new RollSqlRepository(testDb);
    var rulesRepositoryInMemory = new RulesRepositoryInMemory();
    gameRepository = new GameSqlRepository(testDb, playerSqlRepository);
    createPlayer = new CreatePlayerCommand(playerSqlRepository, rulesRepositoryInMemory);
    getGame = new GetGame(gameRepository, rollRepository, rulesRepositoryInMemory);
  }

  @BeforeEach
  void beforeEach() throws SQLException {
    testDb.reset();

    gameRepository.save(unGame());

    createPlayer.handle(aPlayerToCreateChasseur);
    createPlayer.handle(aPlayerToCreateRafistoleur);
    createPlayer.handle(aPlayerToCreateInAnotherGame);

    //When
    gameQueryModel = getGame.handle(playerGameId);
  }

  @Test
  void handle_should_return_players_for_a_game() {
    // Then
    assertThat(gameQueryModel.getPlayers()).hasSize(2);
  }

  @Test
  void handle_should_return_the_game_master() {
    // Then
    assertThat(gameQueryModel.getGameMaster()).isEqualTo(unGame().getGameMaster());
  }
}
