package jydreux.query;

import jydreux.domain.game.command.gear.AddGearCommand;
import jydreux.domain.game.command.weapon.AddWeaponCommand;
import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.rollsPropositions.Bonus;
import jydreux.domain.rollsPropositions.Proposition;
import jydreux.domain.rollsPropositions.RollProposition;
import jydreux.domain.rollsPropositions.query.GetRollsPropositions;
import jydreux.domain.rules.RulesRepository;
import jydreux.infra.PlayerSqlRepository;
import jydreux.infra.RulesRepositoryInMemory;
import jydreux.service.DateService;
import jydreux.service.IdService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.TestDb;

import java.sql.SQLException;
import java.util.Optional;

import static fixtures.domain.EventFixtures.unId;
import static fixtures.domain.EventFixtures.uneDate;
import static fixtures.domain.PlayerFixture.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class GetRollsPropositionsTest {
  private static TestDb testDb;
  private static PlayerSqlRepository playerRepository;
  private static AddWeaponCommand addWeapon;
  private static AddGearCommand addGear;
  private static GetRollsPropositions getRollsPropositions;

  @BeforeAll
  static void setUp() throws SQLException {
    testDb = TestDb.init();

    RulesRepository rulesRepository = new RulesRepositoryInMemory();
    playerRepository = new PlayerSqlRepository(testDb);
    var idService = mock(IdService.class);
    var dateService = mock(DateService.class);
    var eventFactory = new EventFactory(idService, dateService);
    addWeapon = new AddWeaponCommand(playerRepository, eventFactory, mock(EventBus.class));
    addGear = new AddGearCommand(playerRepository, eventFactory, mock(EventBus.class)
    );
    getRollsPropositions = new GetRollsPropositions(playerRepository, rulesRepository);

    given(idService.generate()).willReturn(unId());
    given(dateService.now()).willReturn(uneDate);
  }

  @BeforeEach
  void beforeEach() throws SQLException {
    testDb.reset();
    playerRepository.save(aNakedPlayer());
  }

  @Test
  void handle_should_return_a_skill_proposition_to_3_for_force() {
    // When
    var propositions = getRollsPropositions.handle(aCompletePlayer().getId());

    // Then
    var proposition = getSkillProposition(propositions, "force");
    assertThat(proposition.isPresent()).isTrue();
    assertThat(proposition.get().getSkillDice()).isEqualTo(3);
  }

  @Test
  void handle_should_return_a_attribut_proposition_to_5_for_force() {
    // When
    var propositions = getRollsPropositions.handle(aCompletePlayer().getId());

    // Then
    var proposition = getSkillProposition(propositions, "force");
    assertThat(proposition.isPresent()).isTrue();
    assertThat(proposition.get().getAttributDice()).isEqualTo(5);
  }

  @Test
  void handle_should_return_a_5_attribut_and_2_skill_endurance() {
    // When
    var propositions = getRollsPropositions.handle(aCompletePlayer().getId());

    // Thens
    var proposition = getSkillProposition(propositions, "endurance");
    assertThat(proposition.isPresent()).isTrue();
    assertThat(proposition.get().getSkillDice()).isEqualTo(2);
    assertThat(proposition.get().getAttributDice()).isEqualTo(5);
  }

  @Test
  void handle_should_return_2_bonus_for_massue() {
    // Given
    addWeapon.handle(uneMassue(), aNakedPlayer().getId());

    // When
    var propositions = getRollsPropositions.handle(aNakedPlayer().getId());

    // Then
    var proposition = getStuffBonus(propositions, uneMassue().getId(), "force");
    assertThat(proposition.isPresent()).isTrue();
    assertThat(proposition.get().getValue()).isEqualTo(2);
  }

  @Test
  void handle_should_return_1_bonus_for_massue_with_malus() {
    // Given
    addWeapon.handle(uneMassueAvecMalus(), aNakedPlayer().getId());

    // When
    var propositions = getRollsPropositions.handle(aNakedPlayer().getId());

    // Then
    var proposition = getStuffBonus(propositions, uneMassue().getId(), "force");
    assertThat(proposition.isPresent()).isTrue();
    assertThat(proposition.get().getValue()).isEqualTo(1);
  }

  @Test
  void handle_should_return_5_attribut_2_skill_and_1_stuff_for_a_parchemin_with_malus() {
    // Given
    addGear.handle(unParcheminAvecMalus(), aNakedPlayer().getId());

    // When
    var propositions = getRollsPropositions.handle(aNakedPlayer().getId());

    // Then
    var proposition = getStuffBonus(propositions, unParcheminAvecMalus().getId(), "endurance");
    assertThat(proposition.isPresent()).isTrue();
    assertThat(proposition.get().getValue()).isEqualTo(1);
  }

  @Test
  void handle_should_return_0_attribut_0_skill_and_2_bonus_for_a_bouclier_with_malus() {
    // Given
    addGear.handle(unBouclierAvecMalus(), aNakedPlayer().getId());

    // When
    var propositions = getRollsPropositions.handle(aNakedPlayer().getId());

    // Then
    var proposition = getSkillProposition(propositions, "armor").get();
    assertThat(proposition.getAttributDice()).isEqualTo(0);
    assertThat(proposition.getSkillDice()).isEqualTo(0);
    var bonus = getStuffBonus(propositions, unBouclierAvecMalus().getId(), "armor").get();
    assertThat(bonus).isNotNull();
    assertThat(bonus.getValue()).isEqualTo(1);
  }

  @Test
  void handle_should_only_return_propositions_for_player_skills() {
    // When
    var propositions = getRollsPropositions.handle(aNakedPlayer().getId());

    // Then
    var specialSkillProposition = getSkillProposition(propositions, chasse().getId());
    assertThat(specialSkillProposition).isNotNull();
    var otherSpecialSkillsProposition = getSkillProposition(propositions, "dog");
    assertThat(otherSpecialSkillsProposition.isEmpty()).isTrue();
  }

  private Optional<Proposition> getSkillProposition(RollProposition propositions, String skillId) {
    return propositions.getSkillPropositions().stream()
      .filter(skillPropositions -> skillPropositions.getSkillId().equals(skillId))
      .findFirst();
  }

  private Optional<Bonus> getStuffBonus(RollProposition propositions, String stuffId, String skillId) {
    var skillProposition = propositions.getSkillPropositions().stream()
      .filter(skillPropositions -> skillPropositions.getSkillId().equals(skillId))
      .findFirst();
    return skillProposition.get().getBonuses().stream().filter(bonus -> bonus.getId().equals(stuffId)).findAny();
  }
}
