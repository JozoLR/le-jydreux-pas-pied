package jydreux.query;

import jydreux.domain.notification.query.NotificationQueryModelMapper;
import jydreux.domain.notification.query.GetNotifications;
import jydreux.infra.EventSqlRepository;
import jydreux.infra.GameSqlRepository;
import jydreux.infra.PlayerSqlRepository;
import jydreux.infra.RulesRepositoryInMemory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.TestDb;

import java.sql.SQLException;

import static fixtures.domain.EventFixtures.*;
import static fixtures.domain.PlayerFixture.*;
import static org.assertj.core.api.Assertions.assertThat;

class GetNotificationsTest {

  private TestDb testDb;
  private GetNotifications getNotifications;
  private EventSqlRepository eventSqlRepository;
  private PlayerSqlRepository playerSqlRepository;

  @BeforeEach
  void setUp() throws SQLException {
    testDb = TestDb.init();
    playerSqlRepository = new PlayerSqlRepository(testDb);
    eventSqlRepository = new EventSqlRepository(testDb);
    var gameRepository = new GameSqlRepository(testDb, playerSqlRepository);
    var rulesRepository = new RulesRepositoryInMemory();
    var notificationQueryModelMapper = new NotificationQueryModelMapper(rulesRepository, gameRepository);
    getNotifications = new GetNotifications(eventSqlRepository, playerSqlRepository, notificationQueryModelMapper);
  }

  @BeforeEach
  void beforeEach() throws SQLException {
    testDb.reset();
  }

  @Test
  void handle_should_get_all_notification_for_a_specific_game() {
    // Given
    playerSqlRepository.save(aFreshPlayer());
    playerSqlRepository.save(aPlayerInAnotherGame());

    eventSqlRepository.save(unEvent());
    eventSqlRepository.save(unEventSurUnAutreGame());

    // When
    var notifications = getNotifications.handle(aFreshPlayer().getGameId());

    // Then
    assertThat(notifications).hasSize(1);
    assertThat(notifications.get(0)).isEqualToComparingFieldByFieldRecursively(uneNotification());
  }

  @Test
  void handle_should_add_roll_info_labels() {
    // Given
    playerSqlRepository.save(aCompletePlayer());

    eventSqlRepository.save(uneCompleteRollEvent());

    // When
    var notifications = getNotifications.handle(aCompletePlayer().getGameId());

    // Then
    assertThat(notifications).hasSize(1);
    assertThat(notifications.get(0)).isEqualToComparingFieldByFieldRecursively(uneCompleteRollNotificationQueryModel());
  }

  @Test
  void handle_should_get_basic_skill_roll() {
    // Given
    playerSqlRepository.save(aCompletePlayer());

    eventSqlRepository.save(uneSkillRollEvent());

    // When
    var notifications = getNotifications.handle(aCompletePlayer().getGameId());

    // Then
    assertThat(notifications).hasSize(1);
    assertThat(notifications.get(0)).isEqualToComparingFieldByFieldRecursively(uneSkillRollNotificationQueryModel());
  }

  @Test
  void handle_should_get_only_notifiable_events() {
    // Given
    playerSqlRepository.save(aCompletePlayer());

    eventSqlRepository.save(unEventNonNotifiable());

    // When
    var notifications = getNotifications.handle(aCompletePlayer().getGameId());

    // Then
    assertThat(notifications).hasSize(0);
  }
}
