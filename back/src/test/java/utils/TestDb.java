package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import jydreux.service.Database;

public class TestDb implements Database {
  private final Connection connection;

  public TestDb(Connection connection) {
    this.connection = connection;
  }

  public Connection getConnection() {
    return this.connection;
  }

  public static TestDb init() throws SQLException {
    String dbUser = getEnvOrDefault("POSTGRES_USER", "test");
    String dbPassword = getEnvOrDefault("POSTGRES_PASSWORD", "test");
    String dbUrl = getEnvOrDefault("CI_DATABASE_URL", "jdbc:postgresql://localhost:54432/test");
    return new TestDb(DriverManager.getConnection(dbUrl, dbUser, dbPassword));
  }

  public void reset() throws SQLException {
    Statement statement = connection.createStatement();
    statement.executeUpdate("truncate player");
    statement.executeUpdate("truncate attribut");
    statement.executeUpdate("truncate skill");
    statement.executeUpdate("truncate weapon");
    statement.executeUpdate("truncate gear");
    statement.executeUpdate("truncate resource");
    statement.executeUpdate("truncate game");
    statement.executeUpdate("truncate event");
    statement.executeUpdate("truncate map");
    statement.close();
  }

  private static String getEnvOrDefault(String envVariableName, String defaultValue) {
    var envVariale = System.getenv(envVariableName);
    if (envVariale == null) {
      envVariale = defaultValue;
    }
    return envVariale;
  }
}
