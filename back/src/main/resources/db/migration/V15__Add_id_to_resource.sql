truncate TABLE player;
truncate TABLE roll;
truncate TABLE weapon;
truncate TABLE skill;
truncate TABLE attribut;
truncate TABLE gear;
truncate TABLE resource;

ALTER TABLE resource
alter COLUMN id type varchar(100);
