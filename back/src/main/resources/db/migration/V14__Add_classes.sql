truncate TABLE player;
truncate TABLE roll;
truncate TABLE weapon;
truncate TABLE skill;
truncate TABLE attribut;
truncate TABLE gear;
truncate TABLE resource;

ALTER TABLE player
add COLUMN classId varchar(100) not null;
