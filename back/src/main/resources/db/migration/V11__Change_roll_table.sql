DROP table ROLL;

create table ROLL (
    id uuid not null,
    player_id uuid not null,
    date timestamp not null,
    category varchar(100) not null,
    score integer not null
);
