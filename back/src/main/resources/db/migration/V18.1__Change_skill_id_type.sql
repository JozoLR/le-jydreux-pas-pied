ALTER TABLE skill DROP CONSTRAINT skill_pkey;
ALTER TABLE attribut DROP CONSTRAINT attribut_pkey;

ALTER TABLE skill alter COLUMN id type varchar(100);
ALTER TABLE attribut alter COLUMN id type varchar(100);
