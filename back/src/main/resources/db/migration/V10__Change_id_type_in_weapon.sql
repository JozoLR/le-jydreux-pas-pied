DROP TABLE weapon;

create table weapon (
    id uuid PRIMARY KEY not null,
    player_id uuid not null,
    label varchar(100) not null,
    skill varchar(100) not null,
    bonus integer not null,
    damage integer not null,
    malus integer not null,
    scope varchar(100) not null
);