TRUNCATE table roll;

create type roll_t as enum('ROLL', 'REROLL');

ALTER TABLE roll DROP column category;
ALTER TABLE roll DROP column score;

ALTER TABLE roll ADD COLUMN attribut integer[];
ALTER TABLE roll ADD COLUMN skill integer[];
ALTER TABLE roll ADD COLUMN stuff integer[];
ALTER TABLE roll ADD COLUMN malus integer[];
ALTER TABLE roll ADD COLUMN type roll_t;
