create type skill_t as enum('BASE', 'CLASS', 'ARMOR');

ALTER TABLE skill ADD COLUMN skill_type skill_t;
