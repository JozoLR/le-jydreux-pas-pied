alter type notification_t rename to notification_t_old;

create type notification_t as enum (
    'PLAYER_UPDATED',
    'GEAR_UPDATED',
    'GEAR_ADDED',
    'ATTRIBUT_UPDATED',
    'SKILL_UPDATED',
    'WEAPON_ADDED',
    'WEAPON_UPDATED',
    'RESOURCE_UPDATED',
    'PLAYER_ROLLED',
    'WEAPON_GIVEN',
    'GEAR_GIVEN',
    'PLAYER_UPGRADED'
    );

alter table notification rename column type to type_old;

alter table notification add type notification_t not null default 'PLAYER_UPDATED';

update notification set type = type_old::text::notification_t;

alter table notification drop column type_old;
drop type notification_t_old;
