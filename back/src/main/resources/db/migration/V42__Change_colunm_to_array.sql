ALTER TABLE gear RENAME COLUMN skill TO skills;

alter table gear alter column skills type varchar(255)[] using array[skills];
