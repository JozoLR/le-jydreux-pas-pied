ALTER TABLE player RENAME COLUMN talent_id TO talents;
ALTER TABLE player RENAME COLUMN classid TO class_id;

alter table player alter column talents type varchar(255)[] using array[talents];
