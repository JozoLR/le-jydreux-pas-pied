ALTER TABLE player ADD COLUMN experience integer default 0;
ALTER TABLE player ADD COLUMN rot integer default 0;
ALTER TABLE player ADD COLUMN mutation integer default 0;
