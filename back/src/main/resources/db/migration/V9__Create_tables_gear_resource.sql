create table gear (
    id SERIAL PRIMARY KEY,
    player_id uuid not null,
    label varchar(100) not null,
    skill varchar(100) not null,
    bonus integer not null
);

create table resource (
    id SERIAL PRIMARY KEY,
    player_id uuid not null,
    label varchar(100) not null,
    quantity integer not null
);
