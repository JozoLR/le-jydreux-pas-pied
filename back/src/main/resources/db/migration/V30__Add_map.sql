create table map
(
    id      uuid PRIMARY KEY,
    game_id uuid  not null,
    content jsonb not null
);
