create table GAME
(
    id                uuid         not null,
    name              varchar(100) not null,
    game_master_email varchar(100) not null,
    players_emails    varchar(100)[]
);

TRUNCATE TABLE skill;
TRUNCATE TABLE player;
TRUNCATE TABLE attribut;
TRUNCATE TABLE roll;
TRUNCATE TABLE gear;
TRUNCATE TABLE weapon;
TRUNCATE TABLE resource;
alter table player
    add column game_id uuid;
