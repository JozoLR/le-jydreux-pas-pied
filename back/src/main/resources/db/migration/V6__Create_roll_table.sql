create table ROLL (
    id SERIAL PRIMARY KEY,
    player_id uuid not null,
    date timestamp not null,
    roll jsonb not null
);
