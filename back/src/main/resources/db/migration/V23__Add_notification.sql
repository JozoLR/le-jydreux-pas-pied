create type notification_t as enum (
    'PLAYER_UPDATED',
    'GEAR_UPDATED',
    'GEAR_ADDED',
    'ATTRIBUT_UPDATED',
    'SKILL_UPDATED',
    'WEAPON_ADDED',
    'WEAPON_UPDATED',
    'RESOURCE_UPDATED',
    'PLAYER_ROLLED');

create table notification
(
    id        uuid           not null,
    date      timestamptz    not null,
    player_id uuid           not null,
    game_id   uuid           not null,
    type      notification_t not null,
    content   jsonb
);
