create table attribut (
    id SERIAL PRIMARY KEY,
    player_id uuid not null,
    label varchar(100) not null,
    base integer not null,
    actual integer not null
);

create table skill (
    id SERIAL PRIMARY KEY,
    player_id uuid not null,
    label varchar(100) not null,
    base integer not null,
    actual integer not null
);

create table weapon (
    id SERIAL PRIMARY KEY,
    player_id uuid not null,
    label varchar(100) not null,
    skill varchar(100) not null,
    bonus integer not null,
    damage integer not null,
    malus integer not null,
    scope varchar(100) not null
);

ALTER TABLE player DROP attributs;
ALTER TABLE player DROP competences;
ALTER TABLE player DROP weapons;