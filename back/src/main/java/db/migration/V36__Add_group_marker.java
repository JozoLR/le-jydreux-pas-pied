package db.migration;

import com.google.gson.Gson;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;

import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

public class V36__Add_group_marker extends BaseJavaMigration {
  public void migrate(Context context) throws Exception {
    try (Statement select = context.getConnection().createStatement()) {
      try (ResultSet rows = select.executeQuery("SELECT distinct id FROM map")) {
        var gson = new Gson();
        while (rows.next()) {
          var gameId = rows.getString(1);
          var updateMap = "update map set content = ? where id = ?;";
          try (var statement = context.getConnection().prepareStatement(updateMap)) {
            var map = Map.generate(gameId);
            statement.setObject(1, gson.toJson(map), Types.OTHER);
            statement.setObject(2, gameId, Types.OTHER);
            statement.executeUpdate();
          }
        }
      }
    }
  }

  enum MarkerType {
    GROUP,
    ARK
  }

  static class Map {
    String gameId;
    List<Marker> markers;
    List<Tile> tiles;

    public Map(String gameId, List<Marker> markers, List<Tile> tiles) {
      this.gameId = gameId;
      this.markers = markers;
      this.tiles = tiles;
    }


    static Map generate(String gameId) {
      var arkPosition = new Position(50.0, 50.0);
      var arkMarker = new Marker("Ark", 215, arkPosition, MarkerType.ARK);
      var groupPosition = new Position(51.0, 51.0);
      var groupMarker = new Marker("Group", 215, groupPosition, MarkerType.GROUP);
      return new Map(gameId, List.of(arkMarker, groupMarker), TilesGenerator.get());
    }
  }

  static class Marker {
    String label;
    Integer tileIndex;
    Position position;
    MarkerType type;

    public Marker(String label, Integer tileIndex, Position position, MarkerType type) {
      this.label = label;
      this.tileIndex = tileIndex;
      this.position = position;
      this.type = type;
    }
  }

  static class Position {
    Double x;
    Double y;

    public Position(Double x, Double y) {
      this.x = x;
      this.y = y;
    }
  }

  static class Tile {
    Integer index;
    Boolean visited;
    String note;

    public Tile(Integer index, Boolean visited, String note) {
      this.index = index;
      this.visited = visited;
      this.note = note;
    }

    static Tile generate(Integer index) {
      return new Tile(index, false, "");
    }
  }

  static class TilesGenerator {
    static List<Tile> get() {
      var tiles = new ArrayList<Tile>();
      for (var index = 1; index <= 30 * 21; index++) {
        tiles.add(Tile.generate(index));
      }
      return tiles;
    }
  }
}
