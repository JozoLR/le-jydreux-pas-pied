package db.migration;

import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;

import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;

public class V25__Add_resources extends BaseJavaMigration {
  public void migrate(Context context) throws Exception {
    try (Statement select = context.getConnection().createStatement()) {
      try (ResultSet rows = select.executeQuery("SELECT distinct id FROM player")) {
        while (rows.next()) {
          var playerId = rows.getString(1);
          String insertResource = "insert into resource (id, player_id, label, quantity) values (?,?,?,?)";
          try (var statement = context.getConnection().prepareStatement(insertResource)) {
            statement.setString(1, "rot_water");
            statement.setObject(2, playerId, Types.OTHER);
            statement.setString(3, "Eau souillée");
            statement.setInt(4, 0);
            statement.execute();
          }
          try (var statement = context.getConnection().prepareStatement(insertResource)) {
            statement.setString(1, "rot_food");
            statement.setObject(2, playerId, Types.OTHER);
            statement.setString(3, "Nourriture souillée");
            statement.setInt(4, 0);
            statement.execute();
          }
        }
      }
    }
  }
}
