package jydreux;

import org.flywaydb.core.Flyway;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

  public static void main(String[] args) {
    migrateDatabase();
    SpringApplication.run(Application.class, args);
  }

  private static void migrateDatabase() {
    var dbUrl = getEnvOrDefault("DATABASE_URL", "jdbc:postgresql://localhost:54777/jydreux");
    var dbUser = getEnvOrDefault("DATABASE_USERNAME", "jydreux");
    var dbPassword = getEnvOrDefault("DATABASE_PASSWORD", "jydreux");
    var flyway = Flyway.configure().dataSource(dbUrl, dbUser, dbPassword).load();
    flyway.baseline();
    flyway.migrate();
  }

  private static String getEnvOrDefault(String envVariableName, String defaultValue) {
    var envVariale = System.getenv(envVariableName);
    if (envVariale == null) {
      envVariale = defaultValue;
    }
    return envVariale;
  }
}
