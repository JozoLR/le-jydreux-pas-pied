package jydreux.infra;

import jydreux.domain.rules.*;
import org.springframework.stereotype.Repository;

import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static jydreux.domain.cross.SkillType.*;

@Repository
public class RulesRepositoryInMemory implements RulesRepository {

  public static final String skinnerDescription = "produit des pieces d'armure a partir de peau d'animaux. Faites un jet de chasseur, chaque 6 donne un pts d'armure (max 6 et max armure de l'animal).";
  private static final Attribut vigueur = new Attribut("vigueur", "Vigueur");
  private static final Attribut agilite = new Attribut("agilite", "Agilité");
  private static final Attribut intellect = new Attribut("intellect", "Intellect");
  private static final Attribut empathie = new Attribut("empathie", "Empathie");
  private static final Skill combat = new Skill("combat", "Combat", "vigueur", BASE);
  private static final Skill comprehension = new Skill("comprehension", "Compréhension", "intellect", BASE);
  private static final Skill connaissance_de_la_nature = new Skill("connaissance_de_la_nature", "Connaissance de la nature",
    "intellect", BASE);
  private static final Skill deplacement = new Skill("deplacement", "Déplacement", "agilite", BASE);
  private static final Skill domination = new Skill("domination", "Domination", "empathie", BASE);
  private static final Skill endurance = new Skill("endurance", "Endurance", "vigueur", BASE);
  private static final Skill force = new Skill("force", "Force", "vigueur", BASE);
  private static final Skill furtivite = new Skill("furtivite", "Furtivité", "agilite", BASE);
  private static final Skill psychologie = new Skill("psychologie", "Psychologie", "empathie", BASE);
  private static final Skill soins = new Skill("soins", "Soins", "empathie", BASE);
  private static final Skill tir = new Skill("tir", "Tir", "agilite", BASE);
  private static final Skill vigilance = new Skill("vigilance", "Vigilance", "intellect", BASE);
  private static final Skill chasse = new Skill("chasse", "Chasse", "agilite", CLASS);
  private static final Skill dog = new Skill("dog", "Lacher le chien", "agilite", CLASS);
  private static final Skill systemeD = new Skill("systeme_d", "Système D", "intellect", CLASS);
  private static final Skill trouverLeChemin = new Skill("trouver_chemin", "Trouver le chemin", "agilite", CLASS);
  private static final Skill armor = new Skill("armor", "Armure", null, ARMOR);
  private static final List<Attribut> attributs = List.of(vigueur, agilite, intellect, empathie);
  private static final List<Skill> skills = List.of(combat, comprehension, connaissance_de_la_nature, deplacement, domination,
    endurance, force, furtivite, psychologie, soins, tir, vigilance, chasse, dog, systemeD, armor, trouverLeChemin);
  private static final Resource water = new Resource("water", "Eau");
  private static final Resource food = new Resource("food", "Nourriture");
  private static final Resource ammo = new Resource("ammo", "Balles");
  private static final Resource arrow = new Resource("arrow", "Flèches");
  private static final Resource gas = new Resource("gas", "Essence");
  private static final Resource battery = new Resource("battery", "Batterie");
  private static final Resource rotWater = new Resource("rot_water", "Eau souillée");
  private static final Resource rotFood = new Resource("rot_food", "Nourriture souillée");
  private static final List<Resource> resources = List.of(water, food, ammo, arrow, gas, battery, rotWater, rotFood);
  private static final WeaponRange cac = new WeaponRange("cac", "Portée de main", -3);
  private static final WeaponRange proche = new WeaponRange("proche", "Proche", 0);
  private static final WeaponRange courte = new WeaponRange("courte", "Courte", -1);
  private static final WeaponRange longue = new WeaponRange("longue", "Longue", -2);
  private static final WeaponRange lointaine = new WeaponRange("lointaine", "Lointaine", null);
  private static final List<WeaponRange> weaponRanges = List.of(cac, proche, courte, longue, lointaine);
  private static final Dice attributDice = new Dice("attribut", 6);
  private static final Dice skillDice = new Dice("skill", 6);
  private static final Dice stuffDice = new Dice("stuff", 6);
  private static final Dice malusDice = new Dice("malus", 6);
  private static final List<Dice> dice = List.of(attributDice, skillDice, stuffDice, malusDice);
  private static final GameClass chasseur = new GameClass("chasseur", "Chasseur", "chasse");
  private static final GameClass maitreChien = new GameClass("maitre_chien", "Maître chien", "dog");
  private static final GameClass rafistoleur = new GameClass("rafistoleur", "Rafistoleur", "systeme_d");
  private static final GameClass zonard = new GameClass("zonard", "Zonard", "trouver_chemin");
  private static final List<GameClass> gameClasses = List.of(chasseur, maitreChien, rafistoleur, zonard);
  private static final SkillBonus systemDBonus = new SkillBonus("systeme_d", 2);
  private static final SkillBonus trouverLeCheminDBonus = new SkillBonus("trouver_chemin", 2);
  private static final Talent skinner = new Talent("skinner", "Skinner", "chasseur", skinnerDescription, emptyList());

  private static final String acherieDescription = "Vous pouvez construire de simple arme à distance comme des frondes et des arcs (p77). La fabrication d’arc nécessite l’accès à des matériaux brut comme du bois et du tissus, quelques heures de travail et un succès au jet pour Chasser.";
  private static final Talent archerie = new Talent("acherie", "Archerie", "chasseur", acherieDescription, emptyList());

  private static final String trappeurDescription = "Vous avez maitrisé l’art de poser des pièges et collets. Faites un jet de Chasse pour poser un piège et choisissez si le piège inflige un traumatisme ou emprisonne la victime. Noter le nombre de réussite (6) au jet de chasse. Poser un piège prend quelques minutes. Lorsque quelqu’un marche dans le piège, il doit réussir un jet de Déplacement pour l’éviter. Si la victime rate ce jet, il subit un nombre de dégât égale au nombre de réussite au jet de Chasse (pour un piège blessant), et est emprisonné et doit réussir un jet de Force pour sortir du piège avec un malus égale au nombre de réussite du jet de chasse (s’il s’agit d’un piège immobilisant).";
  private static final Talent trappeur = new Talent("trappeur", "Trappeur", "chasseur", trappeurDescription, emptyList());

  private static final String betameurDescription = "+2 au jet de reparation avec le système D (mais pas pour construire des objets).";
  private static final Talent betameur = new Talent("betameur", "Betameur", "rafistoleur", betameurDescription, List.of(systemDBonus));

  private static final String inventeurDescription = "+2 au jets de Système D pour créer un nouvel objet, mais pas quand vous réparez quelques choses";
  private static final Talent inventeur = new Talent("inventeur", "Inventeur", "rafistoleur", inventeurDescription, emptyList());

  private static final String metalleuxDescription = "+1 lorsque vous utilisez un véhicule pour percuter quelqu’un (Combat) ou fuir un conflit (Déplacement). Vous obtenez également un modificateur de +1 lorsque vous réparez ou modifiez un véhicule avec Système D";
  private static final Talent metalleux = new Talent("metalleux", "Métalleux", "rafistoleur", metalleuxDescription, emptyList());

  private static final String dogFighterDescription = "+2 pour les jets de combat du chien";
  private static final Talent dogFighter = new Talent("dog_fighter", "Chien de combat", "maitre_chien", dogFighterDescription, emptyList());

  private static final String limierDescription = "vous avez entrainé votre chien à être un redoutable pisteur. Vous obtenez un modificateur de +2 lorsque vouss utilisez votre chien pour suivre une piste";
  private static final Talent limier = new Talent("limier", "Limier", "maitrechien", limierDescription, emptyList());

  private static final String lemeilleuramidumutantDescription = "Votre chien peut vous sortir de tous les pétrins. Il peut vous aidez lors de vos jets d’Endurance et de Déplacement. Utilisez votre niveau de compétence Lachez le chien plutôt que celui de vos compétences, ainsi que les attributs de vigeur et d’agilité du chien. Vous pouvez pousser les jets normalement. Tous les traumatismes sont subis par votre chien à votre place, mais vous ne gagnez pas de point de mutation";
  private static final Talent lemeilleuramidumutant = new Talent("lemeilleuramidumutant", "Le meilleur ami du mutant", "maitrechien", lemeilleuramidumutantDescription, emptyList());

  private static final String admirateurDescription = "Un PNJ est très impressionné par vous, et fera tout son possible pour vous aider ou juste rester près de vous. Créer ce PNJ avec l’aide du MJ.";
  private static final Talent admirateur = new Talent("admirateur", "Admirateur", null, admirateurDescription, emptyList());

  private static final String archeologueDescription = "+2 pour essayer de comprendre un bâtiment en ruine ou une installation des Temps d’Avant";
  private static final Talent archeologue = new Talent("archeologue", "Archeologue", null, archeologueDescription, emptyList());

  private static final String assassinDescription = "+1 au dégat si vous tirez et touchez quelqu’un à portée Courte ou portée de main";
  private static final Talent assassin = new Talent("assassin", "Assassin", null, assassinDescription, emptyList());

  private static final String mauvaispresageDescription = "Vous pouvez Observer en utilisant l'Empathie/Instinct plutôt que l'Intellect. ";
  private static final Talent mauvaispresage = new Talent("mauvaispresage", "Mauvais Présage", null, mauvaispresageDescription, emptyList());

  private static final String gardeducorpsDescription = "Si quelqu’un tire et touche quelqu’un à portée de main ou proche de vous, vous pouvez plonger pour prendre la balle à sa place. Faites un test de Déplacement (ne compte pas comme une action). Si vous obtenez un ou plusieurs 6, vous prenez le tir à la place de votre ami. Vous pouvez pousser le jet.";
  private static final Talent gardeducorps = new Talent("gardeducorps", "Garde du corps", null, gardeducorpsDescription, emptyList());

  private static final String boucherDescription = "Lorsque vous tuez un monstre dans la zone, vous pouvez en dépecer un nombre de ration de bouffe égale à la Vigeur de base de la créature. Cela ne fonctionne pas sur les nuées, et les robots. La viande sera généralement Souillée. Combiner ce talent avec un Cuistot de la Zone est une bonne opportunité";
  private static final Talent boucher = new Talent("boucher", "Boucher", null, boucherDescription, emptyList());

  private static final String veteranDescription = "+2 aux jets d’initiative au début d’une confrontation. Vous pouvez sacrifier votre manœuvre une fois par tour, pour augmenter encore le score d’initiative de +2 pts";
  private static final Talent veteran = new Talent("veteran", "Vétéran", null, veteranDescription, emptyList());

  private static final String tetefroideDescription = "Vous pouvez vous Déplacer avec l'Intellect plutôt que l'Agilité";
  private static final Talent tetefroide = new Talent("tetefroide", "Tête froide ", null, tetefroideDescription, emptyList());

  private static final String conseillerDescription = "Lorsque vous aidez quelqu’un, il gagne un modificateur de +1 supplémentaire, mais seulement s’il fait quelque chose que vous lui avez dit de faire.";
  private static final Talent conseiller = new Talent("conseiller", "Conseiller", null, conseillerDescription, emptyList());

  private static final String degaineclaireDescription = "Vous pouvez dégainer votre arme si vite, que cela ne vous coûte pas de manœuvre";
  private static final Talent degaineclaire = new Talent("degaineeclaire", "Dégaine éclaire", null, degaineclaireDescription, emptyList());

  private static final String poidsplumeDescription = "Vous pouvez utilisez votre Agilité plutôt que votre Vigueur pour vos jets de Défense";
  private static final Talent poidsplume = new Talent("poidsplume", "Poids plume", null, poidsplumeDescription, emptyList());

  private static final String amateurdegadgetsDescription = "Vous obtenez un modificateur de +2 lorsque vous essayez de comprendre un artefact des Temps d’Avant";
  private static final Talent amateurdegadgets = new Talent("amateurdegadgets", "Amateur de gadgets", null, amateurdegadgetsDescription, emptyList());

  private static final String bonjeudejambesDescription = "+2 pour vos jets de Défense au corps à corps";
  private static final Talent bonjeudejambes = new Talent("bonjeudejambes", "Bon jeu de jambes", null, bonjeudejambesDescription, emptyList());

  private static final String cogneurDescription = "+2 au jet de Combat pour attaquer si vous ne vous déplacez pas ce tour-ci. Ce talent ne fonctionne pas pour les jets de défense.";
  private static final Talent cogneur = new Talent("cogneur", "Cogneur", null, cogneurDescription, emptyList());

  private static final String betedesommeDescription = "+2 lorsque vous travaillez sur un projet de l’Arche.";
  private static final Talent betedesomme = new Talent("betedesomme", "Bête de somme", null, betedesommeDescription, emptyList());

  private static final String toucheatoutDescription = "Vous adoptez un nouveau rôle et obtenez immédiatement un niveau de 1 dans la compétence de spécialiste de ce dernier. Vous gardez votre niveau actuel dans la compétence de spécialiste de votre ancien rôle, mais ne pouvez plus l’améliorer. Vous pouvez également conserver tous vos talents, mais ne pourrez pas en gagner de nouveaux liés à votre ancien rôle. Ce changement doit avoir une justification cohérente dans l’histoire. Par exemple, pour devenir un Caïd vous devez trouver la bande qui va vous suivre. Limitation : ce talent requiert plus d’expérience que les autres. Vous ne pouvez l’obtenir tant que vous n’avez pas d’abord appris trois autres talents.";
  private static final Talent toucheatout = new Talent("toucheatout", "Touche à tout", null, toucheatoutDescription, emptyList());

  private static final String appetitdoiseauDescription = "Vous n’avez besoin que d’une ration de bouffe par jour pour éviter de perdre de la vigueur.";
  private static final Talent appetitdoiseau = new Talent("appetitdoiseau", "Appétit d'oiseau", null, appetitdoiseauDescription, emptyList());

  private static final String solitaireDescription = "Vous n’avez pas besoin compagnie pour récupérer de l’Empathie, juste quelques heures passée seul.";
  private static final Talent solitaire = new Talent("solitaire", "Solitaire", null, solitaireDescription, emptyList());

  private static final String pasderedditionDescription = "Lorsque vous êtes brisé par les dégats (Vigeur à zéro), vous pouvez vous remettre sur pied immédiatement, sans personne pour prodiguer des Soins. Faites un test d’Endurance sans Dé de base (Votre vigeur est à 0), pour chaque 6, vous récupérer un point de Vigueur pour votre permettre de vous battre plus longtemps. Vous ne pouvez pas pousser ce jet. Vous êtes toujours affectés par la blessure critique.";
  private static final Talent pasdereddition = new Talent("pasdereddition", "Pas de reddition", null, pasderedditionDescription, emptyList());

  private static final String muleDescription = "Vous pouvez porter deux fois plus d’objet que la normale sans être encombré.";
  private static final Talent mule = new Talent("mule", "Mule", null, muleDescription, emptyList());

  private static final String calculateurDescription = "Vous pouvez utiliser votre Intellect pour les jets de Psychologie plutôt que l’Empathie/Instinct.";
  private static final Talent calculateur = new Talent("calculateur", "Calculateur", null, calculateurDescription, emptyList());

  private static final String tireurdeliteDescription = "lorsque vous tirez à portée Longue ou lontaine, vous obtenez un bonus de +1 au dégât.";
  private static final Talent tireurdelite = new Talent("tireurdelite", "Tireur d'élite", null, tireurdeliteDescription, emptyList());

  private static final String insomniaqueDescription = "Vous pouvez récupérer des points d’Intellect par d’autres moyens que le sommeil, soit en utilisant le Système D sur un objet, soit en Pénétrant et en Explorant un nouveau secteur de la Zone, soit en Comprenant un Artefact";
  private static final Talent insomniaque = new Talent("insomniaque", "Insomniaque", null, insomniaqueDescription, emptyList());

  private static final String stoiqueDescription = "Vous pouvez utiliser votre Intellect plutôt que votre Vigueur pour les jets d’Endurance";
  private static final Talent stoique = new Talent("stoique", "Stoïque", null, stoiqueDescription, emptyList());

  private static final String psyDescription = "Vous obtenez un modificateur de +2 lorsque vous Soignez quelqu’un qui est brisé par le Doute";
  private static final Talent psy = new Talent("psy", "Psy", null, psyDescription, emptyList());

  private static final String specialistedesarmesDescription = "Choisissez une arme dans la liste p86 ou un artefact. Vous gagnez un bonus de +2 lorsque vous utilisez cette arme. Vous pouvez choisir ce talent pour des armes différentes. Vous pouvez être un spécialiste des armes à Mains nues.";
  private static final Talent specialistedesarmes = new Talent("specialistedesarmes", "Spécialiste des armes", null, specialistedesarmesDescription, emptyList());

  private static final String cuistotdelazoneDescription = "Vous pouvez purifier l’eau et la bouffe contaminée : Faites un test de Connaissance de la Zone/Nature, pour chaque 6 obtenu vous purifiez 1d6 rations de bouffe ou d’eau.";
  private static final Talent cuistotdelazone = new Talent("cuistotdelazone", "Cuistot de la Zone", null, cuistotdelazoneDescription, emptyList());

  private static final String surinerDescription = "Vous pouvez utiliser votre compétence de Furtivité plutôt que Combat pour vos embuscades (au corps à corps).";
  private static final Talent suriner = new Talent("suriner", "Suriner", null, surinerDescription, emptyList());

  private static final String combatadeuxarmesDescription = "Vous pouvez vous battre avec deux armes à une main dans chaque main (2 pistolets, 1 pistolets/1couteau etc...). Seulement une arme à une main légère, telle qu’un pistolet ou un couteau peut être utilisez. Pour une seule action, vous pouvez attaquer avec chaque arme. Si vous attaquez le même ennemi vous avez un malus de -2 sur vos deux jets d’attaque, si vous attaquez deux ennemis différents, vous obtenez un malus de -3 sur vos jets d’attaque.";
  private static final Talent combatadeuxarmes = new Talent("combatadeuxarmes", "Combat à deux armes", null, combatadeuxarmesDescription, emptyList());

  private static final String evasifDescription = "Au combat, vos actions de Défense compte comme une manœuvre aux lieux d’une action. Ce talent peut être combiné avec Bon Jeux de Jambes (MAZ), Défenseur (Defensive, GEN),  mais pas Mur Impénétrable (Stonewall, GEN).";
  private static final Talent evasif = new Talent("evasif", "Evasif", null, evasifDescription, emptyList());

  private static final String soinsrapidesDescription = "Le temps de rétablissement pour les traumatismes que vous subissez est divisez par deux.";
  private static final Talent soinsrapides = new Talent("soinsrapides", "Soins rapides", null, soinsrapidesDescription, emptyList());

  private static final String sabreurDescription = "Avec ce talent, vous pouvez utiliser votre Agilité plutôt que votre Vigueur quand vous combattez avec un sabre.";
  private static final Talent sabreur = new Talent("sabreur", "Sabreur", null, sabreurDescription, emptyList());

  private static final String coeurdelamachineDescription = "(Humain seulement) Vous pouvez ignorer un 1 quand vous lisez la table de Fièvre de la Machine (p138).";
  private static final Talent coeurdelamachine = new Talent("coeurdelamachine", "Coeur de la Machine", null, coeurdelamachineDescription, emptyList());

  private static final String contremaitreDescription = "Lorsque vous travaillez sur un projet de l’Arche (MAZ), vous pouvez toujours utilisez votre compétence de manipulation à la place de la compétence spécifique au projet.";
  private static final Talent contremaitre = new Talent("contremaitre", "Contre-maître", null, contremaitreDescription, emptyList());

  private static final String tirrapideDescription = "Pendant un Tir de Vigilance, vous pouvez tirer 2x pour une seule action. Les deux tirs ont un modificateur de -2 au jet de tirs. Ce talent ne peut être utilisez qu’avec une arme qui n’a pas besoin d’être recharger entre chaque tir.";
  private static final Talent tirrapide = new Talent("tirrapide", "Tir rapide", null, tirrapideDescription, emptyList());

  private static final String reputeDescription = "(Elysium seulement) : Lorsque vous modifiez votre Réputation après chaque séssion, vous pouvez vous retenir de répondre à une question de la liste (p25).";
  private static final Talent repute = new Talent("repute", "Réputé", null, reputeDescription, emptyList());

  private static final String chasseurderobotDescription = "Vous pouvez choisir quel attribut du robot, ou quel module vous désirez endommager, ce qui veux dire que vous pouvez choisir votre résultat sur la table 88 (voir MECH), à la place de d’obtenir un résultat aléatoire.";
  private static final Talent chasseurderobot = new Talent("chasseurderobot", "Chasseur de Robot", null, chasseurderobotDescription, emptyList());

  private static final String resistantalasouillureDescription = "Vous obtenez un bonus naturel de 3 contre la souillure. Ce talent peut être combinez avec n’importe quel équipement de protection.";
  private static final Talent resistantalasouillure = new Talent("resistantalasouillure", "Résistant à la Souillure", null, resistantalasouillureDescription, emptyList());

  private static final String gloutonDescription = "Vous trouvez du réconfort en mangeant. Quand vous mangez (et vous reposez pour quelques heures) vous regagner un point de Vigueur et un point d’Instinct (ou d’Empathie pour les humains) par ration que vous consommez.";
  private static final Talent glouton = new Talent("glouton", "Glouton", null, gloutonDescription, emptyList());

  private static final String defenseurDescription = "Vous êtes rapides et pouvez facilement éviter les attaques en combat rapprocher. Quand vous vous défendez (p75) Votre jet de Combat est modifié de +2";
  private static final Talent defenseur = new Talent("defenseur", "Défenseur", null, defenseurDescription, emptyList());

  private static final String mecanicienDescription = "Vous pouvez utiliser la compétence de Compréhension pour réparer une pièce cassé d’un équipement. Chaque tentative prend plusieurs heures. Pour chaque réussite (6) obtenus, le bonus d’Équipement de l’objet est restauré de 1 points, jusqu’à la valeur d’origine. Vous pouvez faire une seule tentative par objet endommagé, jusqu’à ce qu’il soit endommagé de nouveau.";
  private static final Talent mecanicien = new Talent("mecanicien", "Mécanicien", null, mecanicienDescription, emptyList());

  private static final String murmureraloreilledesmonstresDescription = "Vous pouvez Dominer un animal sauvage ou un monstre en faisant des grognements et des gestes.  Ce talent n’a pas d’effet sur les insectes, les nués, les Watchers et les créatures mécaniques. Comme les monstres n’ont pas d’instinct vous ne pouvez pas infliger de doute comme un effet bonus.";
  private static final Talent murmureraloreilledesmonstres = new Talent("murmureraloreilledesmonstres", "Murmurer à l'oreille des monstres", null, murmureraloreilledesmonstresDescription, emptyList());

  private static final String naturisteDescription = "Porter des vêtements vous met dans l’inconfort et vous ralentie. Quand vous marcher complètement nu, et ne portez rien de supérieure à la moitie de votre capacité de transport, vous obtenez un bonus de +1 aux jets de Furtivité et Déplacements";
  private static final Talent naturiste = new Talent("naturiste", "Naturiste", null, naturisteDescription, emptyList());

  private static final String neufviesDescription = "Quelques soient les problèmes rencontrer dans la vie, vous avez toujours réussis à les résoudre de justesse. Quand vous subissez des coups critiques, vous pouvez échanger le dé des unités avec celui des dizaines (p80). Si votre assaillant a le talent « Massacreur », l’effet est annulé et la blessure critique est résolue normalement.";
  private static final Talent neufvies = new Talent("neufvies", "Neuf vies", null, neufviesDescription, emptyList());

  private static final String tirinstinctifDescription = "Vous maitriser votre arc ou votre fronde avec une vitesse hors du commun. Vous pouvez tirer avec cette arme sans avoir besoin d’utiliser une manœuvre pour vous préparer (p76).";
  private static final Talent tirinstinctif = new Talent("tirinstinctif", "Tir instinctif", null, tirinstinctifDescription, emptyList());

  private static final String massacreurDescription = "Vous savez où frapper pour faire tomber votre ennemie et qu’il ne s’en remette jamais.  Quand vous infligez une blessure critique à l’ennemie – si vous le désirez-vous pouvez inverser le dé des unités et des dizaines. Si votre victime a le talent Neuf Vies, les deux effets s’annulent et vous lancer le coup critique normalement.";
  private static final Talent massacreur = new Talent("massacreur", "Massacreur", null, massacreurDescription, emptyList());

  private static final String mastodonteDescription = "Vous êtes un combattant offensive qui comte juste sur la force et la puissance, quelques soient les dégâts que vous subissez. Vous utilisez toujours la totalité de votre score de Vigueur pour Combattre, même lorsque votre Vigueur est réduite par un traumatisme. Notez que cela augmente les chances de vous briser vous-mêmes lors d’un jet de Combat.";
  private static final Talent mastodonte = new Talent("mastodonte", "Mastodonte", null, mastodonteDescription, emptyList());

  private static final String tenaceDescription = "Vous refusez d’abandonner, quel qu’en soit le prix. Vous pouvez pousser un jet de dés 2x. Les réussites (6) et les échecs (1) ne peuvent être relancé comme pour le premier jet et ont les effets habituels.";
  private static final Talent tenace = new Talent("tenace", "Tenace", null, tenaceDescription, emptyList());

  private static final String lancerdarmeDescription = "Vous avez appris à lancer des armes avec forces lorsqu’il s’agit d’arme comme des cailloux, des couteaux ou des lances (p77). Vous pouvez utiliser votre force plutôt que votre agilité lorsque vous lancer une arme.";
  private static final Talent lancerdarme = new Talent("lancerdarme", "Lancer d'arme", null, lancerdarmeDescription, emptyList());

  private static final String randonneurDescription = "Vos pieds (ou pattes) et vos jambes sont endurcis après de nombreux voyages. Votre jet d’Endurance est modifié de +2 lors de long voyage.";
  private static final Talent randonneur = new Talent("randonneur", "Randonneur", null, randonneurDescription, emptyList());

  private static final String denicheurDeSouillureDescription = "nouvelle prouesse lorsque vous Trouver le Chemin. Vous trouvrez le chemin ke moins infecté par la Souillure. Le niveau de Souillure baisse de 1 lors de la visite de ce secteur.";
  private static final Talent denicheurDeSouillure = new Talent("denicheur", "Denicheur de souillures", "zonard", denicheurDeSouillureDescription, emptyList());

  private static final String recuperateurDescription = "+2 lors de vos jets de trouver le chemin. Sur un succès vous reperez tous les artefacts de la zone. Noter que cela ne permet pas de la recupérer s'ils sont garder. En revanche vous devez utiliser une prouesse pour identifier les menaces. Vous pouvez choisir d'utiliser ou non ce talent lorsque vous penetrez dans un nouveau secteur.";
  private static final Talent recuperateur = new Talent("recuperateur", "Récupérateur", "zonard", recuperateurDescription, List.of(trouverLeCheminDBonus));

  private static final String chasseurDeMonstreDescription = "+2 lorsque vous faites un jet de connaissance de la nature/zone pour une creature de n'importe quel type.";
  private static final Talent chasseurDeMonstre = new Talent("chasseur_de_monstre", "Chasseur de monstre", "zonard", chasseurDeMonstreDescription, emptyList());

  private static final List<Talent> talents = List.of(trappeur, archerie, betameur, dogFighter, skinner, inventeur, metalleux, limier, lemeilleuramidumutant,
    admirateur, archeologue, assassin, mauvaispresage, gardeducorps, boucher, veteran, tetefroide, conseiller, degaineclaire, poidsplume, amateurdegadgets,
    bonjeudejambes, cogneur, betedesomme, toucheatout, appetitdoiseau, solitaire, pasdereddition, mule, calculateur, tireurdelite, insomniaque, stoique, psy,
    specialistedesarmes, cuistotdelazone, suriner, combatadeuxarmes, evasif, soinsrapides, sabreur, coeurdelamachine, contremaitre, tirrapide, repute,
    chasseurderobot, resistantalasouillure, glouton, defenseur, mecanicien, murmureraloreilledesmonstres, naturiste, neufvies, tirinstinctif, massacreur,
    mastodonte, tenace, lancerdarme, randonneur, denicheurDeSouillure, recuperateur, chasseurDeMonstre);

  private static final GameRules gameCompetences = new GameRules(attributs, skills, weaponRanges, dice, gameClasses, resources, talents);

  @Override
  public GameRules getGameRules() {
    return gameCompetences;
  }

  @Override
  public List<Skill> getBasicSkills() {
    return skills.stream().filter(skill -> skill.getSkillType() == BASE).collect(toList());
  }

  @Override
  public Skill getSkill(String skillId) {
    return skills.stream().filter(skill -> skill.getId().equals(skillId)).collect(toList()).get(0);
  }

  @Override
  public Skill getSkillForClass(String gameClassId) {
    var skillId = gameClasses.stream().filter(gameClass -> gameClass.getId().equals(gameClassId)).collect(toList()).get(0).getSkillId();
    return skills.stream().filter(skill -> skill.getId().equals(skillId)).collect(toList()).get(0);
  }
}
