package jydreux.infra;

import com.google.gson.Gson;
import jydreux.domain.event.Event;
import jydreux.domain.event.EventRepository;
import jydreux.domain.event.EventType;
import jydreux.domain.exception.NotFoundException;
import jydreux.domain.game.notification.*;
import jydreux.domain.roll.model.Roll;
import jydreux.service.Database;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
public class EventSqlRepository implements EventRepository {
  private final Connection connection;
  private final Gson gson;

  public EventSqlRepository(Database database) {
    this.connection = database.getConnection();
    this.gson = new Gson();
  }

  @Override
  public void save(Event<?> event) {
    try {
      var statement = connection.prepareStatement("insert into event (id, date, type, game_id, player_id, content) values (?,?,?,?,?,?);");
      statement.setObject(1, event.getId(), Types.OTHER);
      statement.setTimestamp(2, Timestamp.from(Instant.from(event.getDate())));
      statement.setObject(3, event.getType().name(), java.sql.Types.OTHER);
      statement.setObject(4, event.getGameId(), Types.OTHER);
      statement.setObject(5, event.getPlayerId(), Types.OTHER);
      statement.setObject(6, this.gson.toJson(event.getContent()), Types.OTHER);
      statement.executeUpdate();
      statement.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public Event<?> get(String id) {
    try {
      var sql = "select " +
        "event.date, " +
        "event.type, " +
        "event.game_id, " +
        "event.player_id, " +
        "player.name, " +
        "event.content " +
        "from event join player on event.player_id = player.id " +
        "where event.id = ?;";
      var statement = connection.prepareStatement(sql);
      statement.setObject(1, id, Types.OTHER);
      var resultSet = statement.executeQuery();

      if (!resultSet.next()) {
        throw new NotFoundException("Notification with id " + id + " does not exist");
      }
      var date = resultSet.getTimestamp("date").toInstant().atZone(ZoneId.of("UTC"));
      var type = EventType.valueOf(resultSet.getString("type"));
      var gameId = resultSet.getString("game_id");
      var playerId = resultSet.getString("player_id");
      var playerName = resultSet.getString("name");
      var content = resultSet.getString("content");
      statement.close();
      return buildRawNotification(type, content, id, date, playerId, playerName, gameId);

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public List<Event<?>> getAllByGame(String gameId) {
    try {
      var sql = "select " +
        "event.id, " +
        "event.date, " +
        "event.type, " +
        "event.game_id, " +
        "event.player_id, " +
        "player.name, " +
        "event.content " +
        "from event join player on event.player_id = player.id " +
        "where event.game_id = ? " +
        "order by event.date desc;";
      var statement = connection.prepareStatement(sql);
      statement.setObject(1, gameId, Types.OTHER);
      var resultSet = statement.executeQuery();

      var notifications = new ArrayList<Event<?>>();

      while (resultSet.next()) {
        var id = resultSet.getString("id");
        var date = resultSet.getTimestamp("date").toInstant().atZone(ZoneId.of("UTC"));
        var type = EventType.valueOf(resultSet.getString("type"));
        var playerId = resultSet.getString("player_id");
        var playerName = resultSet.getString("name");
        var content = resultSet.getString("content");
        notifications.add(buildRawNotification(type, content, id, date, playerId, playerName, gameId));
      }

      statement.close();
      return notifications;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  private Event<?> buildRawNotification(EventType type, String content, String id, ZonedDateTime date, String playerId, String playerName, String gameId) {
    switch (type) {
      case GEAR_UPDATED:
      case WEAPON_UPDATED:
      case ATTRIBUT_UPDATED:
      case SKILL_UPDATED:
      case RESOURCE_UPDATED:
      case PLAYER_UPDATED:
        var playerUpdate = this.gson.fromJson(content, PlayerUpdate.class);
        return new Event<>(id, date, playerId, gameId, type, playerUpdate);
      case GEAR_ADDED:
      case WEAPON_ADDED:
        var stuffAdded = this.gson.fromJson(content, StuffAdded.class);
        return new Event<>(id, date, playerId, gameId, type, stuffAdded);
      case PLAYER_ROLLED:
        var roll = this.gson.fromJson(content, Roll.class);
        return new Event<>(id, date, playerId, gameId, type, roll);
      case WEAPON_GIVEN:
      case GEAR_GIVEN:
        var stuffGiven = this.gson.fromJson(content, StuffGiven.class);
        return new Event<>(id, date, playerId, gameId, type, stuffGiven);
      case GEAR_DELETED:
        var stuffDeleted = this.gson.fromJson(content, StuffDeleted.class);
        return new Event<>(id, date, playerId, gameId, type, stuffDeleted);
      case PLAYER_UPGRADED:
        var playerUgraded = this.gson.fromJson(content, PlayerUpgrade.class);
        return new Event<>(id, date, playerId, gameId, type, playerUgraded);
      default:
        return new Event<>(id, date, playerId, gameId, type, content);
    }
  }
}
