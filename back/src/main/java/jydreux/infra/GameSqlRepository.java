package jydreux.infra;

import jydreux.domain.exception.NotFoundException;
import jydreux.domain.game.Game;
import jydreux.domain.game.GameRepository;
import jydreux.domain.game.model.Gear;
import jydreux.domain.game.model.Weapon;
import jydreux.domain.user.User;
import jydreux.service.Database;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Collections.emptyList;

@Repository
public class GameSqlRepository implements GameRepository {
  private final Connection connection;
  private final PlayerSqlRepository playerSqlRepository;

  public GameSqlRepository(Database database, PlayerSqlRepository playerSqlRepository) {
    this.connection = database.getConnection();
    this.playerSqlRepository = playerSqlRepository;
  }

  @Override
  public Game get(String id) {
    try {
      var sql = "select name, game_master_email, players_emails from game where id = ?";
      var statement = connection.prepareStatement(sql);
      statement.setObject(1, id, Types.OTHER);
      var resultSet = statement.executeQuery();

      if (!resultSet.next()) {
        throw new NotFoundException();
      }

      var name = resultSet.getString("name");
      var gameMaster = resultSet.getString("game_master_email");
      var users = Arrays.asList((String[]) resultSet.getArray("players_emails").getArray());
      statement.close();
      var players = playerSqlRepository.getByGame(id);

      return new Game(id, name, gameMaster, users, players);

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return null;
  }

  @Override
  public void save(Game game) {
    try {
      var statement = connection.prepareStatement("insert into game (id, name, game_master_email, players_emails) values (?,?,?,?);");
      statement.setObject(1, game.getId(), Types.OTHER);
      statement.setString(2, game.getName());
      statement.setString(3, game.getGameMaster());
      statement.setArray(4, connection.createArrayOf("varchar", game.getUsers().toArray()));
      statement.executeUpdate();
      statement.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }

  }

  @Override
  public List<Game> findByUser(User user) {
    try {
      var sql = "select id, name, game_master_email, players_emails from game";
      var statement = connection.createStatement();
      var resultSet = statement.executeQuery(sql);

      var games = new ArrayList<Game>();

      while (resultSet.next()) {
        var gameMaster = resultSet.getString("game_master_email");
        var users = Arrays.asList((String[]) resultSet.getArray("players_emails").getArray());

        if (gameMaster.equals(user.getEmail()) || users.contains(user.getEmail())) {
          var id = resultSet.getString("id");
          var name = resultSet.getString("name");
          var players = playerSqlRepository.getByGame(id);
          games.add(new Game(id, name, gameMaster, users, players));
        }
      }

      return games;
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return emptyList();
  }

  @Override
  public Game findByPlayer(String playerId) {
    return null;
  }

  @Override
  public Gear getGear(String gearId) {
    try {
      var statement = connection.prepareStatement("select id, player_id, label, skills, bonus, malus from gear where id = ?");
      statement.setObject(1, gearId, Types.OTHER);
      var result = statement.executeQuery();
      if (!result.next()) {
        throw new NotFoundException("Gear with id " + gearId + " does not exist");
      }
      var id = result.getString("id");
      var label = result.getString("label");
      var skills = new ArrayList<>(Arrays.asList((String[]) result.getArray("skills").getArray()));
      var bonus = result.getInt("bonus");
      var malus = result.getInt("malus");
      var gear = new Gear(id, label, skills, bonus, malus);
      statement.close();
      return gear;
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public Weapon getWeapon(String weaponId) {
    try {
      var statement = connection.prepareStatement("select id, player_id, label, skill, bonus, damage, malus, scope, resource_id from weapon where id = ?");
      statement.setObject(1, weaponId, Types.OTHER);
      var result = statement.executeQuery();
      if (!result.next()) {
        throw new NotFoundException("Gear with id " + weaponId + " does not exist");
      }
      var id = result.getString("id");
      var label = result.getString("label");
      var skill = result.getString("skill");
      var bonus = result.getInt("bonus");
      var damage = result.getInt("damage");
      var malus = result.getInt("malus");
      var scope = result.getString("scope");
      var resource = result.getString("resource_id");
      var weapon = new Weapon(id, label, skill, bonus, damage, scope, malus, resource);
      statement.close();
      return weapon;
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
}
