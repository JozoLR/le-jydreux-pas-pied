package jydreux.infra;

import com.google.gson.Gson;
import jydreux.domain.exception.NotFoundException;
import jydreux.domain.map.Map;
import jydreux.domain.map.MapRepository;
import jydreux.service.Database;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

@Repository
public class MapSqlRepository implements MapRepository {
  private final Connection connection;
  private final Gson gson;

  public MapSqlRepository(Database database) {
    this.connection = database.getConnection();
    this.gson = new Gson();
  }

  @Override
  public void save(Map map) {
    try {
      var statement = connection.prepareStatement("insert into map (id, game_id, content) values (?,?,?);");
      statement.setObject(1, map.getGameId(), Types.OTHER);
      statement.setObject(2, map.getGameId(), Types.OTHER);
      statement.setObject(3, gson.toJson(map), Types.OTHER);
      statement.executeUpdate();
      statement.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public Map get(String gameId) {
    try {
      var sql = "select game_id, content from map where game_id = ?;";
      var statement = connection.prepareStatement(sql);
      statement.setObject(1, gameId, Types.OTHER);
      var resultSet = statement.executeQuery();

      if (!resultSet.next()) {
        throw new NotFoundException("Map with game id " + gameId + " does not exist");
      }
      var content = resultSet.getString("content");
      var map = gson.fromJson(content, Map.class);
      statement.close();
      return map;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public void update(Map map) {
    try {
      var sql = "update map set content = ? where game_id = ?;";
      var statement = connection.prepareStatement(sql);
      statement.setObject(1, gson.toJson(map), Types.OTHER);
      statement.setObject(2, map.getGameId(), Types.OTHER);
      statement.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
