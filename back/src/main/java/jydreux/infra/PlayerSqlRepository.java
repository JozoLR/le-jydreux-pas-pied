package jydreux.infra;

import jydreux.domain.cross.SkillType;
import jydreux.domain.exception.NotFoundException;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.game.model.*;
import jydreux.service.Database;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class PlayerSqlRepository implements PlayerRepository {
  private final Connection connection;

  public PlayerSqlRepository(Database database) {
    this.connection = database.getConnection();
  }

  @Override
  public void save(Player player) {
    try {
      var statement = connection.prepareStatement("insert into player (id, name, class_id, experience, rot, mutation, talents, game_id, notes) values (?,?,?,?,?,?,?,?,?);");
      statement.setObject(1, player.getId(), Types.OTHER);
      statement.setString(2, player.getName());
      statement.setString(3, player.getGameClassId());
      statement.setInt(4, player.getExperience());
      statement.setInt(5, player.getRot());
      statement.setInt(6, player.getMutation());
      statement.setArray(7, connection.createArrayOf("varchar", player.getTalents().toArray()));
      statement.setObject(8, player.getGameId(), Types.OTHER);
      statement.setString(9, player.getNotes());
      statement.executeUpdate();
      statement.close();
      insertAttribut(player);
      insertSkill(player);
      insertWeapon(player);
      insertGear(player);
      insertResource(player);

      statement.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void update(Player player) {
    try {
      updatePlayerFields(player);
      updateAttribut(player);
      updateSkill(player);
      updateWeapon(player);
      updateGear(player);
      updateResource(player);

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public Player get(String id) {
    try {
      var sql = "select name, class_id, experience, rot, mutation, talents, game_id, notes  from player where id= ?;";
      var statement = connection.prepareStatement(sql);
      statement.setObject(1, id, Types.OTHER);
      var resultSet = statement.executeQuery();

      if (!resultSet.next()) {
        throw new NotFoundException("Player with id " + id + " does not exist");
      }
      var name = resultSet.getString("name");
      var classId = resultSet.getString("class_id");
      var experience = resultSet.getInt("experience");
      var rot = resultSet.getInt("rot");
      var mutation = resultSet.getInt("mutation");
      var talents = new ArrayList<>(Arrays.asList((String[]) resultSet.getArray("talents").getArray()));
      var gameId = resultSet.getString("game_id");
      var notes = resultSet.getString("notes");
      statement.close();

      return new Player(id, name, getAttributs(id), getSkills(id), getWeapons(id), getGears(id), getResources(id), experience, rot, mutation,
        classId, talents, gameId, notes);
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return null;
  }

  @Override
  public List<Player> getByGame(String gameId) {
    try {
      var statement = connection.prepareStatement("select id from player where game_id = ?");
      statement.setObject(1, gameId, Types.OTHER);
      var result = statement.executeQuery();
      var players = new ArrayList<Player>();
      while (result.next()) {
        players.add(get(result.getString("id")));
      }
      statement.close();
      return players;
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public String getPlayerName(String playerId) {
    try {
      var sql = "select name from player where id= ? ;";
      var statement = connection.prepareStatement(sql);
      statement.setObject(1, playerId, Types.OTHER);
      var resultSet = statement.executeQuery();
      resultSet.next();
      var name = resultSet.getString("name");
      statement.close();
      return name;
    } catch (final SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public void addGear(String playerId, Gear gear) {
    try {
      var insertGear = "insert into gear (id, label, skills, bonus, malus, player_id) values (?,?,?,?,?,?);";
      var statement = connection.prepareStatement(insertGear);
      statement.setObject(1, gear.getId(), java.sql.Types.OTHER);
      statement.setString(2, gear.getLabel());
      statement.setArray(3, connection.createArrayOf("varchar", gear.getSkillIds().toArray()));
      statement.setInt(4, gear.getBonus());
      statement.setInt(5, gear.getMalus());
      statement.setObject(6, playerId, java.sql.Types.OTHER);
      statement.executeUpdate();
      statement.close();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  @Override
  public void addWeapon(String playerId, Weapon weapon) {
    try {
      var insertGear = "insert into weapon (id, label, skill, bonus, malus, damage, resource_id, scope, player_id) values (?,?,?,?,?,?,?,?,?);";
      var statement = connection.prepareStatement(insertGear);
      statement.setObject(1, weapon.getId(), java.sql.Types.OTHER);
      statement.setString(2, weapon.getLabel());
      statement.setString(3, weapon.getSkillId());
      statement.setInt(4, weapon.getBonus());
      statement.setInt(5, weapon.getMalus());
      statement.setInt(6, weapon.getDamage());
      statement.setString(7, weapon.getResourceId());
      statement.setString(8, weapon.getScope());
      statement.setObject(9, playerId, java.sql.Types.OTHER);
      statement.executeUpdate();
      statement.close();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  @Override
  public void deleteWeapon(String weaponId) {
    try {
      var sql = "update weapon set player_id = null where id = ?;";
      var statement = connection.prepareStatement(sql);
      statement.setObject(1, weaponId, Types.OTHER);
      statement.execute();
      statement.close();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  @Override
  public void giveWeapon(String weaponId, String playerId) {
    try {
      var sql = "update weapon set player_id = ? where id = ?;";
      var statement = connection.prepareStatement(sql);
      statement.setObject(1, playerId, Types.OTHER);
      statement.setObject(2, weaponId, Types.OTHER);
      statement.execute();
      statement.close();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  @Override
  public void deleteGear(String gearId) {
    try {
      var sql = "update gear set player_id = null where id = ?;";
      var statement = connection.prepareStatement(sql);
      statement.setObject(1, gearId, Types.OTHER);
      statement.execute();
      statement.close();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  @Override
  public void giveGear(String gearId, String playerId) {
    try {
      var sql = "update gear set player_id = ? where id = ?;";
      var statement = connection.prepareStatement(sql);
      statement.setObject(1, playerId, Types.OTHER);
      statement.setObject(2, gearId, Types.OTHER);
      statement.execute();
      statement.close();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  private void updatePlayerFields(Player player) throws SQLException {
    var updatePlayerFields = "update player set experience = ?, rot = ?, mutation = ?, notes = ?, talents = ? where id = ?;";
    var statement = connection.prepareStatement(updatePlayerFields);
    statement.setInt(1, player.getExperience());
    statement.setInt(2, player.getRot());
    statement.setInt(3, player.getMutation());
    statement.setString(4, player.getNotes());
    statement.setArray(5, connection.createArrayOf("varchar", player.getTalents().toArray()));
    statement.setObject(6, player.getId(), java.sql.Types.OTHER);
    statement.executeUpdate();
    statement.close();
  }

  private void insertSkill(Player player) throws SQLException {
    var sql = "insert into skill (id, label, actual, skill_type, player_id) values (?,?,?,?,?);";
    var statement = connection.prepareStatement(sql);
    for (var skill : player.getSkills()) {
      statement.setString(1, skill.getId());
      statement.setString(2, skill.getLabel());
      statement.setInt(3, skill.getActual());
      statement.setObject(4, skill.getSkillType().name(), java.sql.Types.OTHER);
      statement.setObject(5, player.getId(), java.sql.Types.OTHER);
      statement.addBatch();
    }
    statement.executeBatch();
    statement.close();
  }

  private void updateSkill(Player player) throws SQLException {
    var updateSkill = "update skill set actual = ? where  player_id = ? and id = ? ;";
    var statement = connection.prepareStatement(updateSkill);
    for (var skill : player.getSkills()) {
      statement.setInt(1, skill.getActual());
      statement.setObject(2, player.getId(), java.sql.Types.OTHER);
      statement.setString(3, skill.getId());
      statement.addBatch();
    }
    statement.executeBatch();
    statement.close();
  }

  private void insertAttribut(Player player) throws SQLException {
    var insertAttribut = "insert into attribut (id, label, base, actual, player_id) values (?,?,?,?,?);";
    var statement = connection.prepareStatement(insertAttribut);
    for (var attribut : player.getAttributs()) {
      statement.setString(1, attribut.getId());
      statement.setString(2, attribut.getLabel());
      statement.setInt(3, attribut.getBase());
      statement.setInt(4, attribut.getActual());
      statement.setObject(5, player.getId(), java.sql.Types.OTHER);
      statement.addBatch();
    }
    statement.executeBatch();
    statement.close();
  }

  private void updateAttribut(Player player) throws SQLException {
    var updateAttribut = "update attribut set actual = ? where  player_id = ? and id = ? ;";
    var statement = connection.prepareStatement(updateAttribut);
    for (var attribut : player.getAttributs()) {
      statement.setInt(1, attribut.getActual());
      statement.setObject(2, player.getId(), java.sql.Types.OTHER);
      statement.setString(3, attribut.getId());
      statement.addBatch();
    }
    statement.executeBatch();
    statement.close();
  }

  private void insertResource(Player player) throws SQLException {
    var insertCompetence = "insert into resource (player_id, id, label, quantity) " + " values (?,?,?,?);";
    var statement = connection.prepareStatement(insertCompetence);
    for (var resource : player.getResources()) {
      statement.setObject(1, player.getId(), java.sql.Types.OTHER);
      statement.setString(2, resource.getId());
      statement.setString(3, resource.getLabel());
      statement.setInt(4, resource.getQuantity());
      statement.addBatch();
    }
    statement.executeBatch();
    statement.close();
  }

  private void updateResource(Player player) throws SQLException {
    var sql = "update resource set quantity = ? where player_id = ? and id = ?;";
    var statement = connection.prepareStatement(sql);
    for (var resource : player.getResources()) {
      statement.setInt(1, resource.getQuantity());
      statement.setObject(2, player.getId(), java.sql.Types.OTHER);
      statement.setString(3, resource.getId());
      statement.addBatch();
    }
    statement.executeBatch();
    statement.close();
  }

  private void insertGear(Player player) throws SQLException {
    var sql = "insert into gear (player_id, label, skills, bonus, id, malus) "
      + " values (?,?,?,?,?,?);";
    var statement = connection.prepareStatement(sql);
    for (var gear : player.getGears()) {
      statement.setObject(1, player.getId(), java.sql.Types.OTHER);
      statement.setString(2, gear.getLabel());
      statement.setArray(3, connection.createArrayOf("varchar", gear.getSkillIds().toArray()));
      statement.setInt(4, gear.getBonus());
      statement.setObject(5, gear.getId(), java.sql.Types.OTHER);
      statement.setInt(6, gear.getMalus());
      statement.addBatch();
    }
    statement.executeBatch();
    statement.close();
  }

  private void updateGear(Player player) throws SQLException {
    var sql = "update gear set malus = ? where player_id = ? and id = ?;";
    var statement = connection.prepareStatement(sql);
    for (var gear : player.getGears()) {
      statement.setInt(1, gear.getMalus());
      statement.setObject(2, player.getId(), java.sql.Types.OTHER);
      statement.setObject(3, gear.getId(), java.sql.Types.OTHER);
      statement.addBatch();
    }
    statement.executeBatch();
    statement.close();
  }

  private void insertWeapon(Player player) throws SQLException {
    var sql = "insert into weapon (player_id, label, skill, bonus, damage, malus, scope, id, resource_id) "
      + " values (?,?,?,?,?,?,?,?,?);";
    var statement = connection.prepareStatement(sql);
    for (var weapon : player.getWeapons()) {
      statement.setObject(1, player.getId(), java.sql.Types.OTHER);
      statement.setString(2, weapon.getLabel());
      statement.setString(3, weapon.getSkillId());
      statement.setInt(4, weapon.getBonus());
      statement.setInt(5, weapon.getDamage());
      statement.setInt(6, weapon.getMalus());
      statement.setString(7, weapon.getScope());
      statement.setObject(8, weapon.getId(), java.sql.Types.OTHER);
      statement.setString(9, weapon.getResourceId());
      statement.addBatch();
    }
    statement.executeBatch();
    statement.close();
  }

  private void updateWeapon(Player player) throws SQLException {
    var sql = "update weapon set malus = ? where player_id = ? and id = ?;";
    var statement = connection.prepareStatement(sql);
    for (var weapon : player.getWeapons()) {
      statement.setInt(1, weapon.getMalus());
      statement.setObject(2, player.getId(), java.sql.Types.OTHER);
      statement.setObject(3, weapon.getId(), java.sql.Types.OTHER);
      statement.addBatch();
    }
    statement.executeBatch();
    statement.close();
  }

  private List<Attribut> getAttributs(String playerId) throws SQLException {
    var statement = connection.prepareStatement("select id, label, base, actual from attribut where player_id = ? order by label");
    statement.setObject(1, playerId, java.sql.Types.OTHER);
    var result = statement.executeQuery();
    var attributs = new ArrayList<Attribut>();
    while (result.next()) {
      var attribut = new Attribut(
        result.getString("id"),
        result.getString("label"),
        result.getInt("base"),
        result.getInt("actual"));
      attributs.add(attribut);
    }
    statement.close();
    return attributs;
  }

  private List<Skill> getSkills(String playerId) throws SQLException {
    var statement = connection.prepareStatement("select id, label, actual, skill_type from skill where player_id = ? order by label");
    statement.setObject(1, playerId, java.sql.Types.OTHER);
    var result = statement.executeQuery();
    var skills = new ArrayList<Skill>();
    while (result.next()) {

      var skill = new Skill(
        result.getString("id"),
        result.getString("label"),
        result.getInt("actual"),
        SkillType.valueOf(result.getString("skill_type")));
      skills.add(skill);
    }
    statement.close();
    return skills;
  }

  private List<Weapon> getWeapons(String playerId) throws SQLException {
    var statement = connection.prepareStatement(
      "select label, skill, bonus, damage, malus, scope, id, resource_id from weapon where player_id = ?");
    statement.setObject(1, playerId, java.sql.Types.OTHER);
    var result = statement.executeQuery();
    var weapons = new ArrayList<Weapon>();
    while (result.next()) {
      var weapon = new Weapon(
        result.getString("id"),
        result.getString("label"),
        result.getString("skill"),
        result.getInt("bonus"),
        result.getInt("damage"),
        result.getString("scope"),
        result.getInt("malus"),
        result.getString("resource_id"));
      weapons.add(weapon);
    }
    statement.close();
    return weapons;
  }

  private List<Gear> getGears(String playerId) throws SQLException {
    var statement = connection
      .prepareStatement("select label, skills, bonus, malus, id from gear where player_id = ?");
    statement.setObject(1, playerId, java.sql.Types.OTHER);
    var result = statement.executeQuery();
    var gears = new ArrayList<Gear>();
    while (result.next()) {
      var gear = new Gear(
        result.getString("id"),
        result.getString("label"),
        new ArrayList<>(Arrays.asList((String[]) result.getArray("skills").getArray())),
      result.getInt("bonus"), result.getInt("malus"));
      gears.add(gear);
    }
    statement.close();
    return gears;
  }

  private List<Resource> getResources(String playerId) throws SQLException {
    var statement = connection.prepareStatement("select id, label, quantity from resource where player_id = ? order by id");
    statement.setObject(1, playerId, java.sql.Types.OTHER);
    var result = statement.executeQuery();
    var resources = new ArrayList<Resource>();
    while (result.next()) {
      var resource = new Resource(result.getString("id"), result.getString("label"), result.getInt("quantity"));
      resources.add(resource);
    }
    statement.close();
    return resources;
  }
}
