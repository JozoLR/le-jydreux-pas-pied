package jydreux.infra;

import jydreux.domain.event.Event;
import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventHandler;
import org.springframework.stereotype.Repository;

@Repository
public class EventBusInMemory implements EventBus {
  private final com.google.common.eventbus.EventBus bus;

  public EventBusInMemory() {
    this.bus = new com.google.common.eventbus.EventBus();
  }

  @Override
  public void dispatch(Event event) {
    this.bus.post(event);
  }

  @Override
  public void addListener(EventHandler eventHandler) {
    this.bus.register(eventHandler);
  }

  @Override
  public void unsubscribe(EventHandler eventHandler) {
    this.bus.unregister(eventHandler);
  }
}
