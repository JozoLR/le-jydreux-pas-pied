package jydreux.infra;

import com.google.gson.Gson;
import jydreux.domain.event.EventType;
import jydreux.domain.roll.RollRepository;
import jydreux.domain.roll.model.Roll;
import jydreux.service.Database;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

@Repository
public class RollSqlRepository implements RollRepository {

  private final Connection connection;

  public RollSqlRepository(Database database) {
    this.connection = database.getConnection();
  }

  @Override
  public List<Roll> findAll() {
    try {
      var sql = "select event.content from event where event.type = ? order by event.date";
      var statement = connection.prepareStatement(sql);
      statement.setObject(1, EventType.PLAYER_ROLLED.name(), Types.OTHER);
      var resultSet = statement.executeQuery();

      var rolls = new ArrayList<Roll>();

      while (resultSet.next()) {
        var content = resultSet.getString("content");
        var roll = new Gson().fromJson(content, Roll.class);
        rolls.add(roll);
      }
      statement.close();
      return rolls;
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public Roll getMoreRecentByPlayer(String playerId) {
    try {
      var sql = "select event.content from event where event.type = ? and event.player_id = ? order by event.date desc";
      var statement = connection.prepareStatement(sql);
      statement.setObject(1, EventType.PLAYER_ROLLED.name(), Types.OTHER);
      statement.setObject(2, playerId, Types.OTHER);
      var resultSet = statement.executeQuery();


      if (resultSet.next()) {
        var content = resultSet.getString("content");
        return new Gson().fromJson(content, Roll.class);
      } else {
        return null;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
}
