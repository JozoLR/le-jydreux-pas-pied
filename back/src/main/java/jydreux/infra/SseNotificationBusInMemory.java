package jydreux.infra;

import jydreux.domain.event.Event;
import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventHandler;
import org.springframework.stereotype.Repository;

@Repository
public class SseNotificationBusInMemory implements EventBus {
  private final com.google.common.eventbus.EventBus bus;

  public SseNotificationBusInMemory() {
    this.bus = new com.google.common.eventbus.EventBus();
  }

  @Override
  public void dispatch(Event<?> event) {
    this.bus.post(event);
  }

  @Override
  public void addListener(EventHandler notificationHandler) {
    this.bus.register(notificationHandler);
  }

  @Override
  public void unsubscribe(EventHandler notificationHandler) {
    this.bus.unregister(notificationHandler);
  }
}
