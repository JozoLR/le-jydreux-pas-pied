package jydreux.infra;

import com.google.gson.Gson;
import jydreux.config.Config;
import jydreux.domain.user.User;
import jydreux.domain.user.UserRepository;
import jydreux.infra.model.UserHttp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.Map;

@Repository
public class UserHttpRepository implements UserRepository {

  private final Config config;
  private final HttpClient httpClient;
  private final Map<String, User> users;
  private final Logger logger = LoggerFactory.getLogger(UserHttpRepository.class);

  public UserHttpRepository(Config config) {
    this.config = config;
    this.httpClient = HttpClient.newBuilder()
      .version(HttpClient.Version.HTTP_2)
      .build();
    this.users = new HashMap<>();
  }

  @Override
  public User getByToken(String token) {
    try {
      var inMemoryUser = users.get(token);
      if (inMemoryUser != null) {
        return inMemoryUser;
      }
      var request = HttpRequest.newBuilder()
        .GET()
        .uri(URI.create("https://" + config.authDomain + "/userinfo"))
        .setHeader("Authorization", "Bearer " + token)
        .build();

      var response = this.httpClient.send(request, HttpResponse.BodyHandlers.ofString());
      try {
        var userHttp = new Gson().fromJson(response.body(), UserHttp.class);
        var user = new User(userHttp.getEmail());
        this.users.put(token, user);
        return user;
      } catch (Exception e) {
        logger.error("Too many request to Auth0");
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}
