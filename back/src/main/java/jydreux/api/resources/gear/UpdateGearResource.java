package jydreux.api.resources.gear;

import jydreux.domain.game.command.gear.UpdateGearCommand;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UpdateGearResource {
  @NotBlank
  private String id;
  @NotNull
  private Integer malus;
  @NotBlank
  private String playerId;
  @NotBlank
  private String gameId;

  public UpdateGearCommand toCommand() {
    return new UpdateGearCommand(id, malus, playerId, gameId);
  }

  public String getId() {
    return id;
  }

  public Integer getMalus() {
    return malus;
  }

  public String getPlayerId() {
    return playerId;
  }

  public String getGameId() {
    return gameId;
  }
}
