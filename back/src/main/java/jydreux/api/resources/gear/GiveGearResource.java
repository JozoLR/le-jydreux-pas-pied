package jydreux.api.resources.gear;

import jydreux.domain.game.command.gear.GiveGearCommand;

import javax.validation.constraints.NotBlank;

public class GiveGearResource {
  @NotBlank
  private String gearId;
  @NotBlank
  private String playerId;
  @NotBlank
  private String beneficiaryId;
  @NotBlank
  private String gameId;

  public GiveGearCommand toCommand() {
    return new GiveGearCommand(gearId, playerId, beneficiaryId, gameId);
  }

  public String getGearId() {
    return gearId;
  }

  public String getPlayerId() {
    return playerId;
  }

  public String getBeneficiaryId() {
    return beneficiaryId;
  }

  public String getGameId() {
    return gameId;
  }
}
