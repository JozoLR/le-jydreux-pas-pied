package jydreux.api.resources.gear;

import jydreux.domain.game.command.gear.DeleteGearCommand;

import javax.validation.constraints.NotBlank;

public class DeleteGearResource {
  @NotBlank
  private String playerId;
  @NotBlank
  private String gameId;
  @NotBlank
  private String gearId;

  public DeleteGearCommand toCommand() {
    return new DeleteGearCommand(gameId, playerId, gearId);
  }

  public String getPlayerId() {
    return playerId;
  }

  public String getGameId() {
    return gameId;
  }

  public String getGearId() {
    return gearId;
  }
}
