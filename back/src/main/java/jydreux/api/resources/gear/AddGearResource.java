package jydreux.api.resources.gear;

import jydreux.domain.game.model.Gear;

import javax.validation.constraints.NotBlank;

import static java.util.Arrays.asList;

public class AddGearResource {
  @NotBlank
  private String playerId;
  @NotBlank
  private String label;
  private String[] skillIds;
  private Integer bonus;

  public Gear toDomain(String id) {
    return new Gear(id, label, asList(skillIds), bonus);
  }

  public String getPlayerId() {
    return playerId;
  }

  public String getLabel() {
    return label;
  }

  public String[] getSkillIds() {
    return skillIds;
  }

  public Integer getBonus() {
    return bonus;
  }
}
