package jydreux.api.resources;

import jydreux.domain.game.model.Resource;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UpdateResourceResource {
  @NotNull
  private String id;
  @NotBlank
  private String label;
  @NotNull
  private Integer quantity;
  @NotBlank
  private String playerId;

  public Resource toDomain() {
    return new Resource(id, label, quantity);
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public String getPlayerId() {
    return playerId;
  }

  public Integer getQuantity() {
    return quantity;
  }
}
