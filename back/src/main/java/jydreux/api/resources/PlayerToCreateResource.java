package jydreux.api.resources;

import jydreux.domain.playerCreation.Attribut;
import jydreux.domain.playerCreation.PlayerToCreate;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class PlayerToCreateResource {
  @NotBlank
  private String name;
  private AttributResource[] attributs;
  @NotBlank
  private String gameClassId;
  @NotBlank
  private String talentId;
  @NotBlank
  private String gameId;

  public PlayerToCreate toDomain(final String id) {
    List<Attribut> attributs = Stream.of(this.attributs)
      .map(attribut -> new Attribut(attribut.getId(), attribut.getLabel(), attribut.getValue())).collect(toList());

    return new PlayerToCreate(id, name, attributs, gameClassId, talentId, gameId);
  }

  public String getName() {
    return name;
  }

  public AttributResource[] getAttributs() {
    return attributs;
  }

  public String getGameId() {
    return gameId;
  }

  public String getGameClassId() {
    return gameClassId;
  }

  public String getTalentId() {
    return talentId;
  }
}
