package jydreux.api.resources;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UpdatePlayerResource {
  @NotBlank
  private final String playerId;
  @NotNull
  private final Integer experience;
  @NotNull
  private final Integer rot;
  @NotNull
  private final Integer mutation;

  public UpdatePlayerResource(@NotBlank String playerId, @NotNull Integer experience, @NotNull Integer rot, @NotNull Integer mutation) {
    this.playerId = playerId;
    this.experience = experience;
    this.rot = rot;
    this.mutation = mutation;
  }

  public String getPlayerId() {
    return playerId;
  }

  public Integer getExperience() {
    return experience;
  }

  public Integer getRot() {
    return rot;
  }

  public Integer getMutation() {
    return mutation;
  }
}
