package jydreux.api.resources.map;

import jydreux.domain.map.model.Marker;
import jydreux.domain.map.model.MarkerType;
import jydreux.domain.map.model.Position;

public class MarkerResource {
  private String label;
  private Integer tileIndex;
  private PositionResource position;
  private MarkerType type;

  public Marker toDomain() {
    return new Marker(label, tileIndex, new Position(position.getX(), position.getY()), type);
  }

  public String getLabel() {
    return label;
  }

  public MarkerType getType() {
    return type;
  }

  public Integer getTileIndex() {
    return tileIndex;
  }

  public PositionResource getPosition() {
    return position;
  }
}
