package jydreux.api.resources.map;

public class PositionResource {
  private Double x;
  private Double y;

  public Double getX() {
    return x;
  }

  public Double getY() {
    return y;
  }
}
