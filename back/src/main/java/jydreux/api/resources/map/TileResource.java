package jydreux.api.resources.map;

public class TileResource {
  private Integer index;
  private Boolean visited;
  private String note;

  public Integer getIndex() {
    return index;
  }

  public Boolean getVisited() {
    return visited;
  }

  public String getNote() {
    return note;
  }
}
