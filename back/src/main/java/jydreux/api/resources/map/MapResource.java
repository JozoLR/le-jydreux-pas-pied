package jydreux.api.resources.map;

import jydreux.domain.map.Map;
import jydreux.domain.map.model.Tile;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class MapResource {
  private String gameId;
  private List<MarkerResource> markers;
  private List<TileResource> tiles;

  public Map toDomain() {
    var markers = this.markers.stream().map(MarkerResource::toDomain).collect(toList());
    var tiles = this.tiles.stream().map(tile -> new Tile(tile.getIndex(), tile.getVisited(), tile.getNote())).collect(toList());
    return new Map(gameId, markers, tiles);
  }

  public String getGameId() {
    return gameId;
  }

  public List<MarkerResource> getMarkers() {
    return markers;
  }

  public List<TileResource> getTiles() {
    return tiles;
  }
}
