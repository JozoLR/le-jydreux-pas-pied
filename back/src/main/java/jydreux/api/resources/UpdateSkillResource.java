package jydreux.api.resources;

import jydreux.domain.cross.SkillType;
import jydreux.domain.game.model.Skill;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UpdateSkillResource {
  @NotBlank
  private String id;
  @NotBlank
  private String label;
  @NotNull
  private Integer actual;
  @NotBlank
  private String skillType;
  @NotBlank
  private String playerId;

  public Skill toDomain() {
    return new Skill(id, label, actual, SkillType.valueOf(skillType));
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public Integer getActual() {
    return actual;
  }

  public String getPlayerId() {
    return playerId;
  }

  public String getSkillType() {
    return skillType;
  }
}
