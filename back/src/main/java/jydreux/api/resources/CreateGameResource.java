package jydreux.api.resources;

import jydreux.domain.game.GameToCreate;

import javax.validation.constraints.NotBlank;
import java.util.Arrays;

public class CreateGameResource {
  private String[] players;
  @NotBlank
  private String name;

  public GameToCreate toDomain(String creatorToken) {
    return new GameToCreate(creatorToken, name, Arrays.asList(players));
  }

  public String getName() {
    return name;
  }

  public String[] getPlayers() {
    return players;
  }
}
