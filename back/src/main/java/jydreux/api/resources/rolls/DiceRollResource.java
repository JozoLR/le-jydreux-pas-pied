package jydreux.api.resources.rolls;

import jydreux.domain.roll.RollService;
import jydreux.domain.roll.model.Dice;
import jydreux.domain.roll.model.RollType;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class DiceRollResource {
  @NotNull
  private Integer attribut;
  @NotNull
  private Integer skill;
  @NotNull
  private Integer stuff;
  @NotNull
  private Integer malus;
  @NotBlank
  private String playerId;
  private RollInfosResource rollInfos;

  public Dice buildCommand(RollService rollService) {
    return new Dice(attribut, skill, stuff, malus, playerId, RollType.ROLL, rollInfos.toDomain(), rollService);
  }

  public Integer getAttribut() {
    return attribut;
  }

  public Integer getSkill() {
    return skill;
  }

  public Integer getMalus() {
    return malus;
  }

  public String getPlayerId() {
    return playerId;
  }

  public Integer getStuff() {
    return stuff;
  }

  public RollInfosResource getRollInfos() {
    return rollInfos;
  }
}
