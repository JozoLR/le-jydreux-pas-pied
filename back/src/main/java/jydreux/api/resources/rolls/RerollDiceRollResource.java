package jydreux.api.resources.rolls;

import javax.validation.constraints.NotBlank;

public class RerollDiceRollResource {
  @NotBlank
  private String playerId;

  public String getPlayerId() {
    return playerId;
  }
}
