package jydreux.api.resources.rolls;

import jydreux.domain.cross.StuffType;
import jydreux.domain.roll.model.RollInfos;

import java.util.List;

public class RollInfosResource {
  private String skillId;
  private final StuffType stuffType;
  private final List<String> stuffIds;

  public RollInfosResource(String skillId, StuffType stuffType, List<String> stuffIds) {
    this.skillId = skillId;
    this.stuffType = stuffType;
    this.stuffIds = stuffIds;
  }

  public RollInfos toDomain() {
    if (skillId.equals("")) {
      skillId = null;
    }
    return new RollInfos(skillId, stuffType, stuffIds);
  }

  public String getSkillId() {
    return skillId;
  }

  public StuffType getStuffType() {
    return stuffType;
  }

  public List<String> getStuffIds() {
    return stuffIds;
  }
}
