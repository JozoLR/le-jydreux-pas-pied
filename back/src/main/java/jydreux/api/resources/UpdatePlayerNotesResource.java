package jydreux.api.resources;

import javax.validation.constraints.NotBlank;

public class UpdatePlayerNotesResource {
  @NotBlank
  private final String playerId;
  private final String notes;

  public UpdatePlayerNotesResource(String playerId, String notes) {
    this.playerId = playerId;
    this.notes = notes;
  }

  public String getPlayerId() {
    return playerId;
  }

  public String getNotes() {
    return notes;
  }
}
