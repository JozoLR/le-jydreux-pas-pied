package jydreux.api.resources;

import jydreux.domain.game.model.Attribut;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UpdateAttributResource {
  @NotBlank
  private String id;
  @NotBlank
  private String label;
  @NotNull
  private Integer base;
  @NotNull
  private Integer actual;
  @NotBlank
  private String playerId;

  public Attribut toDomain() {
    return new Attribut(id, label, base, actual);
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public Integer getBase() {
    return base;
  }

  public Integer getActual() {
    return actual;
  }

  public String getPlayerId() {
    return playerId;
  }
}
