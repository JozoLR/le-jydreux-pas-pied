package jydreux.api.resources;

import jydreux.domain.game.command.player.UpgradePlayerCommand;

import javax.validation.constraints.NotBlank;
import java.util.List;

public class UpgradePlayerResource {
  @NotBlank
  private String playerId;
  @NotBlank
  private String gameId;
  private List<String> skillIds;
  private List<String> talentIds;

  public UpgradePlayerCommand toCommand() {
    return new UpgradePlayerCommand(playerId, skillIds, talentIds);
  }

  public String getPlayerId() {
    return playerId;
  }

  public String getGameId() {
    return gameId;
  }

  public List<String> getSkillIds() {
    return skillIds;
  }

  public List<String> getTalentIds() {
    return talentIds;
  }
}
