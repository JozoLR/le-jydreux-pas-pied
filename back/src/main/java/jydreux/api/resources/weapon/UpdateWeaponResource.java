package jydreux.api.resources.weapon;

import jydreux.domain.game.model.Weapon;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UpdateWeaponResource {
  @NotBlank
  private String id;
  @NotBlank
  private String label;
  @NotBlank
  private String skillId;
  @NotNull
  private Integer bonus;
  @NotNull
  private Integer damage;
  @NotBlank
  private String scope;
  @NotNull
  private Integer malus;
  @NotBlank
  private String playerId;

  public Weapon toDomain() {
    return new Weapon(id, label, skillId, bonus, damage, scope, malus, null);
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public String getSkillId() {
    return skillId;
  }

  public Integer getBonus() {
    return bonus;
  }

  public Integer getMalus() {
    return malus;
  }

  public String getPlayerId() {
    return playerId;
  }

  public Integer getDamage() {
    return damage;
  }

  public String getScope() {
    return scope;
  }
}
