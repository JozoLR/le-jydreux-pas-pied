package jydreux.api.resources.weapon;

import jydreux.domain.game.command.weapon.DeleteWeaponCommand;

import javax.validation.constraints.NotBlank;

public class DeleteWeaponResource {
  @NotBlank
  private String playerId;
  @NotBlank
  private String gameId;
  @NotBlank
  private String weaponId;

  public DeleteWeaponCommand toCommand() {
    return new DeleteWeaponCommand(gameId, playerId, weaponId);
  }

  public String getPlayerId() {
    return playerId;
  }

  public String getGameId() {
    return gameId;
  }

  public String getWeaponId() {
    return weaponId;
  }
}
