package jydreux.api.resources.weapon;

import jydreux.domain.game.command.weapon.GiveWeaponCommand;

import javax.validation.constraints.NotBlank;

public class GiveWeaponResource {
  @NotBlank
  private String gameId;
  @NotBlank
  private String playerId;
  @NotBlank
  private String beneficiaryId;
  @NotBlank
  private String weaponId;

  public GiveWeaponCommand toCommand() {
    return new GiveWeaponCommand(weaponId, playerId, beneficiaryId);
  }

  public String getGameId() {
    return gameId;
  }

  public String getPlayerId() {
    return playerId;
  }

  public String getWeaponId() {
    return weaponId;
  }

  public String getBeneficiaryId() {
    return beneficiaryId;
  }
}
