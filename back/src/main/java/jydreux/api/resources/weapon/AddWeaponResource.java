package jydreux.api.resources.weapon;

import jydreux.domain.game.model.Weapon;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class AddWeaponResource {
  @NotBlank
  private String playerId;
  @NotBlank
  private String label;
  @NotBlank
  private String skillId;
  private String resourceId;
  @NotNull
  private Integer bonus;
  @NotNull
  private Integer damage;
  @NotBlank
  private String scope;

  public Weapon toDomain(String id) {
    var resourceId = this.resourceId;
    if (this.resourceId != null && this.resourceId.equals("")) {
      resourceId = null;
    }
    return new Weapon(id, label, skillId, resourceId, bonus, damage, scope);
  }

  public String getPlayerId() {
    return playerId;
  }

  public String getLabel() {
    return label;
  }

  public String getSkillId() {
    return skillId;
  }

  public Integer getBonus() {
    return bonus;
  }

  public String getResourceId() {
    return resourceId;
  }

  public Integer getDamage() {
    return damage;
  }

  public String getScope() {
    return scope;
  }
}
