package jydreux.api.resources;

public class AttributResource {
  private String id;
  private String label;
  private Integer value;

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public Integer getValue() {
    return value;
  }
}
