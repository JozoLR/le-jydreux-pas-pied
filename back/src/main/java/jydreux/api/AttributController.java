package jydreux.api;

import jydreux.api.resources.UpdateAttributResource;
import jydreux.domain.game.command.player.UpdateAttributCommand;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Validated
@RestController
@RequestMapping("/player/attribut")
public class AttributController {

  private final UpdateAttributCommand updateAttribut;

  public AttributController(UpdateAttributCommand updateAttribut) {
    this.updateAttribut = updateAttribut;
  }

  @PostMapping
  public ResponseEntity<String> handle(@RequestBody @Valid UpdateAttributResource updateAttributResource) {
    this.updateAttribut.handle(updateAttributResource.toDomain(), updateAttributResource.getPlayerId());
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
