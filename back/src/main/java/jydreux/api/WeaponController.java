package jydreux.api;

import jydreux.api.resources.weapon.AddWeaponResource;
import jydreux.api.resources.weapon.DeleteWeaponResource;
import jydreux.api.resources.weapon.GiveWeaponResource;
import jydreux.api.resources.weapon.UpdateWeaponResource;
import jydreux.domain.event.CommandDispatcher;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.command.weapon.AddWeaponCommand;
import jydreux.domain.game.command.weapon.UpdateWeaponCommand;
import jydreux.service.IdService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Validated
@RestController
@RequestMapping("/player/weapon")
public class WeaponController {

  private final AddWeaponCommand addWeapon;
  private final IdService idService;
  private final UpdateWeaponCommand updateWeapon;
  private final CommandDispatcher commandDispatcher;
  private final EventFactory eventFactory;

  public WeaponController(AddWeaponCommand addWeapon, IdService idService, UpdateWeaponCommand updateWeapon, CommandDispatcher commandDispatcher, EventFactory eventFactory) {
    this.addWeapon = addWeapon;
    this.idService = idService;
    this.updateWeapon = updateWeapon;
    this.commandDispatcher = commandDispatcher;
    this.eventFactory = eventFactory;
  }

  @PutMapping
  public ResponseEntity<String> handle(@RequestBody @Valid AddWeaponResource weaponResource) {
    var id = this.idService.generate();
    this.addWeapon.handle(weaponResource.toDomain(id), weaponResource.getPlayerId());
    return new ResponseEntity<>(id, HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity<String> handle(@RequestBody @Valid UpdateWeaponResource updateWeaponResource) {
    this.updateWeapon.handle(updateWeaponResource.toDomain(), updateWeaponResource.getPlayerId());
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PostMapping("/give")
  public ResponseEntity<String> handle(JwtAuthenticationToken authentication, @RequestBody @Valid GiveWeaponResource giveWeaponResource) {
    var command = giveWeaponResource.toCommand();
    var event = eventFactory.build(giveWeaponResource.getGameId(), giveWeaponResource.getPlayerId(), EventType.GIVE_WEAPON, command);
    this.commandDispatcher.validateAndDispatch(event, authentication.getToken().getTokenValue());
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @DeleteMapping
  public ResponseEntity<String> delete(JwtAuthenticationToken authentication, @RequestBody @Valid DeleteWeaponResource resource) {
    var command = eventFactory.build(resource.getGameId(), resource.getPlayerId(), EventType.DELETE_WEAPON, resource.toCommand());
    this.commandDispatcher.validateAndDispatch(command, authentication.getToken().getTokenValue());
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
