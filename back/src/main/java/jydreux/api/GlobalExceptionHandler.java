package jydreux.api;

import jydreux.domain.exception.ForbiddenException;
import jydreux.domain.exception.NotFoundException;
import jydreux.domain.exception.RerollImpossibleException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler({MethodArgumentNotValidException.class})
  public ResponseEntity<?> invalidParamsExceptionHandler(MethodArgumentNotValidException e) {
    String message = resolveBindingResultErrors(e.getBindingResult());
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
  }

  @ExceptionHandler({RerollImpossibleException.class})
  public ResponseEntity<?> invalidParamsExceptionHandler(RerollImpossibleException e) {
    return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
  }

  @ExceptionHandler({ForbiddenException.class})
  public ResponseEntity<?> forbiddenHandler(ForbiddenException e) {
    return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
  }

  @ExceptionHandler({NotFoundException.class})
  public ResponseEntity<?> notFoundHandler(NotFoundException e) {
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
  }


  private String resolveBindingResultErrors(BindingResult bindingResult) {
    return bindingResult.getFieldErrors().stream()
      .map(error -> error.getField() + " " + error.getDefaultMessage())
      .collect(Collectors.joining(", "));
  }
}
