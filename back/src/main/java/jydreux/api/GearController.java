package jydreux.api;

import jydreux.api.resources.gear.DeleteGearResource;
import jydreux.api.resources.gear.AddGearResource;
import jydreux.api.resources.gear.GiveGearResource;
import jydreux.api.resources.gear.UpdateGearResource;
import jydreux.domain.event.CommandDispatcher;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.command.gear.AddGearCommand;
import jydreux.service.IdService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Validated
@RestController
@RequestMapping("/player/gear")
public class GearController {

  private final AddGearCommand addGear;
  private final IdService idService;
  private final CommandDispatcher commandDispatcher;
  private final EventFactory eventFactory;

  public GearController(AddGearCommand addGear, IdService idService, CommandDispatcher commandDispatcher, EventFactory eventFactory) {
    this.addGear = addGear;
    this.idService = idService;
    this.commandDispatcher = commandDispatcher;
    this.eventFactory = eventFactory;
  }

  @PutMapping
  public ResponseEntity<String> handle(@RequestBody @Valid AddGearResource gearResource) {
    var id = this.idService.generate();
    this.addGear.handle(gearResource.toDomain(id), gearResource.getPlayerId());
    return new ResponseEntity<>(id, HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity<String> handle(JwtAuthenticationToken authentication, @RequestBody @Valid UpdateGearResource updateGearResource) {
    var command = updateGearResource.toCommand();
    var event = eventFactory.build(command.getGameId(), command.getPlayerId(), EventType.UPDATE_GEAR, command);
    this.commandDispatcher.validateAndDispatch(event, authentication.getToken().getTokenValue());
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PostMapping("/give")
  public ResponseEntity<String> handle(JwtAuthenticationToken authentication, @RequestBody @Valid GiveGearResource giveGearResource) {
    var command = giveGearResource.toCommand();
    var event = eventFactory.build(command.getGameId(), command.getPlayerId(), EventType.GIVE_GEAR, command);
    this.commandDispatcher.validateAndDispatch(event, authentication.getToken().getTokenValue());
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @DeleteMapping
  public ResponseEntity<String> delete(JwtAuthenticationToken authentication, @RequestBody @Valid DeleteGearResource resource) {
    var command = eventFactory.build(resource.getGameId(), resource.getPlayerId(), EventType.DELETE_GEAR, resource.toCommand());
    this.commandDispatcher.validateAndDispatch(command, authentication.getToken().getTokenValue());
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
