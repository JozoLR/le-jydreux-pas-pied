package jydreux.api.config;

import jydreux.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.core.DelegatingOAuth2TokenValidator;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtDecoders;
import org.springframework.security.oauth2.jwt.JwtValidators;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
  private final String issuer;
  private final String audience;

  public SecurityConfig(Config config) {
    this.issuer = config.authIssuer;
    this.audience = config.authAudience;
  }

  @Bean
  JwtDecoder jwtDecoder() {
    var jwtDecoder = (NimbusJwtDecoder)
      JwtDecoders.fromOidcIssuerLocation(issuer);

    var audienceValidator = new TokenValidator(audience);
    var withIssuer = JwtValidators.createDefaultWithIssuer(issuer);
    var withAudience = new DelegatingOAuth2TokenValidator<>(withIssuer, audienceValidator);

    jwtDecoder.setJwtValidator(withAudience);

    return jwtDecoder;
  }

  @Override
  public void configure(HttpSecurity http) throws Exception {
    http
      .cors()
      .and()
      .authorizeRequests()
      .mvcMatchers("/healthcheck", "/v2/api-docs", "/configuration/ui", "/swagger-resources/**", "/configuration/**", "/swagger-ui.html", "/webjars/**").permitAll()
      .mvcMatchers("/**").authenticated()
      .and()
      .oauth2ResourceServer().jwt();
  }
}
