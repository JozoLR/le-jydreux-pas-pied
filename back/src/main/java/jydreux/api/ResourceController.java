package jydreux.api;

import jydreux.api.resources.UpdateResourceResource;
import jydreux.domain.game.command.player.UpdateResourceCommand;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Validated
@RestController
@RequestMapping("/player/resource")
public class ResourceController {

  private final UpdateResourceCommand updateResource;

  public ResourceController(UpdateResourceCommand updateResource) {
    this.updateResource = updateResource;
  }

  @PostMapping
  public ResponseEntity<String> handle(@RequestBody @Valid UpdateResourceResource updateResourceResource) {
    this.updateResource.handle(updateResourceResource.toDomain(), updateResourceResource.getPlayerId());
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
