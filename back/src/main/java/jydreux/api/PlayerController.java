package jydreux.api;

import jydreux.api.resources.PlayerToCreateResource;
import jydreux.api.resources.UpdatePlayerNotesResource;
import jydreux.api.resources.UpdatePlayerResource;
import jydreux.api.resources.UpgradePlayerResource;
import jydreux.domain.event.CommandDispatcher;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.command.player.CreatePlayerCommand;
import jydreux.domain.game.command.player.UpdatePlayerCommand;
import jydreux.domain.game.command.player.UpdatePlayerNotesCommand;
import jydreux.query.GetPlayer;
import jydreux.query.model.PlayerQueryModel;
import jydreux.service.IdService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/player")
public class PlayerController {

  private final CreatePlayerCommand createPlayer;
  private final UpdatePlayerCommand updatePlayer;
  private final CommandDispatcher commandDispatcher;
  private final EventFactory eventFactory;
  private final GetPlayer getPlayer;
  private final IdService idService;
  private final UpdatePlayerNotesCommand updatePlayerNotes;

  public PlayerController(CreatePlayerCommand createPlayer, UpdatePlayerCommand updatePlayer, CommandDispatcher commandDispatcher, EventFactory eventFactory, IdService idService, GetPlayer getPlayer, UpdatePlayerNotesCommand updatePlayerNotes) {
    this.createPlayer = createPlayer;
    this.updatePlayer = updatePlayer;
    this.commandDispatcher = commandDispatcher;
    this.eventFactory = eventFactory;
    this.idService = idService;
    this.getPlayer = getPlayer;
    this.updatePlayerNotes = updatePlayerNotes;
  }

  @PutMapping
  public ResponseEntity<String> createPlayer(@RequestBody @Valid PlayerToCreateResource playerToCreateResource) {
    var id = this.idService.generate();
    var playerToCreate = playerToCreateResource.toDomain(id);
    this.createPlayer.handle(playerToCreate);
    return new ResponseEntity<>(id, HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity<String> updateResources(@RequestBody @Valid UpdatePlayerResource updatePlayerResource) {
    this.updatePlayer.handle(updatePlayerResource);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @GetMapping("/{playerId}")
  public PlayerQueryModel get(@PathVariable String playerId) {
    return getPlayer.handle(playerId);
  }

  @PostMapping("/notes")
  public ResponseEntity<String> updateNotes(@RequestBody @Valid UpdatePlayerNotesResource updatePlayerNotesResource) {
    updatePlayerNotes.handle(updatePlayerNotesResource.getPlayerId(), updatePlayerNotesResource.getNotes());
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PostMapping("/upgrade")
  public ResponseEntity<String> updateNotes(JwtAuthenticationToken authentication, @RequestBody @Valid UpgradePlayerResource upgradePlayerResource) {
    var command = upgradePlayerResource.toCommand();
    var event = eventFactory.build(upgradePlayerResource.getGameId(), upgradePlayerResource.getPlayerId(), EventType.UPGRADE_PLAYER, command);
    this.commandDispatcher.validateAndDispatch(event, authentication.getToken().getTokenValue());
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
