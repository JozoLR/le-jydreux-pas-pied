package jydreux.api;

import jydreux.api.resources.map.MapResource;
import jydreux.domain.event.CommandDispatcher;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.map.Map;
import jydreux.domain.map.query.GetMap;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Validated
@RestController
@RequestMapping("/map")
public class MapController {

  private final CommandDispatcher commandDispatcher;
  private final EventFactory eventFactory;
  private final GetMap getMap;

  public MapController(CommandDispatcher commandDispatcher, EventFactory eventFactory, GetMap getMap) {
    this.commandDispatcher = commandDispatcher;
    this.eventFactory = eventFactory;
    this.getMap = getMap;
  }

  @PostMapping
  public ResponseEntity<String> handle(JwtAuthenticationToken authentication, @RequestBody @Valid MapResource mapResource) {
    var event = eventFactory.build(mapResource.getGameId(), EventType.UPDATE_MAP, mapResource.toDomain());
    this.commandDispatcher.validateAndDispatch(event, authentication.getToken().getTokenValue());
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @GetMapping("/{gameId}")
  public Map get(@PathVariable String gameId) {
    return getMap.handle(gameId);
  }
}
