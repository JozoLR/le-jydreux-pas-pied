package jydreux.api;

import jydreux.api.resources.UpdateSkillResource;
import jydreux.domain.game.command.player.UpdateSkillCommand;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Validated
@RestController
@RequestMapping("/player/skill")
public class SkillController {

  private final UpdateSkillCommand updateSkill;

  public SkillController(UpdateSkillCommand updateSkill) {
    this.updateSkill = updateSkill;
  }

  @PostMapping
  public ResponseEntity<String> handle(@RequestBody @Valid UpdateSkillResource updateSkillResource) {
    this.updateSkill.handle(updateSkillResource.toDomain(), updateSkillResource.getPlayerId());
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
