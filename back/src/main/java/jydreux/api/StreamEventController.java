package jydreux.api;

import jydreux.domain.event.EventBus;
import jydreux.domain.event.handler.SseNotificationHandler;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.notification.query.NotificationQueryModelMapper;
import jydreux.service.Heartbeat;
import jydreux.service.IdService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.concurrent.Executors;

@RestController
public class StreamEventController {
  private final static Long TROIS_HEURES = 10800000L;
  private final IdService idService;
  private final Logger logger = LoggerFactory.getLogger(StreamEventController.class);
  private final NotificationQueryModelMapper notificationQueryModelMapper;
  private final EventBus notificationBus;
  private final PlayerRepository playerRepository;

  public StreamEventController(@Qualifier("sseNotificationBusInMemory") EventBus notificationBus, IdService idService, NotificationQueryModelMapper notificationQueryModelMapper, PlayerRepository playerRepository) {
    this.notificationBus = notificationBus;
    this.idService = idService;
    this.notificationQueryModelMapper = notificationQueryModelMapper;
    this.playerRepository = playerRepository;
  }

  @GetMapping("/stream/game/{gameId}")
  public SseEmitter streamSseMvc(@PathVariable String gameId) {
    var clientId = idService.generate();
    logger.debug(clientId + " is now connected");
    var emitter = new SseEmitter(TROIS_HEURES);
    var sseMvcExecutor = Executors.newSingleThreadExecutor();
    sseMvcExecutor.execute(() -> {
      var notificationHandler = new SseNotificationHandler(emitter, notificationBus, playerRepository, notificationQueryModelMapper, gameId, clientId);
      notificationBus.addListener(notificationHandler);
    });

    var heartbeat = new Heartbeat(emitter, clientId);
    heartbeat.start();

    emitter.onError(e -> {
      heartbeat.stop();
      logger.debug("Stopping hearbeat for: " + clientId);
    });

    return emitter;
  }
}
