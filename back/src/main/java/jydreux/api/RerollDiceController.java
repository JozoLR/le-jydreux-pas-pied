package jydreux.api;

import jydreux.api.resources.rolls.RerollDiceRollResource;
import jydreux.domain.roll.command.RerollDiceCommand;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/reroll")
public class RerollDiceController {
  private final RerollDiceCommand rerollDice;

  public RerollDiceController(RerollDiceCommand rerollDice) {
    this.rerollDice = rerollDice;
  }

  @PutMapping
  public ResponseEntity<String> handle(@RequestBody @Valid RerollDiceRollResource rerollDiceRollResource) {
    rerollDice.handle(rerollDiceRollResource.getPlayerId());
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
