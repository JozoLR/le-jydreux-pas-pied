package jydreux.api;

import jydreux.api.resources.rolls.DiceRollResource;
import jydreux.domain.roll.RollService;
import jydreux.domain.roll.command.RollDiceCommand;
import jydreux.domain.rollsPropositions.RollProposition;
import jydreux.domain.rollsPropositions.query.GetRollsPropositions;
import jydreux.service.UUID;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/roll")
public class RollController {
  private final RollDiceCommand rollDice;
  private final RollService rollService;
  private final GetRollsPropositions getRollsPropositions;


  public RollController(RollDiceCommand rollDice, RollService rollService, GetRollsPropositions getRollsPropositions) {
    this.rollDice = rollDice;
    this.rollService = rollService;
    this.getRollsPropositions = getRollsPropositions;
  }

  @PutMapping
  public ResponseEntity<String> put(@RequestBody DiceRollResource diceRollResource) {
    rollDice.handle(diceRollResource.buildCommand(rollService));
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @GetMapping("/proposition/{playerId}")
  public RollProposition getPropositions(@PathVariable @Valid @UUID String playerId) {
    return getRollsPropositions.handle(playerId);
  }
}
