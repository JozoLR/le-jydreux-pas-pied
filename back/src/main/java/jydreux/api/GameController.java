package jydreux.api;

import jydreux.api.resources.CreateGameResource;
import jydreux.domain.game.Game;
import jydreux.domain.game.command.CreateGameCommand;
import jydreux.domain.notification.Notification;
import jydreux.domain.notification.query.GetNotifications;
import jydreux.query.GetGame;
import jydreux.query.ListGames;
import jydreux.query.model.GameQueryModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@RequestMapping("/game")
public class GameController {

  private final CreateGameCommand createGame;
  private final ListGames listGames;
  private final GetGame getGame;
  private final GetNotifications getNotifications;

  public GameController(CreateGameCommand createGame, ListGames listGames, GetGame getGame, GetNotifications getNotifications) {
    this.createGame = createGame;
    this.listGames = listGames;
    this.getGame = getGame;
    this.getNotifications = getNotifications;
  }

  @PutMapping
  public ResponseEntity<Game> put(JwtAuthenticationToken authentication, @RequestBody @Valid CreateGameResource createGameResource) {
    var game = this.createGame.handle(createGameResource.toDomain(authentication.getToken().getTokenValue()));
    return new ResponseEntity<>(game, HttpStatus.OK);
  }

  @GetMapping
  public List<Game> list(JwtAuthenticationToken authentication) {
    return this.listGames.handle(authentication.getToken().getTokenValue());
  }

  @GetMapping("/{gameId}")
  public GameQueryModel get(@PathVariable String gameId) {
    return this.getGame.handle(gameId);
  }

  @GetMapping("/{gameId}/notification")
  public List<Notification<?>> getNotifications(@PathVariable String gameId) {
    return this.getNotifications.handle(gameId);
  }
}
