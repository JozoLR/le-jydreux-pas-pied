package jydreux.config;

import org.springframework.stereotype.Service;

@Service
public class Config {
  public final Integer port;
  public final String dbUrl;
  public final String dbUser;
  public final String dbPassword;
  public final String authIssuer;
  public final String authAudience;
  public final String authDomain;

  public Config() {
    this.port = Integer.parseInt(getEnvOrDefault("PORT", "8080"));
    this.dbUrl = getEnvOrDefault("DATABASE_URL", "jdbc:postgresql://localhost:54777/jydreux");
    this.dbUser = getEnvOrDefault("DATABASE_USERNAME", "jydreux");
    this.dbPassword = getEnvOrDefault("DATABASE_PASSWORD", "jydreux");
    this.authIssuer = getEnvOrDefault("AUTH_ISSUER", "Nope");
    this.authAudience = getEnvOrDefault("AUTH_AUDIENCE", "http://localhost:8080");
    this.authDomain = getEnvOrDefault("AUTH_DOMAIN", "nope");
  }

  public static String getEnvOrDefault(String envVariableName, String defaultValue) {
    var envVariale = System.getenv(envVariableName);
    if (envVariale == null) {
      envVariale = defaultValue;
    }
    return envVariale;
  }
}
