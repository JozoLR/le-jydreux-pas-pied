package jydreux.query;

import jydreux.domain.game.Game;
import jydreux.domain.game.GameRepository;
import jydreux.domain.user.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListGames {

  private final GameRepository gameRepository;
  private final UserRepository userRepository;

  public ListGames(GameRepository gameRepository, UserRepository userRepository) {
    this.gameRepository = gameRepository;
    this.userRepository = userRepository;
  }

  public List<Game> handle(String token) {
    var user = userRepository.getByToken(token);
    return gameRepository.findByUser(user);
  }

}
