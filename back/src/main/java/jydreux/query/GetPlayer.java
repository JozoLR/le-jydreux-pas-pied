package jydreux.query;

import jydreux.domain.game.PlayerRepository;
import jydreux.domain.roll.RollRepository;
import jydreux.domain.rules.RulesRepository;
import jydreux.query.model.PlayerQueryModel;
import org.springframework.stereotype.Service;

@Service
public class GetPlayer {
  private final PlayerRepository playerSqlRepository;
  private final RollRepository rollRepository;
  private final RulesRepository rulesRepository;

  public GetPlayer(PlayerRepository playerSqlRepository, RollRepository rollRepository, RulesRepository rulesRepository) {
    this.playerSqlRepository = playerSqlRepository;
    this.rollRepository = rollRepository;
    this.rulesRepository = rulesRepository;
  }

  public PlayerQueryModel handle(String id) {
    var player = playerSqlRepository.get(id);
    var lastRoll = rollRepository.getMoreRecentByPlayer(id);
    return PlayerQueryModel.from(player, rulesRepository.getGameRules(), lastRoll);
  }
}
