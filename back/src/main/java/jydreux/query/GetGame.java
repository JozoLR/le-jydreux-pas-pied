package jydreux.query;

import jydreux.domain.game.GameRepository;
import jydreux.domain.roll.RollRepository;
import jydreux.domain.rules.RulesRepository;
import jydreux.query.model.GameQueryModel;
import jydreux.query.model.PlayerQueryModel;
import org.springframework.stereotype.Service;

import static java.util.stream.Collectors.toList;

@Service
public class GetGame {
  private final GameRepository gameRepository;
  private final RollRepository rollRepository;
  private final RulesRepository rulesRepository;

  public GetGame(GameRepository gameRepository, RollRepository rollRepository, RulesRepository rulesRepository) {
    this.gameRepository = gameRepository;
    this.rollRepository = rollRepository;
    this.rulesRepository = rulesRepository;
  }

  public GameQueryModel handle(String gameId) {
    var game = gameRepository.get(gameId);
    var gameRules = rulesRepository.getGameRules();
    var playersQueryModel = game.getPlayers().stream()
      .map(player -> {
        var lastRoll = rollRepository.getMoreRecentByPlayer(player.getId());
        return PlayerQueryModel.from(player, gameRules, lastRoll);
      })
      .collect(toList());
    return new GameQueryModel(gameId, game.getName(), playersQueryModel, game.getUsers(), game.getGameMaster());
  }
}
