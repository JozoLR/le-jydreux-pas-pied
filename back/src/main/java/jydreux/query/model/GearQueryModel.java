package jydreux.query.model;

import jydreux.domain.game.model.Gear;

import java.util.List;

public class GearQueryModel {
  private final String id;
  private final String label;
  private final List<String> skillIds;
  private final Integer bonus;
  private final Integer malus;

  public GearQueryModel(Gear gear) {
    this.id = gear.getId();
    this.label = gear.getLabel();
    this.skillIds = gear.getSkillIds();
    this.bonus = gear.getBonus();
    this.malus = gear.getMalus();
  }

  public GearQueryModel(String id, String label, List<String> skillIds, Integer bonus, Integer malus) {
    this.id = id;
    this.label = label;
    this.skillIds = skillIds;
    this.bonus = bonus;
    this.malus = malus;
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public List<String> getSkillIds() {
    return skillIds;
  }

  public Integer getBonus() {
    return bonus;
  }

  public Integer getMalus() {
    return malus;
  }
}
