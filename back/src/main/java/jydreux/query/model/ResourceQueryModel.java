package jydreux.query.model;

import jydreux.domain.game.model.Resource;

public class ResourceQueryModel {
  private final String id;
  private final String label;
  private final Integer quantity;

  public ResourceQueryModel(Resource resource) {
    this.id = resource.getId();
    this.label = resource.getLabel();
    this.quantity = resource.getQuantity();
  }

  public ResourceQueryModel(String id, String label, Integer quantity) {
    this.id = id;
    this.label = label;
    this.quantity = quantity;
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public Integer getQuantity() {
    return quantity;
  }
}
