package jydreux.query.model;

import jydreux.domain.game.model.*;
import jydreux.domain.roll.model.Roll;
import jydreux.domain.rules.GameClass;
import jydreux.domain.rules.GameRules;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class PlayerQueryModel {
  private final String id;
  private final String name;
  private final List<AttributQueryModel> attributs;
  private final List<SkillQueryModel> skills;
  private final List<WeaponQueryModel> weapons;
  private final List<GearQueryModel> gears;
  private final List<ResourceQueryModel> resources;
  private final GameClassQueryModel gameClass;
  private final List<TalentQueryModel> talents;
  private final Integer experience;
  private final Integer rot;
  private final Integer mutation;
  private final Roll lastRoll;
  private final String gameId;
  private final String notes;

  public PlayerQueryModel(String id, String name, List<AttributQueryModel> attributs, List<SkillQueryModel> skills, List<WeaponQueryModel> weapons, List<GearQueryModel> gears, List<ResourceQueryModel> resources, GameClassQueryModel gameClass, List<TalentQueryModel> talents, Integer experience, Integer rot, Integer mutation, Roll lastRoll, String gameId, String notes) {
    this.id = id;
    this.name = name;
    this.attributs = attributs;
    this.skills = skills;
    this.weapons = weapons;
    this.gears = gears;
    this.resources = resources;
    this.gameClass = gameClass;
    this.talents = talents;
    this.experience = experience;
    this.rot = rot;
    this.mutation = mutation;
    this.lastRoll = lastRoll;
    this.gameId = gameId;
    this.notes = notes;
  }

  public static PlayerQueryModel from(Player player, GameRules gameRules, Roll lastRoll) {
    var playerClass = gameRules.getGameClasses().stream()
      .filter(gameClass -> gameClass.getId().equals(player.getGameClassId())).collect(toList()).get(0);
    var talents = gameRules.getTalents().stream()
      .filter(gameTalent -> player.getTalents().contains(gameTalent.getId())).collect(toList());
    return build(player, playerClass, talents, lastRoll);
  }

  private static PlayerQueryModel build(Player player, GameClass playerClass, List<jydreux.domain.rules.Talent> talents, Roll lastRoll) {
    return new PlayerQueryModel(player.getId(),
      player.getName(),
      buildAttributQueryModel(player.getAttributs()),
      buildSkillQueryModel(player.getSkills()),
      buildWeaponQueryModel(player.getWeapons()),
      buildGearQueryModel(player.getGears()),
      buildResourceQueryModel(player.getResources()),
      new GameClassQueryModel(playerClass),
      buildTalentsQueryModel(talents),
      player.getExperience(),
      player.getRot(),
      player.getMutation(),
      lastRoll,
      player.getGameId(),
      player.getNotes());
  }

  private static List<AttributQueryModel> buildAttributQueryModel(List<Attribut> attributs) {
    return attributs.stream().map(AttributQueryModel::new).collect(toList());
  }

  private static List<SkillQueryModel> buildSkillQueryModel(List<Skill> skills) {
    return skills.stream().map(SkillQueryModel::new).collect(toList());
  }

  private static List<TalentQueryModel> buildTalentsQueryModel(List<jydreux.domain.rules.Talent> talents) {
    return talents.stream().map(TalentQueryModel::new).collect(toList());
  }

  private static List<WeaponQueryModel> buildWeaponQueryModel(List<Weapon> weapons) {
    return weapons.stream().map(WeaponQueryModel::new).collect(toList());
  }

  private static List<GearQueryModel> buildGearQueryModel(List<Gear> gears) {
    return gears.stream().map(GearQueryModel::new).collect(toList());
  }

  private static List<ResourceQueryModel> buildResourceQueryModel(List<Resource> resources) {
    return resources.stream().map(ResourceQueryModel::new).collect(toList());
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public List<AttributQueryModel> getAttributs() {
    return attributs;
  }

  public List<GearQueryModel> getGears() {
    return gears;
  }

  public List<ResourceQueryModel> getResources() {
    return resources;
  }

  public String getGameId() {
    return gameId;
  }

  public List<SkillQueryModel> getSkills() {
    return skills;
  }

  public List<WeaponQueryModel> getWeapons() {
    return weapons;
  }

  public GameClassQueryModel getGameClass() {
    return gameClass;
  }

  public List<TalentQueryModel> getTalents() {
    return talents;
  }

  public Integer getExperience() {
    return experience;
  }

  public Integer getRot() {
    return rot;
  }

  public Integer getMutation() {
    return mutation;
  }

  public Roll getLastRoll() {
    return lastRoll;
  }

  public String getNotes() {
    return notes;
  }
}
