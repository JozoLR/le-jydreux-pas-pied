package jydreux.query.model;

import jydreux.domain.rules.Talent;

public class TalentQueryModel {
  private final String id;
  private final String label;
  private final String gameClassId;
  private final String description;

  public TalentQueryModel(Talent talent) {
    this.id = talent.getId();
    this.label = talent.getLabel();
    this.gameClassId = talent.getGameClassId();
    this.description = talent.getDescription();
  }

  public TalentQueryModel(String id, String label, String gameClassId, String description) {
    this.id = id;
    this.label = label;
    this.gameClassId = gameClassId;
    this.description = description;
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public String getGameClassId() {
    return gameClassId;
  }

  public String getDescription() {
    return description;
  }
}
