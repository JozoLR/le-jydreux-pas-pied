package jydreux.query.model;

import jydreux.domain.rules.Attribut;
import jydreux.domain.rules.Skill;

public class GameCompetenceQueryModel {
  private final String id;
  private final String label;

  public GameCompetenceQueryModel(Attribut attribut) {
    this.id = attribut.getKey();
    this.label = attribut.getLabel();
  }

  public GameCompetenceQueryModel(Skill skill) {
    this.id = skill.getId();
    this.label = skill.getLabel();
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }
}
