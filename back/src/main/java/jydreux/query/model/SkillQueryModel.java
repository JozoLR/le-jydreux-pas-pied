package jydreux.query.model;

import jydreux.domain.game.model.Skill;

public class SkillQueryModel {
  private final String id;
  private final String label;
  private final Integer actual;
  private final String skillType;

  public SkillQueryModel(Skill skill) {
    this.id = skill.getId();
    this.label = skill.getLabel();
    this.actual = skill.getActual();
    this.skillType = skill.getSkillType().name();
  }

  public SkillQueryModel(String id, String label, Integer actual, String skillType) {
    this.id = id;
    this.label = label;
    this.actual = actual;
    this.skillType = skillType;
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public Integer getActual() {
    return actual;
  }

  public String getSkillType() {
    return skillType;
  }
}
