package jydreux.query.model;

import jydreux.domain.game.model.Weapon;

public class WeaponQueryModel {
  private final String id;
  private final String label;
  private final String skillId;
  private final String resourceId;
  private final Integer bonus;
  private final Integer damage;
  private final String scope;
  private final Integer malus;

  public WeaponQueryModel(Weapon weapon) {
    this.id = weapon.getId();
    this.label = weapon.getLabel();
    this.skillId = weapon.getSkillId();
    this.resourceId = weapon.getResourceId();
    this.bonus = weapon.getBonus();
    this.damage = weapon.getDamage();
    this.scope = weapon.getScope();
    this.malus = weapon.getMalus();
  }

  public WeaponQueryModel(String id, String label, String skillId, String resourceId, Integer bonus, Integer damage, String scope, Integer malus) {
    this.id = id;
    this.label = label;
    this.skillId = skillId;
    this.resourceId = resourceId;
    this.bonus = bonus;
    this.damage = damage;
    this.scope = scope;
    this.malus = malus;
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public String getSkillId() {
    return skillId;
  }

  public Integer getBonus() {
    return bonus;
  }

  public Integer getMalus() {
    return malus;
  }

  public String getResourceId() {
    return resourceId;
  }

  public Integer getDamage() {
    return damage;
  }

  public String getScope() {
    return scope;
  }
}
