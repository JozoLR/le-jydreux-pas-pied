package jydreux.query.model;

import jydreux.domain.game.model.Attribut;

public class AttributQueryModel {
  private final String id;
  private final String label;
  private final Integer base;
  private final Integer actual;

  public AttributQueryModel(Attribut attribut) {
    this.id = attribut.getId();
    this.label = attribut.getLabel();
    this.base = attribut.getBase();
    this.actual = attribut.getActual();
  }

  public AttributQueryModel(String id, String label, Integer base, Integer actual) {
    this.id = id;
    this.label = label;
    this.base = base;
    this.actual = actual;
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public Integer getBase() {
    return base;
  }

  public Integer getActual() {
    return actual;
  }
}
