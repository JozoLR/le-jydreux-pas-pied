package jydreux.query.model;

import jydreux.domain.rules.*;

import java.util.List;
import java.util.stream.Collectors;

public class GameRulesQueryModel {

  private final List<GameCompetenceQueryModel> attributs;
  private final List<GameCompetenceQueryModel> skills;
  private final List<WeaponRangeQueryModel> weaponRanges;
  private final List<DiceQueryModel> dice;
  private final List<GameClassQueryModel> gameClasses;
  private final List<GameResourceQueryModel> resources;
  private final List<TalentQueryModel> talents;

  public GameRulesQueryModel(GameRules gameRules) {
    this.attributs = getAttributsQueryModel(gameRules.getAttributs());
    this.skills = getSkillsQueryModel(gameRules.getSkills());
    this.weaponRanges = getWeaponRangeQueryModel(gameRules.getWeaponRanges());
    this.dice = getDiceQueryModel(gameRules.getDice());
    this.gameClasses = getGameClassQueryModelModel(gameRules.getGameClasses());
    this.resources = getGameResourceQueryModel(gameRules.getResources());
    this.talents = getTalentQueryModel(gameRules.getTalents());
  }

  public List<GameCompetenceQueryModel> getAttributs() {
    return attributs;
  }

  public List<DiceQueryModel> getDice() {
    return dice;
  }

  public List<GameResourceQueryModel> getResources() {
    return resources;
  }

  public List<GameCompetenceQueryModel> getSkills() {
    return skills;
  }

  public List<WeaponRangeQueryModel> getWeaponRanges() {
    return weaponRanges;
  }

  public List<GameClassQueryModel> getGameClasses() {
    return gameClasses;
  }

  public List<TalentQueryModel> getTalents() {
    return talents;
  }

  private List<GameCompetenceQueryModel> getAttributsQueryModel(List<Attribut> competences) {
    return competences.stream().map(GameCompetenceQueryModel::new)
      .collect(Collectors.toList());
  }

  private List<GameCompetenceQueryModel> getSkillsQueryModel(List<Skill> competences) {
    return competences.stream().map(GameCompetenceQueryModel::new)
      .collect(Collectors.toList());
  }

  private List<WeaponRangeQueryModel> getWeaponRangeQueryModel(List<WeaponRange> weaponRanges) {
    return weaponRanges.stream().map(WeaponRangeQueryModel::new)
      .collect(Collectors.toList());
  }

  private List<DiceQueryModel> getDiceQueryModel(List<Dice> dice) {
    return dice.stream().map(DiceQueryModel::new)
      .collect(Collectors.toList());
  }

  private List<GameClassQueryModel> getGameClassQueryModelModel(List<GameClass> gameClasses) {
    return gameClasses.stream().map(GameClassQueryModel::new)
      .collect(Collectors.toList());
  }

  private List<GameResourceQueryModel> getGameResourceQueryModel(List<Resource> resources) {
    return resources.stream().map(GameResourceQueryModel::new)
      .collect(Collectors.toList());
  }

  private List<TalentQueryModel> getTalentQueryModel(List<Talent> talents) {
    return talents.stream().map(TalentQueryModel::new)
      .collect(Collectors.toList());
  }
}
