package jydreux.query.model;

import jydreux.domain.rules.WeaponRange;

public class WeaponRangeQueryModel {
  private final String id;
  private final String label;
  private final Integer effect;

  public WeaponRangeQueryModel(WeaponRange weaponRange) {
    this.id = weaponRange.getId();
    this.label = weaponRange.getLabel();
    this.effect = weaponRange.getEffect();
  }

  public String getLabel() {
    return label;
  }

  public String getId() {
    return id;
  }

  public Integer getEffect() {
    return effect;
  }
}
