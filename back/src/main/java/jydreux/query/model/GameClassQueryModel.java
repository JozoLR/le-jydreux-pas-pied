package jydreux.query.model;

import jydreux.domain.rules.GameClass;

public class GameClassQueryModel {
  private final String id;
  private final String label;
  private final String skillId;

  public GameClassQueryModel(GameClass gameClass) {
    this.id = gameClass.getId();
    this.label = gameClass.getLabel();
    this.skillId = gameClass.getSkillId();
  }

  public GameClassQueryModel(String id, String label, String skillId) {
    this.id = id;
    this.label = label;
    this.skillId = skillId;
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public String getSkillId() {
    return skillId;
  }
}
