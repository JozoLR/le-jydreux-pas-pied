package jydreux.query.model;

import jydreux.domain.rules.Resource;

public class GameResourceQueryModel {
  private final String id;
  private final String label;

  public GameResourceQueryModel(Resource resource) {
    this.id = resource.getId();
    this.label = resource.getLabel();
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }
}
