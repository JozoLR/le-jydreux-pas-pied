package jydreux.query.model;

import jydreux.domain.rules.Dice;

public class DiceQueryModel {
  private final String category;
  private final Integer number;

  public DiceQueryModel(Dice die) {
    this.category = die.getCategory();
    this.number = die.getNumber();
  }

  public String getCategory() {
    return category;
  }

  public Integer getNumber() {
    return number;
  }
}
