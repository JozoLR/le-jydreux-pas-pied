package jydreux.query.model;

import java.util.List;

public class GameQueryModel {
  private final String id;
  private final String name;
  private final List<PlayerQueryModel> players;
  private final List<String> users;
  private final String gameMaster;

  public GameQueryModel(String id, String name, List<PlayerQueryModel> players, List<String> users, String gameMaster) {
    this.id = id;
    this.name = name;
    this.players = players;
    this.users = users;
    this.gameMaster = gameMaster;
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public List<PlayerQueryModel> getPlayers() {
    return players;
  }

  public String getGameMaster() {
    return gameMaster;
  }

  public List<String> getUsers() {
    return users;
  }
}
