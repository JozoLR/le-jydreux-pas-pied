package jydreux.query;

import jydreux.domain.rules.RulesRepository;
import jydreux.query.model.GameRulesQueryModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/game/rules")
public class GetGameRules {

  private final RulesRepository rulesRepository;

  public GetGameRules(RulesRepository rulesRepository) {
    this.rulesRepository = rulesRepository;
  }

  @GetMapping("")
  public GameRulesQueryModel handle() {
    var gameRules = rulesRepository.getGameRules();
    return new GameRulesQueryModel(gameRules);
  }

}
