package jydreux.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

public class Heartbeat implements Runnable {

  private final SseEmitter emitter;
  private final AtomicBoolean running = new AtomicBoolean(false);
  private Thread worker;
  private final String clientId;
  private final Logger logger = LoggerFactory.getLogger(Heartbeat.class);

  public Heartbeat(SseEmitter emitter, String clientId) {
    this.emitter = emitter;
    this.clientId = clientId;
  }

  public void start() {
    worker = new Thread(this);
    worker.start();
  }

  @Override
  public void run() {
    running.set(true);
    while (running.get()) {
      try {
        logger.debug("Send hearbeat to :" + clientId);
        var eventBuilder = SseEmitter.event().comment("heartbeat");
        emitter.send(eventBuilder);
        Thread.sleep(30000);

      } catch (InterruptedException | IOException e) {
        Thread.currentThread().interrupt();
        logger.debug("Heartbeat failed for:" + clientId);
        break;
      }
    }
  }

  public void stop() {
    logger.debug("Stopping thread for:" + clientId);
    running.set(false);
  }
}
