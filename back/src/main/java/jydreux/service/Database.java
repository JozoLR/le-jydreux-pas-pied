package jydreux.service;

import java.sql.Connection;

public interface Database {
  Connection getConnection();
}