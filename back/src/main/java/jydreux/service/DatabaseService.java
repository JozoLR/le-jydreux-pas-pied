package jydreux.service;

import jydreux.config.Config;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Service
public class DatabaseService implements Database {

  private final Connection connection;

  public DatabaseService(Config config) throws SQLException {
    this.connection = DriverManager.getConnection(config.dbUrl, config.dbUser, config.dbPassword);
  }

  public Connection getConnection() {
    return connection;
  }

}
