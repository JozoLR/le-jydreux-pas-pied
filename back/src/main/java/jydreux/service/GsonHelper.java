package jydreux.service;

import com.google.gson.JsonDeserializer;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializer;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class GsonHelper {
  public static final JsonDeserializer<ZonedDateTime> ZDT_DESERIALIZER = (json, typeOfT, context) -> {
    try {
      var jsonObject = json.getAsJsonObject();
      var date = jsonObject.get("dateTime").getAsJsonObject().get("date").getAsJsonObject();
      var year = date.get("year").getAsInt();
      var month = date.get("month").getAsInt();
      var day = date.get("day").getAsInt();

      var time = jsonObject.get("dateTime").getAsJsonObject().get("time").getAsJsonObject();
      var hour = time.get("hour").getAsInt();
      var minute = time.get("minute").getAsInt();
      var second = time.get("second").getAsInt();
      var nano = time.get("nano").getAsInt();

      var zone = jsonObject.get("dateTime").getAsJsonObject().get("zone").getAsString();

      return ZonedDateTime.of(year, month, day, hour, minute, second, nano, ZoneId.of(zone));

    } catch (RuntimeException e) {
      throw new JsonParseException("Unable to parse ZonedDateTime", e);
    }
  };

  public static final JsonSerializer<ZonedDateTime> ZDT_SERIALIZER = (src, typeOfSrc, context) -> {
    JsonObject date = new JsonObject();
    date.addProperty("year", src.getYear());
    date.addProperty("month", src.getMonthValue());
    date.addProperty("day", src.getDayOfMonth());

    JsonObject time = new JsonObject();
    time.addProperty("hour", src.getHour());
    time.addProperty("minute", src.getMinute());
    time.addProperty("second", src.getSecond());
    time.addProperty("nano", src.getNano());

    JsonObject dateTime = new JsonObject();
    dateTime.add("date", date);
    dateTime.add("time", time);
    dateTime.addProperty("zone", src.getZone().getId());

    JsonObject serialized = new JsonObject();
    serialized.add("dateTime", dateTime);

    return serialized;
  };
}
