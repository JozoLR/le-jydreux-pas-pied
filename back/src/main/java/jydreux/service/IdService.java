package jydreux.service;

import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class IdService {
  public String generate() {
    return String.valueOf(UUID.randomUUID());
  }
}
