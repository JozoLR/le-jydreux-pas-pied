package jydreux.service;

import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@Service
public class DateService {

  public ZonedDateTime now() {
    return ZonedDateTime.now(ZoneId.of("UTC"));
  }
}
