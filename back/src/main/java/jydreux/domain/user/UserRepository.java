package jydreux.domain.user;

public interface UserRepository {
  User getByToken(String token);
}
