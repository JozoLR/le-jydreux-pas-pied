package jydreux.domain.game.notification;

public class StuffDeleted {
  private final String playerId;
  private final String playerName;
  private final String stuffId;
  private final String stuffName;

  public StuffDeleted(String playerId, String playerName, String stuffId, String stuffName) {
    this.playerId = playerId;
    this.playerName = playerName;
    this.stuffId = stuffId;
    this.stuffName = stuffName;
  }

  public String getPlayerId() {
    return playerId;
  }

  public String getPlayerName() {
    return playerName;
  }

  public String getStuffId() {
    return stuffId;
  }

  public String getStuffName() {
    return stuffName;
  }
}
