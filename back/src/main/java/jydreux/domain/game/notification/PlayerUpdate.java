package jydreux.domain.game.notification;

public class PlayerUpdate {
  private final String id;
  private final String label;
  private final Integer previousValue;
  private final Integer currentValue;

  public PlayerUpdate(String id, String label, Integer previousValue, Integer currentValue) {
    this.id = id;
    this.label = label;
    this.previousValue = previousValue;
    this.currentValue = currentValue;
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public Integer getPreviousValue() {
    return previousValue;
  }

  public Integer getCurrentValue() {
    return currentValue;
  }
}
