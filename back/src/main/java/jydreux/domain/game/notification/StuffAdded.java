package jydreux.domain.game.notification;

public class StuffAdded {
  private final String id;
  private final String label;

  public StuffAdded(String id, String label) {
    this.id = id;
    this.label = label;
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }
}
