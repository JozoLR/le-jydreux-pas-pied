package jydreux.domain.game.notification;

public class StuffGiven {
  private final String beneficiaryId;
  private final String beneficiaryName;
  private final String stuffId;
  private final String stuffName;

  public StuffGiven(String beneficiaryId, String beneficiaryName, String stuffId, String stuffName) {
    this.beneficiaryId = beneficiaryId;
    this.beneficiaryName = beneficiaryName;
    this.stuffId = stuffId;
    this.stuffName = stuffName;
  }

  public String getBeneficiaryId() {
    return beneficiaryId;
  }

  public String getBeneficiaryName() {
    return beneficiaryName;
  }

  public String getStuffId() {
    return stuffId;
  }

  public String getStuffName() {
    return stuffName;
  }
}
