package jydreux.domain.game.notification;

import jydreux.domain.game.command.player.UpgradePlayerCommand;

import java.util.List;

public class PlayerUpgrade {
  private final List<String> skills;
  private final List<String> talents;

  public PlayerUpgrade(UpgradePlayerCommand command) {
    this.skills = command.getSkillIds();
    this.talents = command.getTalentIds();
  }

  public List<String> getSkills() {
    return skills;
  }

  public List<String> getTalents() {
    return talents;
  }
}
