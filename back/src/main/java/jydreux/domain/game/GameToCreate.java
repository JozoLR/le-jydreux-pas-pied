package jydreux.domain.game;

import java.util.List;

public class GameToCreate {
  private final String creatorToken;
  private final String name;
  private final List<String> users;

  public GameToCreate(String creatorToken, String name, List<String> users) {
    this.creatorToken = creatorToken;
    this.name = name;
    this.users = users;
  }

  public String getCreatorToken() {
    return creatorToken;
  }

  public String getName() {
    return name;
  }

  public List<String> getUsers() {
    return users;
  }
}
