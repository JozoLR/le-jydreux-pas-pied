package jydreux.domain.game.handler;

import com.google.common.eventbus.Subscribe;
import jydreux.domain.event.*;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.game.command.gear.GiveGearCommand;
import jydreux.domain.game.model.Gear;
import jydreux.domain.game.model.Player;
import jydreux.domain.game.notification.StuffGiven;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class GiveGearHandler implements EventHandler {

  private final PlayerRepository playerRepository;
  private final EventFactory eventFactory;
  private final EventBus eventBus;

  public GiveGearHandler(PlayerRepository playerRepository, EventFactory eventFactory, @Qualifier("eventBusInMemory") EventBus eventBus) {
    this.playerRepository = playerRepository;
    this.eventFactory = eventFactory;
    this.eventBus = eventBus;
    eventBus.addListener(this);
  }

  @Override
  @Subscribe
  public void handle(Event<?> event) {
    if (event.getType().equals(EventType.GIVE_GEAR)) {
      var command = (GiveGearCommand) event.getContent();
      var giver = playerRepository.get(command.getPlayerId());
      var optionalWeapon = giver.getGear(command.getGearId());

      optionalWeapon.ifPresent(gear -> {
        playerRepository.giveGear(gear.getId(), command.getBeneficiaryId());
        dispatchNotification(command, giver, gear);
      });
    }
  }

  private void dispatchNotification(GiveGearCommand command, Player giver, Gear gear) {
    var beneficiary = playerRepository.get(command.getBeneficiaryId());
    var stuffGiven = new StuffGiven(
      command.getBeneficiaryId(),
      beneficiary.getName(),
      gear.getId(),
      gear.getLabel()
    );
    var event = eventFactory.build(giver.getGameId(), giver.getId(), EventType.GEAR_GIVEN, stuffGiven);
    eventBus.dispatch(event);
  }
}
