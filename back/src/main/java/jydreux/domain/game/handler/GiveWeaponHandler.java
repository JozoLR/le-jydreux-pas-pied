package jydreux.domain.game.handler;

import com.google.common.eventbus.Subscribe;
import jydreux.domain.event.*;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.game.command.weapon.GiveWeaponCommand;
import jydreux.domain.game.model.Player;
import jydreux.domain.game.model.Weapon;
import jydreux.domain.game.notification.StuffGiven;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class GiveWeaponHandler implements EventHandler {

  private final PlayerRepository playerRepository;
  private final EventFactory eventFactory;
  private final EventBus eventBus;

  public GiveWeaponHandler(PlayerRepository playerRepository, EventFactory eventFactory, @Qualifier("eventBusInMemory") EventBus eventBus) {
    this.playerRepository = playerRepository;
    this.eventFactory = eventFactory;
    this.eventBus = eventBus;
    eventBus.addListener(this);
  }

  @Override
  @Subscribe
  public void handle(Event<?> event) {
    if (event.getType().equals(EventType.GIVE_WEAPON)) {
      var command = (GiveWeaponCommand) event.getContent();
      var giver = playerRepository.get(command.getGiverId());
      var optionalWeapon = giver.getWeapon(command.getWeaponId());

      optionalWeapon.ifPresent(weapon -> {
        playerRepository.giveWeapon(weapon.getId(), command.getBeneficiaryId());
        dispatchNotification(command, giver, weapon);
      });
    }
  }

  private void dispatchNotification(GiveWeaponCommand command, Player giver, Weapon weapon) {
    var beneficiary = playerRepository.get(command.getBeneficiaryId());
    var stuffGiven = new StuffGiven(
      command.getBeneficiaryId(),
      beneficiary.getName(),
      weapon.getId(),
      weapon.getLabel()
    );
    var event = eventFactory.build(giver.getGameId(), giver.getId(), EventType.WEAPON_GIVEN, stuffGiven);
    eventBus.dispatch(event);
  }
}
