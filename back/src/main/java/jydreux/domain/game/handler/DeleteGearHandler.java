package jydreux.domain.game.handler;

import com.google.common.eventbus.Subscribe;
import jydreux.domain.event.*;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.game.command.gear.DeleteGearCommand;
import jydreux.domain.game.notification.StuffDeleted;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class DeleteGearHandler implements EventHandler {

  private final PlayerRepository playerRepository;
  private final EventBus notificationBus;
  private final EventFactory eventFactory;


  public DeleteGearHandler(PlayerRepository playerRepository, @Qualifier("eventBusInMemory") EventBus eventBus, EventFactory eventFactory) {
    this.playerRepository = playerRepository;
    this.notificationBus = eventBus;
    this.eventFactory = eventFactory;
    eventBus.addListener(this);
  }

  @Override
  @Subscribe
  public void handle(Event<?> event) {
    if (event.getType().equals(EventType.DELETE_GEAR)) {
      var player = playerRepository.get(event.getPlayerId());
      var command = (DeleteGearCommand) event.getContent();

      player.getGear(command.getGearId()).ifPresent(
        gear -> {
          player.deleteGear(command.getGearId());
          playerRepository.deleteGear(command.getGearId());
          var stuffDeleted = new StuffDeleted(
            player.getId(),
            player.getName(),
            gear.getId(),
            gear.getLabel()
          );
          var stuffDeletedNotification = eventFactory.build(command.getGameId(), command.getPlayerId(), EventType.GEAR_DELETED, stuffDeleted);
          notificationBus.dispatch(stuffDeletedNotification);
        });
    }
  }
}
