package jydreux.domain.game.handler;

import com.google.common.eventbus.Subscribe;
import jydreux.domain.event.*;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.game.command.player.UpgradePlayerCommand;
import jydreux.domain.game.model.Player;
import jydreux.domain.game.notification.PlayerUpgrade;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class UpgradePlayerHandler implements EventHandler {

  public static final int UPGRADE_COST = 5;
  private final PlayerRepository playerRepository;
  private final EventFactory eventFactory;
  private final EventBus eventBus;

  public UpgradePlayerHandler(PlayerRepository playerRepository, EventFactory eventFactory, @Qualifier("eventBusInMemory") EventBus eventBus) {
    this.playerRepository = playerRepository;
    this.eventFactory = eventFactory;
    this.eventBus = eventBus;
    eventBus.addListener(this);
  }

  @Override
  @Subscribe
  public void handle(Event<?> event) {
    if (event.getType().equals(EventType.UPGRADE_PLAYER)) {
      var command = (UpgradePlayerCommand) event.getContent();
      var player = playerRepository.get(command.getPlayerId());

      var numberOfUpgrades = command.getSkillIds().size() + command.getTalentIds().size();
      if (player.getExperience() >= UPGRADE_COST * numberOfUpgrades) {
        doUpgrade(command, player);
        var playerUpgrade = new PlayerUpgrade(command);
        var playerUpgradeEvent = eventFactory.build(player.getGameId(), player.getId(), EventType.PLAYER_UPGRADED, playerUpgrade);
        eventBus.dispatch(playerUpgradeEvent);
      }
    }
  }

  private void doUpgrade(UpgradePlayerCommand command, Player player) {
    command.getSkillIds().forEach(skillId -> {
      player.updateExperience(player.getExperience() - 5);
      player.getSkill(skillId).get().upgradeActual();
    });

    command.getTalentIds().forEach(talentId -> {
      player.updateExperience(player.getExperience() - 5);
      player.addTalent(talentId);
    });
    playerRepository.update(player);
  }
}
