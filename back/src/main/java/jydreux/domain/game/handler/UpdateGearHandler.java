package jydreux.domain.game.handler;

import com.google.common.eventbus.Subscribe;
import jydreux.domain.event.*;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.game.command.gear.UpdateGearCommand;
import jydreux.domain.game.notification.PlayerUpdate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class UpdateGearHandler implements EventHandler {

  private final PlayerRepository playerRepository;
  private final EventFactory eventFactory;
  private final EventBus eventBus;

  public UpdateGearHandler(PlayerRepository playerRepository, EventFactory eventFactory, @Qualifier("eventBusInMemory") EventBus eventBus) {
    this.playerRepository = playerRepository;
    this.eventFactory = eventFactory;
    this.eventBus = eventBus;
    eventBus.addListener(this);
  }

  @Override
  @Subscribe
  public void handle(Event<?> event) {
    if (event.getType().equals(EventType.UPDATE_GEAR)) {
      var command = (UpdateGearCommand) event.getContent();
      var player = playerRepository.get(command.getPlayerId());
      var currentGear = player.getGears().stream().filter(gear -> gear.getId().equals(command.getId())).findAny();

      currentGear.ifPresent(gear -> {
        gear.setMalus(command.getMalus());
        var playerUpdate = new PlayerUpdate(command.getId(), gear.getLabel(), gear.getMalus(), gear.getMalus());
        player.updateGear(gear);
        playerRepository.update(player);
        var playerUpdateEvent = eventFactory.build(player.getGameId(), player.getId(), EventType.GEAR_UPDATED, playerUpdate);
        eventBus.dispatch(playerUpdateEvent);
      });
    }
  }
}
