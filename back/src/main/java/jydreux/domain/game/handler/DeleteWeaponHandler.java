package jydreux.domain.game.handler;

import com.google.common.eventbus.Subscribe;
import jydreux.domain.event.*;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.game.command.weapon.DeleteWeaponCommand;
import jydreux.domain.game.notification.StuffDeleted;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class DeleteWeaponHandler implements EventHandler {

  private final PlayerRepository playerRepository;
  private final EventBus notificationBus;
  private final EventFactory eventFactory;


  public DeleteWeaponHandler(PlayerRepository playerRepository, @Qualifier("eventBusInMemory") EventBus eventBus, EventFactory eventFactory) {
    this.playerRepository = playerRepository;
    this.notificationBus = eventBus;
    this.eventFactory = eventFactory;
    eventBus.addListener(this);
  }

  @Override
  @Subscribe
  public void handle(Event<?> event) {
    if (event.getType().equals(EventType.DELETE_WEAPON)) {
      var player = playerRepository.get(event.getPlayerId());
      var command = (DeleteWeaponCommand) event.getContent();

      player.getWeapon(command.getWeaponId()).ifPresent(
        weapon -> {
          playerRepository.deleteWeapon(command.getWeaponId());
          var stuffDeleted = new StuffDeleted(
            player.getId(),
            player.getName(),
            weapon.getId(),
            weapon.getLabel()
          );
          var stuffDeletedNotification = eventFactory.build(command.getGameId(), command.getPlayerId(), EventType.WEAPON_DELETED, stuffDeleted);
          notificationBus.dispatch(stuffDeletedNotification);
        });
    }
  }
}
