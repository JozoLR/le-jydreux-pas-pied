package jydreux.domain.game;

import jydreux.domain.game.model.Player;

import java.util.List;

public class Game {
  private final String id;
  private final String name;
  private final String gameMaster;
  private final List<String> users;
  private final List<Player> players;

  public Game(String id, String name, String gameMaster, List<String> users, List<Player> players) {
    this.id = id;
    this.name = name;
    this.gameMaster = gameMaster;
    this.users = users;
    this.players = players;
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getGameMaster() {
    return gameMaster;
  }

  public List<String> getUsers() {
    return users;
  }

  public List<Player> getPlayers() {
    return players;
  }
}
