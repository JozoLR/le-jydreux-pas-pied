package jydreux.domain.game.command.player;

import jydreux.api.resources.UpdatePlayerResource;
import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.game.notification.PlayerUpdate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class UpdatePlayerCommand {
  private final PlayerRepository playerRepository;
  private final EventFactory eventFactory;
  private final EventBus eventBus;

  public UpdatePlayerCommand(PlayerRepository playerRepository, EventFactory eventFactory, @Qualifier("eventBusInMemory") EventBus eventBus) {
    this.playerRepository = playerRepository;
    this.eventFactory = eventFactory;
    this.eventBus = eventBus;
  }

  public void handle(UpdatePlayerResource updatePlayerResource) {
    var player = playerRepository.get(updatePlayerResource.getPlayerId());

    if (!player.getExperience().equals(updatePlayerResource.getExperience())) {
      var playerUpdate = new PlayerUpdate("experience", "Experience", player.getExperience(), updatePlayerResource.getExperience());
      player.updateExperience(updatePlayerResource.getExperience());
      var event = eventFactory.build(player.getGameId(), player.getId(), EventType.PLAYER_UPDATED, playerUpdate);
      eventBus.dispatch(event);
    }

    if (!player.getRot().equals(updatePlayerResource.getRot())) {
      var playerUpdate = new PlayerUpdate("rot", "Rot", player.getRot(), updatePlayerResource.getRot());
      player.updateRot(updatePlayerResource.getRot());
      var event = eventFactory.build(player.getGameId(), player.getId(), EventType.PLAYER_UPDATED, playerUpdate);
      eventBus.dispatch(event);
    }

    if (!player.getMutation().equals(updatePlayerResource.getMutation())) {
      var playerUpdate = new PlayerUpdate("mutation", "Mutation", player.getMutation(), updatePlayerResource.getMutation());
      player.updateMutation(updatePlayerResource.getMutation());
      var event = eventFactory.build(player.getGameId(), player.getId(), EventType.PLAYER_UPDATED, playerUpdate);
      eventBus.dispatch(event);
    }
    playerRepository.update(player);
  }
}
