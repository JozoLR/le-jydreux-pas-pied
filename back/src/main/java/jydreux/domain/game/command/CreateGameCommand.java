package jydreux.domain.game.command;

import jydreux.domain.game.Game;
import jydreux.domain.game.GameRepository;
import jydreux.domain.game.GameToCreate;
import jydreux.domain.map.Map;
import jydreux.domain.map.MapRepository;
import jydreux.domain.user.UserRepository;
import jydreux.service.IdService;
import org.springframework.stereotype.Service;

@Service
public class CreateGameCommand {

  private final UserRepository userRepository;
  private final GameRepository gameRepository;
  private final MapRepository mapRepository;
  private final IdService idService;

  public CreateGameCommand(UserRepository userRepository, GameRepository gameRepository, MapRepository mapRepository, IdService idService) {
    this.userRepository = userRepository;
    this.gameRepository = gameRepository;
    this.mapRepository = mapRepository;
    this.idService = idService;
  }

  public Game handle(GameToCreate gameToCreate) {
    var gameMaster = this.userRepository.getByToken(gameToCreate.getCreatorToken());
    var id = this.idService.generate();
    var game = new Game(id, gameToCreate.getName(), gameMaster.getEmail(), gameToCreate.getUsers(), null);
    gameRepository.save(game);
    mapRepository.save(Map.generate(id));
    return game;
  }
}
