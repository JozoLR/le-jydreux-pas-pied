package jydreux.domain.game.command.player;

import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.game.model.Resource;
import jydreux.domain.game.notification.PlayerUpdate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class UpdateResourceCommand {

  private final PlayerRepository playerRepository;
  private final EventFactory eventFactory;
  private final EventBus eventBus;

  public UpdateResourceCommand(PlayerRepository playerRepository, EventFactory eventFactory, @Qualifier("eventBusInMemory") EventBus eventBus) {
    this.playerRepository = playerRepository;
    this.eventFactory = eventFactory;
    this.eventBus = eventBus;
  }

  public void handle(Resource resourceToUpdate, String playerId) {
    var player = playerRepository.get(playerId);
    var currentResource = player.getResource(resourceToUpdate.getId());

    currentResource.ifPresent(resource -> {
      var playerUpdate = new PlayerUpdate(resource.getId(), resource.getLabel(), resource.getQuantity(), resourceToUpdate.getQuantity());
      player.updateResource(resourceToUpdate);
      playerRepository.update(player);
      var event = eventFactory.build(player.getGameId(), playerId, EventType.RESOURCE_UPDATED, playerUpdate);
      eventBus.dispatch(event);
    });
  }
}
