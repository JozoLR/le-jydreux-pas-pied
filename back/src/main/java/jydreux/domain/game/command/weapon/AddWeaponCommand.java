package jydreux.domain.game.command.weapon;

import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.game.model.Weapon;
import jydreux.domain.game.notification.StuffAdded;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class AddWeaponCommand {
  private final PlayerRepository playerRepository;
  private final EventFactory eventFactory;
  private final EventBus eventBus;

  public AddWeaponCommand(PlayerRepository playerRepository, EventFactory eventFactory, @Qualifier("eventBusInMemory") EventBus eventBus) {
    this.playerRepository = playerRepository;
    this.eventFactory = eventFactory;
    this.eventBus = eventBus;
  }

  public void handle(Weapon weapon, String playerId) {
    playerRepository.addWeapon(playerId, weapon);
    var player = playerRepository.get(playerId);
    var weaponAdded = new StuffAdded(weapon.getId(), weapon.getLabel());
    var event = eventFactory.build(player.getGameId(), playerId, EventType.WEAPON_ADDED, weaponAdded);
    eventBus.dispatch(event);
  }
}
