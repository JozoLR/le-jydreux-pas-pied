package jydreux.domain.game.command.gear;

public class UpdateGearCommand {
  private final String id;
  private final Integer malus;
  private final String playerId;
  private final String gameId;

  public UpdateGearCommand(String id, Integer malus, String playerId, String gameId) {
    this.id = id;
    this.malus = malus;
    this.playerId = playerId;
    this.gameId = gameId;
  }

  public String getId() {
    return id;
  }

  public Integer getMalus() {
    return malus;
  }

  public String getPlayerId() {
    return playerId;
  }

  public String getGameId() {
    return gameId;
  }
}

