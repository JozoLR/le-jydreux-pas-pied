package jydreux.domain.game.command.player;

import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.game.model.Skill;
import jydreux.domain.game.notification.PlayerUpdate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class UpdateSkillCommand {

  private final PlayerRepository playerRepository;
  private final EventFactory eventFactory;
  private final EventBus eventBus;

  public UpdateSkillCommand(PlayerRepository playerRepository, EventFactory eventFactory, @Qualifier("eventBusInMemory") EventBus eventBus) {
    this.playerRepository = playerRepository;
    this.eventFactory = eventFactory;
    this.eventBus = eventBus;
  }

  public void handle(Skill skillToUpdate, String playerId) {
    var player = playerRepository.get(playerId);
    var currentSkill = player.getSkill(skillToUpdate.getId());

    currentSkill.ifPresent(skill -> {
      var playerUpdate = new PlayerUpdate(skill.getId(), skill.getLabel(), skill.getActual(), skillToUpdate.getActual());
      player.updateSkill(skillToUpdate);
      playerRepository.update(player);
      var event = eventFactory.build(player.getGameId(), playerId, EventType.SKILL_UPDATED, playerUpdate);
      eventBus.dispatch(event);
    });
  }
}
