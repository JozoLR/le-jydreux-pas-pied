package jydreux.domain.game.command.player;

import java.util.List;

public class UpgradePlayerCommand {
  private final String playerId;
  private final List<String> skillIds;
  private final List<String> talentIds;

  public UpgradePlayerCommand(String playerId, List<String> skillIds, List<String> talentIds) {
    this.playerId = playerId;
    this.skillIds = skillIds;
    this.talentIds = talentIds;
  }

  public String getPlayerId() {
    return playerId;
  }

  public List<String> getSkillIds() {
    return skillIds;
  }

  public List<String> getTalentIds() {
    return talentIds;
  }
}
