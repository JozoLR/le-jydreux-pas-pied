package jydreux.domain.game.command.weapon;

import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.game.model.Weapon;
import jydreux.domain.game.notification.PlayerUpdate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class UpdateWeaponCommand {

  private final PlayerRepository playerRepository;
  private final EventFactory eventFactory;
  private final EventBus eventBus;

  public UpdateWeaponCommand(PlayerRepository playerRepository, EventFactory eventFactory, @Qualifier("eventBusInMemory") EventBus eventBus) {
    this.playerRepository = playerRepository;
    this.eventFactory = eventFactory;
    this.eventBus = eventBus;
  }

  public void handle(Weapon weaponToUpdate, String playerId) {
    var player = playerRepository.get(playerId);
    var currentWeapon = player.getWeapon(weaponToUpdate.getId());

    currentWeapon.ifPresent(weapon -> {
      var playerUpdate = new PlayerUpdate(weapon.getId(), weapon.getLabel(), weapon.getMalus(), weaponToUpdate.getMalus());
      player.updateWeapon(weaponToUpdate);
      playerRepository.update(player);
      var event = eventFactory.build(player.getGameId(), playerId, EventType.WEAPON_UPDATED, playerUpdate);
      eventBus.dispatch(event);
    });
  }
}
