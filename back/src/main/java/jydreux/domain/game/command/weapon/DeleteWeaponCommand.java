package jydreux.domain.game.command.weapon;

public class DeleteWeaponCommand {
  private final String gameId;
  private final String playerId;
  private final String weaponId;

  public DeleteWeaponCommand(String gameId, String playerId, String weaponId) {
    this.gameId = gameId;
    this.playerId = playerId;
    this.weaponId = weaponId;
  }

  public String getGameId() {
    return gameId;
  }

  public String getPlayerId() {
    return playerId;
  }

  public String getWeaponId() {
    return weaponId;
  }
}
