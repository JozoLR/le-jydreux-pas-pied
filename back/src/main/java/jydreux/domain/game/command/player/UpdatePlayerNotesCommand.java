package jydreux.domain.game.command.player;

import jydreux.domain.game.PlayerRepository;
import org.springframework.stereotype.Service;

@Service
public class UpdatePlayerNotesCommand {
  private final PlayerRepository playerRepository;

  public UpdatePlayerNotesCommand(PlayerRepository playerRepository) {
    this.playerRepository = playerRepository;
  }

  public void handle(String playerId, String notes) {
    var player = playerRepository.get(playerId);
    player.updateNotes(notes);
    playerRepository.update(player);
  }
}
