package jydreux.domain.game.command.weapon;

public class GiveWeaponCommand {
  private final String weaponId;
  private final String giverId;
  private final String beneficiaryId;

  public GiveWeaponCommand(String weaponId, String giverId, String beneficiaryId) {
    this.weaponId = weaponId;
    this.giverId = giverId;
    this.beneficiaryId = beneficiaryId;
  }

  public String getWeaponId() {
    return weaponId;
  }

  public String getGiverId() {
    return giverId;
  }

  public String getBeneficiaryId() {
    return beneficiaryId;
  }
}
