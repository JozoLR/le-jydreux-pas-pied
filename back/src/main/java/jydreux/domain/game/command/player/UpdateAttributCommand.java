package jydreux.domain.game.command.player;

import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.game.model.Attribut;
import jydreux.domain.game.notification.PlayerUpdate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class UpdateAttributCommand {

  private final PlayerRepository playerRepository;
  private final EventFactory eventFactory;
  private final EventBus eventBus;

  public UpdateAttributCommand(PlayerRepository playerRepository, EventFactory eventFactory, @Qualifier("eventBusInMemory") EventBus eventBus) {
    this.playerRepository = playerRepository;
    this.eventFactory = eventFactory;
    this.eventBus = eventBus;
  }

  public void handle(Attribut attributToUpdate, String playerId) {
    var player = playerRepository.get(playerId);
    var currentAttribut = player.getAttribut(attributToUpdate.getId());

    currentAttribut.ifPresent(attribut -> {
      var playerUpdate = new PlayerUpdate(attribut.getId(), attribut.getLabel(), attribut.getActual(), attributToUpdate.getActual());
      player.updateAttribut(attributToUpdate);
      playerRepository.update(player);
      var event = eventFactory.build(player.getGameId(), playerId, EventType.ATTRIBUT_UPDATED, playerUpdate);
      eventBus.dispatch(event);
    });
  }
}
