package jydreux.domain.game.command.player;

import jydreux.domain.game.PlayerRepository;
import jydreux.domain.playerCreation.PlayerToCreate;
import jydreux.domain.rules.RulesRepository;
import org.springframework.stereotype.Service;

@Service
public class CreatePlayerCommand {

  private final PlayerRepository playerRepository;
  private final RulesRepository rulesRepository;

  public CreatePlayerCommand(PlayerRepository playerRepository, RulesRepository rulesRepository) {
    this.playerRepository = playerRepository;
    this.rulesRepository = rulesRepository;
  }

  public String handle(PlayerToCreate playerToCreate) {
    var gameSkills = rulesRepository.getGameRules();
    var player = playerToCreate.toPlayer(gameSkills);
    this.playerRepository.save(player);
    return playerToCreate.getId();
  }
}
