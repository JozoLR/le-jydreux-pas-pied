package jydreux.domain.game.command.gear;

public class DeleteGearCommand {
  private final String gameId;
  private final String playerId;
  private final String gearId;

  public DeleteGearCommand(String gameId, String playerId, String gearId) {
    this.gameId = gameId;
    this.playerId = playerId;
    this.gearId = gearId;
  }

  public String getGameId() {
    return gameId;
  }

  public String getPlayerId() {
    return playerId;
  }

  public String getGearId() {
    return gearId;
  }
}
