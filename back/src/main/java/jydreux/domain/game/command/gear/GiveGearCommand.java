package jydreux.domain.game.command.gear;

public class GiveGearCommand {
  private final String gearId;
  private final String playerId;
  private final String beneficiaryId;
  private final String gameId;

  public GiveGearCommand(String gearId, String playerId, String beneficiaryId, String gameId) {
    this.gearId = gearId;
    this.playerId = playerId;
    this.beneficiaryId = beneficiaryId;
    this.gameId = gameId;
  }

  public String getGearId() {
    return gearId;
  }

  public String getPlayerId() {
    return playerId;
  }

  public String getBeneficiaryId() {
    return beneficiaryId;
  }

  public String getGameId() {
    return gameId;
  }
}
