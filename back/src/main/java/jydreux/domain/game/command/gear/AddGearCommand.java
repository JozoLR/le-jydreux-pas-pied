package jydreux.domain.game.command.gear;

import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.game.model.Gear;
import jydreux.domain.game.notification.StuffAdded;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class AddGearCommand {
  private final PlayerRepository playerRepository;
  private final EventFactory eventFactory;
  private final EventBus eventBus;

  public AddGearCommand(PlayerRepository playerRepository, EventFactory eventFactory, @Qualifier("eventBusInMemory") EventBus eventBus) {
    this.playerRepository = playerRepository;
    this.eventFactory = eventFactory;
    this.eventBus = eventBus;
  }

  public void handle(Gear gear, String playerId) {
    playerRepository.addGear(playerId, gear);
    var player = playerRepository.get(playerId);
    var gearAdded = new StuffAdded(gear.getId(), gear.getLabel());
    var event = eventFactory.build(player.getGameId(), playerId, EventType.GEAR_ADDED, gearAdded);
    eventBus.dispatch(event);
  }
}
