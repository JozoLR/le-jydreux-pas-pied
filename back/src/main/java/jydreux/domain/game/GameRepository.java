package jydreux.domain.game;

import jydreux.domain.game.model.Gear;
import jydreux.domain.game.model.Weapon;
import jydreux.domain.user.User;

import java.util.List;

public interface GameRepository {
  Game get(String id);

  void save(Game game);

  List<Game> findByUser(User user);

  Game findByPlayer(String playerId);

  Gear getGear(String gearId);

  Weapon getWeapon(String weaponId);
}
