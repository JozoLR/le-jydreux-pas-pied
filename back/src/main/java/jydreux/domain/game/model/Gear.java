package jydreux.domain.game.model;

import java.util.List;

public class Gear {
  private final String id;
  private final String label;
  private final List<String> skillIds;
  private final Integer bonus;
  private Integer malus;

  public Gear(String id, String label, List<String> skillIds, Integer bonus) {
    this.id = id;
    this.label = label;
    this.skillIds = skillIds;
    this.bonus = bonus;
    this.malus = 0;
  }

  public Gear(String id, String label, List<String> skillIds, Integer bonus, Integer malus) {
    this.id = id;
    this.label = label;
    this.skillIds = skillIds;
    this.bonus = bonus;
    this.malus = malus;
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public List<String> getSkillIds() {
    return skillIds;
  }

  public Integer getBonus() {
    return bonus;
  }

  public Integer getMalus() {
    return malus;
  }

  public void setMalus(Integer malus) {
    this.malus = malus;
  }
}
