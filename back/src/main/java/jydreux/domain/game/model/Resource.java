package jydreux.domain.game.model;

public class Resource {
  private final String id;
  private final String label;
  private final Integer quantity;

  public Resource(String id, String label, Integer quantity) {
    this.id = id;
    this.label = label;
    this.quantity = quantity;
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public Integer getQuantity() {
    return quantity;
  }
}
