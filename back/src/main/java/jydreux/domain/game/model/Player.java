package jydreux.domain.game.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

public class Player {
  private final String id;
  private final String name;
  private final String gameClassId;
  private final String gameId;
  private List<Attribut> attributs;
  private List<Skill> skills;
  private List<Weapon> weapons;
  private List<Gear> gears;
  private List<Resource> resources;
  private Integer experience;
  private Integer rot;
  private Integer mutation;
  private final List<String> talents;
  private String notes;

  public Player(String id, String name, List<Attribut> attributs, List<Skill> skills, List<Resource> resources, String gameClassId, String talentId, String gameId) {
    this.id = id;
    this.name = name;
    this.attributs = attributs;
    this.skills = skills;
    this.weapons = new ArrayList<>();
    this.gears = new ArrayList<>();
    this.resources = resources;
    this.gameClassId = gameClassId;
    this.experience = 0;
    this.rot = 0;
    this.mutation = 0;
    var talents = new ArrayList<String>();
    talents.add(talentId);
    this.talents = talents;
    this.gameId = gameId;
    this.notes = "";
  }

  public Player(String id, String name, List<Attribut> attributs, List<Skill> skills, List<Weapon> weapons, List<Gear> gears, List<Resource> resources, Integer experience, Integer rot, Integer mutation, String gameClassId, List<String> talents, String gameId, String notes) {
    this.id = id;
    this.name = name;
    this.attributs = attributs;
    this.skills = skills;
    this.weapons = weapons;
    this.gears = gears;
    this.resources = resources;
    this.experience = experience;
    this.rot = rot;
    this.mutation = mutation;
    this.gameClassId = gameClassId;
    this.talents = talents;
    this.gameId = gameId;
    this.notes = notes;
  }

  public static Player buildGameMaster(String gameId) {
    return new Player(null, "Game Master", null, null, null, null,null, gameId);
  }

  public void updateAttribut(Attribut newAttribut) {
    this.attributs = this.attributs.stream().map(attribut -> {
      if (attribut.getId().equals(newAttribut.getId())) {
        return new Attribut(newAttribut.getId(), newAttribut.getLabel(), attribut.getBase(), newAttribut.getActual());
      }
      return attribut;
    }).collect(toList());
  }

  public Optional<Attribut> getAttribut(String attributId) {
    return this.attributs.stream().filter(attribut -> attribut.getId().equals(attributId)).findAny();
  }

  public void updateSkill(Skill newSkill) {
    this.skills = this.skills.stream().map(skill -> {
      if (skill.getId().equals(newSkill.getId())) {
        return newSkill;
      }
      return skill;
    }).collect(toList());
  }

  public Optional<Skill> getSkill(String skillId) {
    return this.skills.stream().filter(skill -> skill.getId().equals(skillId)).findAny();
  }

  public void updateWeapon(Weapon newWeapon) {
    this.weapons = this.weapons.stream().map(weapon -> {
      if (weapon.getId().equals(newWeapon.getId())) {
        return newWeapon;
      }
      return weapon;
    }).collect(toList());
  }

  public Optional<Weapon> getWeapon(String weaponId) {
    return this.weapons.stream().filter(weapon -> weapon.getId().equals(weaponId)).findAny();
  }

  public void deleteWeapon(String weaponId) {
    this.weapons = this.weapons.stream().filter(weapon -> !weapon.getId().equals(weaponId)).collect(toList());
  }

  public void updateGear(Gear newGear) {
    this.gears = this.gears.stream().map(gear -> {
      if (gear.getId().equals(newGear.getId())) {
        return newGear;
      }
      return gear;
    }).collect(toList());
  }

  public Optional<Gear> getGear(String gearId) {
    return this.gears.stream().filter(gear -> gear.getId().equals(gearId)).findAny();
  }

  public void deleteGear(String gearId) {
    this.gears = this.gears.stream().filter(gear -> !gear.getId().equals(gearId)).collect(toList());
  }

  public void updateResource(Resource newResource) {
    this.resources = this.resources.stream().map(resource -> {
      if (resource.getLabel().equals(newResource.getLabel())) {
        return newResource;
      }
      return resource;
    }).collect(toList());
  }

  public Optional<Resource> getResource(String resourceId) {
    return this.resources.stream().filter(resource -> resource.getId().equals(resourceId)).findAny();
  }

  public void updateExperience(Integer experience) {
    this.experience = experience;
  }

  public void updateRot(Integer rot) {
    this.rot = rot;
  }

  public void updateMutation(Integer mutation) {
    this.mutation = mutation;
  }

  public void updateNotes(String notes) {
    this.notes = notes;
  }

  public void addTalent(String talentId) {
    this.talents.add(talentId);
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public List<Attribut> getAttributs() {
    return attributs;
  }

  public List<Skill> getSkills() {
    return skills;
  }

  public List<Weapon> getWeapons() {
    return weapons;
  }

  public List<Gear> getGears() {
    return gears;
  }

  public List<Resource> getResources() {
    return resources;
  }

  public Integer getExperience() {
    return experience;
  }

  public Integer getRot() {
    return rot;
  }

  public Integer getMutation() {
    return mutation;
  }

  public String getGameClassId() {
    return gameClassId;
  }

  public List<String> getTalents() {
    return talents;
  }

  public String getGameId() {
    return gameId;
  }

  public String getNotes() {
    return notes;
  }
}
