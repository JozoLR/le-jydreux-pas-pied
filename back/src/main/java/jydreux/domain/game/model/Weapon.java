package jydreux.domain.game.model;

public class Weapon {
  private final String id;
  private final String label;
  private final String skillId;
  private final Integer bonus;
  private final Integer damage;
  private final String scope;
  private final Integer malus;
  private final String resourceId;

  public Weapon(String id, String label, String skillId, String resourceId, Integer bonus, Integer degat, String scope) {
    this.id = id;
    this.label = label;
    this.skillId = skillId;
    this.resourceId = resourceId;
    this.bonus = bonus;
    this.damage = degat;
    this.scope = scope;
    this.malus = 0;
  }

  public Weapon(String id, String label, String skillId, Integer bonus, Integer damage, String scope, Integer malus, String resourceId) {
    this.id = id;
    this.label = label;
    this.skillId = skillId;
    this.bonus = bonus;
    this.damage = damage;
    this.scope = scope;
    this.malus = malus;
    this.resourceId = resourceId;
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public String getSkillId() {
    return skillId;
  }

  public Integer getBonus() {
    return bonus;
  }

  public Integer getDamage() {
    return damage;
  }

  public String getScope() {
    return scope;
  }

  public Integer getMalus() {
    return malus;
  }

  public String getResourceId() {
    return resourceId;
  }
}
