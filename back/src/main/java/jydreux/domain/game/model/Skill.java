package jydreux.domain.game.model;

import jydreux.domain.cross.SkillType;

public class Skill {
  private final String id;
  private final String label;
  private final SkillType skillType;
  private Integer actual;

  public Skill(String id, String label, Integer actual, SkillType skillType) {
    this.id = id;
    this.label = label;
    this.actual = actual;
    this.skillType = skillType;
  }

  public void upgradeActual() {
    this.actual = actual + 1;
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public Integer getActual() {
    return actual;
  }

  public SkillType getSkillType() {
    return skillType;
  }
}
