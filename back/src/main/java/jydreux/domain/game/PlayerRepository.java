package jydreux.domain.game;

import jydreux.domain.game.model.Gear;
import jydreux.domain.game.model.Player;
import jydreux.domain.game.model.Weapon;

import java.util.List;

public interface PlayerRepository {
  void save(Player player);

  Player get(String id);

  List<Player> getByGame(String gameId);

  void update(Player player);

  String getPlayerName(String playerId);

  void addGear(String playerId, Gear gear);

  void addWeapon(String playerId, Weapon weapon);

  void deleteWeapon(String weaponId);

  void giveWeapon(String weaponId, String playerId);

  void deleteGear(String gearId);

  void giveGear(String gearId, String playerId);
}
