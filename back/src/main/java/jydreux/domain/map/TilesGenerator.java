package jydreux.domain.map;

import jydreux.domain.map.model.Tile;

import java.util.ArrayList;
import java.util.List;

public class TilesGenerator {
  public static List<Tile> get() {
    var tiles = new ArrayList<Tile>();
    for (var index = 1; index <= 30 * 21; index++) {
      tiles.add(Tile.generate(index));
    }
    return tiles;
  }
}
