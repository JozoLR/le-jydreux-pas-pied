package jydreux.domain.map.handler;

import com.google.common.eventbus.Subscribe;
import jydreux.domain.event.*;
import jydreux.domain.map.Map;
import jydreux.domain.map.MapRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class UpdateMapHandler implements EventHandler {

  private final MapRepository mapRepository;
  private final EventBus eventBus;
  private final EventFactory eventFactory;

  public UpdateMapHandler(MapRepository mapRepository, @Qualifier("eventBusInMemory") EventBus eventBus, EventFactory eventFactory) {
    this.mapRepository = mapRepository;
    this.eventFactory = eventFactory;
    eventBus.addListener(this);
    this.eventBus = eventBus;
  }

  @Override
  @Subscribe
  public void handle(Event<?> event) {
    if (event.getType().equals(EventType.UPDATE_MAP)) {
      var map = (Map) event.getContent();
      mapRepository.update(map);
      var notification = eventFactory.build(event.getGameId(), event.getPlayerId(), EventType.MAP_UPDATED, null);
      eventBus.dispatch(notification);
    }
  }
}
