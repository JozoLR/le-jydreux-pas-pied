package jydreux.domain.map;

public interface MapRepository {
  void save(Map map);

  Map get(String gameId);

  void update(Map map);
}
