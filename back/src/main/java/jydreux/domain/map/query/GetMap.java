package jydreux.domain.map.query;

import jydreux.domain.map.Map;
import jydreux.domain.map.MapRepository;
import org.springframework.stereotype.Service;

@Service
public class GetMap {
  private final MapRepository mapRepository;

  public GetMap(MapRepository mapRepository) {
    this.mapRepository = mapRepository;
  }

  public Map handle(String gameId) {
    return mapRepository.get(gameId);
  }
}
