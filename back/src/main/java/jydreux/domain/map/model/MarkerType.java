package jydreux.domain.map.model;

public enum MarkerType {
  GROUP,
  ARK
}
