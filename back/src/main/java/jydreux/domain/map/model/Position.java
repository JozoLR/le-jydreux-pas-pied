package jydreux.domain.map.model;

public class Position {
  private final Double x;
  private final Double y;

  public Position(Double x, Double y) {
    this.x = x;
    this.y = y;
  }

  public Double getY() {
    return y;
  }

  public Double getX() {
    return x;
  }
}
