package jydreux.domain.map.model;

public class Tile {
  private final Integer index;
  private final Boolean visited;
  private final String note;

  public Tile(Integer index, Boolean visited, String note) {
    this.index = index;
    this.visited = visited;
    this.note = note;
  }

  public static Tile generate(Integer index) {
    return new Tile(index, false, "");
  }

  public Integer getIndex() {
    return index;
  }

  public Boolean getVisited() {
    return visited;
  }

  public String getNote() {
    return note;
  }
}
