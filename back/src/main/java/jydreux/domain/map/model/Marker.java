package jydreux.domain.map.model;

public class Marker {
  private final String label;
  private final Integer tileIndex;
  private final Position position;
  private final MarkerType type;

  public Marker(String label, Integer tileIndex, Position position, MarkerType type) {
    this.label = label;
    this.tileIndex = tileIndex;
    this.position = position;
    this.type = type;
  }

  public String getLabel() {
    return label;
  }

  public MarkerType getType() {
    return type;
  }

  public Integer getTileIndex() {
    return tileIndex;
  }

  public Position getPosition() {
    return position;
  }
}
