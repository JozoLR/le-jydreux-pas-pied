package jydreux.domain.map;

import jydreux.domain.map.model.Marker;
import jydreux.domain.map.model.MarkerType;
import jydreux.domain.map.model.Position;
import jydreux.domain.map.model.Tile;

import java.util.ArrayList;
import java.util.List;

public class Map {
  private final String gameId;
  private final List<Marker> markers;
  private final List<Tile> tiles;

  public Map(String gameId, List<Marker> markers, List<Tile> tiles) {
    this.gameId = gameId;
    this.markers = markers;
    this.tiles = tiles;
  }

  public static Map generate(String gameId) {
    return new Map(gameId, getInitialMarkers(), TilesGenerator.get());
  }

  public void addMarker(Marker marker) {
    this.markers.add(marker);
  }

  public String getGameId() {
    return gameId;
  }

  public List<Marker> getMarkers() {
    return markers;
  }

  public List<Tile> getTiles() {
    return tiles;
  }

  private static ArrayList<Marker> getInitialMarkers() {
    var arkPosition = new Position(50.0, 50.0);
    var arkMarker = new Marker("Ark", 215, arkPosition, MarkerType.ARK);

    var groupPosition = new Position(51.0, 51.0);
    var groupMarker = new Marker("Group", 215, groupPosition, MarkerType.GROUP);

    var markers = new ArrayList<Marker>();
    markers.add(arkMarker);
    markers.add(groupMarker);
    return markers;
  }
}
