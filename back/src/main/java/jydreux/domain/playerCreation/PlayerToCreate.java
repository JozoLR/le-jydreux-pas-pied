package jydreux.domain.playerCreation;

import jydreux.domain.game.model.Player;
import jydreux.domain.game.model.Resource;
import jydreux.domain.game.model.Skill;
import jydreux.domain.rules.GameRules;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static jydreux.domain.cross.SkillType.*;

public class PlayerToCreate {
  private final String id;
  private final String name;
  private final List<Attribut> attributs;
  private final String gameClassId;
  private final String talentId;
  private final String gameId;

  public PlayerToCreate(String id, String name, List<Attribut> attributs, String gameClassId, String talentId, String gameId) {
    this.id = id;
    this.name = name;
    this.attributs = attributs;
    this.gameClassId = gameClassId;
    this.talentId = talentId;
    this.gameId = gameId;
  }

  public Player toPlayer(GameRules gameRules) {
    var attributs = this.attributs.stream().map(Attribut::toPlayerAttribut).collect(toList());
    var skills = buildSkills(gameRules);
    var resources = buildResources(gameRules);
    return new Player(id, name, attributs, skills, resources, gameClassId, talentId, gameId);
  }

  private List<Skill> buildSkills(GameRules gameRules) {
    var skills = new ArrayList<Skill>();
    for (var gameSkill : gameRules.getSkills()) {
      switch (gameSkill.getSkillType()) {
        case BASE:
          skills.add(new Skill(gameSkill.getId(), gameSkill.getLabel(), 0, BASE));
          break;
        case ARMOR:
          skills.add(new Skill(gameSkill.getId(), gameSkill.getLabel(), 0, ARMOR));
          break;
        case CLASS:
          var sameClass = gameRules.getGameClasses().stream()
            .filter(gameClass -> gameClass.getId().equals(this.getGameClassId())).findAny();
          if (sameClass.isPresent() && gameSkill.getId().equals(sameClass.get().getSkillId())) {
            skills.add(new Skill(gameSkill.getId(), gameSkill.getLabel(), 0, CLASS));
          }
          break;
      }
    }
    return skills;
  }

  private List<Resource> buildResources(GameRules gameRules) {
    return gameRules.getResources().stream().map(resource -> new Resource(resource.getId(), resource.getLabel(), 0))
      .collect(toList());
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public List<Attribut> getAttributs() {
    return attributs;
  }

  public String getGameClassId() {
    return gameClassId;
  }

  public String getGameId() {
    return gameId;
  }
}
