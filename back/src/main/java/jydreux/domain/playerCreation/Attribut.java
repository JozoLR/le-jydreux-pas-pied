package jydreux.domain.playerCreation;

public class Attribut {
  private final String id;
  private final String label;
  private final Integer base;
  private final Integer actual;

  public Attribut(String id, String label, Integer base) {
    this.id = id;
    this.label = label;
    this.actual = base;
    this.base = base;
  }

  public Attribut(String id, String label, Integer base, Integer actual) {
    this.id = id;
    this.label = label;
    this.base = base;
    this.actual = actual;
  }

  public jydreux.domain.game.model.Attribut toPlayerAttribut() {
    return new jydreux.domain.game.model.Attribut(id, label, base, actual);
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public Integer getBase() {
    return base;
  }

  public Integer getActual() {
    return actual;
  }
}
