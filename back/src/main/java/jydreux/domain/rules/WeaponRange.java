package jydreux.domain.rules;

public class WeaponRange {
  private final String id;
  private final String label;
  private final Integer effect;

  public WeaponRange(String id, String label, Integer effect) {
    this.id = id;
    this.label = label;
    this.effect = effect;
  }

  public Integer getEffect() {
    return effect;
  }

  public String getLabel() {
    return label;
  }

  public String getId() {
    return id;
  }

}
