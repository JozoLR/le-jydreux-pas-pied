package jydreux.domain.rules;

import java.util.List;

public class GameRules {
  private final List<Attribut> attributs;
  private final List<Skill> skills;
  private final List<WeaponRange> weaponRanges;
  private final List<Dice> dice;
  private final List<GameClass> gameClasses;
  private final List<Resource> resources;
  private final List<Talent> talents;

  public GameRules(List<Attribut> attributs, List<Skill> skills, List<WeaponRange> weaponRanges, List<Dice> dice, List<GameClass> gameClasses, List<Resource> resources, List<Talent> talents) {
    this.attributs = attributs;
    this.skills = skills;
    this.weaponRanges = weaponRanges;
    this.dice = dice;
    this.gameClasses = gameClasses;
    this.resources = resources;
    this.talents = talents;
  }

  public Skill getSkill(String skillId) {
    return skills.stream().filter(skill -> skill.getId().equals(skillId)).findAny().get();
  }

  public List<Attribut> getAttributs() {
    return attributs;
  }

  public List<Skill> getSkills() {
    return skills;
  }

  public List<WeaponRange> getWeaponRanges() {
    return weaponRanges;
  }

  public List<Dice> getDice() {
    return dice;
  }

  public List<GameClass> getGameClasses() {
    return gameClasses;
  }

  public List<Resource> getResources() {
    return resources;
  }

  public List<Talent> getTalents() {
    return talents;
  }
}
