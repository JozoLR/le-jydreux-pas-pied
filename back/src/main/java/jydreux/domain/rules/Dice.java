package jydreux.domain.rules;

public class Dice {
  private final String category;
  private final Integer number;

  public Dice(String category, Integer number) {
    this.category = category;
    this.number = number;
  }

  public String getCategory() {
    return category;
  }

  public Integer getNumber() {
    return number;
  }
}
