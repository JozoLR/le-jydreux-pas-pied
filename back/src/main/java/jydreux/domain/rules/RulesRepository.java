package jydreux.domain.rules;

import java.util.List;

public interface RulesRepository {
  GameRules getGameRules();

  List<Skill> getBasicSkills();

  Skill getSkill(String skillId);

  Skill getSkillForClass(String gameClassId);
}
