package jydreux.domain.rules;

import java.util.List;

public class Talent {
  private final String id;
  private final String label;
  private final String gameClassId;
  private final String description;
  private final List<SkillBonus> skillBonuses;

  public Talent(String id, String label, String gameClassId, String description, List<SkillBonus> skillBonuses) {
    this.id = id;
    this.label = label;
    this.gameClassId = gameClassId;
    this.description = description;
    this.skillBonuses = skillBonuses;
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public String getGameClassId() {
    return gameClassId;
  }

  public String getDescription() {
    return description;
  }
}
