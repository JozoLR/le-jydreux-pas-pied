package jydreux.domain.rules;

public class SkillBonus {
  private final String skillId;
  private final Integer bonus;

  public SkillBonus(String skillId, Integer bonus) {
    this.skillId = skillId;
    this.bonus = bonus;
  }

  public String getSkillId() {
    return skillId;
  }

  public Integer getBonus() {
    return bonus;
  }
}
