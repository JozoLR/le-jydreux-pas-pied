package jydreux.domain.rules;

import jydreux.domain.cross.SkillType;

public class Skill {
  private final String id;
  private final String label;
  private final String attributId;
  private final SkillType skillType;

  public Skill(String id, String label, String attributId, SkillType skillType) {
    this.id = id;
    this.label = label;
    this.attributId = attributId;
    this.skillType = skillType;
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public String getAttributId() {
    return attributId;
  }

  public SkillType getSkillType() {
    return skillType;
  }
}
