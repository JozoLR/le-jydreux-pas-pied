package jydreux.domain.rules;

public class GameClass {
  private final String id;
  private final String label;
  private final String skillId;

  public GameClass(String id, String label, String skillId) {
    this.id = id;
    this.label = label;
    this.skillId = skillId;
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public String getSkillId() {
    return skillId;
  }
}
