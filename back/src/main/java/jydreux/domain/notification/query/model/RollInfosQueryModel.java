package jydreux.domain.notification.query.model;

import jydreux.domain.cross.StuffType;

import java.util.List;

public class RollInfosQueryModel {
  private final String skillId;
  private final String skillLabel;
  private final StuffType stuffType;
  private final List<StuffQueryModel> stuffs;

  public RollInfosQueryModel(String skillId, String skillLabel, StuffType stuffType, List<StuffQueryModel> stuffs) {
    this.skillId = skillId;
    this.skillLabel = skillLabel;
    this.stuffType = stuffType;
    this.stuffs = stuffs;
  }

  public String getSkillId() {
    return skillId;
  }

  public String getSkillLabel() {
    return skillLabel;
  }

  public StuffType getStuffType() {
    return stuffType;
  }

  public List<StuffQueryModel> getStuffs() {
    return stuffs;
  }
}
