package jydreux.domain.notification.query;

import jydreux.domain.event.EventRepository;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.game.model.Player;
import jydreux.domain.notification.Notification;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class GetNotifications {

  private final EventRepository eventRepository;
  private final PlayerRepository playerRepository;
  private final NotificationQueryModelMapper notificationQueryModelMapper;

  public GetNotifications(EventRepository eventRepository, PlayerRepository playerRepository, NotificationQueryModelMapper notificationQueryModelMapper) {
    this.eventRepository = eventRepository;
    this.playerRepository = playerRepository;
    this.notificationQueryModelMapper = notificationQueryModelMapper;
  }

  public List<Notification<?>> handle(String gameId) {
    var players = playerRepository.getByGame(gameId);
    var events = eventRepository.getAllByGame(gameId);
    return events.stream()
      .filter(event -> event.getType().shouldNotify())
      .map(event -> notificationQueryModelMapper.from(event, getPlayer(players, event.getPlayerId())))
      .collect(toList());
  }

  private Player getPlayer(List<Player> players, String playerId) {
    return players.stream().filter(player -> player.getId().equals(playerId)).findFirst().get();
  }
}
