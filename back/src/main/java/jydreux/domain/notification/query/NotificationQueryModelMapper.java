package jydreux.domain.notification.query;

import jydreux.domain.cross.StuffType;
import jydreux.domain.event.Event;
import jydreux.domain.game.GameRepository;
import jydreux.domain.game.model.Player;
import jydreux.domain.notification.Notification;
import jydreux.domain.notification.query.model.RollInfosQueryModel;
import jydreux.domain.notification.query.model.RollQueryModel;
import jydreux.domain.notification.query.model.StuffQueryModel;
import jydreux.domain.roll.model.Roll;
import jydreux.domain.roll.model.RollInfos;
import jydreux.domain.rules.RulesRepository;
import org.springframework.stereotype.Service;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

@Service
public class NotificationQueryModelMapper {
  private final RulesRepository rulesRepository;
  private final GameRepository gameRepository;

  public NotificationQueryModelMapper(RulesRepository rulesRepository, GameRepository gameRepository) {
    this.rulesRepository = rulesRepository;
    this.gameRepository = gameRepository;
  }

  public Notification<?> from(Event<?> event, Player player) {
    switch (event.getType()) {
      case PLAYER_ROLLED:
        return buildRollNotification(event, player);
      default:
        return buildNotification(event, player);
    }
  }

  private <C> Notification<C> buildNotification(Event<C> event, Player player) {
    return new Notification<>(
      event.getId(),
      event.getDate(),
      event.getPlayerId(),
      player.getName(),
      event.getGameId(),
      event.getType(),
      event.getContent());
  }

  private Notification<RollQueryModel> buildRollNotification(Event<?> event, Player player) {
    var roll = (Roll) event.getContent();
    var rollInfo = roll.getRollInfos();
    RollInfosQueryModel rollInfosQueryModel;
    if (rollInfo.getSkillId() == null) {
      rollInfosQueryModel = new RollInfosQueryModel(null, null, StuffType.NONE, emptyList());
    } else {
      rollInfosQueryModel = buildRollInfosQueryModel(rollInfo, player);
    }

    var rollQueryModel = new RollQueryModel(
      roll.getAttributRolls(),
      roll.getSkillRolls(),
      roll.getStuffRolls(),
      roll.getMalusRolls(),
      roll.getRollType(),
      rollInfosQueryModel
    );
    return new Notification<>(
      event.getId(),
      event.getDate(),
      event.getPlayerId(),
      player.getName(),
      event.getGameId(),
      event.getType(),
      rollQueryModel);
  }

  private RollInfosQueryModel buildRollInfosQueryModel(RollInfos rollInfo, Player player) {
    var gameRules = rulesRepository.getGameRules();
    var skillLabel = gameRules.getSkill(rollInfo.getSkillId()).getLabel();
    var stuffQueryModels = rollInfo.getStuffIds().stream()
      .map(stuffId -> {
        if (rollInfo.getStuffType().equals(StuffType.GEAR) || rollInfo.getStuffType().equals(StuffType.ARMOR)) {
          var gear = player.getGear(stuffId);
          if (gear.isPresent()) {
            return new StuffQueryModel(stuffId, gear.get().getLabel());
          } else {
            var gearLabel = gameRepository.getGear(stuffId).getLabel();
            return new StuffQueryModel(stuffId, gearLabel);
          }
        } else {
          var weapon = player.getWeapon(stuffId);
          if (weapon.isPresent()) {
            return new StuffQueryModel(stuffId, weapon.get().getLabel());
          } else {
            var weaponLabel = gameRepository.getWeapon(stuffId).getLabel();
            return new StuffQueryModel(stuffId, weaponLabel);
          }
        }
      })
      .collect(toList());

    return new RollInfosQueryModel(
      rollInfo.getSkillId(),
      skillLabel,
      rollInfo.getStuffType(),
      stuffQueryModels
    );
  }

}
