package jydreux.domain.notification.query.model;

import jydreux.domain.roll.model.RollType;

import java.util.List;

public class RollQueryModel {
  private final List<Integer> attributRolls;
  private final List<Integer> skillRolls;
  private final List<Integer> stuffRolls;
  private final List<Integer> malusRolls;
  private final RollType rollType;
  private final RollInfosQueryModel rollInfos;

  public RollQueryModel(List<Integer> attributRolls, List<Integer> skillRolls, List<Integer> stuffRolls, List<Integer> malusRolls, RollType rollType, RollInfosQueryModel rollInfos) {
    this.attributRolls = attributRolls;
    this.skillRolls = skillRolls;
    this.stuffRolls = stuffRolls;
    this.malusRolls = malusRolls;
    this.rollType = rollType;
    this.rollInfos = rollInfos;
  }

  public List<Integer> getAttributRolls() {
    return attributRolls;
  }

  public List<Integer> getSkillRolls() {
    return skillRolls;
  }

  public List<Integer> getStuffRolls() {
    return stuffRolls;
  }

  public List<Integer> getMalusRolls() {
    return malusRolls;
  }

  public RollType getRollType() {
    return rollType;
  }

  public RollInfosQueryModel getRollInfos() {
    return rollInfos;
  }
}
