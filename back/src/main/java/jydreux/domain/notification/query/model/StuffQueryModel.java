package jydreux.domain.notification.query.model;

public class StuffQueryModel {
  private final String id;
  private final String label;

  public StuffQueryModel(String id, String label) {
    this.id = id;
    this.label = label;
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }
}
