package jydreux.domain.rollsPropositions;

import java.util.List;

public class RollProposition {
  private final String playerId;
  private final List<Proposition> skillPropositions;

  public RollProposition(String playerId, List<Proposition> skillPropositions) {
    this.playerId = playerId;
    this.skillPropositions = skillPropositions;
  }

  public String getPlayerId() {
    return playerId;
  }

  public List<Proposition> getSkillPropositions() {
    return skillPropositions;
  }
}
