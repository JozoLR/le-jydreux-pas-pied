package jydreux.domain.rollsPropositions.query;

import jydreux.domain.cross.SkillType;
import jydreux.domain.cross.StuffType;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.game.model.Attribut;
import jydreux.domain.game.model.Gear;
import jydreux.domain.game.model.Weapon;
import jydreux.domain.rollsPropositions.Bonus;
import jydreux.domain.rollsPropositions.Proposition;
import jydreux.domain.rollsPropositions.RollProposition;
import jydreux.domain.rules.RulesRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class GetRollsPropositions {

  private final PlayerRepository playerRepository;
  private final RulesRepository rulesRepository;

  public GetRollsPropositions(PlayerRepository playerRepository, RulesRepository rulesRepository) {
    this.playerRepository = playerRepository;
    this.rulesRepository = rulesRepository;
  }

  public RollProposition handle(String playerId) {
    var player = playerRepository.get(playerId);

    var skillPropositions = new ArrayList<Proposition>();

    for (var skill : player.getSkills()) {
      var skillId = skill.getId();
      var skillLabel = skill.getLabel();

      var attributDice = 0;
      var skillDice = 0;
      if (!skill.getSkillType().equals(SkillType.ARMOR)) {
        attributDice = getAttributDice(player.getAttributs(), skillId);
        skillDice = getSkillDice(player.getSkills(), skillId);
      }

      var weaponsForSkill = player.getWeapons().stream()
        .filter(weapon -> weapon.getSkillId().equals(skillId)).collect(toList());
      var weaponBonuses = getWeaponPropositions(weaponsForSkill);
      var bonuses = new ArrayList<>(weaponBonuses);

      var gearsForSkill = player.getGears().stream()
        .filter(weapon -> weapon.getSkillIds().contains(skillId)).collect(toList());
      var gearBonuses = getGearPropositions(gearsForSkill);
      bonuses.addAll(gearBonuses);

      skillPropositions.add(new Proposition(skillId, skillLabel, attributDice, skillDice, bonuses));
    }

    return new RollProposition(playerId, skillPropositions);
  }

  private List<Bonus> getWeaponPropositions(List<Weapon> weapons) {
    var weaponBonuses = new ArrayList<Bonus>();
    for (var weapon : weapons) {
      var weaponDice = weapon.getBonus();
      if (weapon.getMalus() > 0) {
        weaponDice = weapon.getBonus() - weapon.getMalus();
      }
      var bonus = new Bonus(
        weapon.getId(),
        weapon.getLabel(),
        StuffType.WEAPON,
        weaponDice);
      weaponBonuses.add(bonus);
    }
    return weaponBonuses;
  }

  private List<Bonus> getGearPropositions(List<Gear> gears) {
    var bonuses = new ArrayList<Bonus>();
    for (var gear : gears) {
      var gearDice = gear.getBonus();
      if (gear.getMalus() > 0) {
        gearDice = gear.getBonus() - gear.getMalus();
      }

      var bonus = new Bonus(
        gear.getId(),
        gear.getLabel(),
        StuffType.GEAR,
        gearDice);
      bonuses.add(bonus);
    }
    return bonuses;
  }

  private Integer getAttributDice(List<Attribut> attributs, String skillKey) {
    var attributDice = 0;
    var gameSkill = rulesRepository.getSkill(skillKey);
    var attributKey = gameSkill.getAttributId();

    var attributValue = attributs.stream().filter(attribut -> attribut.getId().equals(attributKey)).findFirst();
    if (attributValue.isPresent()) {
      attributDice = attributValue.get().getActual();
    }
    return attributDice;
  }

  private Integer getSkillDice(List<jydreux.domain.game.model.Skill> skills, String skillKey) {
    var skillDice = 0;
    var skillValue = skills.stream().filter(skill -> skill.getId().equals(skillKey)).findFirst();
    if (skillValue.isPresent()) {
      skillDice = skillValue.get().getActual();
    }
    return skillDice;
  }
}
