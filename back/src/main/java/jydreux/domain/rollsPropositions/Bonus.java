package jydreux.domain.rollsPropositions;

import jydreux.domain.cross.StuffType;

public class Bonus {
  private final String id;
  private final String label;
  private final StuffType stuffType;
  private final Integer value;

  public Bonus(String id, String label, StuffType stuffType, Integer value) {
    this.id = id;
    this.label = label;
    this.stuffType = stuffType;
    this.value = value;
  }

  public String getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }

  public Integer getValue() {
    return value;
  }

  public StuffType getStuffType() {
    return stuffType;
  }
}
