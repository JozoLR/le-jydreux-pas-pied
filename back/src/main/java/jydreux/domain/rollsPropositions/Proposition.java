package jydreux.domain.rollsPropositions;

import java.util.List;

public class Proposition {
  private final String skillId;
  private final String skillLabel;
  private final Integer attributDice;
  private final Integer skillDice;
  private final List<Bonus> bonuses;

  public Proposition(String skillId, String skillLabel, Integer attributDice, Integer skillDice, List<Bonus> bonuses) {
    this.skillId = skillId;
    this.skillLabel = skillLabel;
    this.attributDice = attributDice;
    this.skillDice = skillDice;
    this.bonuses = bonuses;
  }

  public String getSkillId() {
    return skillId;
  }

  public Integer getAttributDice() {
    return attributDice;
  }

  public Integer getSkillDice() {
    return skillDice;
  }

  public List<Bonus> getBonuses() {
    return bonuses;
  }

  public String getSkillLabel() {
    return skillLabel;
  }
}
