package jydreux.domain.cross;

public enum StuffType {
  WEAPON,
  GEAR,
  ARMOR,
  NONE
}
