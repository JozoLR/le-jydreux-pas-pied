package jydreux.domain.cross;

public enum SkillType {
  BASE,
  CLASS,
  ARMOR
}
