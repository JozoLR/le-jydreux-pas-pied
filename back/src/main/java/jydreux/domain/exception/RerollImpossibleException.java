package jydreux.domain.exception;

public class RerollImpossibleException extends RuntimeException {
  public RerollImpossibleException(String message) {
    super(message);
  }
}
