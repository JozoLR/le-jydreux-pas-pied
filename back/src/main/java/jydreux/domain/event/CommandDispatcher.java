package jydreux.domain.event;

import jydreux.domain.exception.ForbiddenException;
import jydreux.domain.game.GameRepository;
import jydreux.domain.user.UserRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class CommandDispatcher {
  private final UserRepository userRepository;
  private final GameRepository gameRepository;
  private final EventBus eventBus;

  public CommandDispatcher(UserRepository userRepository, GameRepository gameRepository, @Qualifier("eventBusInMemory") EventBus eventBus) {
    this.userRepository = userRepository;
    this.gameRepository = gameRepository;
    this.eventBus = eventBus;
  }

  public void validateAndDispatch(Event<?> event, String token) {
    var user = userRepository.getByToken(token);
    var game = gameRepository.get(event.getGameId());
    var userInGame = game.getGameMaster().equals(user.getEmail()) || game.getUsers().contains(user.getEmail());
    if (userInGame) {
      eventBus.dispatch(event);
    } else {
      throw new ForbiddenException("The user " + user.getEmail() + " is not allowed to do this action");
    }
  }
}
