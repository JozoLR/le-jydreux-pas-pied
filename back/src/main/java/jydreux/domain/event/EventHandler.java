package jydreux.domain.event;

public interface EventHandler {
  void handle(Event<?> event);
}
