package jydreux.domain.event;

public enum EventType {
  PLAYER_UPDATED,
  GEAR_UPDATED,
  GEAR_ADDED,
  ATTRIBUT_UPDATED,
  SKILL_UPDATED,
  WEAPON_ADDED,
  WEAPON_UPDATED,
  RESOURCE_UPDATED,
  PLAYER_ROLLED,
  WEAPON_GIVEN,
  GEAR_GIVEN,
  PLAYER_UPGRADED,
  DICE_ROLLED,
  UPDATE_GEAR,
  GIVE_WEAPON,
  GIVE_GEAR,
  UPDATE_MAP,
  UPGRADE_PLAYER,
  DELETE_GEAR,
  GEAR_DELETED,
  DELETE_WEAPON,
  WEAPON_DELETED,
  MAP_UPDATED;

  public final Boolean shouldNotify() {
    return this.equals(PLAYER_UPDATED) ||
      this.equals(GEAR_UPDATED) ||
      this.equals(GEAR_ADDED) ||
      this.equals(ATTRIBUT_UPDATED) ||
      this.equals(SKILL_UPDATED) ||
      this.equals(WEAPON_ADDED) ||
      this.equals(WEAPON_UPDATED) ||
      this.equals(RESOURCE_UPDATED) ||
      this.equals(PLAYER_ROLLED) ||
      this.equals(WEAPON_GIVEN) ||
      this.equals(GEAR_GIVEN) ||
      this.equals(PLAYER_UPGRADED) ||
      this.equals(DICE_ROLLED) ||
      this.equals(GEAR_DELETED) ||
      this.equals(WEAPON_DELETED) ||
      this.equals(MAP_UPDATED);
  }
}
