package jydreux.domain.event.handler;

import com.google.common.eventbus.Subscribe;
import jydreux.domain.event.Event;
import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventHandler;
import jydreux.domain.event.EventRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class AllEventHandler implements EventHandler {

  private final EventRepository eventRepository;
  private final EventBus sseEventBus;

  public AllEventHandler(EventRepository eventRepository, @Qualifier("eventBusInMemory") EventBus eventBus, @Qualifier("sseNotificationBusInMemory") EventBus sseEventBus) {
    this.eventRepository = eventRepository;
    eventBus.addListener(this);
    this.sseEventBus = sseEventBus;
  }

  @Override
  @Subscribe
  public void handle(Event<?> event) {
    eventRepository.save(event);
    if (event.getType().shouldNotify()) {
      sseEventBus.dispatch(event);
    }
  }
}
