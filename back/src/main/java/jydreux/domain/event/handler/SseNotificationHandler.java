package jydreux.domain.event.handler;

import com.google.common.eventbus.Subscribe;
import jydreux.domain.event.Event;
import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventHandler;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.game.model.Player;
import jydreux.domain.notification.query.NotificationQueryModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;

public class SseNotificationHandler implements EventHandler {

  private final SseEmitter emitter;
  private final EventBus sseEventBus;
  private final PlayerRepository playerRepository;
  private final NotificationQueryModelMapper notificationQueryModelMapper;
  private final String clientId;
  private final String gameId;
  private final Logger logger = LoggerFactory.getLogger(SseNotificationHandler.class);

  public SseNotificationHandler(SseEmitter emitter, EventBus sseEventBus, PlayerRepository playerRepository, NotificationQueryModelMapper notificationQueryModelMapper, String gameId, String clientId) {
    this.emitter = emitter;
    this.sseEventBus = sseEventBus;
    this.playerRepository = playerRepository;
    this.notificationQueryModelMapper = notificationQueryModelMapper;
    this.clientId = clientId;
    this.gameId = gameId;
  }

  @Override
  @Subscribe
  public void handle(Event<?> event) {
    if (event.getGameId().equals(gameId)) {
      Player player;
      if (event.getPlayerId() != null) {
        player = playerRepository.get(event.getPlayerId());
      } else {
        player = Player.buildGameMaster(event.getGameId());
      }
      var notificationQueryModel = notificationQueryModelMapper.from(event, player);
      var eventBuilder = SseEmitter.event().data(notificationQueryModel).name("NOTIFICATION");
      try {
        emitter.send(eventBuilder);
        logger.debug("Event - " + event.getType().name() + " sent to " + clientId);
      } catch (IOException | IllegalStateException e) {
        sseEventBus.unsubscribe(this);
        logger.debug(clientId + " unsubscribed");
      }
    }
  }
}
