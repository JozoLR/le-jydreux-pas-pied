package jydreux.domain.event;

public interface EventBus {

  void dispatch(Event<?> event);

  void addListener(EventHandler eventListener);

  void unsubscribe(EventHandler eventListener);
}
