package jydreux.domain.event;

import java.util.List;

public interface EventRepository {
  void save(Event<?> event);

  Event<?> get(String id);

  List<Event<?>> getAllByGame(String gameId);
}
