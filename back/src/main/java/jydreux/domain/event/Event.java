package jydreux.domain.event;

import java.time.ZonedDateTime;

public class Event<C> {
  private final String id;
  private final ZonedDateTime date;
  private final String playerId;
  private final String gameId;
  private final EventType type;
  private final C content;

  public Event(String id, ZonedDateTime date, String playerId, String gameId, EventType type, C content) {
    this.id = id;
    this.date = date;
    this.playerId = playerId;
    this.gameId = gameId;
    this.type = type;
    this.content = content;
  }

  public String getId() {
    return id;
  }

  public ZonedDateTime getDate() {
    return date;
  }

  public String getPlayerId() {
    return playerId;
  }

  public String getGameId() {
    return gameId;
  }

  public EventType getType() {
    return type;
  }

  public C getContent() {
    return content;
  }
}
