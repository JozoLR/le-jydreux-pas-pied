package jydreux.domain.event;

import jydreux.service.DateService;
import jydreux.service.IdService;
import org.springframework.stereotype.Service;

@Service
public class EventFactory {
  private final IdService idService;
  private final DateService dateService;

  public EventFactory(IdService idService, DateService dateService) {
    this.idService = idService;
    this.dateService = dateService;
  }

  public <C> Event<C> build(String gameId, EventType type, C content) {
    return new Event<>(
      idService.generate(),
      dateService.now(),
      null,
      gameId,
      type,
      content
    );
  }

  public <C> Event<C> build(String gameId, String playerId, EventType type, C content) {
    return new Event<>(
      idService.generate(),
      dateService.now(),
      playerId,
      gameId,
      type,
      content
    );
  }
}
