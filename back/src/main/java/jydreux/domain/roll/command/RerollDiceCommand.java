package jydreux.domain.roll.command;

import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.exception.RerollImpossibleException;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.roll.RollRepository;
import jydreux.domain.roll.RollService;
import jydreux.domain.roll.model.Dice;
import jydreux.domain.roll.model.Roll;
import jydreux.domain.roll.model.RollType;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class RerollDiceCommand {
  private final RollRepository rollRepository;
  private final EventFactory eventFactory;
  private final EventBus eventBus;
  private final PlayerRepository playerRepository;
  private final RollService rollService;

  public RerollDiceCommand(RollRepository rollRepository, EventFactory eventFactory, @Qualifier("eventBusInMemory") EventBus eventBus, PlayerRepository playerRepository, RollService rollService) {
    this.rollRepository = rollRepository;
    this.eventFactory = eventFactory;
    this.eventBus = eventBus;
    this.playerRepository = playerRepository;
    this.rollService = rollService;
  }

  public Roll handle(String playerId) {
    Roll lastRoll;
    lastRoll = rollRepository.getMoreRecentByPlayer(playerId);
    if (lastRoll == null || lastRoll.getRollType().equals(RollType.REROLL)) {
      throw new RerollImpossibleException("Il faut roll avant de reroll");
    }

    var reroll = getReroll(playerId, lastRoll);
    var player = playerRepository.get(playerId);
    var event = eventFactory.build(player.getGameId(), playerId, EventType.PLAYER_ROLLED, reroll);
    eventBus.dispatch(event);
    return reroll;
  }

  private Roll getReroll(String playerId, Roll lastRoll) {
    var numberOfAttributDice = 0;
    var attributDiceToKeep = new ArrayList<Integer>();
    for (var roll : lastRoll.getAttributRolls()) {
      if (roll == 6 || roll == 1) {
        attributDiceToKeep.add(roll);
      } else {
        numberOfAttributDice++;
      }
    }

    var numberOfSkillDice = 0;
    var skillDiceToKeep = new ArrayList<Integer>();
    for (var roll : lastRoll.getSkillRolls()) {
      if (roll == 6) {
        skillDiceToKeep.add(roll);
      } else {
        numberOfSkillDice++;
      }
    }

    var numberOfStuffDice = 0;
    var stuffDiceToKeep = new ArrayList<Integer>();
    for (var roll : lastRoll.getStuffRolls()) {
      if (roll == 6 || roll == 1) {
        stuffDiceToKeep.add(roll);
      } else {
        numberOfStuffDice++;
      }
    }

    var numberOfMalusDice = 0;
    var malusDiceToKeep = new ArrayList<Integer>();
    for (var roll : lastRoll.getMalusRolls()) {
      if (roll == 1) {
        malusDiceToKeep.add(roll);
      } else {
        numberOfMalusDice++;
      }
    }

    var rerollCommand = new Dice(
      numberOfAttributDice,
      numberOfSkillDice,
      numberOfStuffDice,
      numberOfMalusDice,
      playerId,
      RollType.REROLL,
      lastRoll.getRollInfos(),
      rollService);
    var reroll = rerollCommand.roll();

    reroll.getAttributRolls().addAll(attributDiceToKeep);
    reroll.getSkillRolls().addAll(skillDiceToKeep);
    reroll.getStuffRolls().addAll(stuffDiceToKeep);
    reroll.getMalusRolls().addAll(malusDiceToKeep);
    return reroll;
  }
}
