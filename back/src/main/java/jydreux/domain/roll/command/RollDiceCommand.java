package jydreux.domain.roll.command;

import jydreux.domain.event.EventBus;
import jydreux.domain.event.EventFactory;
import jydreux.domain.event.EventType;
import jydreux.domain.game.PlayerRepository;
import jydreux.domain.roll.model.Dice;
import jydreux.domain.roll.model.Roll;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class RollDiceCommand {
  private final PlayerRepository playerRepository;
  private final EventFactory eventFactory;
  private final EventBus eventBus;

  public RollDiceCommand(PlayerRepository playerRepository, EventFactory eventFactory, @Qualifier("eventBusInMemory") EventBus eventBus) {
    this.playerRepository = playerRepository;
    this.eventFactory = eventFactory;
    this.eventBus = eventBus;
  }

  public Roll handle(Dice dice) {
    var roll = dice.roll();
    var player = playerRepository.get(dice.getPlayerId());
    var event = eventFactory.build(player.getGameId(), player.getId(), EventType.PLAYER_ROLLED, roll);
    eventBus.dispatch(event);
    return roll;
  }
}
