package jydreux.domain.roll.model;

import jydreux.domain.roll.RollService;

import java.util.ArrayList;

public class Dice {
  private final Integer attribut;
  private final Integer stuff;
  private final String playerId;
  private final RollType rollType;
  private final RollInfos rollInfos;
  private final RollService rollService;
  private Integer skill;
  private Integer malus;

  public Dice(Integer attribut, Integer skill, Integer stuff, Integer malus, String playerId, RollType rollType, RollInfos rollInfos, RollService rollService) {
    this.attribut = attribut;
    this.skill = skill;
    this.stuff = stuff;
    this.malus = malus;
    this.playerId = playerId;
    this.rollType = rollType;
    this.rollInfos = rollInfos;
    this.rollService = rollService;
  }

  public Roll roll() {
    applyMalus();

    var attributDice = new ArrayList<Integer>();
    var skillDice = new ArrayList<Integer>();
    var stuffDice = new ArrayList<Integer>();
    var malusDice = new ArrayList<Integer>();


    for (var i = 0; i < attribut; i++) {
      attributDice.add(rollService.roll());
    }
    for (var i = 0; i < skill; i++) {
      skillDice.add(rollService.roll());
    }
    for (var i = 0; i < stuff; i++) {
      stuffDice.add(rollService.roll());
    }
    for (var i = 0; i < malus; i++) {
      malusDice.add(rollService.roll());
    }

    return new Roll(attributDice, skillDice, stuffDice, malusDice, rollType, rollInfos);
  }

  private void applyMalus() {
    if (malus > 0 && malus >= skill) {
      malus = malus - skill;
      skill = 0;
    }

    if (malus > 0 && skill > malus) {
      skill = skill - malus;
      malus = 0;
    }
  }

  public Integer getAttribut() {
    return attribut;
  }

  public Integer getSkill() {
    return skill;
  }

  public Integer getMalus() {
    return malus;
  }

  public String getPlayerId() {
    return playerId;
  }
}
