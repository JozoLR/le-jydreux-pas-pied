package jydreux.domain.roll.model;

import jydreux.domain.cross.StuffType;

import java.util.Collections;
import java.util.List;

public class RollInfos {
  private final String skillId;
  private final StuffType stuffType;
  private final List<String> stuffIds;

  public RollInfos() {
    this.skillId = null;
    this.stuffType = StuffType.NONE;
    this.stuffIds = Collections.emptyList();
  }

  public RollInfos(String skillId, StuffType stuffType, List<String> stuffIds) {
    this.skillId = skillId;
    this.stuffType = stuffType;
    this.stuffIds = stuffIds;
  }

  public String getSkillId() {
    return skillId;
  }

  public StuffType getStuffType() {
    return stuffType;
  }

  public List<String> getStuffIds() {
    return stuffIds;
  }
}
