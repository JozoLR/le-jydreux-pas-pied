package jydreux.domain.roll.model;

import java.util.List;

public class Roll {
  private final List<Integer> attributRolls;
  private final List<Integer> skillRolls;
  private final List<Integer> stuffRolls;
  private final List<Integer> malusRolls;
  private final RollType rollType;
  private final RollInfos rollInfos;

  public Roll(List<Integer> attributRolls, List<Integer> skillRolls, List<Integer> stuffRolls, List<Integer> malusRolls, RollType rollType, RollInfos rollInfos) {
    this.attributRolls = attributRolls;
    this.skillRolls = skillRolls;
    this.stuffRolls = stuffRolls;
    this.malusRolls = malusRolls;
    this.rollType = rollType;
    this.rollInfos = rollInfos;
  }

  public List<Integer> getAttributRolls() {
    return attributRolls;
  }

  public List<Integer> getSkillRolls() {
    return skillRolls;
  }

  public List<Integer> getStuffRolls() {
    return stuffRolls;
  }

  public List<Integer> getMalusRolls() {
    return malusRolls;
  }

  public RollType getRollType() {
    return rollType;
  }

  public RollInfos getRollInfos() {
    return rollInfos;
  }
}
