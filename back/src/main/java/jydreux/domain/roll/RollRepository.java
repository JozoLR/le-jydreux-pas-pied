package jydreux.domain.roll;

import jydreux.domain.roll.model.Roll;

import java.util.List;

public interface RollRepository {
  List<Roll> findAll();

  Roll getMoreRecentByPlayer(String playerId);
}
