package jydreux.domain.roll;

import org.springframework.stereotype.Service;

@Service
public class RollService {
  public Integer roll() {
    return (int) (Math.random() * 6) + 1;
  }
}
