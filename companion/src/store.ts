import {applyMiddleware, createStore} from 'redux'
import {composeWithDevTools} from 'redux-devtools-extension'
import {initialState, rootReducer} from './store/reducers'
import {epicMiddleware, rootEpic} from './store/epics'


const composeEnhancer = composeWithDevTools({
  name: 'Jydreux'
})

const store = createStore(
  rootReducer,
  initialState,
  composeEnhancer(applyMiddleware(epicMiddleware))
)

epicMiddleware.run(rootEpic)

export default store
