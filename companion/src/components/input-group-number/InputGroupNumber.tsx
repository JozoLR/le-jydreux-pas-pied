import React from 'react'
import { InputNumber } from 'components/input-number/InputNumber'
import { BubleSelector } from 'components/buble-selector/BubleSelector'

export class InputGroupNumber extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      value: this.props.value || 0
    }
  }

  componentDidUpdate(): void {
    if (this.props.value !== undefined && this.props.value !== this.state.value) {
      this.setState({
        value: this.props.value
      })
    }
  }

  render(): JSX.Element {
    return (
      <div className="input-group has-label">
        <div className="label">
          <label className="text-main truncate">{this.props.label}</label>
        </div>
        {this.props.bubbles == true ?
          <BubleSelector max={this.props.max} value={this.state.value} onChange={(value: number): void => this.onChange(value)} /> :
          <InputNumber max={this.props.max} value={this.state.value} onChange={(value: number): void => this.onChange(value)} />
        }
      </div>
    )
  }

  onChange(value: number): void {
    this.setState({
      value
    })
    this.props.onChange(value)
  }
}

export type Props = {
  onChange: (value: number) => void;
  label: string;
  value?: number;
  max?: number;
  bubbles?: boolean;
}

interface State {
  value: number;
}
