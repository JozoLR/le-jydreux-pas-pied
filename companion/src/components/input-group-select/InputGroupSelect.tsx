import React from 'react'
import Dropdown from 'react-bootstrap/Dropdown'

export class InputGroupSelect extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      selectedLabel: this.props.selectedLabel
    }
  }

  componentDidUpdate (prevProps: Readonly<Props>): void {
    if (prevProps !== this.props) {
      this.setState({
        selectedLabel: this.props.selectedLabel
      })
    }
  }

  render(): JSX.Element {
    return <div className="input-group has-label">
      {this.props.label ?
        <div className="label">
          <label className="text-main truncate">{this.props.label}</label>
        </div> : null}
      <Dropdown>
        <Dropdown.Toggle id="dropdown-basic">
          {this.state.selectedLabel}
        </Dropdown.Toggle>
        <Dropdown.Menu>
          {
            this.props.values.map((value: InputGroupValue) =>
              <Dropdown.Item as="button" onClick={(): void => this.onSelect(value)} key={value.id}>
                {value.label}
              </Dropdown.Item>
            )
          }
        </Dropdown.Menu>
      </Dropdown>
    </div>
  }

  onSelect = (value: InputGroupValue): void => {
    this.setState({ ...this.state, selectedLabel: value.label })
    this.props.onChange(value)
  }
}

export type Props = {
  onChange: (value: InputGroupValue) => void;
  label?: string;
  selectedLabel: string;
  values: InputGroupValue[];
}

export interface InputGroupValue {
  id: string;
  label: string;
}

export type State = {
  selectedLabel: string;
}
