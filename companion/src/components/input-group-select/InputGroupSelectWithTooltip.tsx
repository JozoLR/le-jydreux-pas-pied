import React from 'react'
import Dropdown from 'react-bootstrap/Dropdown'
import {OverlayTrigger, Tooltip} from 'react-bootstrap'

export class InputGroupSelectWithTooltip extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      selectLabel: this.props.selectedLabel
    }
  }

  render(): JSX.Element {
    return <div className="input-group has-label">
      {this.props.label ?
        <div className="label">
          <label className="text-main truncate">{this.props.label}</label>
        </div> : null}
      <Dropdown>
        <Dropdown.Toggle id="dropdown-basic">
          {this.state.selectLabel}
        </Dropdown.Toggle>
        <Dropdown.Menu>
          {
            this.props.values.map((value: InputGroupTooltipValue) => (
              <OverlayTrigger
                key={'tooltip'}
                placement={'bottom'}
                overlay={
                  <Tooltip id={'tooltip-bottom'}>
                    <p>{value.description}</p>
                  </Tooltip>
                }>
                <Dropdown.Item as="button" onClick={(): void => this.onSelect(value)} key={value.id}>
                  {value.label}
                </Dropdown.Item>
              </OverlayTrigger>
              )
            )
          }
        </Dropdown.Menu>
      </Dropdown>
    </div>
  }

  onSelect = (value: InputGroupTooltipValue): void => {
    this.setState({ ...this.state, selectLabel: value.label })
    this.props.onChange(value)
  }
}

export type Props = {
  onChange: (value: InputGroupTooltipValue) => void;
  label?: string;
  selectedLabel: string;
  values: InputGroupTooltipValue[];
}

export interface InputGroupTooltipValue {
  id: string;
  label: string;
  description: string;
}

export type State = {
  selectLabel: string;
}
