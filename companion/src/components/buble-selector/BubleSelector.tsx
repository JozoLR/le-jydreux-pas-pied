import './BubleSelector.scss'
import React from 'react'

export class BubleSelector extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      value: this.props.value || 0,
      max: this.props.max || 5
    }
  }

  componentDidUpdate(): void {
    if (this.props.value !== undefined && this.props.value !== this.state.value) {
      this.setState({
        value: this.props.value
      })
    }
  }

  render(): JSX.Element {
    const bubles = []

    for (let index = 1; index <= this.state.max; index++) {
      bubles.push(this.buble(index))
    }

    return (
      <div className="bubles mask">{bubles}</div>
    )
  }

  private buble = (index: number): React.ReactNode => {
    let className = ''
    if (index <= this.state.value) {
      className = 'is-active'
    }
    return <button className={`buble ${className}`} onClick={(): void => {
      this.onClickBuble(index)
    }} key={index}></button>
  }

  private onClickBuble(index: number): void {
    if (index <= this.state.max) {
      let newIndex: number
      if (index === this.state.value) {
        newIndex = index - 1
      } else {
        newIndex = index
      }
      this.setState({...this.state, value: newIndex})
      this.props.onChange(newIndex)
    }
  }
}

export type Props = {
  value?: number;
  max?: number;
  onChange: (value: number) => void;
}

interface State {
  value: number;
  max: number;
}
