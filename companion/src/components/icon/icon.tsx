import * as React from 'react'

import { ReactComponent as Bullet } from 'images/icon-bullets.svg'
import { ReactComponent as Dice } from 'images/icon-dice.svg'
import { ReactComponent as Food } from 'images/icon-food.svg'
import { ReactComponent as Gear } from 'images/icon-gear.svg'
import { ReactComponent as Knife } from 'images/icon-knife.svg'
import { ReactComponent as User } from 'images/icon-user.svg'
import { ReactComponent as Water } from 'images/icon-water.svg'
import { ReactComponent as Weapon } from 'images/icon-weapon.svg'
import { ReactComponent as Present } from 'images/present.svg'
import { ReactComponent as Cursor } from 'images/icon-cursor.svg'
import { ReactComponent as Trash } from 'images/icon-trash.svg'

const switchType = (type: string, className?: string): JSX.Element => {
  switch (type) {
    case 'bullet':
      return <Bullet className={className} />
    case 'dice':
      return <Dice className={className} />
    case 'food':
      return <Food className={className} />
    case 'gear':
      return <Gear className={className} />
    case 'knife':
      return <Knife className={className} />
    case 'user':
      return <User className={className} />
    case 'water':
      return <Water className={className} />
    case 'weapon':
      return <Weapon className={className} />
    case 'present':
      return <Present className={className} />
    case 'cursor':
      return <Cursor className={className} />
    case 'trash':
      return <Trash className={className} />
    default:
      return <Dice className={className} />
  }
}
export const Icon: React.FC<IconProps> = ({ type, className }) => (
  <div className="icon-component">
    {switchType(type, `${className}`)}
  </div>
)

export interface IconProps {
  type: string;
  className?: string;
}
