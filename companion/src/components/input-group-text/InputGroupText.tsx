import './InputGroupText.scss'
import React from 'react'

export class InputGroupText extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      value: this.props.value || ''
    }
  }

  render(): JSX.Element {
    return <div className={`input-group ${this.props.label ? 'has-label' : ''}`}>
      {this.props.label ? <label className="label">{this.props.label}</label> : null}
      <input className={`input ${this.state.value ? 'is-filled' : ''}`} placeholder={this.props.placeholder} type="text" onChange={(event): void => this.onLocalChange(event.target.value)} />
    </div>
  }

  onLocalChange(value: string): void {
    this.setState({value})
    this.props.onChange(value)
  }
}

export type Props = {
  onChange: (value: string) => void;
  label?: string;
  placeholder?: string;
  value?: string;
}

interface State {
  value: string;
}
