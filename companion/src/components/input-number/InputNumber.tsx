import './InputNumber.scss'
import React from 'react'

export class InputNumber extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      value: this.props.value || 0
    }
  }

  componentDidUpdate(): void {
    if (this.props.value !== undefined && this.props.value !== this.state.value) {
      this.setState({
        value: this.props.value
      })
    }
  }

  render(): JSX.Element {
    return <div className={`input-number mask ${this.props.isBig ? 'is-big' : null}`}>
      <button type="button" className="button-number" onClick={(): void => this.onClickButton(ButtonType.Minus)}>
        <span className="material-icons">remove</span>
      </button>
      <input className={`input ${this.state.value ? '' : 'is-zero'} die--${this.props.color}`} type="number" value={this.state.value} readOnly />
      <button type="button" className="button-number" onClick={(): void => this.onClickButton(ButtonType.Plus)}>
        <span className="material-icons">add</span>
      </button>
    </div>
  }

  onClickButton = (type: ButtonType): void => {
    let newValue: number = this.state.value

    switch (type) {
      case ButtonType.Minus:
        newValue--
        break
      case ButtonType.Plus:
        newValue++
        break
    }

    if (newValue < 0) {
      newValue = 0
    }

    if (this.props.max && newValue > this.props.max) {
      return
    }

    this.setState({ ...this.state, value: newValue })
    this.props.onChange(newValue)
  }
}

export enum ButtonType {
  Minus,
  Plus
}

export type Props = {
  onChange: (value: number) => void;
  value?: number;
  max?: number;
  color?: number;
  isBig?: boolean;
}

interface State {
  value: number;
}
