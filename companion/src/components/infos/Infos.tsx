import './Infos.scss'
import React from 'react'
import { Player } from 'domain/player'
import { User } from '../../domain/user'
import portrait from 'images/portrait.jpg'
import { Link } from 'react-router-dom'

export default class Infos extends React.Component<InfosProps> {
  render(): JSX.Element {
    return (
      <div className="player-infos">
        <img className="portrait" src={portrait} alt="" />
        <div className="right">
          <div className="top">
            <div className="info-name">
              <h2 className="name title-2">{this.props.player.name}</h2>
            </div>
            <h3 className="subtitle">{this.props.player.gameClass.label}</h3>
          </div>
          <div className="bottom">
            {
              this.props.player.experience >= 5 &&
              <div className="upgrade-button" onClick={this.props.openModal}>
                <span className="material-icons">add</span>
                <span>Upgrade</span>
              </div>
            }
            <Link to={`/game/${this.props.gameId}`} onClick={this.props.unloadPlayer} className="button">
              <span className="material-icons">arrow_back</span> Back
            </Link>
            <Link to={`/game/${this.props.gameId}/map`} onClick={this.props.unloadPlayer} className="button" target="_blank">
              <span className="material-icons">map</span> Map
            </Link>
            {
              this.props.user && this.props.user.name &&
              <>
                <button className="button" onClick={this.props.logout}>
                  <span className="material-icons">exit_to_app</span> Logout
                </button>
              </>
            }
          </div>
        </div>
      </div>
    )
  }
}

export interface InfosState {
  player: Player;
  user: User;
  gameId: string;
  openModal: () => {};
}

export interface InfosAction {
  unloadPlayer: () => void;
  logout: () => void;
}

export type InfosProps = InfosState & InfosAction;
