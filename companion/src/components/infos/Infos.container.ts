import { connect } from 'react-redux'
import { State } from '../../store/reducers'
import Infos, { InfosState, InfosAction } from 'components/infos/Infos'
import { unloadPlayer } from 'store/pages/player/player.action'
import { logout } from '../../store/auth/auth.action'

function mapStateToDispatch(state: State, ownProps: any): InfosState {
  return {
    player: state.player.player,
    user: state.auth.user,
    openModal: ownProps.openModal,
    gameId: ownProps.gameId
  }
}

const mapDispatchToProps: InfosAction = {
  unloadPlayer: unloadPlayer,
  logout
}

export default connect(mapStateToDispatch, mapDispatchToProps)(Infos)
