import './Frame.scss'
import React from 'react'

export class Frame extends React.Component<Props> {
  render(): JSX.Element {
    return (
      <div className={`frame ${this.props.className ? this.props.className : ''} ${this.props.title ? 'has-title' : ''}`}>
        {
          this.props.title &&
          <div className="frame__title">
            <h3 className="inner text-main"><span className="material-icons">fiber_manual_record</span> {this.props.title}</h3>
          </div>
        }
        <div className="frame__inner">
          <div className="frame__bg" />
          <div className="frame__content">
            {this.props.children}
          </div>
        </div>
      </div>
    )
  }
}

export type Props = {
  children: JSX.Element | JSX.Element[];
  title?: string;
  className?: string;
}
