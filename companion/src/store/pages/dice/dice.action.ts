export enum DiceActionTypes {
  UPDATE_DICE_VALUE = 'dice/update',
}

export function updateDice(value: number, dice: string): UpdateDiceAction {
  return {
    type: DiceActionTypes.UPDATE_DICE_VALUE,
    payload: { value, dice }
  }
}

export interface UpdateDiceAction {
  type: DiceActionTypes.UPDATE_DICE_VALUE;
  payload: {
    value: number;
    dice: string;
  };
}

export type DiceAction = UpdateDiceAction;
