import { DiceAction, DiceActionTypes } from './dice.action'
import { DiceType, DiceModel } from '../../../domain/dice.model'

export const initialDiceState: DiceState = {
  dice: {
    attributs: 0,
    skills: 0,
    stuff: 0
  }
}

export default function diceReducer(state: DiceState = initialDiceState, action: DiceAction): DiceState {
  const newState = { ...state }

  switch (action.type) {
  case DiceActionTypes.UPDATE_DICE_VALUE:
    switch (action.payload.dice) {
    case DiceType.ATTRIBUT:
      newState.dice = {...newState.dice, attributs: action.payload.value}
      break
    case DiceType.SKILL:
      newState.dice = {...newState.dice, skills: action.payload.value}
      break
    case DiceType.STUFF:
      newState.dice = {...newState.dice, stuff: action.payload.value}
      break
    }
    break
  }
  return newState
}

export interface DiceState {
  dice: DiceModel;
}

