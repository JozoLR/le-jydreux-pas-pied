import {combineEpics, Epic} from 'redux-observable'
import {catchError, filter, map, switchMap} from 'rxjs/operators'
import {
  CreateGameAction,
  createGameKo,
  createGameOk,
  CreatePlayerAction,
  createPlayerFailed,
  HomeAction,
  HomeActionTypes,
  loadGamesKo,
  loadGamesOk
} from './home.action'
import {State} from '../../reducers'
import {from, of} from 'rxjs'
import {isOfType} from 'typesafe-actions'
import {get, put} from 'api/api.service'
import {PlayerToCreateResource} from 'api/resources/playerToCreate.resource'
import {createPlayerOk} from '../../game/game.action'

const createPlayerEpic: Epic<HomeAction, any, State> =
  (
    action$
  ) =>
    action$.pipe(
      filter(isOfType(HomeActionTypes.CREATE_PLAYER)),
      switchMap((action: CreatePlayerAction) =>
        from(put('player', PlayerToCreateResource.build(action.payload.player))).pipe(
          map(response => createPlayerOk(response.data, action.payload.player.name)),
          catchError(() => of(createPlayerFailed()))
        )
      )
    )

const loadGamesEpic: Epic<HomeAction, HomeAction, State> = (
  action$
) =>
  action$.pipe(
    filter(isOfType(HomeActionTypes.LOAD_GAMES)),
    switchMap(() =>
      from(get('game')).pipe(
        map(response => loadGamesOk(response.data)),
        catchError(() => of(loadGamesKo()))
      )
    )
  )

const createGameEpic: Epic<HomeAction, HomeAction, State> =
  (
    action$
  ) =>
    action$.pipe(
      filter(isOfType(HomeActionTypes.CREATE_GAME)),
      switchMap((action: CreateGameAction) =>
        from(put('game', action.payload)).pipe(
          map(response => createGameOk(response.data)),
          catchError(() => of(createGameKo()))
        )
      )
    )

export const homeEpic = combineEpics(createPlayerEpic, loadGamesEpic, createGameEpic)
