import {PlayerToCreate} from '../../../domain/player'
import {Game, GameToCreate} from '../../../domain/game'

export enum HomeActionTypes {
  CREATE_PLAYER = 'creation/creation',
  CREATE_PLAYER_FAILED = 'creation/failed',

  LOAD_GAMES = 'LOAD_GAMES',
  LOAD_GAMES_OK = 'LOAD_GAMES_OK',
  LOAD_GAMES_KO = 'LOAD_GAMES_KO',

  CREATE_GAME = 'CREATE_GAME',
  CREATE_GAME_OK = 'CREATE_GAME_OK',
  CREATE_GAME_KO = 'CREATE_GAME_KO',
}

export function createPlayer(player: PlayerToCreate): CreatePlayerAction {
  return {
    type: HomeActionTypes.CREATE_PLAYER,
    payload: {
      player: player
    }
  }
}


export function createPlayerFailed(): CreatePlayerFailedAction {
  return {
    type: HomeActionTypes.CREATE_PLAYER_FAILED
  }
}

export function createGame(game: GameToCreate): CreateGameAction {
  return {
    type: HomeActionTypes.CREATE_GAME,
    payload: game
  }
}

export function createGameOk(game: Game): CreateGameOkAction {
  return {
    type: HomeActionTypes.CREATE_GAME_OK,
    payload: game
  }
}

export function createGameKo(): CreateGameKoAction {
  return {
    type: HomeActionTypes.CREATE_GAME_KO
  }
}

export function loadGames(): LoadGamesAction {
  return {
    type: HomeActionTypes.LOAD_GAMES
  }
}

export function loadGamesOk(games: Game[]): LoadGamesOkAction {
  return {
    type: HomeActionTypes.LOAD_GAMES_OK,
    payload: games
  }
}

export function loadGamesKo(): LoadGamesKoAction {
  return {
    type: HomeActionTypes.LOAD_GAMES_KO
  }
}

export interface CreatePlayerAction {
  type: HomeActionTypes.CREATE_PLAYER;
  payload: {
    player: PlayerToCreate;
  };
}

export interface CreatePlayerFailedAction {
  type: HomeActionTypes.CREATE_PLAYER_FAILED;
}

export interface CreateGameAction {
  type: HomeActionTypes.CREATE_GAME;
  payload: GameToCreate;
}

export interface CreateGameOkAction {
  type: HomeActionTypes.CREATE_GAME_OK;
  payload: Game;
}

export interface CreateGameKoAction {
  type: HomeActionTypes.CREATE_GAME_KO;
}

export interface LoadGamesAction {
  type: HomeActionTypes.LOAD_GAMES;
}

export interface LoadGamesOkAction {
  type: HomeActionTypes.LOAD_GAMES_OK;
  payload: Game[];
}

export interface LoadGamesKoAction {
  type: HomeActionTypes.LOAD_GAMES_KO;
}

export type HomeAction =
  CreatePlayerAction
  | CreatePlayerFailedAction
  | CreateGameAction
  | CreateGameOkAction
  | CreateGameKoAction
  | LoadGamesAction
  | LoadGamesOkAction
  | LoadGamesKoAction;
