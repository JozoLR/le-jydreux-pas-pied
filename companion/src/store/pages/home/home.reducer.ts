import { HomeAction, HomeActionTypes } from './home.action'
import { Player } from '../../../domain/player'
import {Game} from '../../../domain/game'

export const initialHomeState: HomeState = {
  players: [],
  games: [],
}

export default function homeReducer(state: HomeState = initialHomeState, action: HomeAction): HomeState {
  const newState = { ...state }

  switch (action.type) {
    case HomeActionTypes.LOAD_GAMES_OK:
      newState.games = action.payload
      break
    case HomeActionTypes.CREATE_GAME_OK:
      newState.games = [...newState.games]
      newState.games.push(action.payload)
      break
  }
  return newState
}

export interface HomeState {
  players: Player[];
  games: Game[];
}
