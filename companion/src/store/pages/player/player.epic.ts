import {combineEpics, Epic} from 'redux-observable'
import {catchError, filter, map, switchMap} from 'rxjs/operators'
import {from, of} from 'rxjs'
import {isOfType} from 'typesafe-actions'
import {
  addGearKo,
  addGearOk,
  addWeaponKo,
  addWeaponOk,
  deleteGearKo,
  deleteGearOk, deleteWeaponKo, deleteWeaponOk,
  giveGearKo,
  giveGearOk,
  giveWeaponKo,
  giveWeaponOk,
  loadRollPropositionKO,
  loadRollPropositionOK,
  PlayerAction,
  PlayerActionTypes,
  playerLoaded,
  playerLoadedFailed,
  updateAttributChaos,
  updateAttributOk,
  updateGearKo,
  updateGearOk,
  updateNotesKo,
  updateNotesOk,
  updatePlayerPointKo,
  updatePlayerPointOk,
  updateResourceKo,
  updateResourceOk,
  updateSkillChaos,
  updateSkillOk,
  updateWeaponKo,
  updateWeaponOk,
  upgradePlayerKo,
  upgradePlayerOk
} from './player.action'
import {State} from '../../reducers'
import {del, get, post, put} from 'api/api.service'
import {PlayerResource} from 'api/resources/player.resource'
import {RollPropositionResource} from 'api/resources/rollProposition.resource'

const loadPlayerEpic: Epic<PlayerAction, PlayerAction, State> = (
  action$
) =>
  action$.pipe(
    filter(isOfType(PlayerActionTypes.LOAD_PLAYER)),
    switchMap(action =>
      from(get(`player/${action.payload}`)).pipe(
        map(response => playerLoaded(PlayerResource.toDomain(response.data))),
        catchError(() => of(playerLoadedFailed()))
      )
    )
  )

const updateAttributEpic: Epic<PlayerAction, PlayerAction, State> = (
  action$,
  state
) =>
  action$.pipe(
    filter(isOfType(PlayerActionTypes.UPDATE_ATTRIBUT)),
    switchMap((_action) =>
      from(post(
        'player/attribut',
        PlayerResource.buildAttributResource(_action.payload, state.value.player.player)
      )).pipe(
        map(() => updateAttributOk()),
        catchError(() => of(updateAttributChaos()))
      )
    )
  )

const updateSkillEpic: Epic<PlayerAction, PlayerAction, State> = (
  action$,
  state
) =>
  action$.pipe(
    filter(isOfType(PlayerActionTypes.UPDATE_SKILL)),
    switchMap((_action) =>
      from(post(
        'player/skill',
        PlayerResource.buildSkillResource(_action.payload, state.value.player.player)
      )).pipe(
        map(() => updateSkillOk()),
        catchError(() => of(updateSkillChaos()))
      )
    )
  )

const addWeaponEpic: Epic<PlayerAction, PlayerAction, State> = (
  action$,
  state
) =>
  action$.pipe(
    filter(isOfType(PlayerActionTypes.ADD_WEAPON)),
    switchMap((_action) =>
      from(put(
        'player/weapon',
        PlayerResource.buildWeaponResourceToAdd(_action.payload, state.value.player.player))
      ).pipe(
        map((response) => addWeaponOk(response.data)),
        catchError(() => of(addWeaponKo()))
      )
    )
  )

const updateWeaponEpic: Epic<PlayerAction, PlayerAction, State> = (
  action$,
  state
) =>
  action$.pipe(
    filter(isOfType(PlayerActionTypes.UPDATE_WEAPON)),
    switchMap((_action) =>
      from(post(
        'player/weapon',
        PlayerResource.buildWeaponResource(_action.payload, state.value.player.player))).pipe(
        map(() => updateWeaponOk()),
        catchError(() => of(updateWeaponKo()))
      )
    )
  )

const addGearEpic: Epic<PlayerAction, PlayerAction, State> = (
  action$,
  state
) =>
  action$.pipe(
    filter(isOfType(PlayerActionTypes.ADD_GEAR)),
    switchMap((_action) =>
      from(put(
        'player/gear',
        PlayerResource.buildGearResourceToAdd(_action.payload, state.value.player.player))).pipe(
        map((response) => addGearOk(response.data)),
        catchError(() => of(addGearKo()))
      )
    )
  )

const updateGearEpic: Epic<PlayerAction, PlayerAction, State> = (
  action$,
  state
) =>
  action$.pipe(
    filter(isOfType(PlayerActionTypes.UPDATE_GEAR)),
    switchMap((_action) =>
      from(post(
        'player/gear',
        PlayerResource.buildUpdateGearResource(_action.payload, state.value.player.player)
      )).pipe(
        map(() => updateGearOk()),
        catchError(() => of(updateGearKo()))
      )
    )
  )

const updateResourceEpic: Epic<PlayerAction, PlayerAction, State> = (
  action$,
  state
) =>
  action$.pipe(
    filter(isOfType(PlayerActionTypes.UPDATE_RESOURCE)),
    switchMap((_action) =>
      from(post(
        'player/resource',
        PlayerResource.buildResourceResource(_action.payload, state.value.player.player)
      )).pipe(
        map(() => updateResourceOk()),
        catchError(() => of(updateResourceKo()))
      )
    )
  )

const updatePlayerPointEpic: Epic<PlayerAction, PlayerAction, State> = (
  action$,
  state
) =>
  action$.pipe(
    filter(isOfType(PlayerActionTypes.UPDATE_PLAYER_POINT)),
    switchMap((_action) =>
      from(post(
        'player',
        PlayerResource.buildUpdatePlayerResource(_action.payload.category, _action.payload.value, state.value.player.player)
      )).pipe(
        map(() => updatePlayerPointOk()),
        catchError(() => of(updatePlayerPointKo()))
      )
    )
  )

const updateNotesEpic: Epic<PlayerAction, PlayerAction, State> = (
  action$,
  state
) =>
  action$.pipe(
    filter(isOfType(PlayerActionTypes.UPDATE_NOTES)),
    switchMap((_action) =>
      from(post('player/notes', PlayerResource.buildUpdateNotesResource(state.value.player.player.id, _action.payload)
      )).pipe(
        map(() => updateNotesOk()),
        catchError(() => of(updateNotesKo()))
      )
    )
  )

const loadRollPropositionEpic: Epic<PlayerAction, PlayerAction, State> = (
  action$
) =>
  action$.pipe(
    filter(isOfType(PlayerActionTypes.LOAD_ROLL_PROPOSITION)),
    switchMap(action =>
      from(get(`roll/proposition/${action.payload}`)).pipe(
        map(response => loadRollPropositionOK(RollPropositionResource.toDomain(response.data))),
        catchError(() => of(loadRollPropositionKO()))
      )
    )
  )

const giveWeaponEpic: Epic<PlayerAction, PlayerAction, State> = (
  action$
) =>
  action$.pipe(
    filter(isOfType(PlayerActionTypes.GIVE_WEAPON)),
    switchMap(action =>
      from(post('player/weapon/give', action.payload)).pipe(
        map(() => giveWeaponOk()),
        catchError(() => of(giveWeaponKo()))
      )
    )
  )

const giveGearEpic: Epic<PlayerAction, PlayerAction, State> = (
  action$
) =>
  action$.pipe(
    filter(isOfType(PlayerActionTypes.GIVE_GEAR)),
    switchMap(action =>
      from(post('player/gear/give', action.payload)).pipe(
        map(() => giveGearOk()),
        catchError(() => of(giveGearKo()))
      )
    )
  )

const deleteGearEpic: Epic<PlayerAction, PlayerAction, State> = (
  action$
) =>
  action$.pipe(
    filter(isOfType(PlayerActionTypes.DELETE_GEAR)),
    switchMap(action =>
      from(del('player/gear',
        PlayerResource.buildGearResourceToDelete(action.payload.gearId, action.payload.player))).pipe(
        map(() => deleteGearOk()),
        catchError(() => of(deleteGearKo()))
      )
    )
  )

const deleteWeaponEpic: Epic<PlayerAction, PlayerAction, State> = (
  action$
) =>
  action$.pipe(
    filter(isOfType(PlayerActionTypes.DELETE_WEAPON)),
    switchMap(action =>
      from(del('player/weapon',
        PlayerResource.buildWeaponResourceToDelete(action.payload.weaponId, action.payload.player))).pipe(
        map(() => deleteWeaponOk()),
        catchError(() => of(deleteWeaponKo()))
      )
    )
  )

const upgradePlayerEpic: Epic<PlayerAction, PlayerAction, State> = (
  action$
) =>
  action$.pipe(
    filter(isOfType(PlayerActionTypes.UPGRADE_PLAYER)),
    switchMap(action =>
      from(post('player/upgrade', action.payload)).pipe(
        map(() => upgradePlayerOk()),
        catchError(() => of(upgradePlayerKo()))
      )
    )
  )

export const playerEpic = combineEpics(
  loadPlayerEpic,
  updateAttributEpic,
  updateSkillEpic,
  addWeaponEpic,
  addGearEpic,
  loadRollPropositionEpic,
  updateWeaponEpic,
  updateGearEpic,
  updateResourceEpic,
  updatePlayerPointEpic,
  updateNotesEpic,
  giveWeaponEpic,
  giveGearEpic,
  upgradePlayerEpic,
  deleteGearEpic,
  deleteWeaponEpic
)
