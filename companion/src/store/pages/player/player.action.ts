import {Attribut, Gear, Player, PointType, Resource, Skill, UpgradePlayerCommand, Weapon} from '../../../domain/player'
import {RollProposition} from 'domain/rollProposition'

export enum PlayerActionTypes {
  LOAD_PLAYER = 'player/load',
  UNLOAD_PLAYER = 'player/unload',
  PLAYER_LOADED = 'player/loaded',
  PLAYER_LOADED_FAILED = 'player/failed',

  UPDATE_ATTRIBUT = 'update/attribut',
  UPDATE_ATTRIBUT_OK = 'update/attribut/ok',
  UPDATE_ATTRIBUT_CHAOS = 'update/attribut/chaos',

  UPDATE_SKILL = 'update/skill',
  UPDATE_SKILL_OK = 'update/skill/ok',
  UPDATE_SKILL_CHAOS = 'update/skill/chaos',

  ADD_WEAPON = 'add/weapon',
  ADD_WEAPON_OK = 'add/weapon/ok',
  ADD_WEAPON_KO = 'add/weapon/ko',

  UPDATE_WEAPON = 'update/weapon',
  UPDATE_WEAPON_OK = 'update/weapon/ok',
  UPDATE_WEAPON_KO = 'update/weapon/ko',

  ADD_GEAR = 'add/gear',
  ADD_GEAR_OK = 'add/gear/ok',
  ADD_GEAR_KO = 'add/gear/ko',

  UPDATE_GEAR = 'update/gear',
  UPDATE_GEAR_OK = 'update/gear/ok',
  UPDATE_GEAR_KO = 'update/gear/ko',

  UPDATE_RESOURCE = 'update/resource',
  UPDATE_RESOURCE_OK = 'update/resource/ok',
  UPDATE_RESOURCE_KO = 'update/resource/ko',

  UPDATE_PLAYER_POINT = 'UPDATE_PLAYER_POINT',
  UPDATE_PLAYER_POINT_OK = 'UPDATE_PLAYER_POINT_OK',
  UPDATE_PLAYER_POINT_KO = 'UPDATE_PLAYER_POINT_KO',

  UPDATE_NOTES = 'UPDATE_NOTES',
  UPDATE_NOTES_OK = 'UPDATE_NOTES_OK',
  UPDATE_NOTES_KO = 'UPDATE_NOTES_KO',

  LOAD_ROLL_PROPOSITION = 'player/rolls/proposition/load',
  LOAD_ROLL_PROPOSITION_OK = 'player/rolls/proposition/load/ok',
  LOAD_ROLL_PROPOSITION_KO = 'player/rolls/proposition/load/ko',
  LOAD_ROLL_PROPOSITION_EVENT = 'player/rolls/proposition/event',

  ROLL_DICE_OK = 'game/roll/ok',
  ROLL_DICE_KO = 'game/roll/ko',

  REROLL_DICE_OK = 'game/roll/ok',
  REROLL_DICE_KO = 'game/roll/ko',

  GIVE_WEAPON = 'GIVE_WEAPON',
  GIVE_WEAPON_OK = 'GIVE_WEAPON_OK',
  GIVE_WEAPON_KO = 'GIVE_WEAPON_KO',

  DELETE_WEAPON = 'DELETE_WEAPON',
  DELETE_WEAPON_OK = 'DELETE_WEAPON_OK',
  DELETE_WEAPON_KO = 'DELETE_WEAPON_KO',

  GIVE_GEAR = 'GIVE_GEAR',
  GIVE_GEAR_OK = 'GIVE_GEAR_OK',
  GIVE_GEAR_KO = 'GIVE_GEAR_KO',

  DELETE_GEAR = 'DELETE_GEAR',
  DELETE_GEAR_OK = 'DELETE_GEAR_OK',
  DELETE_GEAR_KO = 'DELETE_GEAR_KO',

  UPGRADE_PLAYER = 'UPGRADE_PLAYER',
  UPGRADE_PLAYER_OK = 'UPGRADE_PLAYER_OK',
  UPGRADE_PLAYER_KO = 'UPGRADE_PLAYER_KO',
}

export function loadPlayer(playerId: string): LoadPlayerAction {
  return {
    type: PlayerActionTypes.LOAD_PLAYER,
    payload: playerId
  }
}

export function unloadPlayer(): UnloadPlayerAction {
  return {
    type: PlayerActionTypes.UNLOAD_PLAYER
  }
}

export function playerLoaded(player: Player): PlayerLoadedAction {
  return {
    type: PlayerActionTypes.PLAYER_LOADED,
    payload: {
      player
    }
  }
}

export function playerLoadedFailed(): PlayerLoadedFailedAction {
  return {
    type: PlayerActionTypes.PLAYER_LOADED_FAILED
  }
}

export function updateAttribut(attribut: Attribut): UpdateAttributAction {
  return {
    type: PlayerActionTypes.UPDATE_ATTRIBUT,
    payload: attribut
  }
}

export function updateAttributOk(): UpdateAttributOkAction {
  return {
    type: PlayerActionTypes.UPDATE_ATTRIBUT_OK
  }
}

export function updateAttributChaos(): UpdateAttributChaosAction {
  return {
    type: PlayerActionTypes.UPDATE_ATTRIBUT_CHAOS
  }
}

export function updateSkill(skill: Skill): UpdateSkillAction {
  return {
    type: PlayerActionTypes.UPDATE_SKILL,
    payload: skill
  }
}


export function updateSkillOk(): UpdateSkillOkAction {
  return {
    type: PlayerActionTypes.UPDATE_SKILL_OK
  }
}

export function updateSkillChaos(): UpdateSkillChaosAction {
  return {
    type: PlayerActionTypes.UPDATE_SKILL_CHAOS
  }
}

export function addWeapon(weapon: Weapon): AddWeaponAction {
  return {
    type: PlayerActionTypes.ADD_WEAPON,
    payload: weapon
  }
}

export function addWeaponOk(id: string): AddWeaponOkAction {
  return {
    type: PlayerActionTypes.ADD_WEAPON_OK,
    payload: id
  }
}

export function addWeaponKo(): AddWeaponKoAction {
  return {
    type: PlayerActionTypes.ADD_WEAPON_KO
  }
}

export function updateWeapon(weapon: Weapon): UpdateWeaponAction {
  return {
    type: PlayerActionTypes.UPDATE_WEAPON,
    payload: weapon
  }
}

export function updateWeaponOk(): UpdateWeaponOkAction {
  return {
    type: PlayerActionTypes.UPDATE_WEAPON_OK,
  }
}

export function updateWeaponKo(): UpdateWeaponKoAction {
  return {
    type: PlayerActionTypes.UPDATE_WEAPON_KO,
  }
}

export function addGear(gear: Gear): AddGearAction {
  return {
    type: PlayerActionTypes.ADD_GEAR,
    payload: gear
  }
}

export function addGearOk(id: string): AddGearOkAction {
  return {
    type: PlayerActionTypes.ADD_GEAR_OK,
    payload: id
  }
}

export function addGearKo(): AddGearKoAction {
  return {
    type: PlayerActionTypes.ADD_GEAR_KO
  }
}

export function updateGear(gear: Gear): UpdateGearAction {
  return {
    type: PlayerActionTypes.UPDATE_GEAR,
    payload: gear
  }
}

export function updateGearOk(): UpdateGearOkAction {
  return {
    type: PlayerActionTypes.UPDATE_GEAR_OK,
  }
}

export function updateGearKo(): UpdateGearKoAction {
  return {
    type: PlayerActionTypes.UPDATE_GEAR_KO,
  }
}

export function updateResource(resource: Resource): UpdateResourceAction {
  return {
    type: PlayerActionTypes.UPDATE_RESOURCE,
    payload: resource
  }
}

export function updateResourceOk(): UpdateResourceOkAction {
  return {
    type: PlayerActionTypes.UPDATE_RESOURCE_OK,
  }
}

export function updateResourceKo(): UpdateResourceKoAction {
  return {
    type: PlayerActionTypes.UPDATE_RESOURCE_KO,
  }
}

export function updatePlayerPoint(category: PointType, value: number): UpdatePlayerPointAction {
  return {
    type: PlayerActionTypes.UPDATE_PLAYER_POINT,
    payload: {
      category,
      value
    }
  }
}

export function updatePlayerPointOk(): UpdatePlayerPointOkAction {
  return {
    type: PlayerActionTypes.UPDATE_PLAYER_POINT_OK,
  }
}

export function updatePlayerPointKo(): UpdatePlayerPointKoAction {
  return {
    type: PlayerActionTypes.UPDATE_PLAYER_POINT_KO,
  }
}

export function updateNotes(notes: string): UpdateNotesAction {
  return {
    type: PlayerActionTypes.UPDATE_NOTES,
    payload: notes
  }
}

export function updateNotesOk(): UpdateNotesOkAction {
  return {
    type: PlayerActionTypes.UPDATE_NOTES_OK,
  }
}

export function updateNotesKo(): UpdateNotesKoAction {
  return {
    type: PlayerActionTypes.UPDATE_NOTES_KO
  }
}

export function loadRollProposition(playerId: string): LoadRollProposition {
  return {
    type: PlayerActionTypes.LOAD_ROLL_PROPOSITION,
    payload: playerId
  }
}

export function loadRollPropositionOK(rollProposition: RollProposition): LoadRollPropositionOK {
  return {
    type: PlayerActionTypes.LOAD_ROLL_PROPOSITION_OK,
    payload: rollProposition
  }
}

export function loadRollPropositionKO(): LoadRollPropositionKO {
  return {
    type: PlayerActionTypes.LOAD_ROLL_PROPOSITION_KO
  }
}

export function rollDiceOk(): RollDiceOkAction {
  return {
    type: PlayerActionTypes.ROLL_DICE_OK
  }
}

export function rollDiceKo(): RollDiceKoAction {
  return {
    type: PlayerActionTypes.ROLL_DICE_KO
  }
}

export function rerollDiceOk(): RerollDiceOkAction {
  return {
    type: PlayerActionTypes.REROLL_DICE_OK
  }
}

export function rerollDiceKo(): RerollDiceKoAction {
  return {
    type: PlayerActionTypes.REROLL_DICE_KO
  }
}

export function giveWeapon(weaponId: string, player: Player, beneficiaryId: string): GiveWeaponAction {
  return {
    type: PlayerActionTypes.GIVE_WEAPON,
    payload: {
      weaponId,
      playerId: player.id,
      gameId: player.gameId,
      beneficiaryId
    }
  }
}

export function giveWeaponOk(): GiveWeaponOkAction {
  return {
    type: PlayerActionTypes.GIVE_WEAPON_OK
  }
}

export function giveWeaponKo(): GiveWeaponKoAction {
  return {
    type: PlayerActionTypes.GIVE_WEAPON_KO
  }
}

export function deleteWeapon(weaponId: string, player: Player): DeleteWeaponAction {
  return {
    type: PlayerActionTypes.DELETE_WEAPON,
    payload: {
      weaponId,
      player
    }
  }
}

export function deleteWeaponOk(): DeleteWeaponOkAction {
  return {
    type: PlayerActionTypes.DELETE_WEAPON_OK
  }
}

export function deleteWeaponKo(): DeleteWeaponKoAction {
  return {
    type: PlayerActionTypes.DELETE_WEAPON_KO
  }
}

export function giveGear(gearId: string, player: Player, beneficiaryId: string): GiveGearAction {
  return {
    type: PlayerActionTypes.GIVE_GEAR,
    payload: {
      gearId,
      playerId: player.id,
      gameId: player.gameId,
      beneficiaryId
    }
  }
}

export function giveGearOk(): GiveGearOkAction {
  return {
    type: PlayerActionTypes.GIVE_GEAR_OK
  }
}

export function giveGearKo(): GiveGearKoAction {
  return {
    type: PlayerActionTypes.GIVE_GEAR_KO
  }
}

export function deleteGear(gearId: string, player: Player): DeleteGearAction {
  return {
    type: PlayerActionTypes.DELETE_GEAR,
    payload: {
      gearId,
      player
    }
  }
}

export function deleteGearOk(): DeleteGearOkAction {
  return {
    type: PlayerActionTypes.DELETE_GEAR_OK
  }
}

export function deleteGearKo(): DeleteGearKoAction {
  return {
    type: PlayerActionTypes.DELETE_GEAR_KO
  }
}

export function upgradePlayer(upgradePlayerCommand: UpgradePlayerCommand): UpgradePlayerAction {
  return {
    type: PlayerActionTypes.UPGRADE_PLAYER,
    payload: upgradePlayerCommand
  }
}

export function upgradePlayerOk(): UpgradePlayerOkAction {
  return {
    type: PlayerActionTypes.UPGRADE_PLAYER_OK
  }
}

export function upgradePlayerKo(): UpgradePlayerKoAction {
  return {
    type: PlayerActionTypes.UPGRADE_PLAYER_KO
  }
}

export interface LoadPlayerAction {
  type: PlayerActionTypes.LOAD_PLAYER;
  payload: string;
}

export interface UnloadPlayerAction {
  type: PlayerActionTypes.UNLOAD_PLAYER;
}

export interface PlayerLoadedAction {
  type: PlayerActionTypes.PLAYER_LOADED;
  payload: {
    player: Player;
  };
}

export interface PlayerLoadedFailedAction {
  type: PlayerActionTypes.PLAYER_LOADED_FAILED;
}

export interface UpdateAttributAction {
  type: PlayerActionTypes.UPDATE_ATTRIBUT;
  payload: Attribut;
}

export interface UpdateAttributOkAction {
  type: PlayerActionTypes.UPDATE_ATTRIBUT_OK;
}

export interface UpdateAttributChaosAction {
  type: PlayerActionTypes.UPDATE_ATTRIBUT_CHAOS;
}

export interface UpdateSkillAction {
  type: PlayerActionTypes.UPDATE_SKILL;
  payload: Skill;
}

export interface UpdateSkillOkAction {
  type: PlayerActionTypes.UPDATE_SKILL_OK;
}

export interface UpdateSkillChaosAction {
  type: PlayerActionTypes.UPDATE_SKILL_CHAOS;
}

export interface AddWeaponAction {
  type: PlayerActionTypes.ADD_WEAPON;
  payload: Weapon;
}

export interface AddWeaponOkAction {
  type: PlayerActionTypes.ADD_WEAPON_OK;
  payload: string;
}

export interface AddWeaponKoAction {
  type: PlayerActionTypes.ADD_WEAPON_KO;
}

export interface UpdateWeaponAction {
  type: PlayerActionTypes.UPDATE_WEAPON;
  payload: Weapon;
}

export interface UpdateWeaponOkAction {
  type: PlayerActionTypes.UPDATE_WEAPON_OK;
}

export interface UpdateWeaponKoAction {
  type: PlayerActionTypes.UPDATE_WEAPON_KO;
}

export interface AddGearAction {
  type: PlayerActionTypes.ADD_GEAR;
  payload: Gear;
}

export interface AddGearOkAction {
  type: PlayerActionTypes.ADD_GEAR_OK;
  payload: string;
}

export interface AddGearKoAction {
  type: PlayerActionTypes.ADD_GEAR_KO;
}

export interface UpdateGearAction {
  type: PlayerActionTypes.UPDATE_GEAR;
  payload: Gear;
}

export interface UpdateGearOkAction {
  type: PlayerActionTypes.UPDATE_GEAR_OK;
}

export interface UpdateGearKoAction {
  type: PlayerActionTypes.UPDATE_GEAR_KO;
}

export interface UpdateResourceAction {
  type: PlayerActionTypes.UPDATE_RESOURCE;
  payload: Resource;
}

export interface UpdateResourceOkAction {
  type: PlayerActionTypes.UPDATE_RESOURCE_OK;
}

export interface UpdateResourceKoAction {
  type: PlayerActionTypes.UPDATE_RESOURCE_KO;
}

export interface UpdatePlayerPointAction {
  type: PlayerActionTypes.UPDATE_PLAYER_POINT;
  payload: {
    category: PointType;
    value: number;
  };
}

export interface UpdatePlayerPointOkAction {
  type: PlayerActionTypes.UPDATE_PLAYER_POINT_OK;
}

export interface UpdatePlayerPointKoAction {
  type: PlayerActionTypes.UPDATE_PLAYER_POINT_KO;
}

export interface UpdateNotesAction {
  type: PlayerActionTypes.UPDATE_NOTES;
  payload: string;
}

export interface UpdateNotesOkAction {
  type: PlayerActionTypes.UPDATE_NOTES_OK;
}

export interface UpdateNotesKoAction {
  type: PlayerActionTypes.UPDATE_NOTES_KO;
}

export interface LoadRollProposition {
  type: PlayerActionTypes.LOAD_ROLL_PROPOSITION;
  payload: string;
}

export interface LoadRollPropositionOK {
  type: PlayerActionTypes.LOAD_ROLL_PROPOSITION_OK;
  payload: RollProposition;
}

export interface LoadRollPropositionKO {
  type: PlayerActionTypes.LOAD_ROLL_PROPOSITION_KO;
}

export interface LoadRollPropositionEvent {
  type: PlayerActionTypes.LOAD_ROLL_PROPOSITION_EVENT;
  payload: RollProposition;
}

export interface RollDiceOkAction {
  type: PlayerActionTypes.ROLL_DICE_OK;
}

export interface RollDiceKoAction {
  type: PlayerActionTypes.ROLL_DICE_KO;
}

export interface RerollDiceOkAction {
  type: PlayerActionTypes.REROLL_DICE_OK;
}

export interface RerollDiceKoAction {
  type: PlayerActionTypes.REROLL_DICE_KO;
}

export interface GiveWeaponAction {
  type: PlayerActionTypes.GIVE_WEAPON;
  payload: {
    weaponId: string;
    playerId: string;
    gameId: string;
    beneficiaryId: string;
  };
}

export interface GiveWeaponOkAction {
  type: PlayerActionTypes.GIVE_WEAPON_OK;
}

export interface GiveWeaponKoAction {
  type: PlayerActionTypes.GIVE_WEAPON_KO;
}

export interface GiveGearAction {
  type: PlayerActionTypes.GIVE_GEAR;
  payload: {
    gearId: string;
    playerId: string;
    gameId: string;
    beneficiaryId: string;
  };
}

export interface GiveGearOkAction {
  type: PlayerActionTypes.GIVE_GEAR_OK;
}

export interface GiveGearKoAction {
  type: PlayerActionTypes.GIVE_GEAR_KO;
}

export interface DeleteGearAction {
  type: PlayerActionTypes.DELETE_GEAR;
  payload: {
    gearId: string;
    player: Player;
  };
}

export interface DeleteGearOkAction {
  type: PlayerActionTypes.DELETE_GEAR_OK;
}

export interface DeleteGearKoAction {
  type: PlayerActionTypes.DELETE_GEAR_KO;
}

export interface DeleteWeaponAction {
  type: PlayerActionTypes.DELETE_WEAPON;
  payload: {
    weaponId: string;
    player: Player;
  };
}

export interface DeleteWeaponOkAction {
  type: PlayerActionTypes.DELETE_WEAPON_OK;
}

export interface DeleteWeaponKoAction {
  type: PlayerActionTypes.DELETE_WEAPON_KO;
}

export interface UpgradePlayerAction {
  type: PlayerActionTypes.UPGRADE_PLAYER;
  payload: UpgradePlayerCommand;
}

export interface UpgradePlayerOkAction {
  type: PlayerActionTypes.UPGRADE_PLAYER_OK;
}

export interface UpgradePlayerKoAction {
  type: PlayerActionTypes.UPGRADE_PLAYER_KO;
}


export type PlayerAction =
  LoadPlayerAction
  | UnloadPlayerAction
  | PlayerLoadedAction
  | PlayerLoadedFailedAction
  | UpdateAttributAction
  | UpdateAttributOkAction
  | UpdateAttributChaosAction
  | UpdateSkillAction
  | UpdateSkillOkAction
  | UpdateSkillChaosAction
  | AddWeaponAction
  | AddGearAction
  | LoadRollProposition
  | LoadRollPropositionOK
  | LoadRollPropositionKO
  | LoadRollPropositionEvent
  | AddGearOkAction
  | AddGearKoAction
  | AddWeaponOkAction
  | AddWeaponKoAction
  | UpdateWeaponAction
  | UpdateWeaponOkAction
  | UpdateWeaponKoAction
  | UpdateGearAction
  | UpdateGearOkAction
  | UpdateGearKoAction
  | UpdateResourceAction
  | UpdateResourceOkAction
  | UpdateResourceKoAction
  | UpdatePlayerPointAction
  | UpdatePlayerPointOkAction
  | UpdatePlayerPointKoAction
  | RollDiceOkAction
  | RollDiceKoAction
  | RerollDiceOkAction
  | RerollDiceKoAction
  | UpdateNotesAction
  | UpdateNotesOkAction
  | UpdateNotesKoAction
  | GiveWeaponAction
  | GiveWeaponOkAction
  | GiveWeaponKoAction
  | GiveGearAction
  | GiveGearOkAction
  | GiveGearKoAction
  | DeleteGearAction
  | DeleteGearOkAction
  | DeleteGearKoAction
  | DeleteWeaponAction
  | DeleteWeaponOkAction
  | DeleteWeaponKoAction
  | UpgradePlayerAction
  | UpgradePlayerOkAction
  | UpgradePlayerKoAction
