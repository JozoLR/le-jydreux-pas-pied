import {PlayerAction, PlayerActionTypes} from './player.action'
import {Attribut, Gear, Player, Resource, Skill, Weapon} from '../../../domain/player'
import {RollProposition} from 'domain/rollProposition'

export const initialPlayerState: PlayerState = {
  player: {
    id: '',
    name: '',
    attributs: [],
    skills: [],
    weapons: [],
    gears: [],
    resources: [],
    gameClass: {
      id: '',
      label: '',
      skillId: ''
    },
    talents: [],
    experience: 0,
    rot: 0,
    mutation: 0,
    lastRoll: undefined,
    gameId: '',
    notes: ''
  },
  rollProposition: {
    playerId: '',
    skillPropositions: []
  }
}

export default function playerReducer(state: PlayerState = initialPlayerState, action: PlayerAction): PlayerState {
  let newState = {...state}

  switch (action.type) {
    case PlayerActionTypes.PLAYER_LOADED:
      newState.player = {...action.payload.player}
      break
    case PlayerActionTypes.UPDATE_ATTRIBUT:
      newState.player = {...newState.player}
      const stateAttribut: Attribut | undefined = [...newState.player.attributs].find(
        (attribut: Attribut) => attribut.label == action.payload.label
      )
      if (stateAttribut) {
        stateAttribut.actual = (action.payload).actual
      }
      break
    case PlayerActionTypes.UPDATE_SKILL:
      newState.player = {...newState.player}
      const stateSkill: Skill | undefined = [...newState.player.skills].find(
        (skill: Skill) => skill.label == (action.payload).label
      )
      if (stateSkill) {
        stateSkill.actual = (action.payload).actual
      }
      break
    case PlayerActionTypes.ADD_WEAPON:
      newState.player = {...newState.player}
      newState.player.weapons.push(action.payload)
      break
    case PlayerActionTypes.ADD_WEAPON_OK:
      newState.player.weapons[newState.player.weapons.length - 1].id = action.payload
      break
    case PlayerActionTypes.UPDATE_WEAPON:
      const weapons = newState.player.weapons
      const stateWeapon: Weapon | undefined = weapons.find(
        (weapon: Weapon) => weapon.id === action.payload.id)
      if (stateWeapon) {
        stateWeapon.malus = action.payload.malus
      }
      newState.player = {
        ...newState.player,
        weapons: [...weapons]
      }
      break
    case PlayerActionTypes.ADD_GEAR:
      newState.player = {...newState.player}
      newState.player.gears.push(action.payload)
      break
    case PlayerActionTypes.ADD_GEAR_OK:
      newState.player.gears[newState.player.gears.length - 1].id = action.payload
      break
    case PlayerActionTypes.UPDATE_GEAR:
      const gears = newState.player.gears
      const stateGear: Gear | undefined = gears.find(
        (gear: Gear) => gear.id === action.payload.id)
      if (stateGear) {
        stateGear.malus = action.payload.malus
      }
      newState.player = {
        ...newState.player,
        gears: [...gears]
      }
      break
    case PlayerActionTypes.UPDATE_RESOURCE:
      const resources = newState.player.resources
      const stateResources: Resource | undefined = resources.find(
        (resource: Resource) => resource.id === action.payload.id)
      if (stateResources) {
        stateResources.quantity = action.payload.quantity
      }
      newState.player = {
        ...newState.player,
        resources: [...resources]
      }
      break
    case PlayerActionTypes.UPDATE_PLAYER_POINT:
      newState.player[action.payload.category] = action.payload.value
      break
    case PlayerActionTypes.LOAD_ROLL_PROPOSITION_OK:
      newState.player = {...newState.player}
      newState.rollProposition = {
        playerId: action.payload.playerId,
        skillPropositions: action.payload.skillPropositions
      }
      break
    case PlayerActionTypes.LOAD_ROLL_PROPOSITION_EVENT:
      if (action.payload.playerId === state.player.id) {
        newState.player = {...newState.player}
        newState.rollProposition = {
          playerId: action.payload.playerId,
          skillPropositions: action.payload.skillPropositions
        }
      }
      break
    case PlayerActionTypes.UNLOAD_PLAYER:
      newState = initialPlayerState
      break
  }

  return newState
}

export interface PlayerState {
  player: Player;
  rollProposition: RollProposition;
}
