import { combineReducers } from 'redux'
import homeReducer, { initialHomeState, HomeState } from './pages/home/home.reducer'
import playerReducer, { PlayerState, initialPlayerState } from './pages/player/player.reducer'
import gameReducer, { GameState, initialGameState } from './game/game.reducer'
import diceReducer, { DiceState, initialDiceState } from './pages/dice/dice.reducer'
import authReducer, {AuthState, initialAuthState} from './auth/auth.reducer'
import notificationReducer, {initialNotificationState, NotificationState} from './notification/notification.reducer'
import mapReducer, { MapState, initialMapState } from 'store/map/map.reducer'

export interface State {
  home: HomeState;
  player: PlayerState;
  game: GameState;
  dice: DiceState;
  auth: AuthState;
  notification: NotificationState;
  map: MapState;
}

export const initialState: State = {
  home: initialHomeState,
  player: initialPlayerState,
  game: initialGameState,
  dice: initialDiceState,
  auth: initialAuthState,
  notification: initialNotificationState,
  map: initialMapState
}

export const rootReducer = combineReducers({
  home: homeReducer,
  player: playerReducer,
  game: gameReducer,
  dice: diceReducer,
  auth: authReducer,
  notification: notificationReducer,
  map: mapReducer
})
