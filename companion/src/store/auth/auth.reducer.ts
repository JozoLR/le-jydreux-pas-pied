import {AuthActions, AuthActionTypes} from './auth.action'
import {User} from '../../domain/user'
import Auth0Client from '@auth0/auth0-spa-js/dist/typings/Auth0Client'

export const initialAuthState: AuthState = {
  user: {
    email: '',
    email_verified: false,
    family_name: '',
    given_name: '',
    locale: '',
    name: '',
    nickname: '',
    picture: '',
    sub: '',
    updated_at: '',
  },
  isAuthenticated: false,
  client: null,
}

export default function authReducer(state: AuthState = initialAuthState, action: AuthActions): AuthState {
  switch (action.type) {
    case AuthActionTypes.AUTHORIZE_OK:
      state = {
        ...state,
        client: action.payload.client,
        user: action.payload.user,
        isAuthenticated: true
      }
      break
  }
  return state
}

export interface AuthState {
  user: User;
  isAuthenticated: boolean;
  client: Auth0Client | null;
}
