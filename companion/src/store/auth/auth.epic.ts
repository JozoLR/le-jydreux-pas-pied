import {catchError, filter, map, switchMap} from 'rxjs/operators'
import {isOfType} from 'typesafe-actions'
import {from, of} from 'rxjs'
import {combineEpics, Epic} from 'redux-observable'
import {State} from '../reducers'
import {AuthActions, AuthActionTypes, authorizeKo, authorizeOk, logoutKo, logoutOk} from './auth.action'
import createAuth0Client from '@auth0/auth0-spa-js'
import Auth0Client from '@auth0/auth0-spa-js/dist/typings/Auth0Client'
import {User} from '../../domain/user'

const config: AuthConfig = {
  domain: process.env.REACT_APP_AUTH0_DOMAIN || '',
  clientId: process.env.REACT_APP_AUTH0_CLIENT_ID || '',
  redirectUri: window.location.origin,
  audience: process.env.REACT_APP_AUTH0_AUDIENCE || 'http://localhost:8080'
}

const authorizeEpic: Epic<AuthActions, AuthActions, State> = (
  action$
) =>
  action$.pipe(
    filter(isOfType(AuthActionTypes.AUTHORIZE)),
    switchMap(() =>
      from(getAuthClientAndUser()).pipe(
        map(authSetup => authorizeOk(authSetup.client, authSetup.user)),
        catchError(() => of(authorizeKo()))
      )
    )
  )

const logoutEpic: Epic<AuthActions, AuthActions, State> = (
  action$,
  state
) =>
  action$.pipe(
    filter(isOfType(AuthActionTypes.LOGOUT)),
    switchMap(() => {
        return from(logout(state.value.auth.client!)).pipe(
          map(() => logoutOk()),
          catchError(() => of(logoutKo()))
        )
      }
    )
  )


async function getAuthClientAndUser(): Promise<AuthSetup> {
  const client = await createAuth0Client({
    domain: config.domain,
    client_id: config.clientId,
    cacheLocation: 'localstorage',
    useRefreshTokens: true,
    audience: config.audience
  })

  if (!await client.isAuthenticated()) {
    await client.loginWithRedirect({
      redirect_uri: config.redirectUri
    })
  }

  if (await client.isAuthenticated()) {
    const query = window.location.search
    if (query.includes('code=') && query.includes('state=')) {
      await client.handleRedirectCallback()
      window.history.replaceState({}, document.title, '/')
    }
  }

  const user = await client.getUser()
  return {client, user}
}

async function logout(client: Auth0Client): Promise<void> {
  client.logout({client_id: config.clientId})
}

export const authEpic = combineEpics(authorizeEpic, logoutEpic)

interface AuthConfig {
  domain: string;
  clientId: string;
  audience: string;
  redirectUri: string;
}

interface AuthSetup {
  client: Auth0Client;
  user: User;
}
