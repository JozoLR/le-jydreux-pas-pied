import {User} from '../../domain/user'
import Auth0Client from '@auth0/auth0-spa-js/dist/typings/Auth0Client'

export enum AuthActionTypes {
  AUTHORIZE = 'AUTHORIZE',
  AUTHORIZE_OK = 'AUTHORIZE_OK',
  AUTHORIZE_KO = 'AUTHORIZE_KO',

  LOGOUT = 'LOGOUT',
  LOGOUT_OK = 'LOGOUT_OK',
  LOGOUT_KO = 'LOGOUT_KO',
}

export function authorize(): AuthorizeAction {
  return {
    type: AuthActionTypes.AUTHORIZE
  }
}

export function authorizeOk(authClient: Auth0Client, user: User): AuthorizeOkAction {
  return {
    type: AuthActionTypes.AUTHORIZE_OK,
    payload: {
      client: authClient,
      user
    }
  }
}

export function authorizeKo(): AuthorizeKoAction {
  return {
    type: AuthActionTypes.AUTHORIZE_KO
  }
}

export function logout(): LogoutAction {
  return {
    type: AuthActionTypes.LOGOUT
  }
}

export function logoutOk(): LogoutOkAction {
  return {
    type: AuthActionTypes.LOGOUT_OK
  }
}

export function logoutKo(): LogoutKoAction {
  return {
    type: AuthActionTypes.LOGOUT_KO
  }
}

export interface AuthorizeAction {
  type: AuthActionTypes.AUTHORIZE;
}

export interface AuthorizeOkAction {
  type: AuthActionTypes.AUTHORIZE_OK;
  payload: {
    client: Auth0Client;
    user: User;
  };
}

export interface AuthorizeKoAction {
  type: AuthActionTypes.AUTHORIZE_KO;
}

export interface LogoutAction {
  type: AuthActionTypes.LOGOUT;
}

export interface LogoutOkAction {
  type: AuthActionTypes.LOGOUT_OK;
}

export interface LogoutKoAction {
  type: AuthActionTypes.LOGOUT_KO;
}

export type AuthActions =
  AuthorizeAction
  | AuthorizeOkAction
  | AuthorizeKoAction
  | LogoutAction
  | LogoutOkAction
  | LogoutKoAction
