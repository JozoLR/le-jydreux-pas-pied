import { combineEpics, createEpicMiddleware } from 'redux-observable'
import { Action } from 'redux'
import { State } from './reducers'
import { homeEpic } from './pages/home/home.epic'
import { playerEpic } from './pages/player/player.epic'
import { gameEpic } from './game/game.epic'
import { authEpic } from './auth/auth.epic'
import { notificationEpic } from './notification/notification.epic'
import { mapEpic } from 'store/map/map.epic'

export const rootEpic = combineEpics(homeEpic, playerEpic, gameEpic, authEpic, notificationEpic, mapEpic)
export const epicMiddleware = createEpicMiddleware<Action, Action, State>()
