import {Roll} from '../../domain/roll'
import {GameAction, GameActionTypes} from './game.action'
import {GameRules} from 'domain/game-rules'
import {Game} from '../../domain/game'

export const initialGameState: GameState = {
  rolls: [],
  game: {
    gameMaster: '',
    id: '',
    name: '',
    players: [],
    users: []
  },
  gameRules: {
    attributs: [],
    skills: [],
    weaponRanges: [],
    dice: [],
    gameClasses: [],
    resources: [],
    talents: []
  }
}

export default function gameReducer(state: GameState = initialGameState, action: GameAction): GameState {
  switch (action.type) {
    case GameActionTypes.ROLLS_RECEIVED:
      state = {
        ...state,
        rolls: [...action.payload]
      }
      break
    case GameActionTypes.ROLL_RECEIVED:
      state = {
        ...state,
        rolls: [...state.rolls, action.payload]
      }
      break
    case GameActionTypes.UPDATE_PLAYER:
      const players = [...state.game.players].filter(player => player.id !== action.payload.id)
      players.push(action.payload)
      const game = {...state.game, players}
      state = {...state, game}
      break
    case GameActionTypes.LOAD_GAMERULES_OK:
      state = {
        ...state,
        gameRules: {
          ...action.payload
        }
      }
      break
    case GameActionTypes.LOAD_GAME_OK:
      state = {
        ...state,
        game: action.payload
      }
      break
    case GameActionTypes.CREATE_PLAYER_OK:
      const newPlayers = [...state.game.players]
      newPlayers.push({
        id: action.payload.id,
        name: action.payload.name,
        attributs: [],
        skills: [],
        weapons: [],
        gears: [],
        resources: [],
        gameClass: {
          id: '',
          label: '',
          skillId: ''
        },
        talents: [],
        experience: 0,
        rot: 0,
        mutation: 0,
        lastRoll: undefined,
        gameId: '',
        notes: ''
      })
      state = {
        ...state,
        game: {...state.game, players: newPlayers}
      }
  }
  return state
}

export interface GameState {
  rolls: Roll[];
  game: Game;
  gameRules: GameRules;
}
