import { catchError, filter, map, switchMap } from 'rxjs/operators'
import { isOfType } from 'typesafe-actions'
import { from, of } from 'rxjs'
import { combineEpics, Epic } from 'redux-observable'
import { State } from '../reducers'
import {
  GameAction,
  GameActionTypes,
  loadGameRulesOk,
  loadGameRulesFailed, loadGameOk, loadGameKo
} from './game.action'
import { get, put } from 'api/api.service'
import { rerollDiceKo, rerollDiceOk, rollDiceKo, rollDiceOk } from '../pages/player/player.action'

const rollDiceEpic: Epic<GameAction, any, State> = (
  action$,
) =>
  action$.pipe(
    filter(isOfType(GameActionTypes.ROLL_DICE)),
    switchMap(action =>
      from(put('roll', action.payload)).pipe(
        map(() => rollDiceOk()),
        catchError(() => of(rollDiceKo()))
      )
    )
  )

const rerollDiceEpic: Epic<GameAction, any, State> = (
  action$,
) =>
  action$.pipe(
    filter(isOfType(GameActionTypes.REROLL_DICE)),
    switchMap(action =>
      from(put('reroll', { playerId: action.payload })).pipe(
        map(() => rerollDiceOk()),
        catchError(() => of(rerollDiceKo()))
      )
    )
  )


const loadGameRulesEpic: Epic<GameAction, GameAction, State> = (
  action$
) =>
  action$.pipe(
    filter(isOfType(GameActionTypes.LOAD_GAMERULES)),
    switchMap(() =>
      from(get('game/rules')).pipe(
        map(response => loadGameRulesOk(response.data)),
        catchError(() => of(loadGameRulesFailed()))
      )
    )
  )

const loadGameEpic: Epic<GameAction, GameAction, State> = (
  action$
) =>
  action$.pipe(
    filter(isOfType(GameActionTypes.LOAD_GAME)),
    switchMap(action =>
      from(get(`game/${action.payload}`)).pipe(
        map(response => loadGameOk(response.data)),
        catchError(() => of(loadGameKo()))
      )
    )
  )

export const gameEpic = combineEpics(rollDiceEpic, rerollDiceEpic, loadGameRulesEpic, loadGameEpic)
