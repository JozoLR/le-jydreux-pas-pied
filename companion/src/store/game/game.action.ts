import {Roll, RollDiceCommand} from '../../domain/roll'
import {Player} from 'domain/player'
import {GameRules} from 'domain/game-rules'
import {Game} from '../../domain/game'

export enum GameActionTypes {
  GET_ROLLS = 'game/get/rolls',
  ROLLS_RECEIVED = 'game/receive/rolls',
  ROLLS_RECEIVED_FAILED = 'game/receive/rolls/failed',
  ROLL_RECEIVED = 'game/receive/roll',

  FOLLOW_ROLLS = 'game/roll/follow',
  ROLL_DICE = 'game/roll',
  REROLL_DICE = 'REROLL_DICE',

  UPDATE_PLAYER = 'game/player/update',

  LOAD_GAMERULES = 'load/gamerules',
  LOAD_GAMERULES_OK = 'load/gamerules/ok',
  LOAD_GAMERULES_FAILED = 'load/gamerules/failed',

  LOAD_GAME = 'LOAD_GAME',
  LOAD_GAME_OK = 'LOAD_GAME_OK',
  LOAD_GAME_KO = 'LOAD_GAME_KO',

  CREATE_PLAYER_OK = 'CREATE_PLAYER_OK'
}

export function getRolls(gameId: string): GetRollsAction {
  return {
    type: GameActionTypes.GET_ROLLS,
    payload: gameId
  }
}

export function receiveRolls(rolls: Roll[]): ReceiveRollsAction {
  return {
    type: GameActionTypes.ROLLS_RECEIVED,
    payload: rolls
  }
}

export function receiveRoll(roll: Roll): ReceiveRollAction {
  return {
    type: GameActionTypes.ROLL_RECEIVED,
    payload: roll
  }
}

export function receiveRollsFailed(): ReceiveRollsFailedAction {
  return {
    type: GameActionTypes.ROLLS_RECEIVED_FAILED
  }
}

export function rollDice(rollDiceCommand: RollDiceCommand): RollDiceAction {
  return {
    type: GameActionTypes.ROLL_DICE,
    payload: rollDiceCommand
  }
}

export function rerollDice(playerId: string): ReRollDiceAction {
  return {
    type: GameActionTypes.REROLL_DICE,
    payload: playerId
  }
}

export function updatePlayer(player: Player): UpdatePlayerAction {
  return {
    type: GameActionTypes.UPDATE_PLAYER,
    payload: player
  }
}

export function loadGameRules(): LoadGameRulesAction {
  return {
    type: GameActionTypes.LOAD_GAMERULES
  }
}

export function loadGameRulesOk(gameRules: GameRules): LoadGameRulesOkAction {
  return {
    type: GameActionTypes.LOAD_GAMERULES_OK,
    payload: gameRules
  }
}

export function loadGameRulesFailed(): LoadGameRulesFailedAction {
  return {
    type: GameActionTypes.LOAD_GAMERULES_FAILED
  }
}

export function loadGame(gameId: string): LoadGameAction {
  return {
    type: GameActionTypes.LOAD_GAME,
    payload: gameId
  }
}

export function loadGameOk(game: Game): LoadGameOkAction {
  return {
    type: GameActionTypes.LOAD_GAME_OK,
    payload: game
  }
}

export function loadGameKo(): LoadGameKoAction {
  return {
    type: GameActionTypes.LOAD_GAME_KO
  }
}

export function createPlayerOk(id: string, name: string): CreatePlayerOkAction {
  return {
    type: GameActionTypes.CREATE_PLAYER_OK,
    payload: {
      id,
      name
    }
  }
}

export interface LoadGameRulesAction {
  type: GameActionTypes.LOAD_GAMERULES;
}

export interface LoadGameRulesOkAction {
  type: GameActionTypes.LOAD_GAMERULES_OK;
  payload: GameRules;
}

export interface LoadGameRulesFailedAction {
  type: GameActionTypes.LOAD_GAMERULES_FAILED;
}

export interface GetRollsAction {
  type: GameActionTypes.GET_ROLLS;
  payload: string;
}

export interface ReceiveRollsAction {
  type: GameActionTypes.ROLLS_RECEIVED;
  payload: Roll[];
}

export interface ReceiveRollAction {
  type: GameActionTypes.ROLL_RECEIVED;
  payload: Roll;
}

export interface ReceiveRollsFailedAction {
  type: GameActionTypes.ROLLS_RECEIVED_FAILED;
}

export interface FollowRollsAction {
  type: GameActionTypes.FOLLOW_ROLLS;
}

export interface RollDiceAction {
  type: GameActionTypes.ROLL_DICE;
  payload: RollDiceCommand;
}

export interface ReRollDiceAction {
  type: GameActionTypes.REROLL_DICE;
  payload: string;
}

export interface UpdatePlayerAction {
  type: GameActionTypes.UPDATE_PLAYER;
  payload: Player;
}

export interface LoadGameAction {
  type: GameActionTypes.LOAD_GAME;
  payload: string;
}

export interface LoadGameOkAction {
  type: GameActionTypes.LOAD_GAME_OK;
  payload: Game;
}

export interface LoadGameKoAction {
  type: GameActionTypes.LOAD_GAME_KO;
}

export interface CreatePlayerOkAction {
  type: GameActionTypes.CREATE_PLAYER_OK;
  payload: {
    id: string;
    name: string;
  };
}

export type GameAction =
  GetRollsAction
  | ReceiveRollsAction
  | ReceiveRollsFailedAction
  | ReceiveRollAction
  | FollowRollsAction
  | RollDiceAction
  | ReRollDiceAction
  | UpdatePlayerAction
  | LoadGameRulesAction
  | LoadGameRulesOkAction
  | LoadGameRulesFailedAction
  | LoadGameAction
  | LoadGameOkAction
  | LoadGameKoAction
  | CreatePlayerOkAction
