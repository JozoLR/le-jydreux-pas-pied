import {Notification} from '../../domain/notification'

export enum NotificationActionTypes {
  GET_NOTIFICATIONS = 'GET_NOTIFICATIONS',
  GET_NOTIFICATIONS_OK = 'GET_NOTIFICATIONS_OK',
  GET_NOTIFICATIONS_KO = 'GET_NOTIFICATIONS_KO',

  RECEIVE_NOTIFICATION = 'RECEIVE_NOTIFICATION'
}

export function getNotifications(gameId: string): GetNotificationsAction {
  return {
    type: NotificationActionTypes.GET_NOTIFICATIONS,
    payload: gameId
  }
}

export function getNotificationsOk(notifications: Notification[]): GetNotificationsOkAction {
  return {
    type: NotificationActionTypes.GET_NOTIFICATIONS_OK,
    payload: notifications
  }
}

export function getNotificationsKo(): GetNotificationsKoAction {
  return {
    type: NotificationActionTypes.GET_NOTIFICATIONS_KO,
  }
}

export function receiveNotification(notification: Notification): ReceiveNotificationAction {
  return {
    type: NotificationActionTypes.RECEIVE_NOTIFICATION,
    payload: notification
  }
}

export interface GetNotificationsAction {
  type: NotificationActionTypes.GET_NOTIFICATIONS;
  payload: string;
}

export interface GetNotificationsOkAction {
  type: NotificationActionTypes.GET_NOTIFICATIONS_OK;
  payload: Notification[];
}

export interface GetNotificationsKoAction {
  type: NotificationActionTypes.GET_NOTIFICATIONS_KO;
}

export interface ReceiveNotificationAction {
  type: NotificationActionTypes.RECEIVE_NOTIFICATION;
  payload: Notification;
}

export type NotificationAction =
  GetNotificationsAction
  | GetNotificationsOkAction
  | GetNotificationsKoAction
  | ReceiveNotificationAction

