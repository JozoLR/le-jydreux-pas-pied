import { NotificationAction, NotificationActionTypes } from './notification.action'
import { Notification } from '../../domain/notification'

export const initialNotificationState: NotificationState = {
  notifications: []
}

export default function notificationReducer(state: NotificationState = initialNotificationState, action: NotificationAction): NotificationState {
  switch (action.type) {
    case NotificationActionTypes.GET_NOTIFICATIONS_OK:
      state = {
        ...state,
        notifications: [...action.payload]
      }
      break
    case NotificationActionTypes.RECEIVE_NOTIFICATION:
      const currentNotifications = [action.payload, ...state.notifications]
      state = {
        ...state,
        notifications: currentNotifications
      }
      break
  }
  return state
}

export interface NotificationState {
  notifications: Notification[];
}
