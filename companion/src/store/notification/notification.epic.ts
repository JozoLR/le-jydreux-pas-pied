import {catchError, filter, map, switchMap} from 'rxjs/operators'
import {isOfType} from 'typesafe-actions'
import {from, of} from 'rxjs'
import {combineEpics, Epic} from 'redux-observable'
import {State} from '../reducers'
import {
  getNotificationsKo,
  getNotificationsOk,
  NotificationAction,
  NotificationActionTypes
} from './notification.action'
import {get} from 'api/api.service'
import {NotificationResource} from '../../api/resources/notification.resource'

const getNotificationsEpic: Epic<NotificationAction, NotificationAction, State> = (
  action$
) =>
  action$.pipe(
    filter(isOfType(NotificationActionTypes.GET_NOTIFICATIONS)),
    switchMap(action =>
      from(get(`game/${action.payload}/notification`)).pipe(
        map(response => getNotificationsOk(NotificationResource.toDomainNotifications(response.data))),
        catchError(() => of(getNotificationsKo()))
      )
    )
  )

export const notificationEpic = combineEpics(getNotificationsEpic)
