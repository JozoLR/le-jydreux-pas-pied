import { Map } from 'domain/map'

export enum MapActionTypes {
  GET_MAP = 'GET_MAP',
  GET_MAP_OK = 'GET_MAP_OK',
  GET_MAP_KO = 'GET_MAP_KO',
  UPDATE_MAP = 'UPDATE_MAP',
  UPDATE_MAP_OK = 'UPDATE_MAP_OK',
  UPDATE_MAP_KO = 'UPDATE_MAP_KO'
}

export function getMap(gameId: string): GetMapAction {
  return {
    type: MapActionTypes.GET_MAP,
    payload: gameId
  }
}

export function getMapOk(map: Map): GetMapOkAction {
  return {
    type: MapActionTypes.GET_MAP_OK,
    payload: map
  }
}

export function getMapKo(): GetMapKoAction {
  return {
    type: MapActionTypes.GET_MAP_KO,
  }
}

export function updateMap(map: Map): UpdateMapAction {
  return {
    type: MapActionTypes.UPDATE_MAP,
    payload: map
  }
}

export function updateMapOk(): UpdateMapOkAction {
  return {
    type: MapActionTypes.UPDATE_MAP_OK
  }
}

export function updateMapKo(): UpdateMapKoAction {
  return {
    type: MapActionTypes.UPDATE_MAP_KO,
  }
}

export interface GetMapAction {
  type: MapActionTypes.GET_MAP;
  payload: string;
}

export interface GetMapOkAction {
  type: MapActionTypes.GET_MAP_OK;
  payload: Map;
}

export interface GetMapKoAction {
  type: MapActionTypes.GET_MAP_KO;
}

export interface UpdateMapAction {
  type: MapActionTypes.UPDATE_MAP;
  payload: Map;
}

export interface UpdateMapOkAction {
  type: MapActionTypes.UPDATE_MAP_OK;
}

export interface UpdateMapKoAction {
  type: MapActionTypes.UPDATE_MAP_KO;
}

export type MapAction =
  GetMapAction
  | GetMapOkAction
  | GetMapKoAction
  | UpdateMapAction
  | UpdateMapOkAction
  | UpdateMapKoAction
