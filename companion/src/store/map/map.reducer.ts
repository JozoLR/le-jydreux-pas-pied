import { MapAction, MapActionTypes } from './map.action'
import { Map } from 'domain/map'

export const initialMapState: MapState = {
  map: undefined
}

export default function mapReducer(state: MapState = initialMapState, action: MapAction): MapState {
  switch (action.type) {
    case MapActionTypes.GET_MAP_OK:
      state = {
        ...state,
        map: action.payload
      }
      break
  }
  return state
}

export interface MapState {
  map: Map | undefined;
}
