import { catchError, filter, map, switchMap } from 'rxjs/operators'
import { isOfType } from 'typesafe-actions'
import { from, of } from 'rxjs'
import { combineEpics, Epic } from 'redux-observable'
import { State } from '../reducers'
import { getMapKo, getMapOk, MapAction, MapActionTypes, updateMapOk, updateMapKo } from './map.action'
import { get, post } from 'api/api.service'

const getMapEpic: Epic<MapAction, MapAction, State> = (
  action$
) =>
  action$.pipe(
    filter(isOfType(MapActionTypes.GET_MAP)),
    switchMap(action =>
      from(get(`map/${action.payload}`)).pipe(
        map(response => getMapOk(response.data)),
        catchError(() => of(getMapKo()))
      )
    )
  )

const updateMapEpic: Epic<MapAction, MapAction, State> = (
  action$
) =>
  action$.pipe(
    filter(isOfType(MapActionTypes.UPDATE_MAP)),
    switchMap(action =>
      from(post('map', action.payload)).pipe(
        map(() => updateMapOk()),
        catchError(() => of(updateMapKo()))
      )
    )
  )

export const mapEpic = combineEpics(getMapEpic, updateMapEpic)
