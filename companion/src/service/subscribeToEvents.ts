import {loadGame} from '../store/game/game.action'
import {loadPlayer, loadRollProposition} from '../store/pages/player/player.action'
import {apiUrl} from 'api/api.service'
import store from '../store'
// @ts-ignore
import {EventSourcePolyfill} from 'event-source-polyfill'
import {receiveNotification} from '../store/notification/notification.action'
import {NotificationResource} from '../api/resources/notification.resource'
import {isMapUpdated, isStuffGiven, StuffGiven} from '../domain/notification'
import {getMap} from '../store/map/map.action'

export async function subscribeToEvents(gameId: string, playerId?: string): Promise<EventSourcePolyfill> {
  const token = await store.getState().auth.client?.getTokenSilently()

  const eventSource = new EventSourcePolyfill(`${apiUrl}stream/game/${gameId}`, {
    headers: {
      'Authorization': `Bearer ${token}`
    }
  })

  eventSource.addEventListener('NOTIFICATION', function (message: EventStreamMessage): void {
    // @ts-ignore
    const notification = NotificationResource.toDomain(JSON.parse(message.data) as NotificationResource)
    if (notification.playerId === playerId) {
      store.dispatch(loadPlayer(notification.playerId))
      store.dispatch(loadRollProposition(notification.playerId))
    }
    if (isStuffGiven(notification) && notification.playerId !== playerId) {
      store.dispatch(loadPlayer((notification.content as StuffGiven).beneficiaryId))
    }
    if(isMapUpdated(notification)) {
      store.dispatch(getMap(notification.gameId))
    }
    store.dispatch(receiveNotification(notification))
    store.dispatch(loadGame(gameId))
  })
  return eventSource
}

interface EventStreamMessage {
  data: object;
}
