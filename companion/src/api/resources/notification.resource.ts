import {DateTime} from 'luxon'
import {Notification, NotificationType, PlayerUpdate, StuffAdded, StuffGiven} from '../../domain/notification'
import {Roll} from '../../domain/roll'


export interface NotificationResource {
  id: string;
  date: string;
  playerId: string;
  playerName: string;
  gameId: string;
  type: NotificationType;
  content: PlayerUpdate | StuffAdded | StuffGiven | Roll;
}

export namespace NotificationResource {
  export function toDomainNotifications(notificationResources: NotificationResource[]): Notification[] {
    return notificationResources.map(
      notificationResource => ({
        id: notificationResource.id,
        date: DateTime.fromISO(notificationResource.date),
        playerId: notificationResource.playerId,
        playerName: notificationResource.playerName,
        gameId: notificationResource.gameId,
        type: notificationResource.type as NotificationType,
        content: notificationResource.content
      })
    )
  }

  export function toDomain(notificationResource: NotificationResource): Notification {
    return {
      id: notificationResource.id,
      date: DateTime.fromISO(notificationResource.date),
      playerId: notificationResource.playerId,
      playerName: notificationResource.playerName,
      gameId: notificationResource.gameId,
      type: notificationResource.type as NotificationType,
      content: notificationResource.content
    }
  }
}
