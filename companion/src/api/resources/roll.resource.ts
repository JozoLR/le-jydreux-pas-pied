import {Roll, RollInfos, RollType} from '../../domain/roll'

export namespace RollResource {

  export interface RollResource {
    attributRolls: number[];
    skillRolls: number[];
    stuffRolls: number[];
    malusRolls: number[];
    rollType: RollType;
    rollInfos: RollInfos;
  }

  export function toDomain(rollResource: RollResource): Roll {
    return {
      attributRolls: rollResource.attributRolls,
      skillRolls: rollResource.skillRolls,
      stuffRolls: rollResource.stuffRolls,
      malusRolls: rollResource.malusRolls,
      rollType: rollResource.rollType,
      rollInfos: rollResource.rollInfos
    }
  }
}
