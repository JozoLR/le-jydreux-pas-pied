/* eslint-disable @typescript-eslint/no-non-null-assertion */
import {Attribut, Gear, Player, PointType, Resource, Skill, Weapon} from 'domain/player'
import {GameRules} from 'domain/game-rules'
import {Roll} from '../../domain/roll'

export type PlayerResource = {
  id: string;
  name: string;
  attributs: Attribut[];
  skills: Skill[];
  weapons: WeaponResource[];
  gears: GearResource[];
  resources: Resource[];
  gameClass: GameRules.GameClass;
  talents: GameRules.Talent[];
  experience: number;
  rot: number;
  mutation: number;
  lastRoll: Roll | undefined;
  gameId: string;
  notes: string;
}

export interface UpdatePlayerResource {
  playerId: string;
  experience: number;
  rot: number;
  mutation: number;
}

export interface UpdateAttributResource {
  id: string;
  label: string;
  actual: number;
  base: number;
  playerId: string;
}

export interface UpdateSkillResource {
  id: string;
  label: string;
  actual: number;
  skillType: string;
  playerId: string;
}

export interface UpdateNotesResource {
  playerId: string;
  notes: string;
}

export interface AddWeaponResource {
  label: string;
  bonus: number;
  damage: number;
  scope: string;
  skillId: string;
  resourceId: string | undefined;
  playerId: string;
}

export interface AddGearResource {
  label: string;
  bonus: number;
  skillIds: string[];
  playerId: string;
}

export interface WeaponResource {
  id: string;
  label: string;
  bonus: number;
  malus: number;
  damage: number;
  scope: string;
  skillId: string;
  resourceId: string | undefined;
  playerId: string;
}

export interface GearResource {
  id: string;
  label: string;
  bonus: number;
  malus: number;
  skillIds: string[];
  playerId: string;
  gameId: string;
}

export interface UpdateGearResource {
  id: string;
  malus: number;
  playerId: string;
  gameId: string;
}

export interface ResourceResource {
  id: string;
  label: string;
  quantity: number;
  playerId: string;
}

export interface DeleteGearResource {
  playerId: string;
  gameId: string;
  gearId: string;
}

export interface DeleteWeaponResource {
  playerId: string;
  gameId: string;
  weaponId: string;
}

export interface UpgradePlayerResource {
  playerId: string;
  skillIds: string[];
  talentIds: string[];
}

export namespace PlayerResource {
  export function toDomain(playerResource: PlayerResource): Player {
    return {
      id: playerResource.id,
      name: playerResource.name,
      attributs: playerResource.attributs,
      skills: playerResource.skills,
      gears: playerResource.gears.map(gear => new Gear(gear.id, gear.label, gear.skillIds, gear.bonus, gear.malus)),
      weapons: playerResource.weapons.map(toWeapon),
      resources: playerResource.resources,
      gameClass: playerResource.gameClass,
      talents: playerResource.talents,
      experience: playerResource.experience,
      rot: playerResource.rot,
      mutation: playerResource.mutation,
      lastRoll: playerResource.lastRoll,
      gameId: playerResource.gameId,
      notes: playerResource.notes
    }
  }

  export function buildAttributResource(attribut: Attribut, player: Player): UpdateAttributResource {
    return {
      ...attribut,
      playerId: player.id
    }
  }

  export function buildSkillResource(skill: Skill, player: Player): UpdateSkillResource {
    return {
      ...skill,
      playerId: player.id
    }
  }

  export function buildWeaponResource(weapon: Weapon, player: Player): WeaponResource {
    return {
      id: weapon.id!,
      malus: weapon.malus,
      label: weapon.label,
      bonus: weapon.bonus,
      damage: weapon.damage,
      scope: weapon.scope,
      skillId: weapon.skillId[0],
      resourceId: weapon.resourceId,
      playerId: player.id
    }
  }

  export function buildUpdateGearResource(gear: Gear, player: Player): UpdateGearResource {
    return {
      id: gear.id!,
      malus: gear.malus,
      playerId: player.id,
      gameId: player.gameId
    }
  }

  export function buildResourceResource(resource: Resource, player: Player): ResourceResource {
    return {
      ...resource,
      playerId: player.id
    }
  }

  export function buildUpdatePlayerResource(category: PointType, value: number, player: Player): UpdatePlayerResource {
    return {
      playerId: player.id,
      experience: player.experience,
      mutation: player.mutation,
      rot: player.rot,
      [category]: value
    }
  }

  export function buildUpdateNotesResource(playerId: string, notes: string): UpdateNotesResource {
    return {
      playerId,
      notes,
    }
  }


  export function buildWeaponResourceToAdd(weapon: Weapon, player: Player): AddWeaponResource {
    const weaponResource = {...weapon}
    delete weaponResource.malus
    const resourceId = (!weapon.resourceId || weapon.resourceId === 'None') ? undefined : weapon.resourceId
    return {
      bonus: weapon.bonus,
      damage: weapon.damage,
      label: weapon.label,
      scope: weapon.scope,
      skillId: weapon.skillId[0],
      resourceId: resourceId,
      playerId: player.id
    }
  }

  export function buildGearResourceToAdd(gear: Gear, player: Player): AddGearResource {
    const gearResource = {...gear}
    delete gearResource.malus
    return {
      ...gearResource,
      playerId: player.id
    }
  }

  export function buildGearResourceToDelete(gearId: string, player: Player): DeleteGearResource {
    return {
      playerId: player.id,
      gameId: player.gameId,
      gearId
    }
  }

  export function buildWeaponResourceToDelete(weaponId: string, player: Player): DeleteWeaponResource {
    return {
      playerId: player.id,
      gameId: player.gameId,
      weaponId
    }
  }

  function toWeapon(weaponResource: WeaponResource): Weapon {
    return new Weapon(
      weaponResource.id,
      weaponResource.label,
      weaponResource.skillId,
      weaponResource.bonus,
      weaponResource.malus,
      weaponResource.damage,
      weaponResource.scope,
      weaponResource.resourceId)
  }
}
