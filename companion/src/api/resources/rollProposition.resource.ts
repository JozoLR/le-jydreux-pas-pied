import { RollProposition } from 'domain/rollProposition'
import {GameRules} from '../../domain/game-rules'

export interface RollPropositionResource {
  playerId: string;
  skillPropositions: RollPropositionResource.SkillPropositionResource[];
}

export namespace RollPropositionResource {
  export interface SkillPropositionResource {
    skillId: string;
    skillLabel: string;
    attributDice: number;
    skillDice: number;
    bonuses: BonusResource[];
  }

  export interface BonusResource {
    id: string;
    label: string;
    stuffType: GameRules.StuffType;
    value: number;
  }

  export function toDomain(rollPropositionResource: RollPropositionResource): RollProposition {
    return {
    ...rollPropositionResource
    }
  }
}
