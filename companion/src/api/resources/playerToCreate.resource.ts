import { AttributToCreate, PlayerToCreate } from 'domain/player'

export interface PlayerToCreateResource {
  name: string;
  attributs: AttributToCreate[];
  gameClassId: string;
  talentId: string;
  gameId: string;
}

export namespace PlayerToCreateResource {
  export function build(player: PlayerToCreate): PlayerToCreateResource {
    return {
      name: player.name,
      attributs: player.attributs,
      gameClassId: player.gameClassId,
      talentId: player.talentId,
      gameId: player.gameId
    }
  }
}
