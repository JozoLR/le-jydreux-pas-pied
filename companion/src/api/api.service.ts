import axios from 'axios'
import store from '../store'

export const apiUrl: string = process.env.REACT_APP_API_URL || 'http://localhost:8080/'

export async function get(path: string): Promise<Response> {
  const token = await getToken()
  return axios.get(`${apiUrl}${path}`, {
    headers: {
      'Authorization': `Bearer ${token}`
    }
  })
}

export async function post(path: string, payload: Payload): Promise<Response> {
  const token = await getToken()
  return axios.post(`${apiUrl}${path}`, payload, {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }
  })
}

export async function put(path: string, payload: Payload): Promise<Response> {
  const token = await getToken()
  return axios.put(`${apiUrl}${path}`, payload, {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }
  })
}

export async function del(path: string, payload: Payload): Promise<Response> {
  const token = await getToken()
  return axios.delete(`${apiUrl}${path}`, {
    data: payload,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }
  })
}

async function getToken(): Promise<string> {
  const state = store.getState()
  return state.auth.client?.getTokenSilently()
}

export interface Response {
  data: Payload;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type Payload = any
