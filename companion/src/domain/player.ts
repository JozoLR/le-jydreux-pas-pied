import {GameRules} from './game-rules'
import {Roll} from './roll'

export type Player = {
  id: string;
  name: string;
  attributs: Attribut[];
  skills: Skill[];
  weapons: Weapon[];
  gears: Gear[];
  resources: Resource[];
  gameClass: GameRules.GameClass;
  talents: GameRules.Talent[];
  experience: number;
  rot: number;
  mutation: number;
  lastRoll: Roll | undefined;
  gameId: string;
  notes: string;
}

export interface Attribut {
  id: string;
  label: string;
  actual: number;
  base: number;
}

export interface Skill {
  id: string;
  label: string;
  actual: number;
  skillType: string;
}

export enum SKillType {
  ARMOR = 'ARMOR',
  BASE = 'BASE',
  CLASS = 'CLASS'
}

export enum PointType {
  EXPERIENCE = 'experience',
  ROT = 'rot',
  MUTATION = 'mutation'
}

export class Weapon {
  constructor(
    public id: string | null,
    public label: string,
    public skillId: string,
    public bonus: number,
    public malus: number,
    public damage: number,
    public scope: string,
    public resourceId: string | undefined) {}
}

export class Gear {
  constructor(
    public id: string | null,
    public label: string,
    public skillIds: string[],
    public bonus: number,
    public malus: number) {}
}

export interface Resource {
  id: string;
  label: string;
  quantity: number;
}


export interface PlayerToCreate {
  name: string;
  attributs: AttributToCreate[];
  gameClassId: string;
  gameClassLabel: string;
  talentId: string;
  talentLabel: string;
  gameId: string;
}

export interface AttributToCreate {
  id: string;
  label: string;
  value: number;
}

export interface UpgradePlayerCommand {
  playerId: string;
  gameId: string;
  skillIds: string[];
  talentIds: string[];
}
