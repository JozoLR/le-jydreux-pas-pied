export interface Tile {
  index: number;
  visited: boolean;
  note: string | undefined;
}

export enum MarkerType {
  GROUP = 'GROUP',
  ARK = 'ARK'
}

export interface Marker {
  label: string;
  tileIndex: number;
  position: { x: number; y: number };
  type: MarkerType;
}

export interface Map {
  tiles: Tile[];
  markers: Marker[];
  gameId: string;
}
