export enum DiceType {
  ATTRIBUT = 'ATTRIBUT',
  SKILL = 'SKILL',
  STUFF = 'STUFF',
  MALUS = 'MALUS'
}

export interface DiceModel {
  attributs: number;
  skills: number;
  stuff: number;
}
