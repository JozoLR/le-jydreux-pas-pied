import {GameRules} from './game-rules'

export interface RollProposition {
  playerId: string;
  skillPropositions: RollProposition.SkillProposition[];
}

export namespace RollProposition {
  export interface SkillProposition {
    skillId: string;
    skillLabel: string;
    attributDice: number;
    skillDice: number;
    bonuses: Bonus[];
  }

  export interface Bonus {
    id: string;
    label: string;
    stuffType: GameRules.StuffType;
    value: number;
  }
}
