import {Player} from './player'

export interface Game {
  id: string;
  name: string;
  gameMaster: string;
  users: string[];
  players: Player[];
}

export interface GameToCreate {
  name: string;
  users: string[];
}
