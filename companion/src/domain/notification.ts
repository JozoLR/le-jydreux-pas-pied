import { DateTime } from 'luxon'
import {Roll} from './roll'

export interface Notification {
  id: string;
  date: DateTime;
  playerId: string;
  playerName: string;
  gameId: string;
  type: NotificationType;
  content: PlayerUpdate | StuffAdded | Roll | StuffGiven | StuffDeleted;
}

export function isPlayerUpdate(notification: Notification): boolean {
  return notification.type === NotificationType.PLAYER_UPDATED ||
    notification.type === NotificationType.GEAR_UPDATED ||
    notification.type === NotificationType.ATTRIBUT_UPDATED ||
    notification.type === NotificationType.WEAPON_UPDATED ||
    notification.type === NotificationType.RESOURCE_UPDATED ||
    notification.type === NotificationType.SKILL_UPDATED
}

export function isRoll(notification: Notification): boolean {
  return notification.type === NotificationType.PLAYER_ROLLED
}

export function isStuffAdded(notification: Notification): boolean {
  return notification.type === NotificationType.GEAR_ADDED || notification.type === NotificationType.WEAPON_ADDED
}

export function isStuffGiven(notification: Notification): boolean {
  return notification.type === NotificationType.WEAPON_GIVEN || notification.type === NotificationType.GEAR_GIVEN
}

export function isStuffDeleted(notification: Notification): boolean {
  return notification.type === NotificationType.GEAR_DELETED || notification.type === NotificationType.WEAPON_DELETED
}

export function isPlayerUgraded(notification: Notification): boolean {
  return notification.type === NotificationType.PLAYER_UPGRADED
}

export function isMapUpdated(notification: Notification): boolean {
  return notification.type === NotificationType.MAP_UPDATED
}

export interface PlayerUpdate {
  id: string;
  label: string;
  previousValue: number;
  currentValue: number;
}

export interface StuffAdded {
  id: string;
  label: string;
}

export interface StuffDeleted {
  playerId: string;
  playerName: string;
  stuffId: string;
  stuffName: string;
}

export interface StuffGiven {
  beneficiaryId: string;
  beneficiaryName: string;
  stuffId: string;
  stuffName: string;
}
export enum NotificationType {
  PLAYER_UPDATED = 'PLAYER_UPDATED',
  GEAR_UPDATED = 'GEAR_UPDATED',
  GEAR_ADDED = 'GEAR_ADDED',
  ATTRIBUT_UPDATED = 'ATTRIBUT_UPDATED',
  SKILL_UPDATED = 'SKILL_UPDATED',
  WEAPON_ADDED = 'WEAPON_ADDED',
  WEAPON_UPDATED = 'WEAPON_UPDATED',
  RESOURCE_UPDATED = 'RESOURCE_UPDATED',
  PLAYER_ROLLED = 'PLAYER_ROLLED',
  WEAPON_GIVEN = 'WEAPON_GIVEN',
  GEAR_GIVEN = 'GEAR_GIVEN',
  GEAR_DELETED = 'GEAR_DELETED',
  WEAPON_DELETED = 'WEAPON_DELETED',
  PLAYER_UPGRADED = 'PLAYER_UPGRADED',
  MAP_UPDATED = 'MAP_UPDATED'
}
