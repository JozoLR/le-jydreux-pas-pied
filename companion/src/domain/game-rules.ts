export interface GameRules {
  attributs: GameRules.Competence[];
  skills: GameRules.Competence[];
  weaponRanges: GameRules.WeaponRange[];
  dice: GameRules.Dice[];
  gameClasses: GameRules.GameClass[];
  resources: GameRules.Resource[];
  talents: GameRules.Talent[];
}

export namespace GameRules {
  export interface Competence {
    id: string;
    label: string;
  }

  export interface WeaponRange {
    id: string;
    label: string;
    effect: number;
  }

  export interface Dice {
    category: string;
    number: number;
  }

  export interface GameClass {
    id: string;
    label: string;
    skillId: string;
  }

  export interface Resource {
    id: string;
    label: string;
  }

  export interface Talent {
    id: string;
    label: string;
    gameClassId: string;
    description: string;
  }

  export enum StuffType {
    WEAPON= 'WEAPON' ,
    GEAR= 'GEAR',
    ARMOR= 'ARMOR',
    NONE= 'NONE'
  }
}
