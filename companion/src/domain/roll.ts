import {GameRules} from './game-rules'

export interface Roll {
  attributRolls: number[];
  skillRolls: number[];
  stuffRolls: number[];
  malusRolls: number[];
  rollType: RollType;
  rollInfos: RollInfos;
}

export interface RollInfos {
  skillId: string;
  skillLabel: string;
  stuffType: GameRules.StuffType;
  stuffs: { id: string; label: string }[];
}

export interface RollDiceCommand {
  playerId: string;
  skill: number;
  attribut: number;
  stuff: number;
  malus: number;
  rollInfos: RollInfosCommand;
}

export interface RollInfosCommand {
  skillId: string;
  stuffType: GameRules.StuffType;
  stuffIds: string[];
}

export enum RollType {
  ROLL = 'ROLL',
  REROLL = 'REROLL'
}
