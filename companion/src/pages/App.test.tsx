import React from 'react'
import {render} from '@testing-library/react'
import App, {AppProps} from './App'
import {Provider} from 'react-redux'
import {combineReducers, createStore} from 'redux'
import homeReducer from '../store/pages/home/home.reducer'

const reducers = combineReducers({
  home: homeReducer
})

const store = createStore(
  reducers
)

test('renders the list of thee players', () => {
  const props: AppProps = {
    players: [{name: 'John', id: '1', attributs: [], skills: [], weapons: []}],
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    loadPlayers: (): void => {},
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    loadGameRules: (): void => {}
  }
  const {getByText} = render(
    <Provider store={store}>
      <App {...props}/>
    </Provider>
  )
  const linkElement = getByText(/HEADER/i)
  // @ts-ignore
  expect(linkElement).toBeInTheDocument()
})
