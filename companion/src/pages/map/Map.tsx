import './Map.scss'
import React from 'react'
import { fabric } from 'fabric'
import mainMapSrc from 'images/map/map.jpg'
import iconCrossSrc from 'images/map/icon-cross.svg'
import { Map as DomainMap, Marker, MarkerType, Tile } from 'domain/map'
import { Canvas } from 'fabric/fabric-impl'

export default class Map extends React.Component<PageProps, State> {

  constructor(props: PageProps) {
    super(props)
    this.state = {
      canvas: null,
      tiles: [],
      markers: [],
      numberTilesHorizontal: 30,
      numberTilesVertical: 21,
      currentTile: undefined,
      tmpNote: undefined,
      displaySaveButton: false
    }
  }

  componentDidMount(): void {
    this.props.getMap(this.props.gameId)
  }

  componentDidUpdate(prevProps: Readonly<PageProps>): void {
    // console.log(this.props)
    if (prevProps !== this.props && this.props.map) {
      if (!this.state.canvas) {
        this.initTilesUi(this.props.map.tiles)
        this.setState({ markers: this.props.map.markers })
        this.initCanvas()
      } else {
        this.updateTilesUi(this.props.map.tiles)
        this.updateMarkersPositions(this.props.map.markers)
        this.state.canvas!.renderAll()
      }
    }
  }

  updateTilesUi(tilesToUpdate: Tile[]): void {
    const tiles: TileUi[] = this.state.tiles
    tilesToUpdate.forEach((tileToUpdate: Tile) => {
      const tileUiToUpdate = tiles.find(tile => tile.index == tileToUpdate.index)!
      tileUiToUpdate.visited = tileToUpdate.visited
      tileUiToUpdate.note = tileToUpdate.note
      this.setTileElementState(tileUiToUpdate)
    })
    this.setState({ tiles })
  }

  updateMarkersPositions(markersToUpdate: Marker[]): void {
    markersToUpdate.forEach((markerToUpdate: Marker) => {
      const markerElement = this.getCanvasElement(markerToUpdate.type)
      if (markerElement) {
        markerElement.left = markerToUpdate.position.x / 100 * this.state.canvas!.width!
        markerElement.top = markerToUpdate.position.y / 100 * this.state.canvas!.height!
        markerElement.setCoords()
      }
    })
    this.setState({ markers: markersToUpdate })
  }

  render(): JSX.Element {
    return (
      <div className="map">
        <canvas id="canvas" width="300" height="300" />
        {
          this.state.currentTile &&
          <div className="tile-infos">
            <div className="top">
              <h3 className="title">{this.getAlphabetLetter(this.state.currentTile.cellY)} {this.state.currentTile.cellX}</h3>
              <div className="right">
                <button className="button" onClick={(): void => this.onSaveVisited()}>
                  <span className="material-icons">
                    {this.state.currentTile.visited ? 'check_box' : 'check_box_outline_blank'}
                  </span> Visited
                </button>
                <button className="button-close" onClick={(): void => this.closeTileInfos()}>
                  <span className="material-icons">close</span>
                </button>
              </div>
            </div>
            <div className="note">
              <textarea className="textarea text-note" placeholder="Type note here..."
                defaultValue={this.state.currentTile.note}
                onChange={(event): void => this.onChangeNote(event.target.value)} />
              {
                this.state.displaySaveButton &&
                <button className="button button--green" onClick={(): void => this.onSaveNote()}>
                  <span className="material-icons">save</span> Save
                </button>
              }
            </div>
          </div>
        }
      </div>
    )
  }

  initTilesUi(domainTiles: Tile[]): void {
    const tiles: TileUi[] = domainTiles.map(tile => ({
      index: tile.index,
      visited: tile.visited,
      note: tile.note,
      cellX: 0,
      cellY: 0
    }))
    this.setState({ tiles })
  }

  parseTilesUi(tilesUi: TileUi[]): Tile[] {
    return tilesUi.map(tile => ({
      index: tile.index,
      visited: tile.visited,
      note: tile.note
    }))
  }

  updateMap(): void {
    this.props.updateMap({
      tiles: this.parseTilesUi(this.state.tiles),
      markers: this.state.markers,
      gameId: this.props.gameId
    })
  }

  initCanvas(): void {
    const canvas = new fabric.Canvas('canvas')
    canvas.selection = false

    // Load main map image
    const img = new Image()
    img.src = mainMapSrc
    img.onload = (): void => {
      // const canvasWidth: number = window.innerWidth - 320
      const canvasWidth: number = window.innerWidth - 352
      const canvasHeight: number = canvasWidth * img.height / img.width

      canvas.setWidth(canvasWidth)
      canvas.setHeight(canvasHeight)

      const imgInstance = new fabric.Image(img, {
        left: 0,
        top: 0,
        selectable: false,
        hoverCursor: 'default'
      })
      imgInstance.scaleToWidth(canvasWidth)
      imgInstance.scaleToHeight(canvasHeight)
      canvas.add(imgInstance)

      this.addTiles()
      this.addPins()
      this.addEventListeners()
    }

    this.setState({ canvas })
  }

  addEventListeners(): void {
    this.state.canvas!.on('mouse:up', (event: any) => {
      if (event.target.name === MarkerType.GROUP) {
        const markers = [...this.state.markers]
        markers
          .filter((marker: Marker) => marker.type === MarkerType.GROUP)!
          .map(marker => {
            const x = event.target.left / this.state.canvas!.width! * 100
            const y = event.target.top / this.state.canvas!.height! * 100
            marker.position.x = x
            marker.position.y = y
          })
        this.setState({ markers })
        this.updateMap()
      }
    })

    this.state.canvas!.on('mouse:down', (event: any) => {
      this.closeTileInfos()
      if (event.target.name == 'tile' || event.target.name == 'note') {
        const currentTile = this.state.tiles.find(tile => tile.index == event.target.data.index)!
        currentTile.element.set('stroke', 'rgba(0,0,0,.5)')
        currentTile.element.set('strokeWidth', '2')
        this.setState({ currentTile })
      }
    })
  }

  clearTilesStyle(): void {
    const objects = this.state.canvas!.getObjects()
    objects.forEach((object: any) => {
      if (object.name == 'tile') {
        object.set('stroke', 'rgba(0,0,0,.1)')
        object.set('strokeWidth', '1')
      }
    })
    this.state.canvas!.renderAll()
  }

  addTiles(): void {
    const tileWidth: number = this.state.canvas!.width! / this.state.numberTilesHorizontal
    let index = 1

    for (let i = 0; i < this.state.numberTilesVertical; i++) {
      for (let j = 0; j < this.state.numberTilesHorizontal; j++) {
        const tile = this.state.tiles.find(tile => tile.index == index)!

        const tileEl = new fabric.Rect({
          top: i * tileWidth,
          left: j * tileWidth,
          width: tileWidth,
          height: tileWidth,
          fill: 'transparent',
          selectable: false,
          hoverCursor: 'pointer',
          name: 'tile',
          stroke: 'rgba(0,0,0,.1)',
          strokeWidth: 1,
          objectCaching: false,
          data: {
            index: index
          }
        })
        this.state.canvas!.add(tileEl)

        tile.cellX = j + 1
        tile.cellY = i + 1
        tile.element = tileEl
        this.setTileElementState(tile)

        index++
      }
    }
  }

  addPins(): void {
    this.props.map?.markers
      .filter(marker => marker.type === MarkerType.GROUP)
      .forEach((marker: Marker) => {
        this.addPin(marker)
      })
  }

  addPin(marker: Marker): void {
    const img = new Image()
    img.src = iconCrossSrc
    img.onload = (): void => {
      const imgInstance = new fabric.Image(img, {
        left: marker.position.x / 100 * this.state.canvas!.width!,
        top: marker.position.y / 100 * this.state.canvas!.height!,
        hasControls: false,
        name: marker.type
      })
      imgInstance.scaleToHeight(15)
      imgInstance.scaleToWidth(15)
      this.state.canvas!.add(imgInstance)
    }
  }

  onChangeNote(value: string): void {
    this.setState({ tmpNote: value })
    if (this.state.currentTile && value != this.state.currentTile.note) {
      this.setState({ displaySaveButton: true })
    } else {
      this.setState({ displaySaveButton: false })
    }
  }

  onSaveNote(): void {
    if (this.state.currentTile) {
      const tile = this.state.tiles.find(tile => tile.index == this.state.currentTile!.index)!
      tile.note = this.state.tmpNote
      this.setTileElementState(tile)
      this.setState({ displaySaveButton: false })
      this.state.canvas!.renderAll()
      this.updateMap()
    }
  }

  onSaveVisited(): void {
    if (this.state.currentTile) {
      const tile = this.state.tiles.find(tile => tile.index == this.state.currentTile!.index)!
      tile.visited = !tile.visited
      this.setState({ currentTile: tile })
      this.setTileElementState(tile)
      this.state.canvas!.renderAll()
      this.updateMap()
    }
  }

  getCanvasElement(name: string, index?: number): fabric.Object | undefined {
    const objects = this.state.canvas!.getObjects()
    if (index != null) {
      return objects.find((object: fabric.Object) => object.name == name && object.data.index == index)
    } else {
      return objects.find((object: fabric.Object) => object.name == name)
    }
  }

  setTileElementState(tile: TileUi): void {
    if (tile.visited) {
      tile.element.set('fill', 'rgba(55, 125, 34, .4)')
    } else {
      tile.element.set('fill', 'transparent')
    }

    if (tile.note && tile.note != '') {
      const noteEl = new fabric.Triangle({
        top: tile.element.top + (tile.element.width - 13),
        left: tile.element.left + (tile.element.width - 13),
        width: 10,
        height: 10,
        fill: 'blue',
        selectable: false,
        name: 'note',
        hoverCursor: 'pointer',
        objectCaching: false,
        data: {
          index: tile.index
        }
      })
      this.state.canvas!.add(noteEl)
    } else {
      const noteEl = this.getCanvasElement('note', tile.index)
      this.state.canvas!.remove(noteEl!)
    }
  }

  getAlphabetLetter(index: number): string {
    const alphabet: string[] = 'abcdefghijklmnopqrstuvwxyz'.split('')
    return alphabet[index - 1].toUpperCase()
  }

  closeTileInfos(): void {
    this.clearTilesStyle()
    this.setState({ currentTile: undefined })
  }
}

export interface MapProps {
  map: DomainMap | undefined;
  gameId: string;
}

export interface MapActions {
  getMap: (gameId: string) => void;
  updateMap: (map: DomainMap) => void;
}

export interface State {
  canvas: Canvas | null;
  tiles: TileUi[];
  markers: Marker[];
  numberTilesHorizontal: number;
  numberTilesVertical: number;
  currentTile: TileUi | undefined;
  tmpNote: string | undefined;
  displaySaveButton: boolean;
}

export interface TileUi {
  index: number;
  visited: boolean;
  note: string | undefined;
  cellX: number;
  cellY: number;
  element?: any;
}

export type PageProps = MapProps & MapActions;
