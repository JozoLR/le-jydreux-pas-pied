import { connect } from 'react-redux'
import Map, { MapProps, MapActions } from './Map'
import { State } from '../../store/reducers'
import { getMap, updateMap } from 'store/map/map.action'

function mapStateToDispatch(state: State, ownProps: {gameId: string}): MapProps {
  return {
    map: state.map.map,
    gameId: ownProps.gameId
  }
}

const mapDispatchToProps: MapActions = {
  getMap,
  updateMap
}

export default connect(mapStateToDispatch, mapDispatchToProps)(Map)
