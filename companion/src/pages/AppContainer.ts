import { connect } from 'react-redux'
import { App, AppDispatchProps, AppStateProps } from './App'
import { loadGameRules } from '../store/game/game.action'
import { State } from '../store/reducers'
import { authorize } from '../store/auth/auth.action'

const mapStateToDispatch = (state: State): AppStateProps => ({
  isAuthenticated: state.auth.isAuthenticated
})

const mapDispatchToProps: AppDispatchProps = {
  loadGameRules,
  authorize
}

export default connect(mapStateToDispatch, mapDispatchToProps)(App)
