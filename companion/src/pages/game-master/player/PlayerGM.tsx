/* eslint-disable react/prop-types */
import * as React from 'react'
import './PlayerGM.scss'
import {Player} from 'domain/player'
import {Frame} from '../../../components/frame/Frame'

export const PlayerGM: React.FC<PlayerGMProps> = ({ player}) => (
    <div className="player-container">
      <h2 className="title-2">{player.name}</h2>
      <Frame>
        <>
          <h3 className="title-3">Attributs (Actual/Base)</h3>
          <div className="player-entry">
            {player.attributs.map(attribut => (
              <div>
                {attribut.label} : {attribut.actual}/{attribut.base}
              </div>
            ))}
          </div>
          <div className="player-block">
            <h3 className="title-3">Skills</h3>
            <div className="player-entry">
              {player.skills.filter(skill => skill.actual !== 0).map(skill => (
                <div>
                  {skill.label} : {skill.actual}
                </div>
              ))}
            </div>
          </div>
          <div className="player-block">
            <h3 className="title-3">Weapons (Bon/Mal/Dam)</h3>
            <div className="player-entry">
              {player.weapons.map(weapon => (
                <div>
                  {weapon.label} : {weapon.bonus}/{weapon.malus}/{weapon.damage}
                </div>
              ))}
            </div>
          </div>
          <div className="player-block">
            <h3 className="title-3">Gears (Bon/Mal)</h3>
            <div className="player-entry">
              {player.gears.map(gear => (
                <div>
                  {gear.label} : {gear.bonus}/{gear.malus}
                </div>
              ))}
            </div>
          </div>
          <div className="player-block">
            <h3 className="title-3">Resources</h3>
            <div className="player-entry">
              {player.resources.filter(resource => resource.quantity !== 0).map(resource => (
                <div>
                  {resource.label} : {resource.quantity}
                </div>
              ))}
            </div>
          </div>
        </>
      </Frame>
    </div>
)

export interface PlayerGMProps {
  player: Player;
}
