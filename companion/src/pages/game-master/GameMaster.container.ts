import { State } from '../../store/reducers'
import { connect } from 'react-redux'
import GameMaster, { GameMasterActions, GameMasterProps } from './GameMaster'
import { loadGame } from '../../store/game/game.action'
import { getNotifications } from '../../store/notification/notification.action'

function mapStateToDispatch(state: State): GameMasterProps {
  return {
    game: state.game.game,
    notifications: state.notification.notifications
  }
}

const mapDispatchToProps: GameMasterActions = {
  loadGame,
  getNotifications
}

export default connect(mapStateToDispatch, mapDispatchToProps)(GameMaster)
