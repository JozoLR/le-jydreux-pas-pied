import React from 'react'
import './GameMaster.scss'
import { PlayerGM } from './player/PlayerGM'
import { NotificationsGM } from './notifications/NotificationsGM'
import { subscribeToEvents } from '../../service/subscribeToEvents'
import { Game } from '../../domain/game'
import { Notification } from '../../domain/notification'

export default class GameMaster extends React.Component<GameMasterProps & GameMasterActions> {
  eventSource: EventSource | undefined
  // @ts-ignore
  gameId = this.props.match.params.gameId

  async componentDidMount(): Promise<void> {
    this.props.loadGame(this.gameId)
    this.props.getNotifications(this.gameId)
    this.eventSource = await subscribeToEvents(this.gameId)
  }

  componentWillUnmount(): void {
    this.eventSource?.close()
  }

  render(): JSX.Element {
    return <div className="gamemaster-container">
      <div className="players-container">
        {this.props.game.players.map((player) => <PlayerGM key={player.id} player={player} />)}
      </div>
      <div className="notification-container">
        <NotificationsGM notifications={this.props.notifications} />
      </div>
    </div>
  }
}

export interface GameMasterActions {
  loadGame: (id: string) => void;
  getNotifications: (gameId: string) => void;
}

export interface GameMasterProps {
  game: Game;
  notifications: Notification[];
}
