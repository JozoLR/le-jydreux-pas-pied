import * as React from 'react'
import { Notification } from '../../../domain/notification'
import { NotificationHistory } from '../../player-detail/notification/NotificationHistory'
import './NotificationsGM.scss'

export const NotificationsGM: React.FC<PlayerGMProps> = ({ notifications }) => (<div className="dicegm-rolls">
    <div className="dicegm-rolls-container">
      <NotificationHistory notifications={notifications}/>
    </div>
  </div>)

export interface PlayerGMProps {
  notifications: Notification[];
}
