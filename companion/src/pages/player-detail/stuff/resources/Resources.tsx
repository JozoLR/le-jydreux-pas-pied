/* eslint-disable react/prop-types */
import * as React from 'react'
import { Frame } from 'components/frame/Frame'
import { Resource } from 'domain/player'
import { InputGroupNumber } from 'components/input-group-number/InputGroupNumber'

export const Resources: React.FC<ResourcesProps> = ({ resources, updateResource }) => (
  <>
    <ul className="list">
      <Frame title="Resources">
        {resources.map((resource: Resource, index: number) =>
          <InputGroupNumber
            key={index}
            label={resource.label}
            value={resource.quantity}
            onChange={(value: number): void => updateResource({ ...resource, quantity: value })} />
        )}
      </Frame>
    </ul>
  </>
)

export interface ResourcesProps {
  resources: Resource[];
  updateResource: (resource: Resource) => void;
}
