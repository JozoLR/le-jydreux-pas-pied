import { connect } from 'react-redux'
import { State } from 'store/reducers'
import StuffComponent, { StuffProps, StuffActions } from './StuffComponent'
import { updateWeapon, updateGear, updateResource } from 'store/pages/player/player.action'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function mapStateToProps(state: State): StuffProps {
  return {
    weapons: state.player.player.weapons,
    gears: state.player.player.gears,
    resources: state.player.player.resources
  }
}

const mapDispatchToProps: StuffActions = {
  updateWeapon,
  updateGear,
  updateResource
}

export default connect(mapStateToProps, mapDispatchToProps)(StuffComponent)
