import './AddGear.scss'
import React from 'react'
import {Gear, Skill} from 'domain/player'
import {InputGroupNumber} from 'components/input-group-number/InputGroupNumber'
import {InputGroupSelect, InputGroupValue} from 'components/input-group-select/InputGroupSelect'
import {Icon} from 'components/icon/icon'
import {InputGroupText} from 'components/input-group-text/InputGroupText'

export default class AddGear extends React.Component<AddGearProps, State> {
  constructor(props: AddGearProps) {
    super(props)
    this.state = {
      gear: new Gear(null, '', [], 0, 0),
      selectedSkills: []
    }
  }

  render(): JSX.Element {
    return (
      <div className="add-stuff">
        <div className="modal-icon">
          <Icon type='gear'/>
        </div>
        <InputGroupText placeholder="Name" onChange={(value: string): void => this.onChange('label', value)}/>
        <InputGroupNumber label="Bonus" onChange={(value: number): void => this.onChange('bonus', value)}/>
        <InputGroupSelect label="Skill" selectedLabel="Select" values={this.skillList()}
                          onChange={(value: InputGroupValue): void => this.onSkillChange(value.id, value.label)}/>
        {this.state.selectedSkills.map(skill => <div>{skill.label}</div>)}
        <button className="button-add" onClick={this.createStuff}>Add Gear</button>
      </div>
    )
  }

  skillList = (): InputGroupValue[] => {
    const list: InputGroupValue[] = []
    this.props.skills.forEach(skill => {
      list.push({
        id: skill.id,
        label: skill.label
      })
    })
    return list
  }

  onChange = (key: string, value: number | string | string[]): void => {
    const newStuff = this.state.gear
    // @ts-ignore
    newStuff[key] = value
    this.setState({gear: newStuff})
  }

  onSkillChange = (id: string, label: string): void => {
    if (!this.state.gear.skillIds.find(existingId => id === existingId)) {
      this.state.gear.skillIds.push(id)
      this.state.selectedSkills.push({id, label})
      this.setState(this.state)
    }
  }

  createStuff = (): void => {
    const gear = this.state.gear
    this.props.addGear(new Gear(null, gear.label, gear.skillIds, gear.bonus, 0))
    this.props.closeModal()
  }
}

export type AddGearStateProps = {
  skills: Skill[];
  closeModal: () => void;
}

export type AddGearActionProps = {
  addGear: (gear: Gear) => void;
}

export type AddGearProps = AddGearStateProps & AddGearActionProps;

export type State = {
  gear: Gear;
  selectedSkills: {id: string; label: string}[];
}
