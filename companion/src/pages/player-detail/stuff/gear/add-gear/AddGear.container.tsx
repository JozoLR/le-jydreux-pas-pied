import {connect} from 'react-redux'
import {addGear} from 'store/pages/player/player.action'
import {State} from 'store/reducers'
import AddGear, {AddGearActionProps, AddGearStateProps} from './AddGear'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function mapStateToDispatch(state: State, ownProps: any): AddGearStateProps {
  return {
    skills: state.player.player.skills,
    closeModal: ownProps.closeModal
  }
}

const mapDispatchToProps: AddGearActionProps = {
  addGear: addGear
}

export default connect(mapStateToDispatch, mapDispatchToProps)(AddGear)
