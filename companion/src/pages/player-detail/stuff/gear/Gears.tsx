import * as React from 'react'
import './Gears.scss'
import { Gear } from 'domain/player'
import { Frame } from 'components/frame/Frame'
import { InputNumber } from 'components/input-number/InputNumber'
import Modal from 'react-bootstrap/Modal'
import GiveStuffContainer from '../give-stuff/GiveStuff.container'
import DeleteStuffContainer from '../delete-stuff/DeleteStuff.container'
import { Icon } from '../../../../components/icon/icon'
import { GameRules } from '../../../../domain/game-rules'

export class Gears extends React.Component<GearsProps, GearsState> {
  constructor(props: GearsProps) {
    super(props)
    this.state = {
      openGiveStuffModal: null,
      openDeleteStuffModal: null
    }
  }

  render() {
    const { gears, updateGear } = this.props
    return (
      <>
        <Frame title="Gears">
          <>
            {
              !gears.length &&
              <div className="empty">
                No gears added yet
              </div>
            }
            <ul className="list">
              {gears.map((gear: Gear, index: number) =>
                <li className="list__entry" key={index}>
                  {this.state.openGiveStuffModal && this.modalGiveStuff(gear.id!)}
                  {this.state.openDeleteStuffModal && this.modalDeleteStuff(gear.id!)}
                  <div className="stuff-entry">
                    <div className="top">
                      <h3 className="title-with-bg mask">{gear.label}</h3>
                      <div className="actions">
                        <button className="action"
                          onClick={(): void => this.setState({ openGiveStuffModal: gear.id! })}>
                          <Icon type="present" />
                        </button>
                        <button className="action"
                          onClick={(): void => this.setState({ openDeleteStuffModal: gear.id! })}>
                          <Icon type="trash" />
                        </button>
                      </div>
                    </div>
                    <ul className="infos">
                      <li className="infos__entry">
                        <h5 className="info-name text-main">Bonus</h5>
                        <span className="info-value text-main">{gear.bonus}</span>
                      </li>
                      <li className="infos__entry">
                        <h5 className="info-name text-main">Malus</h5>
                        <InputNumber
                          key={gear.id || 'id'}
                          value={gear.malus}
                          onChange={(value: number): void => updateGear({ ...gear, malus: value })} />
                      </li>
                      <li className="infos__entry">
                        <h5 className="info-name text-main">Skills</h5>
                        {gear.skillIds.length &&
                          <span className="info-value text-main">{gear.skillIds.join(', ')}</span>
                        }
                      </li>
                    </ul>
                  </div>
                </li>)
              }
            </ul>
          </>
        </Frame>
      </>
    )
  }

  private modalGiveStuff = (gearId: string): React.ReactNode => (
    <Modal show={this.state.openGiveStuffModal === gearId} onHide={this.closeModal.bind(this)}>
      <GiveStuffContainer stuffId={gearId} stuffType={GameRules.StuffType.GEAR}
        closeModal={this.closeModal.bind(this)} />
    </Modal>
  )

  private modalDeleteStuff = (gearId: string): React.ReactNode => (
    <Modal show={this.state.openDeleteStuffModal === gearId} onHide={this.closeModal.bind(this)}>
      <DeleteStuffContainer stuffId={gearId} stuffType={GameRules.StuffType.GEAR}
        closeModal={this.closeModal.bind(this)} />
    </Modal>
  )

  closeModal = (): void => {
    this.setState({
      openGiveStuffModal: null,
      openDeleteStuffModal: null
    })
  }
}

export interface GearsProps {
  gears: Gear[];
  updateGear: (gear: Gear) => void;
}

export interface GearsState {
  openGiveStuffModal: string | null;
  openDeleteStuffModal: string | null;
}
