import React from 'react'
import {GameRules} from '../../../../domain/game-rules'
import {Player} from '../../../../domain/player'

export default class DeleteStuff extends React.Component<DeleteStuffProps, State> {
  constructor(props: DeleteStuffProps) {
    super(props)
    this.state = {
      beneficiaryId: ''
    }
  }

  render(): JSX.Element {
    return (
      <div className="add-stuff">
        <button className="button-add" onClick={this.deleteStuff}>Delete Stuff</button>
      </div>
    )
  }

  onChange = (value: string): void => {
    this.setState({beneficiaryId: value})
  }

  deleteStuff = (): void => {
    if (this.props.stuffType === GameRules.StuffType.GEAR) {
      this.props.deleteGear(this.props.stuffId, this.props.player)
    }
    if (this.props.stuffType === GameRules.StuffType.WEAPON) {
      this.props.deleteWeapon(this.props.stuffId, this.props.player)
    }
    this.props.closeModal()
  }
}
export type DeleteStuffStateProps = {
  player: Player;
  stuffId: string;
  stuffType: GameRules.StuffType;
  closeModal: () => void;
}

export type DeleteStuffActionProps = {
  deleteGear: (gearId: string, player: Player) => void;
  deleteWeapon: (weaponId: string, player: Player) => void;
}

export type DeleteStuffProps = DeleteStuffStateProps & DeleteStuffActionProps;

export type State = {
  beneficiaryId: string;
}
