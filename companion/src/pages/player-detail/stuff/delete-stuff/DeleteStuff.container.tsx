import {connect} from 'react-redux'
import {deleteGear, deleteWeapon} from 'store/pages/player/player.action'
import {State} from 'store/reducers'
import DeleteStuff, {DeleteStuffActionProps, DeleteStuffStateProps} from './DeleteStuff'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function mapStateToDispatch(state: State, ownProps: any): DeleteStuffStateProps {
  return {
    player: state.player.player,
    stuffId: ownProps.stuffId,
    closeModal: ownProps.closeModal,
    stuffType: ownProps.stuffType
  }
}

const mapDispatchToProps: DeleteStuffActionProps = {
  deleteGear,
  deleteWeapon
}

export default connect(mapStateToDispatch, mapDispatchToProps)(DeleteStuff)
