import {connect} from 'react-redux'
import {addWeapon} from 'store/pages/player/player.action'
import {State} from 'store/reducers'
import AddWeapon, {AddWeaponActionProps, AddWeaponStateProps} from './AddWeapon'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function mapStateToDispatch(state: State, ownProps: any): AddWeaponStateProps {
  return {
    skills: state.player.player.skills,
    gameRules: state.game.gameRules,
    closeModal: ownProps.closeModal
  }
}

const mapDispatchToProps: AddWeaponActionProps = {
  addWeapon: addWeapon,
}

export default connect(mapStateToDispatch, mapDispatchToProps)(AddWeapon)
