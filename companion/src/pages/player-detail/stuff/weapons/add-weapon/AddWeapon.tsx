import './AddWeapon.scss'
import React from 'react'
import {Skill, Weapon} from 'domain/player'
import {InputGroupNumber} from 'components/input-group-number/InputGroupNumber'
import {InputGroupSelect, InputGroupValue} from 'components/input-group-select/InputGroupSelect'
import {GameRules} from 'domain/game-rules'
import {Icon} from 'components/icon/icon'
import {InputGroupText} from 'components/input-group-text/InputGroupText'

export default class AddWeapon extends React.Component<AddWeaponProps, State> {
  constructor(props: AddWeaponProps) {
    super(props)
    this.state = {
      weapon: new Weapon(null, '', '', 0, 0, 0, '', undefined)
    }
  }

  render(): JSX.Element {
    return (
      <div className="add-stuff">
        <div className="modal-icon">
          <Icon type='knife'/>
        </div>
        <InputGroupText placeholder="Name" onChange={(value: string): void => this.onChange('label', value)}/>
        <InputGroupNumber label="Bonus" onChange={(value: number): void => this.onChange('bonus', value)}/>
        <InputGroupNumber label="Damage" onChange={(value: number): void => this.onChange('damage', value)}/>
        <InputGroupSelect label="Skill" selectedLabel="Select" values={this.skillList()}
                          onChange={(value: InputGroupValue): void => this.onChange('skillId', [value.id])}/>
        <InputGroupSelect label="Range" selectedLabel="Select" values={this.rangeList()}
                          onChange={(value: InputGroupValue): void => this.onChange('scope', value.id)}/>
        <InputGroupSelect label="Resource" selectedLabel="Select" values={this.resourceList()}
                          onChange={(value: InputGroupValue): void => this.onChange('resourceId', value.id)}/>
        <button className="button-add" onClick={this.createStuff}>Add Weapon</button>
      </div>
    )
  }

  skillList = (): InputGroupValue[] => {
    const list: InputGroupValue[] = []
    this.props.skills.forEach(skill => {
      list.push({
        id: skill.id,
        label: skill.label
      })
    })
    return list
  }

  rangeList = (): InputGroupValue[] => {
    const list: InputGroupValue[] = []
    this.props.gameRules.weaponRanges.forEach(range => {
      list.push({
        id: range.id,
        label: range.label
      })
    })
    return list
  }

  resourceList = (): InputGroupValue[] => {
    const list: InputGroupValue[] = [{id: 'None', label: 'None'}]
    this.props.gameRules.resources.forEach(resource => {
      list.push({
        id: resource.id,
        label: resource.label
      })
    })
    return list
  }

  onChange = (key: string, value: number | string | string[]): void => {
    const newStuff = this.state.weapon
    // @ts-ignore
    newStuff[key] = value
    this.setState({weapon: newStuff})
  }

  createStuff = (): void => {
    const weapon = this.state.weapon
    this.props.addWeapon(new Weapon(null, weapon.label, weapon.skillId, weapon.bonus, 0, weapon.damage, weapon.scope, weapon.resourceId))
    this.props.closeModal()
  }
}

export type AddWeaponStateProps = {
  skills: Skill[];
  gameRules: GameRules;
  closeModal: () => void;
}

export type AddWeaponActionProps = {
  addWeapon: (weapon: Weapon) => void;
}

export type AddWeaponProps = AddWeaponStateProps & AddWeaponActionProps;

export type State = {
  weapon: Weapon;
}
