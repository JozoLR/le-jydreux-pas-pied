import * as React from 'react'
import './Weapon.scss'
import { Weapon } from 'domain/player'
import { Frame } from 'components/frame/Frame'
import { InputNumber } from 'components/input-number/InputNumber'
import Modal from 'react-bootstrap/Modal'
import { Icon } from '../../../../components/icon/icon'
import GiveStuffContainer from '../give-stuff/GiveStuff.container'
import { GameRules } from '../../../../domain/game-rules'
import DeleteStuffContainer from '../delete-stuff/DeleteStuff.container'

export class Weapons extends React.Component<WeaponsProps, WeaponsState> {
  constructor(props: WeaponsProps) {
    super(props)
    this.state = {
      openGiveStuffModal: null,
      openDeleteStuffModal: null
    }
  }

  render() {
    const { weapons, updateWeapon } = this.props
    return (
      <>
        <ul className="list">
          <Frame title="Weapons">
            <>
              {
                !weapons.length &&
                <div className="empty">
                  No weapons added yet
                </div>
              }
              {weapons.map((weapon: Weapon, index: number) =>
                <li className="list__entry" key={index}>
                  {this.state.openGiveStuffModal && this.modalGiveStuff(weapon.id!)}
                  {this.state.openDeleteStuffModal && this.modalDeleteStuff(weapon.id!)}
                  <div className="stuff-entry">
                    <div className="top">
                      <h3 className="title-with-bg mask">{weapon.label}</h3>
                      <div className="actions">
                        <button className="action"
                          onClick={(): void => this.setState({ openGiveStuffModal: weapon.id! })}>
                          <Icon type="present" />
                        </button>
                        <button className="action"
                          onClick={(): void => this.setState({ openDeleteStuffModal: weapon.id! })}>
                          <Icon type="trash" />
                        </button>
                      </div>
                    </div>
                    <ul className="infos">
                      <li className="infos__entry">
                        <h5 className="info-name text-main">Damage</h5>
                        <span className="info-value text-main is-damage">{weapon.damage}</span>
                      </li>
                      <li className="infos__entry">
                        <h5 className="info-name text-main">Bonus</h5>
                        <span className="info-value text-main">{weapon.bonus}</span>
                      </li>
                      <li className="infos__entry">
                        <h5 className="info-name text-main">Range</h5>
                        <span className="info-value text-main">{weapon.scope}</span>
                      </li>
                      <li className="infos__entry">
                        <h5 className="info-name text-main">Skill</h5>
                        <span className="info-value text-main">{weapon.skillId}</span>
                      </li>
                      {weapon.resourceId &&
                        <li className="infos__entry">
                          <h5 className="info-name text-main">Ressource</h5>
                          <span className="info-value text-main">{weapon.resourceId}</span>
                        </li>
                      }
                      <li className="infos__entry">
                        <h5 className="info-name text-main">Malus</h5>
                        <InputNumber
                          key={weapon.id || 'id'}
                          value={weapon.malus}
                          onChange={(value: number): void => updateWeapon({ ...weapon, malus: value })} />
                      </li>
                    </ul>
                  </div>
                </li>)
              }
            </>
          </Frame>
        </ul>
      </>
    )
  }

  private modalGiveStuff = (gearId: string): React.ReactNode => (
    <Modal show={this.state.openGiveStuffModal === gearId} onHide={this.closeModal.bind(this)}>
      <GiveStuffContainer stuffId={gearId} stuffType={GameRules.StuffType.WEAPON} closeModal={this.closeModal.bind(this)} />
    </Modal>
  )

  private modalDeleteStuff = (gearId: string): React.ReactNode => (
    <Modal show={this.state.openDeleteStuffModal === gearId} onHide={this.closeModal.bind(this)}>
      <DeleteStuffContainer stuffId={gearId} stuffType={GameRules.StuffType.WEAPON} closeModal={this.closeModal.bind(this)} />
    </Modal>
  )

  closeModal = (): void => {
    this.setState({
      openGiveStuffModal: null,
      openDeleteStuffModal: null
    })
  }
}

export interface WeaponsProps {
  weapons: Weapon[];
  updateWeapon: (weapon: Weapon) => void;
}

export interface WeaponsState {
  openGiveStuffModal: string | null;
  openDeleteStuffModal: string | null;
}
