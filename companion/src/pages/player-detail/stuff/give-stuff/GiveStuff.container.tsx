import {connect} from 'react-redux'
import {giveGear, giveWeapon} from 'store/pages/player/player.action'
import {State} from 'store/reducers'
import GiveStuff, {GiveStuffActionProps, GiveStuffStateProps} from './GiveStuff'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function mapStateToDispatch(state: State, ownProps: any): GiveStuffStateProps {
  return {
    players: state.game.game.players,
    player: state.player.player,
    stuffId: ownProps.stuffId,
    closeModal: ownProps.closeModal,
    stuffType: ownProps.stuffType
  }
}

const mapDispatchToProps: GiveStuffActionProps = {
  giveWeapon,
  giveGear
}

export default connect(mapStateToDispatch, mapDispatchToProps)(GiveStuff)
