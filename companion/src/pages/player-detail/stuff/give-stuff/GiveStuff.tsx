import React from 'react'
import {Player} from 'domain/player'
import {InputGroupSelect, InputGroupValue} from 'components/input-group-select/InputGroupSelect'
import {Icon} from 'components/icon/icon'
import {GameRules} from '../../../../domain/game-rules'

export default class GiveStuff extends React.Component<GiveStuffProps, State> {
  constructor(props: GiveStuffProps) {
    super(props)
    this.state = {
      beneficiaryId: ''
    }
  }

  render(): JSX.Element {
    return (
      <div className="add-stuff">
        <div className="modal-icon">
          <Icon type='present'/>
        </div>
        <InputGroupSelect label="Player" selectedLabel="Select" values={this.playerList()}
                          onChange={(value: InputGroupValue): void => this.onChange(value.id)}/>
        <button className="button-add" onClick={this.giveStuff}>Send Stuff</button>
      </div>
    )
  }

  playerList = (): InputGroupValue[] => {
    const list: InputGroupValue[] = []
    this.props.players.filter(player => player.id !== this.props.player.id).forEach(player => {
      list.push({
        id: player.id,
        label: player.name
      })
    })
    return list
  }

  onChange = (value: string): void => {
    this.setState({beneficiaryId: value})
  }

  giveStuff = (): void => {
    if (this.props.stuffType === GameRules.StuffType.WEAPON) {
      this.props.giveWeapon(this.props.stuffId, this.props.player, this.state.beneficiaryId)
    }
    if (this.props.stuffType === GameRules.StuffType.GEAR) {
      this.props.giveGear(this.props.stuffId, this.props.player, this.state.beneficiaryId)
    }
    this.props.closeModal()
  }
}
export type GiveStuffStateProps = {
  players: Player[];
  player: Player;
  stuffId: string;
  stuffType: GameRules.StuffType;
  closeModal: () => void;
}

export type GiveStuffActionProps = {
  giveWeapon: (weaponId: string, player: Player, beneficiaryId: string) => void;
  giveGear: (gearId: string, player: Player, beneficiaryId: string) => void;
}

export type GiveStuffProps = GiveStuffStateProps & GiveStuffActionProps;

export type State = {
  beneficiaryId: string;
}
