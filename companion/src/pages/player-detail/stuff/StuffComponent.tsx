import './Stuff.scss'
import React from 'react'
import { Gear, Resource, Weapon } from '../../../domain/player'
import Modal from 'react-bootstrap/Modal'
import AddWeaponContainer from 'pages/player-detail/stuff/weapons/add-weapon/AddWeapon.container'
import AddGearContainer from 'pages/player-detail/stuff/gear/add-gear/AddGear.container'
import { Weapons } from './weapons/Weapons'
import { Gears } from './gear/Gears'
import { Resources } from './resources/Resources'

export default class StuffComponent extends React.Component<StuffProps & StuffActions, StuffState> {
  constructor(props: StuffProps & StuffActions) {
    super(props)
    this.state = {
      openAddWeaponModal: false,
      openAddGearModal: false
    }
  }

  render(): JSX.Element {
    return (
      <main className="player-detail-main stuff">
        <div className="stuff-category">
          <Resources resources={this.props.resources} updateResource={this.props.updateResource} />
        </div>
        <div className="stuff-category">
          <Weapons weapons={this.props.weapons} updateWeapon={this.props.updateWeapon} />
          <button onClick={(): void => this.setState({ openAddWeaponModal: true })} className="button-add-small">
            <span className="material-icons">add</span> Add
          </button>
        </div>
        <div className="stuff-category">
          <Gears gears={this.props.gears} updateGear={this.props.updateGear} />
          <button onClick={(): void => this.setState({ openAddGearModal: true })} className="button-add-small">
            <span className="material-icons">add</span> Add
          </button>
        </div>
        {this.modalAddWeapon()}
        {this.modalAddGear()}
      </main>
    )
  }

  private modalAddWeapon = (): React.ReactNode => (
    <Modal show={this.state.openAddWeaponModal} onHide={this.closeModal.bind(this)}>
      <button className="button-close-modal" onClick={this.closeModal.bind(this)}><span
        className="material-icons">close</span></button>
      <AddWeaponContainer closeModal={this.closeModal.bind(this)} />
    </Modal>
  )

  private modalAddGear = (): React.ReactNode => (
    <Modal show={this.state.openAddGearModal} onHide={this.closeModal.bind(this)}>
      <button className="button-close-modal" onClick={this.closeModal.bind(this)}><span
        className="material-icons">close</span></button>
      <AddGearContainer closeModal={this.closeModal.bind(this)} />
    </Modal>
  )


  closeModal = (): void => {
    this.setState({ openAddGearModal: false, openAddWeaponModal: false })
  }
}

export interface StuffProps {
  weapons: Weapon[];
  gears: Gear[];
  resources: Resource[];
}

export interface StuffActions {
  updateWeapon: (weapon: Weapon) => void;
  updateGear: (gear: Gear) => void;
  updateResource: (resource: Resource) => void;
}

export interface StuffState {
  openAddWeaponModal: boolean;
  openAddGearModal: boolean;
}
