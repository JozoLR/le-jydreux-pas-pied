import './PlayerDetail.scss'
import React from 'react'
import { Player } from '../../domain/player'
import { Notification } from '../../domain/notification'
import DiceContainer from './dice/Dice.container'
import StuffComponentContainer from './stuff/Stuff.container'
import { subscribeToEvents } from '../../service/subscribeToEvents'
import { match } from 'react-router-dom'
import Modal from 'react-bootstrap/Modal'
import UpgradePlayerContainer from './upgrade-player/UpgradePlayer.container'
import { NotificationHistory } from './notification/NotificationHistory'
import MapContainer from 'pages/map/Map.container'
import CharacterContainer from './character/Character.container'

export default class PlayerDetail extends React.Component<PageProps, DetailPlayerState> {
  eventSource: EventSource | undefined
  playerId = this.props.route.params.playerId
  gameId = this.props.route.params.gameId

  constructor(props: Readonly<PageProps>) {
    super(props)
    this.state = {
      openModal: false,
      activeTab: PlayerDetailTab.CHARACTER
    }
  }

  async componentDidMount(): Promise<void> {
    this.props.loadPlayer(this.playerId)
    this.props.getNotifications(this.gameId)
    this.props.getRolls(this.gameId)
    this.props.loadGame(this.gameId)
    this.eventSource = await subscribeToEvents(this.gameId, this.playerId)
  }

  componentWillUnmount(): void {
    this.eventSource?.close()
  }

  render(): JSX.Element {
    return <>
      <div className="player-details">
        <header className="header">
          <div className="left">
            <h2 className="title text-main">{this.props.player.name}</h2>
            {
              this.props.player.experience >= 5 &&
              <button className="button-upgrade" onClick={this.openModal}>
                <span className="material-icons">add</span> upgrade
                </button>
            }
          </div>
          <div className="middle">
            <nav className="menu">
              <button className={`menu__entry ${this.state.activeTab == PlayerDetailTab.CHARACTER ? 'is-active' : ''}`} onClick={(): void => this.onClickMenu(PlayerDetailTab.CHARACTER)}>Character</button>
              <button className={`menu__entry ${this.state.activeTab == PlayerDetailTab.INVENTORY ? 'is-active' : ''}`} onClick={(): void => this.onClickMenu(PlayerDetailTab.INVENTORY)}>Inventory</button>
              <button className={`menu__entry ${this.state.activeTab == PlayerDetailTab.MAP ? 'is-active' : ''}`} onClick={(): void => this.onClickMenu(PlayerDetailTab.MAP)}>Map</button>
            </nav>
          </div>
          <div className="right">
            <button className="button-simple" onClick={this.props.logout}>Logout</button>
          </div>
        </header>
        <div className="sidebar">
          <div className="notifications-wrapper">
            <NotificationHistory notifications={this.props.notifications} />
          </div>
          <div className="dice-wrapper">
            <DiceContainer playerId={this.playerId} gameId={this.gameId} />
          </div>
        </div>
        {this.displayTab()}
      </div>
      {this.modalUpgradePlayer()}
    </>
  }

  private displayTab = (): React.ReactNode => {
    switch (this.state.activeTab) {
      case PlayerDetailTab.CHARACTER:
        return <CharacterContainer />
      case PlayerDetailTab.INVENTORY:
        return <StuffComponentContainer />
      case PlayerDetailTab.MAP:
        return <MapContainer gameId={this.gameId} />
    }
  }

  private onClickMenu = (tab: PlayerDetailTab): void => {
    this.setState({ activeTab: tab })
  }

  private modalUpgradePlayer = (): React.ReactNode => (
    <Modal show={this.state.openModal} onHide={this.closeModal.bind(this)}>
      <button className="button-close-modal" onClick={this.closeModal.bind(this)}><span className="material-icons">close</span></button>
      <UpgradePlayerContainer closeModal={this.closeModal.bind(this)} />
    </Modal>
  )

  closeModal = (): void => { this.setState({ openModal: false }) }
  openModal = (): void => { this.setState({ openModal: true }) }
}

export interface DetailPlayerStateProps {
  player: Player;
  notifications: Notification[];
  route: match<PlayerDetailRouteParam>;
}

export interface DetailPlayerDispatchProps {
  loadPlayer: (playerId: string) => void;
  loadGame: (gameId: string) => void;
  getNotifications: (gameId: string) => void;
  getRolls: (gameId: string) => void;
  logout: () => void;
}

export interface DetailPlayerState {
  openModal: boolean;
  activeTab: PlayerDetailTab;
}

export enum PlayerDetailTab {
  CHARACTER = 'CHARACTER',
  INVENTORY = 'INVENTORY',
  MAP = 'MAP'
}

export interface PlayerDetailRouteParam {
  playerId: string;
  gameId: string;
}

export type PageProps = DetailPlayerStateProps & DetailPlayerDispatchProps;
