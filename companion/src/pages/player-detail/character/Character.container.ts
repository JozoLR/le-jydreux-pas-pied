import { connect } from 'react-redux'
import Character, { CharacterProps, CharacterAction } from './Character'
import { State } from '../../../store/reducers'
import { updateAttribut, updatePlayerPoint, updateSkill } from 'store/pages/player/player.action'

function mapStateToDispatch(state: State): CharacterProps {
  return {
    player: state.player.player
  }
}

const mapDispatchToProps: CharacterAction = {
  updateAttribut,
  updateSkill,
  updatePlayerPoint
}

export default connect(mapStateToDispatch, mapDispatchToProps)(Character)
