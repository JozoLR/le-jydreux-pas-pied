import './Character.scss'
import React from 'react'
import { Attribut, Player, PointType, Skill, SKillType } from '../../../domain/player'
import { GameRules } from '../../../domain/game-rules'
import { InputGroupNumber } from 'components/input-group-number/InputGroupNumber'
import { Frame } from 'components/frame/Frame'
import NotesContainer from '../notes/Notes.container'

export default class Character extends React.Component<Global> {
  render(): JSX.Element {
    return (
      <main className="player-detail-main">
        <div>
          <Frame title="Talents">
            <>
              <h3 className="player-class">{this.props.player.gameClass.label}</h3>
              <ul>
                {this.talents(this.props.player.talents)}
              </ul>
            </>
          </Frame>
          <Frame title="Attributs">
            <ul>
              {this.attributs(this.props.player.attributs)}
            </ul>
          </Frame>
          <Frame title="Points">
            <ul>
              {this.points(this.props.player)}
            </ul>
          </Frame>
        </div>
        <Frame title="Skills">
          <ul>
            {this.skills(this.props.player.skills)}
          </ul>
        </Frame>
        <NotesContainer />
      </main>
    )
  }

  private talents = (talents: GameRules.Talent[]): React.ReactNode => (
    talents.map((talent: GameRules.Talent) => (
      <li className="talent" key={talent.label}>
        <h3 className="title text-main">{talent.label}</h3>
        <p className="text-mini">{talent.description}</p>
      </li>
    ))
  )

  private attributs = (attributs: Attribut[]): React.ReactNode => (
    attributs.map((attribut: Attribut) =>
      <InputGroupNumber
        key={attribut.label}
        label={attribut.label}
        value={attribut.actual}
        max={attribut.base}
        bubbles={true}
        onChange={(value: number): void => this.props.updateAttribut({ ...attribut, actual: value })} />
    )
  )

  private skills = (skills: Skill[]): React.ReactNode => (
    skills.filter(skill => skill.skillType !== SKillType.ARMOR).map((skill: Skill) =>
      <InputGroupNumber
        key={skill.label}
        label={skill.label}
        value={skill.actual}
        onChange={(value: number): void => this.props.updateSkill({ ...skill, actual: value })} />
    )
  )

  private points = (player: Player): React.ReactNode => (
    <>
      <InputGroupNumber
        key={PointType.EXPERIENCE}
        label={'Experience'}
        value={player.experience}
        onChange={(value: number): void => { this.props.updatePlayerPoint(PointType.EXPERIENCE, value) }} />
      <InputGroupNumber
        key={PointType.ROT}
        label={'Rot'}
        value={player.rot}
        onChange={(value: number): void => { this.props.updatePlayerPoint(PointType.ROT, value) }} />
      <InputGroupNumber
        key={PointType.MUTATION}
        label={'Mutation'}
        value={player.mutation}
        onChange={(value: number): void => { this.props.updatePlayerPoint(PointType.MUTATION, value) }} />
    </>
  )
}

export interface CharacterProps {
  player: Player;
}

export interface CharacterAction {
  updateAttribut: (attribut: Attribut) => void;
  updateSkill: (skill: Skill) => void;
  updatePlayerPoint: (category: PointType, value: number) => void;
}

export type Global = CharacterAction & CharacterProps;
