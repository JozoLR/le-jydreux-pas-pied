import './NotificationHistory.scss'
import React from 'react'
import {
  isPlayerUgraded,
  isPlayerUpdate,
  isRoll,
  isStuffAdded, isStuffDeleted,
  isStuffGiven,
  Notification,
  PlayerUpdate, StuffAdded, StuffDeleted,
  StuffGiven
} from '../../../domain/notification'
import { Roll, RollType } from 'domain/roll'
import { DiceHistoryEntry } from './diceHistoryEntry/DiceHistoryEntry'
import { GameRules } from '../../../domain/game-rules'

export class NotificationHistory extends React.Component<NotificationProps> {

  render(): JSX.Element {
    return (
      <ul className="notifications">
        {
          this.props.notifications.map(notification => (
            <>
              {
                isRoll(notification) &&
                <li className="notification">
                  <div className="top">
                    <div className="player-name">{notification.playerName}</div>
                    <div className="action">{this.rollLabel(notification.content as Roll)}</div>
                    <div className="date">{notification.date.toRelative()}</div>
                  </div>
                  <div className="bottom">
                    <DiceHistoryEntry notification={notification} />
                  </div>
                </li>
              }
              {
                isPlayerUpdate(notification) &&
                <li className="notification">
                  <div className="top">
                    <div className="player-name">{notification.playerName}</div>
                    <div className="date">{notification.date.toRelative()}</div>
                  </div>
                  <div className="bottom">
                    <span className="item">{(notification.content as PlayerUpdate).label}</span> value is now <span
                      className="value">{(notification.content as PlayerUpdate).currentValue}</span>
                  </div>
                </li>
              }
              {
                isStuffAdded(notification) &&
                <li className="notification">
                  <div className="top">
                    <div className="player-name">{notification.playerName}</div>
                    <div className="date">{notification.date.toRelative()}</div>
                  </div>
                  <div className="bottom">
                    <span className="item">Has a brand new </span>
                    <span className="value">{(notification.content as StuffAdded).label}</span>
                  </div>
                </li>
              }
              {
                isStuffGiven(notification) &&
                <li className="notification">
                  <div className="top">
                    <div className="player-name">{notification.playerName}</div>
                    <div className="date">{notification.date.toRelative()}</div>
                  </div>
                  <div className="bottom">
                    <span className="item">{(notification.content as StuffGiven).stuffName}</span> given to <span
                      className="value">{(notification.content as StuffGiven).beneficiaryName}</span>
                  </div>
                </li>
              }
              {
                isStuffDeleted(notification) &&
                <li className="notification">
                  <div className="top">
                    <div className="player-name">{notification.playerName}</div>
                    <div className="date">{notification.date.toRelative()}</div>
                  </div>
                  <div className="bottom">
                    <span className="value">{(notification.content as StuffDeleted).stuffName}</span>
                    <span className="item"> was deleted</span>
                  </div>
                </li>
              }
              {
                isPlayerUgraded(notification) &&
                <li className="notification">
                  <div className="top">
                    <div className="player-name">{notification.playerName}</div>
                    <div className="date">{notification.date.toRelative()}</div>
                  </div>
                  <div className="bottom">
                    <span className="item">Has upgraded</span>
                  </div>
                </li>
              }
            </>
          ))
        }
      </ul>
    )
  }

  rollLabel(roll: Roll): string {
    const rollOrReroll = roll.rollType == RollType.ROLL ? 'roll' : 'reroll'
    const skillLabel = roll.rollInfos.skillId ? roll.rollInfos.skillLabel : ''
    const stuffLabel = roll.rollInfos.stuffType === GameRules.StuffType.NONE ? '' :
      `with ${roll.rollInfos.stuffs
        .map(stuff => stuff.label)
        .reduce((previous, newStuff) => `${previous} and ${newStuff}`)}`
    return `did a ${skillLabel} ${rollOrReroll} ${stuffLabel}`
  }
}

export interface NotificationProps {
  notifications: Notification[];
}
