import './DiceHistoryEntry.scss'
import React from 'react'
import { DiceType } from '../../../../domain/dice.model'
import {Notification} from '../../../../domain/notification'
import {Roll} from '../../../../domain/roll'

export class DiceHistoryEntry extends React.Component<DiceHistoryEntryProps> {
  render(): JSX.Element {
    return (
      <ul className="dice-history-entry" key={this.props.notification.date.valueOf()}>
        {(this.props.notification.content as Roll).attributRolls
          .map((die, index: number) => this.die(die, `${index}`, DiceType.ATTRIBUT))}
        {(this.props.notification.content as Roll).skillRolls
          .map((die, index: number) => this.die(die, `${index}`, DiceType.SKILL))}
        {(this.props.notification.content as Roll).stuffRolls
          .map((die, index: number) => this.die(die, `${index}`, DiceType.STUFF))}
        {(this.props.notification.content as Roll).malusRolls
          .map((die, index: number) => this.die(die, `${index}`, DiceType.MALUS))}
      </ul>
    )
  }

  private die = (value: number, index: string, category: string): React.ReactNode => (
    <li className={`die ${value == 1 || value == 6 ? 'is-active' : ''} die--${this.mapCategoryToIndex(category)}`} key={index}>
      <span>{value}</span>
    </li>
  )

  mapCategoryToIndex(category: string): number {
    switch (category) {
      case DiceType.ATTRIBUT:
        return 0
      case DiceType.SKILL:
        return 1
      case DiceType.STUFF:
        return 2
      case DiceType.MALUS:
        return 3
      default:
        return 4
    }
  }
}

interface DiceHistoryEntryProps {
  notification: Notification;
}
