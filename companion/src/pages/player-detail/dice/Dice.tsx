import './Dice.scss'
import React from 'react'
import { Roll, RollDiceCommand, RollInfos, RollType } from '../../../domain/roll'
import { GameRules } from 'domain/game-rules'
import { RollProposition } from 'domain/rollProposition'
import { DiceProposition } from './diceProposition/DiceProposition'
import { InputNumber } from 'components/input-number/InputNumber'
import { Icon } from 'components/icon/icon'

export default class Dice extends React.Component<DicePropsAction, DiceState> {

  constructor(props: DicePropsAction) {
    super(props)
    this.state = {
      attribut: 0,
      skill: 0,
      stuff: 0,
      malus: 0,
      playerId: '',
      selectedBonuses: [],
      selectedSkillProposition: undefined,
      rollInfos: {
        skillId: '',
        skillLabel: '',
        stuffs: [],
        stuffType: GameRules.StuffType.NONE
      }
    }
  }

  componentDidMount(): void {
    this.props.loadRollProposition(this.props.playerId)
    this.setState({
      playerId: this.props.playerId
    })
  }

  render(): JSX.Element {
    return (
      <div className="roll-dice">
        <div className="roll-button-wrapper">
          <DiceProposition
            rollProposition={this.props.rollProposition}
            selectedSkillProposition={this.state.selectedSkillProposition}
            selectedBonuses={this.state.selectedBonuses}
            useProposition={this.useProposition.bind(this)}
            addBonus={this.addBonus.bind(this)}
            removeBonus={this.removeBonus.bind(this)}
          />
        </div>
        <ul className="dice-list">
          <li key="attribut" className="die-input">
            <InputNumber value={this.state.attribut} onChange={(value: number): void => {
              this.setState({ attribut: value })
            }} color={0} />
            <label className="label">Attribut</label>
          </li>
          <li key="skill" className="die-input">
            <InputNumber value={this.state.skill} onChange={(value: number): void => {
              this.setState({ skill: value })
            }} color={1} />
            <label className="label">Skill</label>
          </li>
          <li key="stuff" className="die-input">
            <InputNumber value={this.state.stuff} onChange={(value: number): void => {
              this.setState({ stuff: value })
            }} color={2} />
            <label className="label">Stuff</label>
          </li>
          <li key="malus" className="die-input">
            <InputNumber value={this.state.malus} onChange={(value: number): void => {
              this.setState({ malus: value })
            }} color={3} />
            <label className="label">Malus</label>
          </li>
        </ul>
        <div className="bottom">
          <div className="inner">
            <button className="button" onClick={(): void => { this.clearDice() }}>
              <span className="material-icons">clear</span>
            </button>
            <button className="roll-button" onClick={(): void => this.roll()}>
              <Icon type='dice' />
            </button>
            <button disabled={!(this.props.lastRoll && this.props.lastRoll.rollType == RollType.ROLL)}
              className="button" onClick={(): void => this.reroll()}>
              <span className="material-icons">refresh</span>
            </button>
          </div>
        </div>
      </div>
    )
  }

  roll(): void {
    const rollDiceCommand: RollDiceCommand = {
      playerId: this.state.playerId,
      attribut: this.state.attribut,
      skill: this.state.skill,
      stuff: this.state.stuff,
      malus: this.state.malus,
      rollInfos: {
        skillId: this.state.rollInfos.skillId,
        stuffType: this.state.rollInfos.stuffType,
        stuffIds: this.state.rollInfos.stuffs.map(stuff => stuff.id)
      }
    }
    this.props.rollDice(rollDiceCommand)
  }

  reroll(): void {
    this.props.reroll(this.props.playerId)
  }

  clearDice(): void {
    this.setState({
      attribut: 0,
      skill: 0,
      stuff: 0,
      malus: 0,
      selectedBonuses: [],
      selectedSkillProposition: undefined,
      rollInfos: {
        skillId: '',
        skillLabel: '',
        stuffs: [],
        stuffType: GameRules.StuffType.NONE
      }
    })
  }

  useProposition(proposition: RollProposition.SkillProposition): void {
    this.setState({
      attribut: proposition.attributDice,
      skill: proposition.skillDice,
      stuff: 0,
      malus: 0,
      selectedSkillProposition: proposition,
      selectedBonuses: [],
      rollInfos: {
        skillId: proposition.skillId,
        skillLabel: proposition.skillLabel,
        stuffType: GameRules.StuffType.NONE,
        stuffs: []
      }
    })
  }

  addBonus(bonus: RollProposition.Bonus): void {
    const rollInfos = {
      ...this.state.rollInfos,
      stuffType: bonus.stuffType
    }
    rollInfos.stuffs.push(
      { id: bonus.id, label: bonus.label }
    )
    this.state.selectedBonuses.push(bonus)
    this.setState({
      rollInfos,
      selectedBonuses: this.state.selectedBonuses,
      stuff: this.state.stuff + bonus.value
    })
  }

  removeBonus(bonus: RollProposition.Bonus): void {
    const rollInfos = {
      ...this.state.rollInfos,
      stuffType: bonus.stuffType,
      stuffs: this.state.rollInfos.stuffs.filter(stuff => stuff.id !== bonus.id)
    }
    this.setState({
      rollInfos,
      stuff: this.state.stuff - bonus.value
    })
  }
}

export interface DiceState {
  playerId: string;
  skill: number;
  attribut: number;
  stuff: number;
  malus: number;
  rollInfos: RollInfos;
  selectedSkillProposition: RollProposition.SkillProposition | undefined;
  selectedBonuses: RollProposition.Bonus[];
}

export interface DiceProps {
  rolls: Roll[];
  playerId: string;
  gameId: string;
  dice: GameRules.Dice[];
  rollProposition: RollProposition;
  lastRoll: Roll | undefined;
}

export interface DiceAction {
  rollDice: (rollDiceCommand: RollDiceCommand) => void;
  reroll: (playerId: string) => void;
  updateDice: (value: number, dice: string) => void;
  loadRollProposition: (playerId: string) => void;
}

export type DicePropsAction = DiceProps & DiceAction;

