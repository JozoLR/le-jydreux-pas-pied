import { RollProposition } from 'domain/rollProposition'
import React from 'react'
import { InputGroupSelect, InputGroupValue } from '../../../../components/input-group-select/InputGroupSelect'
import './DiceProposition.scss'

export class DiceProposition extends React.Component<DicePropositionProps, DicePropositionState> {

  constructor(props: DicePropositionProps) {
    super(props)
    this.state = {
      possibleStuff: []
    }
  }

  render(): JSX.Element {
    return <div className="dice-proposition-bar">
      <InputGroupSelect
        selectedLabel={this.props.selectedSkillProposition ? this.props.selectedSkillProposition.skillLabel : 'Skill'}
        values={this.skillList()}
        onChange={(value: InputGroupValue): void => this.onSkillChange(value.id)} />
      {
        this.props.selectedBonuses.map(bonus => (
          <InputGroupSelect
            selectedLabel={bonus.label}
            values={[]}
            onChange={(): void => { }} />
        ))
      }
      {
        (this.props.selectedSkillProposition && this.state.possibleStuff.length) ?
          <InputGroupSelect
            selectedLabel="Stuff"
            values={this.stuffList()}
            onChange={(value: InputGroupValue): void => this.onStuffChange(value.id)} /> : <></>
      }
    </div>
  }

  changeSkill(proposition: RollProposition.SkillProposition): void {
    const possibleStuff = proposition.bonuses
    this.setState({
      possibleStuff
    })

    this.props.useProposition(proposition)
  }

  private skillList(): InputGroupValue[] {
    return this.props.rollProposition.skillPropositions.map(proposition => ({
      id: proposition.skillId,
      label: proposition.skillLabel
    }))
  }

  private stuffList(): InputGroupValue[] {
    return this.state.possibleStuff.map(bonus => ({
      id: bonus.id,
      label: bonus.label
    }))
  }

  private onSkillChange(id: string): void {
    const proposition = this.props.rollProposition.skillPropositions
      .find(proposition => proposition.skillId === id)!

    const possibleStuff = proposition.bonuses
    this.setState({
      possibleStuff
    })

    this.props.useProposition(proposition)
  }

  private onStuffChange(id: string): void {
    const bonus = this.state.possibleStuff.find(bonus => bonus.id === id)!
    this.props.addBonus(bonus)
    const remainingStuff = this.state.possibleStuff.filter(bonus => bonus.id !== id)
    this.setState({
      possibleStuff: remainingStuff,
    })
  }
}

interface DicePropositionProps {
  rollProposition: RollProposition;
  selectedSkillProposition: RollProposition.SkillProposition | undefined;
  selectedBonuses: RollProposition.Bonus[];
  useProposition: (proposition: RollProposition.SkillProposition) => void;
  addBonus: (bonus: RollProposition.Bonus) => void;
  removeBonus: (bonus: RollProposition.Bonus) => void;
}

interface DicePropositionState {
  possibleStuff: RollProposition.Bonus[];
}
