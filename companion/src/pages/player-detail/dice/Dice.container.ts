import { connect } from 'react-redux'
import Dice, { DiceAction, DiceProps } from './Dice'
import { State } from '../../../store/reducers'
import { rerollDice, rollDice } from '../../../store/game/game.action'
import { updateDice } from '../../../store/pages/dice/dice.action'
import { loadRollProposition } from 'store/pages/player/player.action'

function mapStateToDispatch(state: State, ownProps: {playerId: string; gameId: string}): DiceProps {
  return {
    rolls: state.game.rolls,
    playerId: ownProps.playerId,
    gameId: ownProps.gameId,
    dice: state.game.gameRules.dice,
    rollProposition: state.player.rollProposition,
    lastRoll: state.player.player.lastRoll
  }
}

const mapDispatchToProps: DiceAction = {
  rollDice: rollDice,
  reroll: rerollDice,
  updateDice: updateDice,
  loadRollProposition: loadRollProposition
}

export default connect(mapStateToDispatch, mapDispatchToProps)(Dice)
