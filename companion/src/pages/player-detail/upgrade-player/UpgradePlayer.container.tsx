import { connect } from 'react-redux'
import { State } from 'store/reducers'
import UpgradePlayer, { UpgradePlayerActionProps, UpgradePlayerStateProps } from './UpgradePlayer'
import { upgradePlayer } from '../../../store/pages/player/player.action'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function mapStateToDispatch(state: State, ownProps: any): UpgradePlayerStateProps {
  return {
    player: state.player.player,
    talents: state.game.gameRules.talents,
    closeModal: ownProps.closeModal
  }
}

const mapDispatchToProps: UpgradePlayerActionProps = {
  upgradePlayer
}

export default connect(mapStateToDispatch, mapDispatchToProps)(UpgradePlayer)
