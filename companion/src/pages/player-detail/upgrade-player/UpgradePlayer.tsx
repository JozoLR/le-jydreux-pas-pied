import React from 'react'
import { Player, UpgradePlayerCommand } from 'domain/player'
import { InputGroupSelect, InputGroupValue } from 'components/input-group-select/InputGroupSelect'
import { Icon } from 'components/icon/icon'
import { GameRules } from '../../../domain/game-rules'
import {
  InputGroupSelectWithTooltip,
  InputGroupTooltipValue
} from '../../../components/input-group-select/InputGroupSelectWithTooltip'

export default class UpgradePlayer extends React.Component<UpgradePlayerProps, State> {
  constructor(props: UpgradePlayerProps) {
    super(props)
    this.state = {
      currentSkillSelection: [],
      currentTalentSelection: [],
      experienceLeft: 0
    }
  }

  componentDidMount(): void {
    if (this.state.experienceLeft !== this.props.player.experience) {
      this.setState({
        experienceLeft: this.props.player.experience
      })
    }
  }

  componentDidUpdate(prevProps: Readonly<UpgradePlayerProps>): void {
    if (prevProps !== this.props) {
      this.setState({
        experienceLeft: this.props.player.experience
      })
    }
  }

  render(): JSX.Element {
    return (
      <div className="add-stuff">
        <div className="modal-icon">
          <Icon type='user' />
        </div>
        <div>
          <span>Experience points left to use: </span>
          <span>{this.state.experienceLeft}</span>
        </div>

        <InputGroupSelect label="Improve skill" selectedLabel="Select" values={this.skillsList()}
          onChange={(value: InputGroupValue): void => this.selectSkill(value)} />
        <InputGroupSelectWithTooltip label="Add talent" selectedLabel="Select" values={this.talentList()}
          onChange={(value: InputGroupTooltipValue): void => this.selectTalent(value)} />

        <div>
          <span>Selection</span>
          {this.state.currentSkillSelection.length ?
            <div>
              Skills
            {this.state.currentSkillSelection.map((skill, index) => (<li key={index}>{skill.label}</li>))}
            </div> : <div />
          }
          {this.state.currentTalentSelection.length ?
            <div>
              Talents
            {this.state.currentTalentSelection.map((talent, index) => (<li key={index}>{talent.label}</li>))}
            </div> : <div />
          }
        </div>
        {!this.canAdd() && <div>No more selection possible</div>}
        <button className="button-add" onClick={this.upgrade}>Upgrade</button>
      </div>
    )
  }

  skillsList = (): InputGroupValue[] => {
    const list: InputGroupValue[] = []
    if (this.canAdd()) {
      this.props.player.skills.forEach(skill => {
        list.push({
          id: skill.id,
          label: skill.label
        })
      })
    }
    return list
  }

  talentList = (): InputGroupTooltipValue[] => {
    const list: InputGroupTooltipValue[] = []
    if (this.canAdd()) {
      this.props.talents
        .filter(talent => talent.gameClassId === this.props.player.gameClass.id || !talent.gameClassId)
        .forEach(talent => {
          list.push({
            id: talent.id,
            label: talent.label,
            description: talent.description
          })
        })
    }
    return list
  }

  selectSkill = (value: InputGroupValue): void => {
    const currentSkillSelection = this.state.currentSkillSelection
    currentSkillSelection.push(value)
    this.setState({
      currentSkillSelection,
      experienceLeft: this.state.experienceLeft - 5
    })
  }

  selectTalent = (value: InputGroupValue): void => {
    const currentTalentSelection = this.state.currentTalentSelection
    currentTalentSelection.push(value)
    this.setState({
      currentTalentSelection,
      experienceLeft: this.state.experienceLeft - 5
    })
  }

  upgrade = (): void => {
    const command: UpgradePlayerCommand = {
      playerId: this.props.player.id,
      gameId: this.props.player.gameId,
      talentIds: this.state.currentTalentSelection.map(talent => talent.id),
      skillIds: this.state.currentSkillSelection.map(skill => skill.id)
    }
    this.props.upgradePlayer(command)
    this.props.closeModal()
  }

  canAdd = (): boolean => {
    return this.state.experienceLeft >= 5
  }
}
export type UpgradePlayerStateProps = {
  player: Player;
  talents: GameRules.Talent[];
  closeModal: () => void;
}

export type UpgradePlayerActionProps = {
  upgradePlayer: (upgradePlayerCommand: UpgradePlayerCommand) => void;
}

export type UpgradePlayerProps = UpgradePlayerStateProps & UpgradePlayerActionProps;

export type State = {
  currentTalentSelection: InputGroupValue[];
  currentSkillSelection: InputGroupValue[];
  experienceLeft: number;
}
