import './Notes.scss'
import React from 'react'
import { Frame } from 'components/frame/Frame'

export class Notes extends React.Component<NotesProps & NotesActions, NotesState> {

  constructor(props: Readonly<NotesProps & NotesActions>) {
    super(props)
    this.state = {
      notes: ''
    }
  }

  componentDidUpdate(prevProps: Readonly<NotesProps & NotesActions>): void {
    if (prevProps !== this.props) {
      this.setState({
        notes: this.props.notes
      })
    }
  }


  render(): JSX.Element {
    const { notes } = this.state
    return (
      <div className="notes-container">
        <Frame title="Notes">
          <div className="notes-content">
            <textarea name="body"
              className="textarea text-note"
              onChange={(event): void => this.onChange(event.target.value)}
              value={notes} />
          </div>
        </Frame>
      </div>
    )
  }

  onChange(notes: string): void {
    this.setState({
      notes: notes
    })
    this.props.updateNotes(notes)
  }
}

export interface NotesState {
  notes: string;
}

export interface NotesProps {
  notes: string;
}

export interface NotesActions {
  updateNotes: (note: string) => {};
}

