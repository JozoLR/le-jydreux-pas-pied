import { connect } from 'react-redux'
import { Notes, NotesActions, NotesProps } from './Notes'
import { updateNotes } from '../../../store/pages/player/player.action'
import { State } from '../../../store/reducers'

function mapStateToDispatch(state: State): NotesProps {
  return {
    notes: state.player.player.notes
  }
}

const mapDispatchToProps: NotesActions = {
  updateNotes
}

export default connect(mapStateToDispatch, mapDispatchToProps)(Notes)
