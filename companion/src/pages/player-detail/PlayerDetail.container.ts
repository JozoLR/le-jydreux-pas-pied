import { connect } from 'react-redux'
import { State } from '../../store/reducers'
import PlayerDetail, {DetailPlayerDispatchProps, DetailPlayerStateProps, PlayerDetailRouteParam} from './PlayerDetail'
import { loadPlayer } from '../../store/pages/player/player.action'
import { getNotifications } from '../../store/notification/notification.action'
import { getRolls, loadGame } from 'store/game/game.action'
import { logout } from '../../store/auth/auth.action'
import { match } from 'react-router-dom'

function mapStateToDispatch(state: State, ownProps: { match: match<PlayerDetailRouteParam> }): DetailPlayerStateProps {
  return {
    player: state.player.player,
    notifications: state.notification.notifications,
    route: ownProps.match
  }
}

const mapDispatchToProps: DetailPlayerDispatchProps = {
  loadPlayer,
  getNotifications,
  getRolls,
  loadGame,
  logout
}

export default connect(mapStateToDispatch, mapDispatchToProps)(PlayerDetail)
