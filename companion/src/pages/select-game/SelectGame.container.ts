import SelectGame, { GameSelectionDispatchProps, GameSelectionStateProps } from './SelectGame'
import { State } from '../../store/reducers'
import { connect } from 'react-redux'
import {loadGames} from '../../store/pages/home/home.action'

function mapStateToDispatch(state: State): GameSelectionStateProps {
  return {
    games: state.home.games
  }
}

const mapDispatchToProps: GameSelectionDispatchProps = {
  loadGames
}

export default connect(mapStateToDispatch, mapDispatchToProps)(SelectGame)
