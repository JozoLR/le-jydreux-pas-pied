import { connect } from 'react-redux'
import CreateGame , { CreateGameProps, CreateGameActions } from './CreateGame'
import { State } from 'store/reducers'
import {createGame} from 'store/pages/home/home.action'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function mapStateToDispatch(state: State, ownProps: any): CreateGameProps {
  return {
    closeModal: ownProps.closeModal
  }
}

const mapDispatchToProps: CreateGameActions = {
  createGame
}

export default connect(mapStateToDispatch, mapDispatchToProps)(CreateGame)
