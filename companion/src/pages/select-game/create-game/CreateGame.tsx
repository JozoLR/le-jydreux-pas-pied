import React from 'react'
import { GameToCreate } from '../../../domain/game'
import { InputGroupText } from 'components/input-group-text/InputGroupText'

export default class CreateGame extends React.Component<PageProps, CreateGameState> {
  constructor(props: PageProps) {
    super(props)
    this.state = {
      gameToCreate: {
        name: '',
        users: ['', '', '', '', '']
      },
    }
  }

  render(): JSX.Element {
    return <>
      <h2 className="title-2">Create a new game</h2>
      <InputGroupText placeholder="Name" onChange={(value: string): void => this.onChangeName(value)} />
      <h3 className="title-3">Add players e-mails:</h3>
      <div className="emails">
        {
          this.createEmailInput()
        }
      </div>
      <button disabled={!this.isAllowed()} className="button-add" onClick={this.createGame.bind(this)}>
        Create Game
      </button>
    </>
  }


  createEmailInput = (): JSX.Element[] => {
    const emails = []
    for (let i = 0; i < 5; i++) {
      emails.push(
        <InputGroupText key={i} placeholder={`Email player ${i + 1}`} onChange={(value: string): void => this.onChangeEmail(value, i)} />
      )
    }
    return emails
  }

  onChangeName(value: string): void {
    this.setState({
      gameToCreate: {
        ...this.state.gameToCreate,
        name: value
      }
    })
  }

  onChangeEmail(value: string, index: number): void {
    const newPlayers = [...this.state.gameToCreate.users]
    newPlayers[index] = value

    this.setState({
      gameToCreate: {
        ...this.state.gameToCreate,
        users: newPlayers
      }
    })
  }

  private createGame(): void {
    const clearedPlayers = this.state.gameToCreate.users.filter(player => player)
    const gameToCreate = {
      ...this.state.gameToCreate,
      players: clearedPlayers
    }

    this.props.createGame(gameToCreate)
    this.props.closeModal()
  }

  private isAllowed(): boolean {
    return Boolean(this.state.gameToCreate.name)
  }
}

interface CreateGameState {
  gameToCreate: GameToCreate;
}

export interface CreateGameProps {
  closeModal: () => void;
}

export interface CreateGameActions {
  createGame: (game: GameToCreate) => void;
}

export type PageProps = CreateGameProps & CreateGameActions;
