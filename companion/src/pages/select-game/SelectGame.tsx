import './SelectGame.scss'
import React from 'react'
import { Link } from 'react-router-dom'
import Modal from 'react-bootstrap/Modal'
import { Frame } from 'components/frame/Frame'
import { Game } from '../../domain/game'
import CreateGameContainer from './create-game/CreateGame.container'

export default class SelectGame extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      openModal: false
    }
  }

  componentDidMount(): void {
    this.props.loadGames()
  }

  render(): JSX.Element {
    return <>
      <div className="content-centered">
        <h1 className="title-1">Select your game</h1>
        <ul>
          {this.props.games.map((game: Game) => (
            <li className="player-entry" key={game.id}>
              <Link to={`/game/${game.id}`}>
                <Frame>
                  {/* <span className="material-icons">person</span> */}
                  <span className="text-main">{game.name}</span>
                </Frame>
              </Link>
            </li>
          ))}
        </ul>
        <button onClick={(): void => this.setState({ ...this.state, openModal: true })} className="button-add mask">
          <span className="material-icons">add</span> Add
        </button>
      </div>
      <Modal show={this.state.openModal} onHide={this.closeModal.bind(this)}>
        <button className="button-close-modal" onClick={this.closeModal.bind(this)}><span
          className="material-icons">close</span></button>
        <CreateGameContainer closeModal={this.closeModal.bind(this)} />
      </Modal>
    </>
  }

  closeModal = (): void => {
    this.setState({ ...this.state, openModal: false })
  }
}

export interface GameSelectionStateProps {
  games: Game[];
}

export interface GameSelectionDispatchProps {
  loadGames: () => void;
}

export type State = {
  openModal: boolean;
}

export type Props = GameSelectionStateProps & GameSelectionDispatchProps;
