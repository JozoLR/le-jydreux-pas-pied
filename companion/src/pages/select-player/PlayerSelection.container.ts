import PlayerSelection, { PlayerSelectionDispatchProps, PlayerSelectionStateProps } from './PlayerSelection'
import { State } from '../../store/reducers'
import { connect } from 'react-redux'
import {loadGame} from '../../store/game/game.action'

function mapStateToDispatch(state: State): PlayerSelectionStateProps {
  return {
    game: state.game.game,
    user: state.auth.user
  }
}

const mapDispatchToProps: PlayerSelectionDispatchProps = {
  loadGame
}

export default connect(mapStateToDispatch, mapDispatchToProps)(PlayerSelection)
