import { connect } from 'react-redux'
import CreatePlayer, { CreatePlayerProps, CreatePlayerActions } from './CreatePlayer'
import { State } from 'store/reducers'
import { createPlayer } from 'store/pages/home/home.action'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function mapStateToDispatch(state: State, ownProps: any): CreatePlayerProps {
  return {
    closeModal: ownProps.closeModal,
    gameId: ownProps.gameId,
    gameRules: state.game.gameRules
  }
}

const mapDispatchToProps: CreatePlayerActions = {
  createPlayer
}

export default connect(mapStateToDispatch, mapDispatchToProps)(CreatePlayer)
