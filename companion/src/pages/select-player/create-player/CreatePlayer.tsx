import './CreatePlayer.scss'
import React from 'react'
import {GameRules} from 'domain/game-rules'
import {AttributToCreate, PlayerToCreate} from 'domain/player'
import {InputGroupNumber} from 'components/input-group-number/InputGroupNumber'
import Dropdown from 'react-bootstrap/Dropdown'
import {InputGroupText} from 'components/input-group-text/InputGroupText'
import {
  InputGroupSelectWithTooltip,
  InputGroupTooltipValue
} from '../../../components/input-group-select/InputGroupSelectWithTooltip'

export default class CreatePlayer extends React.Component<PageProps, CreatePlayerState> {
  constructor(props: PageProps) {
    super(props)

    this.state = {
      playerToCreate: {
        name: '',
        attributs: [],
        gameClassId: '',
        gameClassLabel: 'Class',
        talentId: '',
        talentLabel: 'Talent',
        gameId: this.props.gameId
      },
    }
  }

  componentDidMount(): void {
    if (!this.state.playerToCreate.attributs.length) {
      const attributs: AttributToCreate[] = []
      this.props.gameRules.attributs.map(attribut => {
        attributs.push({
          id: attribut.id,
          label: attribut.label,
          value: 0
        })
      })
      this.setState({
        playerToCreate: {
          ...this.state.playerToCreate,
          attributs
        }
      })
    }
  }

  render(): JSX.Element {
    return <>
      <h2 className="title-2">Create a new player</h2>
      <InputGroupText placeholder="Name" onChange={(value: string): void => this.onChangeName(value)}/>
      <h3 className="title-3">Add player&apos;s skills:</h3>
      {
        this.props.gameRules.attributs.map((attribut: GameRules.Competence) =>
          <InputGroupNumber
            key={attribut.id}
            label={attribut.label}
            max={5}
            onChange={(value: number): void => this.onChangeAttribut(attribut.id, value)}/>
        )
      }
      <h3 className="title-3">Add player&apos;s class:</h3>
      <div className="dropdown-wrapper">
        <Dropdown>
          <Dropdown.Toggle id="dropdown-basic">
            {this.state.playerToCreate.gameClassLabel}
          </Dropdown.Toggle>
          <Dropdown.Menu>
            {
              this.props.gameRules.gameClasses.length && this.props.gameRules.gameClasses.map((gameClass, index) =>
                <Dropdown.Item key={index} as="button" onClick={(): void => {
                  this.onChangeGameClass(gameClass)
                }}>{gameClass.label}</Dropdown.Item>
              )
            }
          </Dropdown.Menu>
        </Dropdown>
        { this.state.playerToCreate.gameClassId &&
        <InputGroupSelectWithTooltip label="" selectedLabel="Select" values={this.talentList()}
                                     onChange={(value: InputGroupTooltipValue): void => this.onChangeTalent(value)}/>
        }
      </div>
      <button disabled={!this.isAllowed()} className="button-add" onClick={this.createPlayer.bind(this)}>
        Create Player
      </button>
    </>
  }

  onChangeName(value: string): void {
    this.setState({
      playerToCreate: {
        ...this.state.playerToCreate,
        name: value
      }
    })
  }

  onChangeAttribut(key: string, value: number): void {
    const newAttributs: AttributToCreate[] = [...this.state.playerToCreate.attributs]
    const attribut: AttributToCreate | undefined = newAttributs.find(attribut => attribut.id == key)
    if (attribut) {
      attribut.value = value
    }
    this.setState({
      playerToCreate: {
        ...this.state.playerToCreate,
        attributs: newAttributs
      }
    })
  }

  onChangeGameClass(gameClass: GameRules.GameClass): void {
    this.setState({
      playerToCreate: {
        ...this.state.playerToCreate,
        gameClassId: gameClass.id,
        gameClassLabel: gameClass.label,
        talentId: '',
        talentLabel: 'Talent'
      }
    })
  }

  onChangeTalent(talent: InputGroupTooltipValue): void {
    this.setState({
      playerToCreate: {
        ...this.state.playerToCreate,
        talentId: talent.id,
        talentLabel: talent.label
      }
    })
  }

  private createPlayer(): void {
    this.props.createPlayer(this.state.playerToCreate)
    this.props.closeModal()
  }

  private isAllowed(): boolean {
    return Boolean(
      this.state.playerToCreate.gameClassId &&
      this.state.playerToCreate.talentId &&
      this.state.playerToCreate.name)
  }

  talentList = (): InputGroupTooltipValue[] => {
    return this.props.gameRules.talents
      .map(talent => ({
        id: talent.id,
        label: talent.label,
        description: talent.description
      }))
  }
}

interface CreatePlayerState {
  playerToCreate: PlayerToCreate;
}

export interface CreatePlayerProps {
  closeModal: () => void;
  gameRules: GameRules;
  gameId: string;
}

export interface CreatePlayerActions {
  createPlayer: (player: PlayerToCreate) => void;
}

export type PageProps = CreatePlayerProps & CreatePlayerActions;
