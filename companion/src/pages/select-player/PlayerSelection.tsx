import './PlayerSelection.scss'
import React from 'react'
import { Link } from 'react-router-dom'
import { Player } from '../../domain/player'
import Modal from 'react-bootstrap/Modal'
import CreatePlayerContainer from 'pages/select-player/create-player/CreatePlayer.container'
import { Frame } from 'components/frame/Frame'
import { User } from '../../domain/user'
import {Game} from '../../domain/game'

export default class PlayerSelection extends React.Component<Props, State> {
  // @ts-ignore
  gameId = this.props.match.params.gameId

  constructor(props: Props) {
    super(props)
    this.state = {
      openModal: false
    }
  }

  componentDidMount(): void {
    this.props.loadGame(this.gameId)
  }

  render(): JSX.Element {
    return <>
      <div className="content-centered">
        <h1 className="title-1">Select your player</h1>
        <ul>
          {
            this.props.user.email === this.props.game.gameMaster &&
            <li className="player-entry" key={'gamemaster'}>
              <Link to={`/game/${this.gameId}/master`}>
                <Frame>
                  <span className="material-icons">person</span>
                  <span className="text-main">Game Master</span>
                </Frame>
              </Link>
            </li>
          }
          {this.props.game.players.map((player: Player) => (
            <li className="player-entry" key={player.id}>
              <Link to={`/game/${this.gameId}/player/${player.id}`}>
                <Frame>
                  <span className="material-icons">person</span>
                  <span className="text-main">{player.name}</span>
                </Frame>
              </Link>
            </li>
          ))}
        </ul>
        <button onClick={(): void => this.setState({ ...this.state, openModal: true })} className="button-add mask">
          <span className="material-icons">add</span> Add
        </button>
      </div>
      <Modal show={this.state.openModal} onHide={this.closeModal.bind(this)}>
        <button className="button-close-modal" onClick={this.closeModal.bind(this)}><span
          className="material-icons">close</span></button>
        <CreatePlayerContainer gameId={this.gameId} closeModal={this.closeModal.bind(this)} />
      </Modal>
    </>
  }

  closeModal = (): void => {
    this.setState({ ...this.state, openModal: false })
  }
}

export interface PlayerSelectionStateProps {
  game: Game;
  user: User;
}

export interface PlayerSelectionDispatchProps {
  loadGame: (id: string) => void;
}

export type State = {
  openModal: boolean;
}

export type Props = PlayerSelectionStateProps & PlayerSelectionDispatchProps;
