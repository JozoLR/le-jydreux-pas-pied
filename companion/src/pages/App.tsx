import React, { useEffect } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import PlayerDetail from './player-detail/PlayerDetail.container'
import SelectGame from './select-game/SelectGame.container'
import PlayerSelection from './select-player/PlayerSelection.container'
import GameMaster from './game-master/GameMaster.container'

export const App: React.FC<AppProps> = ({ loadGameRules, isAuthenticated, authorize }) => {
  useEffect(() => {
    if (!isAuthenticated) {
      authorize()
    }

    if (isAuthenticated) {
      loadGameRules()
    }
  })

  return (isAuthenticated ?
    <Router>
      <div className="main-wrapper">
        <Switch>
          <Route exact path="/" component={SelectGame} />
          <Route exact path="/game/:gameId" component={PlayerSelection} />
          <Route exact path="/game/:gameId/player/:playerId" component={PlayerDetail} />
          <Route exact path="/game/:gameId/master" component={GameMaster} />
        </Switch>
      </div>
    </Router> : <div />
  )
}

export interface AppStateProps {
  isAuthenticated: boolean;
}

export interface AppDispatchProps {
  loadGameRules: () => void;
  authorize: () => void;
}

export type AppProps = AppStateProps & AppDispatchProps;

