module.exports = {
  parser: "@typescript-eslint/parser",
  extends: [
    "plugin:react/recommended",
    "plugin:@typescript-eslint/recommended"
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    }
  },
  rules: {
    "semi": ["error", "never"],
    "@typescript-eslint/ban-ts-ignore": "off",
    "react/jsx-key": "warn",
    "react/prop-types": "off",
    "@typescript-eslint/no-empty-function": "warn",
    "@typescript-eslint/no-use-before-define": "off",
    "no-console": "warn",
    "@typescript-eslint/no-namespace": "off",
    "quotes": ["warn", "single"],
    "@typescript-eslint/camelcase": "off"
  },
  settings: {
    react: {
      version: "detect"
    }
  }
};
